/**
 * LogoutController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  logout: ((req, res) =>{
    req.session.destroy((err)=>{
      if(err){
        return res.serverError(error);
      }
      setTimeout(() => {
        res.redirect('/'); 
      }, 2500);
    });
  }),
};

