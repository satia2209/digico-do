/**
 * DeliveryOrderController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

var moment = require('moment');
var mail_content = require('../../config/mail_content');
const mail_config = require('../../config/mail_config');
const axios = require('axios');
module.exports = {

  index: (req, res) => {
    res.locals.layout = 'layouts/default';
    return res.view('pages/delivery_order/list_deliveryorder',{sdata: req.session, menu: process.env['MENU_DO']});
  },

  listDeliveryOrderbySearch: (req, res) => {
    var knex = sails.config.knex;
    // let check_api = req.body.check_api;
    let check_api = '';
    let id_company_api = req.body.id_company;
    let search_by = req.body.search_by;
    let search_for = req.body.search_for;
    console.log('delivery order');
    console.log(check_api);

    var id_company = '';

    if (check_api == undefined) {
        id_company = req.session.companyID;
    } else {
        id_company = id_company_api;
    }

    id_company = 'test';

    console.log(search_by);
    console.log(search_for);

    let get_delivery = knex.select(
      'requests.id as id_req',
      'requests.reqnum',
      "requests.tag",
      'req_sl_delivery.blnum',
      'req_sl_delivery.ffname',
      'req_sl_delivery.slname',
      'req_sl_delivery.donum',
      'req_sl_delivery.total',
      'requests.status',
      'banks.name',
      'req_sl_delivery.createdate',
      'req_sl_delivery.id',
      'requests.payment_code',
      'mstatus.status_desc'
    );
    get_delivery.select(knex.raw(`to_char(req_sl_delivery.duedate, 'dd-mm-yyyy') as due_date`));
    get_delivery.select(knex.raw(`to_char(req_sl_delivery.createdate, 'dd-mm-yyyy HH24:MI:SS') as create_date`));
    get_delivery.from('requests');
    get_delivery.leftJoin('req_sl_delivery', 'requests.id', '=', 'req_sl_delivery.request_id');
    get_delivery.leftJoin('banks', 'requests.bank_id', '=', 'banks.id');
    get_delivery.leftJoin('mstatus', 'requests.status', '=', 'mstatus.status');
    get_delivery.where({'req_sl_delivery.ffid' : id_company});
    get_delivery.where({'req_sl_delivery.ffid' : id_company});
    if (search_by != '' && search_for != '') {
      if (search_by == '0') {
        get_delivery.where({'requests.reqnum' : search_for});
      } else if (search_by == '1'){
        get_delivery.where({'req_sl_delivery.blnum' : search_for});
      } else if (search_by == '2'){
        get_delivery.where({'req_sl_delivery.ffname' : search_for});
      } else if (search_by == '3'){
        get_delivery.where({'req_sl_delivery.slname' : search_for});
      } else if (search_by == '4'){
        get_delivery.where({'req_sl_delivery.donum' : search_for});
      } else if (search_by == '5'){
        get_delivery.where({'req_sl_delivery.duedate' : search_for});
      } else if (search_by == '6'){
        get_delivery.where({'req_sl_delivery.total' : search_for});
      } else if (search_by == '7'){
        get_delivery.where({'requests.status' : search_for});
      } else if (search_by == '8'){
        get_delivery.where({'banks.name' : search_for});
      } else if (search_by == '9'){
        get_delivery.where({'requests.payment_code' : search_for});
      }
    }
    get_delivery.orderBy('req_sl_delivery.createdate', 'desc');
    get_delivery.then(data => {
      res.send({ data: data });
    })
    .catch(err => {
      console.log('error ' + err);
    });
  },

  listDeliveryOrder: (req, res) => {
    var knex = sails.config.knex;
    let check_api = req.body.check_api;
    let id_company_api = req.body.id_company;
    console.log('delivery order');
    console.log(check_api);

    var id_company = '';

    if (check_api == undefined) {
        id_company = req.session.companyID;
    } else {
        id_company = id_company_api;
    }

    knex
    .select(
      'requests.id as id_req',
      'requests.reqnum',
      "requests.tag",
      'req_sl_delivery.blnum',
      'req_sl_delivery.ffname',
      'req_sl_delivery.slname',
      'req_sl_delivery.donum',
      'req_sl_delivery.total',
      'requests.status',
      'banks.name',
      'req_sl_delivery.createdate',
      'req_sl_delivery.id',
      'requests.payment_code',
      'mstatus.status_desc'
    )
    .select(knex.raw(`to_char(req_sl_delivery.duedate, 'dd-mm-yyyy') as due_date`))
    .select(knex.raw(`to_char(req_sl_delivery.createdate, 'dd-mm-yyyy HH24:MI:SS') as create_date`))
    .from('requests')
    .leftJoin('req_sl_delivery', 'requests.id', '=', 'req_sl_delivery.request_id')
    .leftJoin('banks', 'requests.bank_id', '=', 'banks.id')
    .leftJoin('mstatus', 'requests.status', '=', 'mstatus.status')
    .where({'req_sl_delivery.ffid' : id_company})
    .orderBy('req_sl_delivery.createdate', 'desc')
    .then(data => {
      res.send({ data: data });
    })
    .catch(err => {
      console.log('error ' + err);
    });
  },

  searchListDeliveryOrder: (req, res) => {
    var knex = sails.config.knex;
    let searchBy = req.body.searchByDO;
    let searchFor = req.body.searchFor;
    // let searchForExp = req.body.searchForExp;
    if(searchBy === 'exp_do'){
      if (searchFor === '') {
        searchFor = null;
      } else {
        searchFor = moment(searchFor, 'DD-MM-YYYY').format('YYYY-MM-DD');
      }
    }

    var list = knex
    .select(
      'requests.id as id_req',
      'requests.reqnum',
      'req_sl_delivery.blnum',
      'req_sl_delivery.ffname',
      'req_sl_delivery.slname',
      'req_sl_delivery.donum',
      'req_sl_delivery.total',
      'requests.status',
      'banks.name',
      'req_sl_delivery.createdate',
      'req_sl_delivery.id',
      'requests.payment_code',
      'mstatus.status_desc'
    )
    list.select(knex.raw(`to_char(req_sl_delivery.duedate, 'dd-mm-yyyy') as due_date`))
    list.select(knex.raw(`to_char(req_sl_delivery.createdate, 'dd-mm-yyyy HH24:MI:SS') as create_date`))
    list.from('requests')
    list.leftJoin('req_sl_delivery', 'requests.id', '=', 'req_sl_delivery.request_id')
    list.leftJoin('banks', 'requests.bank_id', '=', 'banks.id')
    list.leftJoin('mstatus', 'requests.status', '=', 'mstatus.status')
    list.where({'req_sl_delivery.ffid' : req.session.companyID})
    if(searchBy === 'req_number'){
        // list.where({'requests.reqnum' : })
        list.whereRaw('requests.reqnum like \'%' + searchFor + '%\'')
    }
    if(searchBy === 'bl_number'){
        list.whereRaw('req_sl_delivery.blnum like \'%' + searchFor + '%\'')
    }
    if(searchBy === 'cargo_owner'){
        list.whereRaw('LOWER(req_sl_delivery.ffname) like \'%' + searchFor.toLowerCase() + '%\'')
    }
    if (searchBy ==='shipping'){
        list.whereRaw('LOWER(req_sl_delivery.slname) like \'%' + searchFor.toLowerCase() + '%\'')
    }
    if(searchBy === 'no_do'){
      list.whereRaw('req_sl_delivery.donum like \'%' + searchFor + '%\'')
    }
    if (searchBy === 'exp_do'){
      list.where('req_sl_delivery.duedate', '=', searchFor)
    }
    if(searchBy === 'amount'){
      list.whereRaw('req_sl_delivery.total like \'%' + searchFor + '%\'')
    }
    if (searchBy === 'status'){
      list.whereRaw('LOWER(mstatus.status_desc) like \'%' + searchFor.toLowerCase() + '%\'')
    }
    if (searchBy === 'bank'){
      list.whereRaw('LOWER(banks.name) like \'%' + searchFor.toLowerCase() + '%\'')
    }
    if (searchBy === 'kd_bayar'){
      list.whereRaw('requests.payment_code like \'%' + searchFor + '%\'')
    }
    list.orderBy('req_sl_delivery.createdate', 'desc')
    list.then(data => {
      res.send({ data: data });
    })
    .catch(err => {
      console.log('error ' + err);
    });
  },
  
  viewDeliveryOrder: ((req, res) => {
    let id = req.body.id;
    let check_api = req.body.check_api;
    console.log(check_api);

    var knex = sails.config.knex;

    sails.log(id);
    if (check_api == undefined) {
      res.locals.layout = 'layouts/default';
    }
    knex
    .select(
      'requests.id',
      'requests.reqnum',
      "requests.tag",
      "requests.company_id as id_consigne",
      'requests.note_request',
      'req_sl_delivery.amount',
      'req_sl_delivery.amount2',
      'req_sl_delivery.blnum',
      'req_sl_delivery.ffname',
      'req_sl_delivery.slname',
      'req_sl_delivery.donum',
      'req_sl_delivery.total',
      'req_sl_delivery.transfee',
      'req_sl_delivery.fileproformainv',
      'req_sl_delivery.fileinv',
      'req_sl_delivery.filedo',
      'req_sl_delivery.refid',
      'req_sl_delivery.vessel',
      'req_sl_delivery.voyage',
      'req_sl_delivery.pkk',
      'req_sl_delivery.no_proforma',
      'req_sl_delivery.service_type',
      'requests.status',
      'banks.name',
      'req_sl_delivery.createdate',
      'req_sl_delivery.id as id_do',
      'companies.name as name_company'
    )
    .select(knex.raw(`to_char(req_sl_delivery.duedate, 'dd-mm-yyyy') as due_date`))
    .select(knex.raw(`to_char(req_sl_delivery.createdate, 'dd-mm-yyyy HH24:MI:SS') as create_date`))
    .where({ 'requests.id': id })
    .from('requests')
    .leftJoin('req_sl_delivery', 'requests.id', '=', 'req_sl_delivery.request_id')
    .leftJoin('banks', 'requests.bank_id', '=', 'banks.id')
    .leftJoin('companies', 'requests.company_id', '=', 'companies.id')
    .orderBy('req_sl_delivery.createdate', 'desc')
    .then(result => {
      console.log('result')
      console.log(result)
      if (check_api == undefined) {
          sails.log(JSON.stringify(result));
          return res.view('pages/delivery_order/view_deliveryorder', {
            data: result,
            sdata: req.session,
            menu: process.env['MENU_DO']
          });
      } else {
        knex.select(
          'detail_container.id',
          'detail_container.delivery_id',
          'detail_container.no_container'
        )
          .from('detail_container')
          .where({ 'detail_container.delivery_id': result[0].id_do})
          .leftJoin('req_sl_delivery', 'detail_container.delivery_id', '=', 'req_sl_delivery.request_id')
          .then(data_container => {
              knex.select(
                  'detail_files.id',
                  'detail_files.file_name'
              )
                  .from('detail_files')
                  .where({ 'detail_files.delivery_id': result[0].id_do })
                  .leftJoin('req_sl_delivery', 'detail_files.delivery_id', '=', 'req_sl_delivery.request_id')
                  .then(data_files => {
                      
                      res.send({status: true , data: result, data_container: data_container, data_files: data_files});
                      
                  }).catch(err => {
                      console.log('error ' + err);
                  });
          }).catch(err => {
              console.log('error ' + err);
          });
      }
    });
  }),
  
  detailContainer: (req, res) => {
    var knex = sails.config.knex;
    var id_do = req.body.id_do;
    knex
    .select(
      'id',
      'no_container'
    )
    .where({delivery_id : id_do})
    .from('detail_container')
    .orderBy('createdate', 'desc')
    .then(data => {
      res.send({ data: data });
    })
    .catch(err => {
      console.log('error ' + err);
    });
  },
  detailFiles: (req, res) => {
    var knex = sails.config.knex;
    var id_do = req.body.id_do;
    knex
    .select(
      'id',
      'file_name'
    )
    .where({delivery_id : id_do})
    .from('detail_files')
    .orderBy('createdate', 'desc')
    .then(data => {
      res.send({ data: data });
    })
    .catch(err => {
      console.log('error ' + err);
    });
  },

  downloadFiles:((req, res)=>{
    const Path = require('path');
    const fs = require('fs');
    const file = null;
    var id = req.body.id;
    var knex = sails.config.knex;
    knex.select(
        'file_name',
    )
    .from('detail_files')
    .where({id: id})
    .then(result => {
      console.log(result[0].file_name);
      console.log(res);
      file = fs.createReadStream(Path.resolve('./assets/file/'+result[0].file_name))
        .on('error', (err) => {
          console.log('error ' + err);
          return res.serverError(err);
        })
        // .pipe(res);
        .pipe(file);
    });
  }),

  addContainer: ((req, res) => {
    var knex = sails.config.knex;
    let id_do = req.body.id_do;
    let cont_no = req.body.cont_no_view;
    let count = req.body.count_view;
    let user = req.session.userName;
    let company_id = req.session.companyID;

    //insert table detail_container 
    knex.insert({
      no_container: cont_no,
      delivery_id: id_do,
      createby: user,
      createdate: moment().format('YYYY-MM-DD HH:mm:ss')
    })
      .into('detail_container')
      .then(detail => {
        if (count === 1) {
          return res.send({status: 'S', message: 'Success'})
        }else{
          if (count > 1) {
            sails.log('masuk array loop');
            var index = 1;
            for (index; index < count; index++) {
              sails.log(index);
              var i_cont = 'cont_no_view' + index;
              var cont_array = req.body[i_cont];
              sails.log(cont_array);
              // insert container
              knex.insert({
                no_container: cont_array,
                delivery_id: id_do,
                createby: user,
                createdate: moment().format('YYYY-MM-DD HH:mm:ss')
              })
                .into('detail_container')
                .then(detail => {
                  // res.send({ message:'success', detail: detail })
                }).catch((err) => {
                  req.session.flash['warning'].push('error database');
                  console.log('error ' + err);
                });
            }
          }
        }
        return res.send({status: 'S', message: 'Success'})
      }).catch((err) => {
        req.session.flash['warning'].push('error database');
        console.log('error ' + err);
      });
  }),

  editContainer:((req, res)=>{
    var knex = sails.config.knex;
    let id = req.body.id;
    let delivery_id = req.body.delivery_id;
    let container_before = req.body.container_before;
    let container_after  = req.body.container_after;
    let user = req.session.userName;
    sails.log(id);
    // check
    if (container_before !== container_after) {
      return knex.select('id')
        .from('detail_container')
        .where({'delivery_id': delivery_id, 'no_container': container_after})
        .then(result =>{
          sails.log(result);
          if (result.length > 0) {
            res.send({status: 'F', message:'Container Number Already !'})
          }else{
            return knex.update({
              no_container: container_after,
              updateby: user,
              updatedate: moment().format('YYYY-MM-DD HH:mm:ss')
            })
              .from('detail_container')
              .where({id: id})
              .then(resultt =>{
                sails.log(resultt);
                res.send({status: 'S', message:'Container Number has been update !'})
              }).catch((err) => {
                req.session.flash['warning'].push(sails.__('error_database'));
                console.log('error ' + err);
              });
          }
        }).catch((err) => {
          req.session.flash['warning'].push(sails.__('error_database'));
          console.log('error ' + err);
        });
    }else{
      res.send({status: 'S', message:'Container Number nothing update !'})
    }
  }),

  deleteContainer: ((req, res) => {
    var knex = sails.config.knex;
    let id = req.body.id;
    var user_id = req.session.userID;
    knex('detail_container')
      .where('id', id)
      .del()
      .then(result => {
        res.send({status: 'S', message: 'Success'});
      })
      .catch(err => {
        req.session.flash['warning'].push(JSON.stringify(err));
        console.log('error ' + err);
      });
  }),

  rejectDO: ((req, res) => {
    var knex = sails.config.knex;
    let id = req.body.id;
    let user = req.session.userName;
    let company_id = '';
    let ket = req.body.ket;

    let check_api = req.body.check_api;
    let id_company_api = req.body.id_company;
    console.log(check_api);

    if (check_api == undefined) {
      company_id = req.session.companyID;
    } else {
      company_id = id_company_api;
    }

    console.log(company_id);

    knex.update({
      status: 'RJCT',
      notes: ket,
      updateby: user,
      updatedate: moment().format('YYYY-MM-DD HH:mm:ss')
    })
      .from('requests')
      .where({id: id})
      .then(resultt =>{

        return knex.select('requests.*','companies.name', 'req_sl_delivery.id as delivery_id')
          .where({ 'requests.id': id })
          .from('requests')
          .leftJoin('companies', 'requests.company_id', '=', 'companies.id')
          .leftJoin('req_sl_delivery', 'requests.id', '=', 'req_sl_delivery.request_id')
          .then(result => {

            return knex.select('*').from('req_sl_delivery')
              .where({ id: result[0].delivery_id })
              .then(req_sql => {
                return knex.select('*').from('companies')
                  .where({ id: company_id })
                  .then(comp_req => {
                    // sails.log('cpm '+comp_req +' comp id'+req.session.companyID)
                    let status_1 = 'Job Order Rejected Notification';
                    let dear = 'Dear ' + result[0].name;
                    let status_2 = 'Job Order has been rejected from ' + comp_req[0].name;
                    let bl_number = req_sql[0].blnum;
                    let status_3 = 'JOB ORDER REJECTED';
                    let date = moment(result[0].createdate, 'DD-MM-YYYY').format('DD MMM YYYY');
                    let menu = 'Modul Delivery Order';
                    var email_content = mail_content.notif_order_reject(status_1, dear, status_2, bl_number, status_3, date, menu, ket);
                    return knex.select('name','email').from('companies')
                      .where({ id: result[0].company_id})
                      .then(comp => {
                        var email_forwading = process.env['EMAIL_FORWARD'];
                        var mail = mail_config.config();
                        console.log(comp[0].email);
                        // setup e-mail data
                        var mailOptions = {
                          from: '"Digico" <' + email_forwading + '>', // sender address (who sends)
                          to: '' + comp[0].email + '', // list of receivers (who receives)
                          cc: comp_req[0].email,
                          subject: '[NOTIFICATIONS] Digico.id JOB ORDER REJECTED (BL NUMBER ' + bl_number + ')',
                          text: '',
                          html: email_content
                        };
                        // sails.log('mail ' + mail)
                        // send mail with defined transport object
                        mail.sendMail(mailOptions, (error, info) => {
                          if(error) {
                            console.log(error);
                            console.log(error.response);
                            if (check_api == undefined) {
                              req.session.flash['warning'].push(error.response);
                              res.redirect('/dashboard');
                            } else {
                              res.send({status: false, message: error.response});
                            }
                          } else {
                            console.log("Message sent: " + info.response);
                            if (check_api == undefined) {
                              res.send({status: 'S', message: 'Success Email'});
                            } else {
                              res.send({status: true, message: 'Success Email'});
                            }
                          }
                        });
                      }).catch((err) => {
                        console.log('error ' + err);
                        if (check_api == undefined) {
                          req.session.flash['warning'].push(sails.__('error_database'));
                          res.redirect('/dashboard');
                        } else {
                          res.send({status: false, message: sails.__('error_database')});
                        }
                      });
                  }).catch((err) => {
                    console.log('error ' + err);
                    if (check_api == undefined) {
                      req.session.flash['warning'].push(sails.__('error_database'));
                      res.redirect('/dashboard');
                    } else {
                      res.send({status: false, message: sails.__('error_database')});
                    }
                  });
              }).catch((err) => {
                console.log('error ' + err);
                if (check_api == undefined) {
                  req.session.flash['warning'].push(sails.__('error_database'));
                  res.redirect('/dashboard');
                } else {
                  res.send({status: false, message: sails.__('error_database')});
                }
              });
          }).catch((err) => {
            console.log('error ' + err);
            if (check_api == undefined) {
              req.session.flash['warning'].push(sails.__('error_database'));
              res.redirect('/dashboard');
            } else {
              res.send({status: false, message: sails.__('error_database')});
            }
          });
      }).catch((err) => {
        console.log('error ' + err);
        if (check_api == undefined) {
          req.session.flash['warning'].push(sails.__('error_database'));
        } else {
          res.send({status: false, message: sails.__('error_database')});
        }
      });
  }),

  inquiry: ((req, res) => {
    var knex = sails.config.knex;

    var no_proforma = req.body.no_proforma;
    var amount = req.body.amount;
    var service_type = req.body.service_type;
    var shipping_line = req.body.shipping_line;
    var id_consigne = req.body.id_consigne;
    var no_request = req.body.no_request;

    console.log(no_proforma);
    console.log(amount);
    console.log(service_type);
    console.log(shipping_line);
    console.log(id_consigne);
    console.log(no_request);

    knex.select('name','email').from('companies')
        .where({ id: id_consigne})
        .then(result => {
            if (result.length == 0) {
                res.send({status: '02', message: 'Consigne not found'});
            }
            sails.log(result[0].name);
            console.log(result[0].name);

            var url = process.env['IP_PAYMENT'] + process.env['PAYMENT_INQUIRY'];
            console.log(url);

            var input_data = 
            {
              data: [
                {
                  proforma: no_proforma,
                  amount: amount,
                  requestno: no_request,
                  service: service_type,
                  consignee: result[0].name,
                  shippingline: shipping_line
                },
              ]
            };

            console.log(input_data);

            axios({
              method: 'post',
              url: url,
              timeout: 10000,
              headers: {
                'Content-Type': 'application/json',
                'username': process.env['USERNAME_DIGICO'],
                'password': process.env['PASS_DIGICO'],
              },
              data: input_data
            })
            .then((result)=>{
                console.log("data_result");
                console.log(result.data);

                var url_encypt = process.env['IP_PAYMENT'] + process.env['GET_ENCRYPT'];
                console.log(url_encypt);

                var secret_key = process.env['SECRET_KEY_DIGICO'];
                var app_id = process.env['APP_ID_DIGICO'];
                var username = process.env['USERNAME_DIGICO'];
                var pass = process.env['PASS_DIGICO'];
                var no_rek = '';
                var kode_bank = '';
                var cust_id = id_consigne;
                var pid = result.data;

                var data_concat = secret_key + app_id + username + pass + no_rek + kode_bank + cust_id + pid;
                console.log(data_concat);
                // concat($secret_key.$app_id.$user.$pass.$no_rek.$kode_bank.$cust_id.$pid)

                var input_data_encrypt = 
                {
                  data: data_concat
                };

                console.log(input_data_encrypt);

                axios({
                  method: 'post',
                  url: url_encypt,
                  timeout: 10000,
                  headers: {
                    'Content-Type': 'application/json',
                    // 'username': 'guddodigico',
                    // 'password': 'guddodigico',
                  },
                  data: input_data_encrypt
                })
                .then((result_encrypt)=>{
                    console.log("data_result");
                    console.log(result_encrypt.data);

                    res.send({status: '01', message: 'Inquiry Success', pid: result.data, encrypt: result_encrypt.data, cust_id: id_consigne});

                }).catch((error) => {
                    console.log(error);
                    sails.log(error);
                    res.send({status: '02', message: error});
                });
    
                // res.send({status: '01', message: 'Inquiry Success', pid: result.data});
            }).catch((error) => {
                console.log(error);
                sails.log(error);
                res.send({status: '02', message: error});
            });

        }).catch((err) => {
            console.log(err);
            res.send({status: '02', message: err});
                      
        });

    // var http = axios.create({
    //   baseURL: url,
    //   timeout: 10000});

    // axios.post('/ImageType/create',{
    //     typeimagecode: codetypeimage,
    //     path: path_url,
    //     description: description  
    // }, axiosConfig)
    // .then((result)=>{
    // if(result.data.status === true){
    //     this.errormessages=false
    //     this.dialogsuccess=true
    //     this.dialog = false
    //     setTimeout(() => { this.dialogsuccess=false }, 1000)
    //     this.clear_data()
    //     this.reload()
    // }else{
    //     this.errormessages=true
    //     this.messageerror=result.data.message
    // }
    // }).catch((error) => {
    //     this.errormessages=true
    //     this.messageerror=error
    // })

    // 

  }),

  updateStatusPayment: ((req, res) => {
    var knex = sails.config.knex;
    var data_update_status = req.body.data;

    if (Array.isArray(data_update_status)) {
        let data_request = [];
        for(var i = 0; i < data_update_status.length; i++) {
          var request_number = data_update_status[i].requestno;
          data_request.push(request_number);
        }
        console.log(data_request)
        if (data_request.length != 0) {
            knex.update({
              status: 'PAID',
              updatedate: moment().format('YYYY-MM-DD HH:mm:ss')
            })
            .from('requests')
            .whereIn('reqnum', data_request)
            .then(result =>{
                console.log(result);
                res.send({status: true, message: 'Success update payment'});
            }).catch((err) => {
                console.log('error ' + err);
                res.send({status: false, message: sails.__('error_database')});
            });
        } else {
          res.send({status: false, message: 'Data is empty !'});
        }
    } else {
      knex.update({
        status: 'PAID',
        updatedate: moment().format('YYYY-MM-DD HH:mm:ss')
      })
        .from('requests')
        .where({reqnum: data_update_status.proforma})
        .then(result =>{
          res.send({status: true, message: 'Success update payment'});
        }).catch((err) => {
          console.log('error ' + err);
          res.send({status: false, message: sails.__('error_database')});
        });
    }
  }),

  approveDO: ((req, res) => {
    var knex = sails.config.knex;
    let id = req.body.id;
    let payment = req.body.payment;
    let user = req.session.userName;
    let company_id = '';
    let note_request = req.body.note_request;
    let check_api = req.body.check_api;
    let id_company_api = req.body.id_company;
    console.log(check_api);

    if (check_api == undefined) {
      company_id = req.session.companyID;
    } else {
      company_id = id_company_api;
    }

    console.log(company_id);

    console.log("note_request");
    console.log(note_request);
    knex.update({
      status: 'RQST',
      tag: payment,
      note_request: note_request,
      updateby: user,
      updatedate: moment().format('YYYY-MM-DD HH:mm:ss')
    })
      .from('requests')
      .where({id: id})
      .then(resultt =>{

        return knex.select('requests.*', 'companies.name', 'req_sl_delivery.id as delivery_id', 'req_sl_delivery.slid' ,'req_sl_delivery.slname')
          .where({ 'requests.id': id })
          .from('requests')
          .leftJoin('companies', 'requests.company_id', '=', 'companies.id')
          .leftJoin('req_sl_delivery', 'requests.id', '=', 'req_sl_delivery.request_id')
          .then(result => {
            return knex.select('*').from('req_sl_delivery')
              .where({ id: result[0].delivery_id })
              .then(req_sql => {
                return knex.select('*').from('companies')
                  .where({ id: company_id })
                  .then(comp_req => {
                    // sails.log('cpm '+comp_req +' comp id'+req.session.companyID)
                    let status_1 = 'Delivery Order Requested Notification';
                    let dear = 'Dear ' + result[0].slname;
                    let status_2 = 'New Delivery Order has been requested from ' + comp_req[0].name;
                    let bl_number = req_sql[0].blnum;
                    let status_3 = 'DELIVERY ORDER REQUESTED';
                    let date = moment(result[0].createdate, 'DD-MM-YYYY').format('DD MMM YYYY');
                    let menu = 'Modul Delivery Order';
                    var email_content = mail_content.notif_order(status_1, dear, status_2, bl_number, status_3, date, menu);
                    return knex.select('name','email').from('companies')
                      .where({ id: result[0].slid})
                      .then(comp => {
                        var email_forwading = process.env['EMAIL_FORWARD'];
                        var mail = mail_config.config();

                        sails.log('send email');
                        sails.log(comp[0].email);
                        sails.log(comp_req[0].email);

                        // setup e-mail data
                        var mailOptions = {
                          from: '"Digico" <' + email_forwading + '>', // sender address (who sends)
                          to: '' + comp[0].email + '', // list of receivers (who receives)
                          cc: comp_req[0].email,
                          subject: '[NOTIFICATIONS] Digico.id DELIVERY ORDER REQUESTED (BL NUMBER ' + bl_number + ')',
                          text: '',
                          html: email_content
                        };
                        // sails.log('mail ' + mail)
                        // send mail with defined transport object
                        mail.sendMail(mailOptions, (error, info) => {
                          if(error) {
                            console.log(error);
                            console.log(error.response);
                            if (check_api == undefined) {
                              req.session.flash['warning'].push(error.response);
                              res.redirect('/dashboard');
                            } else {
                              res.send({status: false, message: error.response});
                            }
                          } else {
                            console.log("Message sent: " + info.response);
                            if (check_api == undefined) {
                              res.send({status: 'S', message: 'Success Email'});
                            } else {
                              res.send({status: true, message: 'Success Email'});
                            }
                            // res.redirect('/dashboard');
                          }
                        });
                      }).catch((err) => {
                        console.log('error ' + err);
                        if (check_api == undefined) {
                          req.session.flash['warning'].push(sails.__('error_database'));
                          res.redirect('/dashboard');
                        } else {
                          res.send({status: false, message: sails.__('error_database')});
                        }
                      });
                  }).catch((err) => {
                    console.log('error ' + err);
                    if (check_api == undefined) {
                      req.session.flash['warning'].push(sails.__('error_database'));
                      res.redirect('/dashboard');
                    } else {
                      res.send({status: false, message: sails.__('error_database')});
                    }
                  });
              }).catch((err) => {
                console.log('error ' + err);
                if (check_api == undefined) {
                  req.session.flash['warning'].push(sails.__('error_database'));
                  res.redirect('/dashboard');
                } else {
                  res.send({status: false, message: sails.__('error_database')});
                }
              });
          }).catch((err) => {
            console.log('error ' + err);
            if (check_api == undefined) {
              req.session.flash['warning'].push(sails.__('error_database'));
              res.redirect('/dashboard');
            } else {
              res.send({status: false, message: sails.__('error_database')});
            }
          });
      }).catch((err) => {
        console.log('error ' + err);
        if (check_api == undefined) {
          req.session.flash['warning'].push(sails.__('error_database'));
        } else {
          res.send({status: false, message: sails.__('error_database')});
        }
      });
  }),

  view_list_payment: (req, res) => {
    res.locals.layout = 'layouts/default';
    return res.view('pages/delivery_order/list_multi_payment',{sdata: req.session, menu: process.env['MENU_DO']});
  },

  calculate_amount_payment: (req, res) => {
    var knex = sails.config.knex;
    let list_req_id = req.body.list_req_id;
    console.log(list_req_id);

    knex.select(
      'total'
    )
    .from('req_sl_delivery')
    .whereIn('request_id', list_req_id)
    .then(data => {
      if (data.length != 0) {
        console.log(data);
        var total_amount = 0;
        for (var i = 0; i < data.length; i++) {
          total_amount = total_amount + parseFloat(data[i].total);
          console.log(total_amount);
        }
        res.send(
          {
            status: true, 
            code: '01', 
            total: total_amount
          }
        );
      } else {
        res.send(
          {
            status: false, 
            code: '02', 
            message: 'Data is empty !'
          }
        );
      }
    })
    .catch(err => {
      res.send(
        {
          status: false, 
          code: '02', 
          message: sails.__('error_database')
        }
      );
    });

  },

  listPayment: (req, res) => {
    var knex = sails.config.knex;
    let check_api = req.body.check_api;
    let id_company_api = req.body.id_company;
    console.log('delivery order payment');
    console.log(check_api);

    var id_company = '';

    if (check_api == undefined) {
        id_company = req.session.companyID;
    } else {
        id_company = id_company_api;
    }

    knex
    .select(
      'requests.id as id_req',
      'requests.reqnum',
      "requests.tag",
      'req_sl_delivery.blnum',
      'req_sl_delivery.ffname',
      'req_sl_delivery.slname',
      'req_sl_delivery.donum',
      'req_sl_delivery.no_proforma',
      'req_sl_delivery.total',
      'requests.status',
      'banks.name',
      'req_sl_delivery.createdate',
      'req_sl_delivery.id',
      'requests.payment_code',
      'mstatus.status_desc'
    )
    .select(knex.raw(`to_char(req_sl_delivery.duedate, 'dd-mm-yyyy') as due_date`))
    .select(knex.raw(`to_char(req_sl_delivery.createdate, 'dd-mm-yyyy HH24:MI:SS') as create_date`))
    .from('requests')
    .leftJoin('req_sl_delivery', 'requests.id', '=', 'req_sl_delivery.request_id')
    .leftJoin('banks', 'requests.bank_id', '=', 'banks.id')
    .leftJoin('mstatus', 'requests.status', '=', 'mstatus.status')
    .where({'req_sl_delivery.ffid' : id_company})
    .where({'requests.status' : 'BILL'})
    .where({'requests.tag' : '2'})
    .orderBy('req_sl_delivery.createdate', 'desc')
    .then(data => {
      res.send({ data: data });
    })
    .catch(err => {
      console.log('error ' + err);
    });
  },

  multiple_inquiry: ((req, res) => {
    var knex = sails.config.knex;

    var knex = sails.config.knex;
    let list_req_id = req.body.list_req_id;
    console.log(list_req_id);

    knex.select(
      'req_sl_delivery.no_proforma',
      'req_sl_delivery.total',
      'req_sl_delivery.service_type',
      'req_sl_delivery.slname',
      'requests.company_id',
      'requests.reqnum',
      'companies.name',
    )
    .from('requests')
    .leftJoin('req_sl_delivery', 'requests.id', '=', 'req_sl_delivery.request_id')
    .leftJoin('companies', 'companies.id', '=', 'requests.company_id')
    .whereIn('requests.id', list_req_id)
    .then(result => {
      console.log('result');
      console.log(result);

      if (result.length == 0) {
        res.send(
          {
            status: false, 
            code: '02', 
            message: 'Data is empty !'
          }
        );
      } else {
        let data_payment = [];
        var id_consigne = result[0].company_id;
        for (var i = 0; i < result.length; i++) {
          object_data_payment = {
            proforma: result[i].no_proforma,
            amount: result[i].total,
            requestno: result[i].reqnum,
            service: result[i].service_type,
            consignee: result[i].name,
            shippingline: result[i].slname
          }
          data_payment.push(object_data_payment);
        }
        console.log('data_payment')
        console.log(data_payment)

        var url = process.env['IP_PAYMENT'] + process.env['PAYMENT_INQUIRY'];
        console.log(url);

        var input_data = 
        {
          data: data_payment
        };

        console.log(input_data);

        axios({
          method: 'post',
          url: url,
          timeout: 10000,
          headers: {
            'Content-Type': 'application/json',
            'username': process.env['USERNAME_DIGICO'],
            'password': process.env['PASS_DIGICO'],
          },
          data: input_data
        })
        .then((result)=>{
            console.log("data_result");
            console.log(result.data);

            var url_encypt = process.env['IP_PAYMENT'] + process.env['GET_ENCRYPT'];
            console.log(url_encypt);

            var secret_key = process.env['SECRET_KEY_DIGICO'];
            var app_id = process.env['APP_ID_DIGICO'];
            var username = process.env['USERNAME_DIGICO'];
            var pass = process.env['PASS_DIGICO'];
            var no_rek = '';
            var kode_bank = '';
            var cust_id = id_consigne;
            var pid = result.data;

            var data_concat = secret_key + app_id + username + pass + no_rek + kode_bank + cust_id + pid;
            console.log(data_concat);
            // concat($secret_key.$app_id.$user.$pass.$no_rek.$kode_bank.$cust_id.$pid)

            var input_data_encrypt = 
            {
              data: data_concat
            };

            console.log(input_data_encrypt);

            axios({
              method: 'post',
              url: url_encypt,
              timeout: 10000,
              headers: {
                'Content-Type': 'application/json',
                // 'username': 'guddodigico',
                // 'password': 'guddodigico',
              },
              data: input_data_encrypt
            })
            .then((result_encrypt)=>{
                console.log("data_result");
                console.log(result_encrypt.data);

                res.send({status: true, code: '01', message: 'Inquiry Success', pid: result.data, encrypt: result_encrypt.data, cust_id: id_consigne});

            }).catch((error) => {
                console.log(error);
                sails.log(error);
                res.send(
                  {
                    status: false, 
                    code: '02', 
                    message: sails.__('error_database')
                  }
                );
            });

            // res.send({status: '01', message: 'Inquiry Success', pid: result.data});
        }).catch((error) => {
            console.log(error);
            sails.log(error);
            res.send(
              {
                status: false, 
                code: '02', 
                message: sails.__('error_database')
              }
            );
        });

      }
    }).catch((err) => {
        console.log(err);
        res.send(
          {
            status: false, 
            code: '02', 
            message: sails.__('error_database')
          }
        );        
    });
  }),

}

