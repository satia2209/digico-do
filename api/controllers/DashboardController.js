/* eslint-disable eqeqeq */
/* eslint-disable camelcase */
/**
 * DashboardController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
module.exports = {
  index: ((req, res) => {
    if (!req.session.flash) {
      req.session.flash = {};
      req.session.flash['success'] = [];
      req.session.flash['warning'] = [];
    }
    sails.log('id comp '+req.session.roleID);
    if (req.session.roleID == process.env['ROLE_ADMIN'] || req.session.roleID == process.env['ROLE_ADMIN_OPERATION'] || req.session.roleID == process.env['ROLE_CONSIGNEE'] || req.session.roleID == process.env['ROLE_CONSIGNEE_FORWARDER']) {
      //co
      res.redirect('/dopayment/request/billoflading');
      return;
    } else if (req.session.roleID == process.env['ROLE_SHIPPING_LINE']) {
      //Shipping
      res.redirect('/dopayment/request/billpayment');
      return;
    } else if (req.session.roleID == process.env['ROLE_FORWARDER']) {
      //forwarder
      res.redirect('/dopayment/requests/delivery');
      return;
    }
      // res.locals.layout = 'layouts/default';
      // return res.view('pages/dashboard', {flash : req.session.flash, sdata: req.session, menu: '0'});
    
  }),
  
};

