/* eslint-disable eqeqeq */
/* eslint-disable camelcase */
/**
 * CompaniesController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
module.exports = {
    forwarder: ((req, res)=>{
        var knex = sails.config.knex;
        var input = req.body.Search;
        var forwarder = knex.select('id', 'name').where({ status: 'ACTIVE', type: '9' })
        .whereRaw(`name != '' `)
        .from('companies')
        if (typeof input !== 'undefined') {
            forwarder.whereRaw('LOWER(name) like \'%' + input.toLowerCase() + '%\'')
        }
        if (typeof input == 'undefined') {
            forwarder.then(data => {
                res.send({data: data});
                var a = [];
                res.send(a);
            })    
        }
        forwarder.then(data => {
            var a = [];
            for (var i = 0; i < data.length; i++) {
                a.push({ id: data[i].id, name: data[i].name });
            }
            res.send(a);
        })
    }),
    shipping: ((req, res)=>{
        var knex = sails.config.knex;
        var input = req.body.Search;
        var forwarder = knex.select('id', 'name').where({ status: 'ACTIVE', type: '2' })
        .whereRaw(`name != '' `)
        .from('companies')
        if (typeof input !== 'undefined') {
            forwarder.whereRaw('LOWER(name) like \'%' + input.toLowerCase() + '%\'')
        }
        if (typeof input == 'undefined') {
            forwarder.then(data => {
                res.send({data: data});
                var a = [];
                res.send(a);
            })    
        }
        forwarder.then(data => {
            var a = [];
            for (var i = 0; i < data.length; i++) {
                a.push({ id: data[i].id, name: data[i].name });
            }
            res.send(a);
        })
    })
}