/* eslint-disable camelcase */
/**
 * LoginController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

const crypto = require('crypto');
module.exports = {
  index: ((req, res) => {
    if (!req.session.flash) {
      req.session.flash = {};
      req.session.flash['success'] = [];
      req.session.flash['warning'] = [];
    }
    if (req.session.authenticated === true) {
      res.redirect('/dopayment/request/billoflading');
    } else {
      return res.view('pages/login', { layout: '' });
    }
  }),
  login: ((req, res) => {
    var knex = sails.config.knex;
    let username = req.body.username;
    let password = req.body.password;
    let capcha = req.body.capcha;
    let capchatext = req.body.capchatext;
    let check_api = req.body.check_api;
    // console.log(check_api);
    knex.select('users.id', 'users.password', 'users.username', 'users.company_id', 'users.role_id', 'users.is_active', 'users.email', 'companies.type')
    .where({ username: username }).from('users')
    .leftJoin('companies', 'users.company_id', '=', 'companies.id')
      .then(result => {
        if (capchatext !== capcha) {
          if (check_api == undefined) {
            req.session.flash['warning'].push('captcha not match, try again !');
            res.redirect('/');
          } else {
            res.send({status: false, message: 'captcha not match, try again !'});
          }
        } else {
          if (result.length > 0) {
            if (result[0].is_active === 1) {
              var hash_pass = crypto.createHash('md5').update(password).digest('hex');
              if (hash_pass === result[0].password) {
                req.session.authenticated = true;
                req.session.userID = result[0].id;
                req.session.userName = result[0].username;
                req.session.email = result[0].email;
                req.session.companyID = result[0].company_id;
                req.session.compType = result[0].type;
                req.session.roleID = result[0].role_id;
                console.log(result[0].role_id);
                if (check_api == undefined) {
                  res.redirect('/dashboard');
                } else {
                  res.send({status: true, data_user: result[0], message: 'login success'});
                }
              } else {
                if (check_api == undefined) {
                  req.session.flash['warning'].push('invalid password');
                  res.redirect('/');
                } else {
                  res.send({status: false, message: 'invalid password'});
                }
              }
            } else {
              if (check_api == undefined) {
                req.session.flash['warning'].push('account is not active');
                res.redirect('/');
              } else {
                res.send({status: false, message: 'account is not active'});
              }
            }
          } else {
            if (check_api == undefined) {
              req.session.flash['warning'].push('failed login');
              res.redirect('/');
            } else {
              res.send({status: false, message: 'failed login'});
            }
          }
        }
      }).catch((err) => {
        console.log('error ' + err);
        if (check_api == undefined) {
          req.session.flash['warning'].push('error database');
          res.redirect('/');
        } else {
          res.send({status: false, message: 'error database'});
        }
      });
  }),

  captcha_load: ((req, res) => {
    
    const svgCaptcha = require('svg-captcha');

    const cap = svgCaptcha.create({
      size: 4,
      width: 160,
      height: 75,
      fontSize: 80,
      ignoreChars: '0oO1ilI',
      noise: 2,
      color: true,
      background: '#eee'
    });
    res.send({ captcha: cap });
  })
};

