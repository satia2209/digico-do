/* eslint-disable eqeqeq */
/* eslint-disable camelcase */
/**
 * CustomerSupportController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
var mail_content = require('../../config/mail_content');
const mail_config = require('../../config/mail_config');

module.exports = {

    index: ((req, res) => {
        if (!req.session.flash) {
            req.session.flash = {};
            req.session.flash['success'] = [];
            req.session.flash['warning'] = [];
        }
        res.locals.layout = 'layouts/default';
        console.log('menu_help');
        console.log(process.env['MENU_HELP']);
        return res.view('pages/customer_support/form_customer_support', { sdata: req.session, menu: process.env['MENU_HELP'] });
    }),

    help: ((req, res) => {
        if (!req.session.flash) {
            req.session.flash = {};
            req.session.flash['success'] = [];
            req.session.flash['warning'] = [];
        }
        res.locals.layout = 'layouts/default';
        return res.view('pages/customer_support/help_customer_support', { sdata: req.session, menu: process.env['MENU_SUPPORT'] });
    }),

    send_trouble: ((req, res) => {
        let email = req.session.email;
        let header_message = req.body.header_message + ' (' + email + ')';
        let detail_message = req.body.detail_message;
        
        console.log(header_message);
        console.log(detail_message);
        console.log(email);

        var email_forwading = process.env['EMAIL_FORWARD'];
        var email_custcare = process.env['EMAIL_CUSTCARE'];
        var mail = mail_config.config();
        // setup e-mail data
        var mailOptions = {
            from: email_forwading, // sender address (who sends)
            to: '' + email_custcare + '', // list of receivers (who receives)
            subject: header_message,
            text: detail_message,
        };
        // sails.log('mail ' + mail)
        // send mail with defined transport object
        mail.sendMail(mailOptions, (error, info) => {
            if (error) {
                console.log(error);
                console.log(error.response);
                // req.session.flash['warning'].push(error.response);
                // res.redirect('/dopayment/support/form');
                return res.status(200).send({
                    status: "false",
                    message: "failed"
                });
            } else {
                console.log("Message sent: " + info.response);
                // res.send({ status: 'S', message: 'Success Email' });
                // res.redirect('/dopayment/support/form');
                return res.status(200).send({
                    status: "true",
                    message: "success"
                });
            }
        });
    }),

}