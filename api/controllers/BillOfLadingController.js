/* eslint-disable eqeqeq */
/* eslint-disable camelcase */
/**
 * BillOfLadingController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
var moment = require('moment');
const cryptoRandomString = require('crypto-random-string');
var char = '1234567890ABCDEFGHJKLMNPQRSTUVWXYZ';
var mail_content = require('../../config/mail_content');
const mail_config = require('../../config/mail_config');

module.exports = {
    index: ((req, res) => {
        if (!req.session.flash) {
            req.session.flash = {};
            req.session.flash['success'] = [];
            req.session.flash['warning'] = [];
        }
        res.locals.layout = 'layouts/default';
        // var sdata = req.session;
        // sails.log('print '+JSON.stringify(sdata))
        // sails.log('data '+sdata['companyID'])
        return res.view('pages/BillOfLading/list_order', {sdata: req.session, menu: process.env['MENU_BOL']});

    }),
    billoflading_list_search: ((req, res) => {
        var knex = sails.config.knex;
        let check_api = req.body.check_api;
        let id_company_api = req.body.id_company;
        let search_by = req.body.search_by;
        let search_for = req.body.search_for;
        console.log('bill of lading');
        console.log(check_api);
        console.log(search_by);
        console.log(search_for);

        var id_company = '';

        if (check_api == undefined) {
            id_company = req.session.companyID;
        } else {
            id_company = id_company_api;
        }
        
        let get_bol = knex.select(
            'requests.id',
            'requests.reqnum',
            'req_sl_delivery.blnum',
            'req_sl_delivery.ffname',
            'req_sl_delivery.slname',
            'requests.status',
            'mstatus.status_desc'
        );
        get_bol.select(knex.raw(`to_char(requests.createdate, 'dd-mm-yyyy HH24:MI:SS') as create_date`));
        get_bol.from('requests');
        get_bol.leftJoin('req_sl_delivery', 'requests.id', '=', 'req_sl_delivery.request_id');
        get_bol.leftJoin('mstatus', 'requests.status', '=', 'mstatus.status');
        get_bol.where({'requests.company_id' : id_company});
        if (search_by != '' && search_for != '') {
            if (search_by == '0') {
                get_bol.where({'requests.reqnum' : search_for});
            } else if (search_by == '1'){
                get_bol.where({'req_sl_delivery.blnum' : search_for});
            } else if (search_by == '2'){
                get_bol.where({'req_sl_delivery.ffname' : search_for});
            } else if (search_by == '3'){
                get_bol.where({'req_sl_delivery.slname' : search_for});
            } else if (search_by == '4'){
                get_bol.where({'requests.status' : search_for});
            }
          }
        get_bol.orderBy('requests.createdate', 'desc');
        get_bol.then(data => {
            console.log(data);
            res.send({ data: data });
            return;
        })
        .catch(err => {
            console.log('error ' + err);
        });
    }),
    billoflading_list: ((req, res) => {
        var knex = sails.config.knex;
        let check_api = req.body.check_api;
        let id_company_api = req.body.id_company;
        console.log('bill of lading');
        console.log(check_api);

        var id_company = '';

        if (check_api == undefined) {
            id_company = req.session.companyID;
        } else {
            id_company = id_company_api;
        }
        
        knex.select(
            'requests.id',
            'requests.reqnum',
            'req_sl_delivery.blnum',
            'req_sl_delivery.ffname',
            'req_sl_delivery.slname',
            'requests.status',
            'mstatus.status_desc'
        )
            .select(knex.raw(`to_char(requests.createdate, 'dd-mm-yyyy HH24:MI:SS') as create_date`))
            .from('requests')
            .leftJoin('req_sl_delivery', 'requests.id', '=', 'req_sl_delivery.request_id')
            .leftJoin('mstatus', 'requests.status', '=', 'mstatus.status')
            .where({'requests.company_id' : id_company})
            .orderBy('requests.createdate', 'desc')
            .then(data => {
                console.log(data);
                res.send({ data: data });
                return;
            })
            .catch(err => {
                console.log('error ' + err);
            });
    }),
    billoflading_view: ((req, res) => {
        var id = req.body.id;
        let check_api = req.body.check_api;

        var knex = sails.config.knex;

        knex.select(
            'requests.id',
            'requests.reqnum',
            'requests.status as status_do',
            "requests.tag",
            'req_sl_delivery.id as req_sl_delivery_id',
            'req_sl_delivery.blnum',
            'req_sl_delivery.donum',
            'req_sl_delivery.ffname',
            'req_sl_delivery.slname'
        )
            .select(knex.raw(`to_char(req_sl_delivery.duedate, 'dd-mm-yyyy') as duedate`))
            .select(knex.raw(`to_char(requests.createdate, 'dd-mm-yyyy HH24:MI:SS') as create_date`))
            .from('requests')
            .leftJoin('req_sl_delivery', 'requests.id', '=', 'req_sl_delivery.request_id')
            .where({ 'requests.id': id })
            .then(data => {
                return knex.select(
                    'detail_container.id',
                    'detail_container.delivery_id',
                    'detail_container.no_container'
                )
                    .from('detail_container')
                    .where({ 'detail_container.delivery_id': data[0].req_sl_delivery_id })
                    .leftJoin('req_sl_delivery', 'detail_container.delivery_id', '=', 'req_sl_delivery.request_id')
                    .then(data_container => {
                        return knex.select(
                            'detail_files.id',
                            'detail_files.file_name'
                        )
                            .from('detail_files')
                            .where({ 'detail_files.delivery_id': data[0].req_sl_delivery_id })
                            .leftJoin('req_sl_delivery', 'detail_files.delivery_id', '=', 'req_sl_delivery.request_id')
                            .then(data_files => {
                                if (check_api == undefined) {
                                    res.locals.layout = 'layouts/default';
                                    return res.view('pages/BillOfLading/detail_order', { data: data, data_container: data_container, data_files: data_files,sdata: req.session, menu: process.env['MENU_BOL'] });
                                } else {
                                    // id_company = id_company_api;
                                    res.send({status: true , data: data, data_container: data_container, data_files: data_files });
                                }
                                
                            }).catch(err => {
                                console.log('error ' + err);
                            });
                    }).catch(err => {
                        console.log('error ' + err);
                    });
            })
            .catch(err => {
                console.log('error ' + err);
            });
        //       knex.on( 'query', function( queryData ) {
        //   console.log( queryData );
        //   });
    }),
    submit_request: ((req, res) => {
        var knex = sails.config.knex;
        var bl_number = req.body.bl_number;
        let id_forwarder = req.body.id_forwarder;
        let name_forwarder = req.body.name_forwarder;
        let id_shipping = req.body.id_shipping;
        let name_shipping = req.body.name_shipping;
        let cont_no = req.body.cont_no;
        let count = req.body.count;
        var count_file = req.body.count_file;
        console.log('count_file');
        console.log(count_file);
        console.log('count_container');
        console.log(count);
        var time_code = moment().format('ssmmHHMMDDYYYY');
        let req_code = 'REQ' + moment().format('YYYYMMDD') + cryptoRandomString({ length: 5, characters: char });
        
        let  = req.session.userName;
        let id_company_api = req.body.id_company;
        let username = req.body.username;
        let check_api = req.body.check_api;
        
        var company_id = '';

        if (check_api == undefined) {
            company_id = req.session.companyID;
        } else {
            company_id = id_company_api;
        }

        var user = '';

        if (check_api == undefined) {
            user = req.session.userName;
        } else {
            user = username;
        }

        console.log('check_api');
        console.log(check_api);
        console.log(company_id);
        console.log(user);

        sails.log(count);
        //insert table requests
        knex
            .insert({
                reqnum: req_code,
                status: 'PNDG',
                createby: user,
                company_id: company_id,
                createdate: moment().format('YYYY-MM-DD HH:mm:ss')
            })
            .returning('id')
            .into('requests')
            .then(id_req => {
                //insert table re_sl_delivery
                sails.log(id_req[0]);
                return knex.insert({
                    blnum: bl_number,
                    ffid: id_forwarder,
                    ffname: name_forwarder,
                    slid: id_shipping,
                    slname: name_shipping,
                    request_id: id_req[0],
                    createby: user,
                    createdate: moment().format('YYYY-MM-DD HH:mm:ss')
                })
                    .returning('id')
                    .into('req_sl_delivery')
                    .then(delivery_id => {
                        req.file('files').upload({
                            dirname: require('path').resolve(sails.config.appPath, 'assets/file/'),
                            saveAs: ((file, cb) => {
                                var extension = file.filename.split('.').pop();
                                cb(null, user + time_code + '.' + extension);
                            })
                        }, (err, file) => {
                            if (err) {
                                return res.serverError(err);
                            }
                            if (file.length > 0) {
                                //insert file
                                knex.insert({
                                    file_name: file[0].fd.split('/').pop(),
                                    delivery_id: delivery_id[0],
                                    createby: user,
                                    createdate: moment().format('YYYY-MM-DD HH:mm:ss')
                                })
                                    .into('detail_files')
                                    .then(succ => {
                                        // sails.log(succ);
                                        console.log('success single file');
                                    })
                            }
                        });
                        // insert container
                        console.log(cont_no);
                        console.log(cont_no);
                        knex.insert({
                            no_container: cont_no,
                            delivery_id: delivery_id[0],
                            createby: user,
                            createdate: moment().format('YYYY-MM-DD HH:mm:ss')
                        })
                            .into('detail_container')
                            .then(detail => {
                                // sails.log(detail);
                                console.log('success single container');
                                return null;
                            }).catch((err) => {
                                console.log('error ' + err);
                                if (check_api == undefined) {
                                    req.session.flash['warning'].push(sails.__('error_database'));
                                    res.redirect('/dopayment/request/billoflading');
                                } else {
                                    res.send({status: false , message: sails.__('error_database') });
                                }
                            });
                            console.log('count container ' + count);
                        if (count >= 1) {
                            sails.log('masuk array loop')
                            var index= 1;
                            for (index; index <= count; index++) {
                                sails.log('masuk array container');
                                var i_cont = 'cont_no' + index;
                                console.log(i_cont);
                                var cont_array = req.body[i_cont];
                                sails.log(cont_array);
                                // insert container
                                knex.insert({
                                    no_container: cont_array,
                                    delivery_id: delivery_id[0],
                                    createby: user,
                                    createdate: moment().format('YYYY-MM-DD HH:mm:ss')
                                })
                                    .into('detail_container')
                                    .then(detail => {
                                        console.log('success array container');
                                        // sails.log(detail);
                                    }).catch((err) => {
                                        console.log('error ' + err);
                                        if (check_api == undefined) {
                                            req.session.flash['warning'].push(sails.__('error_database'));
                                            return res.redirect('/dopayment/request/billoflading');
                                        } else {
                                            res.send({status: false , message: sails.__('error_database') });
                                        }
                                    });
                            }
                        }
                        console.log('count file ' + count_file);
                            if (count_file >= 1) {
                                sails.log('masuk array loop')
                                var indexs = 1;
                                for (indexs; indexs <= count_file; indexs++) {
                                    var files = 'files' + indexs;
                                    console.log(files)
                                    req.file(files).upload({
                                        dirname: require('path').resolve(sails.config.appPath, 'assets/file/'),
                                        saveAs: ((file, cb) => {
                                            var extension = file.filename.split('.').pop();
                                            cb(null, user + time_code + indexs + '.' + extension);
                                        })
                                    }, (err, file) => {
                                        if (err) {
                                            return res.serverError(err);
                                        }
                                        if (file.length > 0) {
                                            //insert file
                                            knex.insert({
                                                file_name: file[0].fd.split('/').pop(),
                                                delivery_id: delivery_id[0],
                                                createby: user,
                                                createdate: moment().format('YYYY-MM-DD HH:mm:ss')
                                            })
                                                .into('detail_files')
                                                .then(succ => {
                                                    // sails.log(succ);
                                                    console.log('success array file');
                                                    return null;
                                                }).catch((err) => {
                                                    console.log('error ' + err);
                                                    if (check_api == undefined) {
                                                        req.session.flash['warning'].push(sails.__('error_database'));
                                                        res.redirect('/dopayment/request/billoflading');
                                                    } else {
                                                        res.send({ status: false, message: sails.__('error_database') });
                                                    }
                                                });
                                        }
                                    });
                                }
                            }
                        return knex.select('*').from('requests')
                            .where({ id: id_req[0] })
                            .then(result => {
                                return knex.select('*').from('req_sl_delivery')
                                    .where({ id: delivery_id[0] })
                                    .then(req_sql => {
                                        return knex.select('*').from('companies')
                                            .where({ id: company_id })
                                            .then(comp_req => {
                                                // sails.log('cpm '+comp_req +' comp id'+req.session.companyID)

                                                let status_1 = 'Job Order Notification';
                                                let dear = 'Dear ' + name_forwarder;
                                                let status_2 = 'New Job Order has been received from ' + comp_req[0].name;
                                                let bl_number = req_sql[0].blnum;
                                                let status_3 = 'JOB ORDER';
                                                let date = moment(result[0].createdate, 'DD-MM-YYYY').format('DD MMM YYYY');
                                                let menu = 'Modul Bill Of Lading';
                                                var email_content = mail_content.notif_order(status_1, dear, status_2, bl_number, status_3, date, menu);
                                                return knex.select('name','email').from('companies')
                                                    .where({ id: id_forwarder })
                                                    .then(comp => {
                                                        var email_forwading = process.env['EMAIL_FORWARD'];
                                                        var mail = mail_config.config();

                                                        sails.log('send email');
                                                        sails.log(comp[0].email);
                                                        sails.log(comp_req[0].email);
                                                        
                                                        // setup e-mail data
                                                        var mailOptions = {
                                                            from: '"Digico" <' + email_forwading + '>', // sender address (who sends)
                                                            to: '' + comp[0].email + '', // list of receivers (who receives)
                                                            cc: comp_req[0].email,
                                                            subject: '[NOTIFICATIONS] Digico.id JOB ORDER (BL NUMBER ' + bl_number + ')',
                                                            text: '',
                                                            html: email_content
                                                        };

                                                        // sails.log('mail ' + mail)
                                                        // send mail with defined transport object
                                                        mail.sendMail(mailOptions, function (error, info) {
                                                            if (error) {
                                                                console.log(error);
                                                                console.log(error.response);
                                                                if (check_api == undefined) {
                                                                    req.session.flash['warning'].push('Failed Send email !');
                                                                    res.redirect('/dashboard');
                                                                } else {
                                                                    res.send({status: false , message: error.response });
                                                                }
                                                            } else {
                                                                console.log("Message sent: " + info.response);
                                                                if (check_api == undefined) {
                                                                    req.session.flash['success'].push(sails.__('success'));
                                                                    res.redirect('/dashboard');
                                                                } else {
                                                                    res.send({status: true , message: sails.__('success') });
                                                                }
                                                                
                                                            }
                                                        });
                                                    }).catch((err) => {
                                                        console.log('error ' + err);
                                                        if (check_api == undefined) {
                                                            req.session.flash['warning'].push(sails.__('error_database'));
                                                            res.redirect('/dashboard');
                                                        } else {
                                                            res.send({status: false , message: sails.__('error_database') });
                                                        }

                                                    });
                                            }).catch((err) => {
                                                console.log('error ' + err);
                                                if (check_api == undefined) {
                                                    req.session.flash['warning'].push(sails.__('error_database'));
                                                    res.redirect('/dashboard');
                                                } else {
                                                    res.send({status: false , message: sails.__('error_database') });
                                                }
                                            });
                                    }).catch((err) => {
                                        console.log('error ' + err);
                                        if (check_api == undefined) {
                                            req.session.flash['warning'].push(sails.__('error_database'));
                                            res.redirect('/dashboard');
                                        } else {
                                            res.send({status: false , message: sails.__('error_database') });
                                        }
                                    });
                            }).catch((err) => {
                                console.log('error ' + err);
                                if (check_api == undefined) {
                                    req.session.flash['warning'].push(sails.__('error_database'));
                                    res.redirect('/dashboard');
                                } else {
                                    res.send({status: false , message: sails.__('error_database') });
                                }
                            });
                    }).catch((err) => {
                        console.log('error ' + err);
                        if (check_api == undefined) {
                            req.session.flash['warning'].push(sails.__('error_database'));
                            res.redirect('/dopayment/request/billoflading');
                        } else {
                            res.send({status: false , message: sails.__('error_database') });
                        }
                    });
            }).catch((err) => {
                console.log('error ' + err);
                if (check_api == undefined) {
                    req.session.flash['warning'].push(sails.__('error_database'));
                    res.redirect('/dopayment/request/billoflading');
                } else {
                    res.send({status: false , message: sails.__('error_database') });
                }
            });
    }),
    download_file: ((req, res) => {
        const Path = require('path');
        const fs = require('fs');
        const file = null;
        var id = req.body.id;
        var knex = sails.config.knex;
        knex.select(
            'file_name',
        )
            .from('detail_files')
            .where({ id: id })
            .then(result => {
                fs.createReadStream(Path.resolve('./assets/file/' + result[0].file_name))
                    .on('error', function (err) {
                        return res.serverError(err);
                    })
                    .pipe(res);
            })
    }),
    add_container: ((req, res) => {
        let id_req = req.body.id_req;
        // let id_req = '1';
        let cont_no = req.body.cont_no;
        let count = req.body.count_req;
        // let user = req.session.userName;
        let user = '';
        var check_api= req.body.check_api;
        // console.log(check_api);

        if (check_api == undefined) {
            user = req.session.userName;
        } else {
            user = req.body.username;
        }

        console.log(id_req);
        console.log(cont_no);
        console.log(count);
        console.log(user);
        console.log(check_api);

        var knex = sails.config.knex;
        // insert container
        knex.insert({
            no_container: cont_no,
            delivery_id: id_req,
            createby: user,
            createdate: moment().format('YYYY-MM-DD HH:mm:ss')
        })
            .into('detail_container')
            .then(detail => {
                if (count == 1) {
                    return res.send({ status: 'S', message: 'Success' });
                } else {
                    if (count > 1) {
                        var index = 1
                        for (index; index < count; index++) {
                            var i_cont = 'cont_no' + index;
                            var cont_array = req.body[i_cont];
                            // insert container
                            knex.insert({
                                no_container: cont_array,
                                delivery_id: id_req,
                                createby: user,
                                createdate: moment().format('YYYY-MM-DD HH:mm:ss')
                            })
                                .into('detail_container')
                                .then(detail => {
                                    // sails.log(detail);
                                }).catch((err) => {
                                    console.log('error ' + err);
                                    if (check_api == undefined) {
                                        req.session.flash['warning'].push(sails.__('error_database'));
                                        res.redirect('/dashboard');
                                    } else {
                                        res.send({status: false , message: err});
                                    }
                                });
                        }
                    }
                }
                return res.send({ status: 'S', message: 'Success' });
            }).catch((err) => {
                console.log('error ' + err);
                if (check_api == undefined) {
                    req.session.flash['warning'].push(sails.__('error_database'));
                    res.redirect('/dopayment/request/billoflading');
                } else {
                    res.send({status: false , message: err});
                }
            });

    }),
    delete_container: ((req, res) => {
        let id = req.body.id;
        var check_api = req.body.check_api;

        var knex = sails.config.knex;
        return knex.del().from('detail_container')
            .where({ id: id })
            .then(result => {
                res.send({ status: 'S', message: 'Success' });
            }).catch((err) => {
                console.log('error ' + err);
                if (check_api == undefined) {
                    req.session.flash['warning'].push(sails.__('error_database'));
                    res.redirect('/dopayment/request/billoflading');
                } else {
                    res.send({status: false , message: err});
                }
            });
    }),
    edit_container: ((req, res) => {
        let id = req.body.id;
        let delivery_id = req.body.delivery_id;
        let container_before = req.body.container_before;
        let container_after = req.body.container_after;
        let user = '';
        var check_api = req.body.check_api;

        if (check_api == undefined) {
            user = req.session.userName;
        } else {
            user = req.body.username;
        }

        console.log(id);
        console.log(delivery_id);
        console.log(container_before);
        console.log(container_after);
        console.log(check_api);
        console.log(user);

        var knex = sails.config.knex;

        // check
        if (container_before !== container_after) {
            return knex.select('id')
                .from('detail_container')
                .where({ 'delivery_id': delivery_id, 'no_container': container_after })
                .then(result => {
                    if (result.length > 0) {
                        res.send({ status: 'F', message: 'Container Number Already !' })
                    } else {
                        return knex.update({
                            no_container: container_after,
                            updateby: user,
                            updatedate: moment().format('YYYY-MM-DD HH:mm:ss')
                        })
                            .from('detail_container')
                            .where({ id: id })
                            .then(resultt => {
                                res.send({ status: 'S', message: 'Container Number has been update !' })
                            }).catch((err) => {
                                console.log('error ' + err);
                                if (check_api == undefined) {
                                    req.session.flash['warning'].push(sails.__('error_database'));
                                    res.redirect('/dopayment/request/billoflading');
                                } else {
                                    res.send({status: false , message: err});
                                }
                            });
                    }
                }).catch((err) => {
                    console.log('error ' + err);
                    if (check_api == undefined) {
                        req.session.flash['warning'].push(sails.__('error_database'));
                        res.redirect('/dopayment/request/billoflading');
                    } else {
                        res.send({status: false , message: err});
                    }
                    
                });
        } else {
            res.send({ status: 'S', message: 'Container Number nothing update !' })
        }
    }),
    search_list: ((req, res)=>{
        var knex = sails.config.knex;
        let searchBy = req.body.searchBy;
        let searchFor = req.body.searchFor;
        var list = knex.select(
            'requests.id',
            'requests.reqnum',
            'req_sl_delivery.blnum',
            'req_sl_delivery.ffname',
            'req_sl_delivery.slname',
            'requests.status',
            'mstatus.status_desc'
        )
            list.select(knex.raw(`to_char(requests.createdate, 'dd-mm-yyyy HH24:MI:SS') as create_date`))
            list.from('requests')
            list.leftJoin('req_sl_delivery', 'requests.id', '=', 'req_sl_delivery.request_id')
            list.leftJoin('mstatus', 'requests.status', '=', 'mstatus.status')
            list.where({'requests.company_id' : req.session.companyID})
            if(searchBy == 'req_number'){
                // list.where({'requests.reqnum' : })
                list.whereRaw('requests.reqnum like \'%' + searchFor + '%\'')
            }
            if(searchBy == 'bl_number'){
                list.whereRaw('req_sl_delivery.blnum like \'%' + searchFor + '%\'')
            }
            if(searchBy == 'forwarder'){
                list.whereRaw('LOWER(req_sl_delivery.ffname) like \'%' + searchFor.toLowerCase() + '%\'')
            }
            if (searchBy == 'shipping'){
                list.whereRaw('LOWER(req_sl_delivery.slname) like \'%' + searchFor.toLowerCase() + '%\'')
            }
            if (searchBy == 'status'){
                list.whereRaw('LOWER(mstatus.status_desc) like \'%' + searchFor.toLowerCase() + '%\'')
            }
            list.orderBy('requests.createdate', 'desc')
            list.then(data => {
                res.send({ data: data });
                return;
            })
            .catch(err => {
                console.log('error ' + err);
            });   
    })
}