/* eslint-disable eqeqeq */
/* eslint-disable camelcase */
/**
 * BillPaymentController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
var moment = require('moment');
const cryptoRandomString = require('crypto-random-string');
var char = '1234567890ABCDEFGHJKLMNPQRSTUVWXYZ';
var mail_content = require('../../config/mail_content');
const mail_config = require('../../config/mail_config');
const axios = require('axios');
const Client = require('ssh2-sftp-client');

module.exports = {
  index: ((req, res) => {
    if (!req.session.flash) {
      req.session.flash = {};
      req.session.flash['success'] = [];
      req.session.flash['warning'] = [];
    }
    res.locals.layout = 'layouts/default';
    return res.view('pages/BillPayment/list_bill', { sdata: req.session, menu: process.env['MENU_BILL'] });
  }),
  billpayment_list: ((req, res) => {
    var knex = sails.config.knex;
    knex.select(
      'requests.id',
      'requests.reqnum',
      'requests.payment_code',
      'req_sl_delivery.blnum',
      'req_sl_delivery.donum',
      'req_sl_delivery.ffname',
      'req_sl_delivery.slname',
      'requests.status',
      'requests.note_request',
      'mstatus.status_desc',
      'companies.name as comp_name',
      'req_sl_delivery.total',
      'banks.name',
    )
      .select(knex.raw(`to_char(requests.createdate, 'dd-mm-yyyy HH24:MI:SS') as create_date`))
      .select(knex.raw(`to_char(req_sl_delivery.duedate, 'dd-mm-yyyy') as due_date`))
      .from('requests')
      .leftJoin('req_sl_delivery', 'requests.id', '=', 'req_sl_delivery.request_id')
      .leftJoin('mstatus', 'requests.status', '=', 'mstatus.status')
      .leftJoin('companies', 'requests.company_id', '=', 'companies.id')
      .leftJoin('banks', 'requests.bank_id', '=', 'banks.id')
      .where({ 'req_sl_delivery.slid': req.session.companyID })
      .orderBy('requests.createdate', 'desc')
      .then(data => {
        res.send({ data: data });
        return;
      })
      .catch(err => {
        console.log('error ' + err);
      });
  }),
  download_file: ((req, res) => {
    const Path = require('path');
    const fs = require('fs');
    const file = null;
    var id = req.body.id;
    var knex = sails.config.knex;
    knex.select(
      'file_name',
    )
      .from('detail_files')
      .where({ id: id })
      .then(result => {
        fs.createReadStream(Path.resolve('./assets/file/' + result[0].file_name))
          .on('error', (err) => {
            return res.serverError(err);
          })
          .pipe(res);
      });
  }),
  billpayment_rejectDO: ((req, res) => {
    var knex = sails.config.knex;
    let id = req.body.id;
    let user = req.session.userName;
    let company_id = req.session.companyID;
    let ket = req.body.ket;

    knex.update({
      status: 'RJCS',
      notes: ket,
      updateby: user,
      updatedate: moment().format('YYYY-MM-DD HH:mm:ss')
    })
      .from('requests')
      .where({ id: id })
      .then(resultt => {

        return knex.select('requests.*', 'companies.name', 'companies.email as email_co', 'req_sl_delivery.id as delivery_id', 'req_sl_delivery.slid', 'req_sl_delivery.slname', 'req_sl_delivery.ffid', 'req_sl_delivery.ffname')
          .where({ 'requests.id': id })
          .from('requests')
          .leftJoin('companies', 'requests.company_id', '=', 'companies.id')
          .leftJoin('req_sl_delivery', 'requests.id', '=', 'req_sl_delivery.request_id')
          .then(result => {
            sails.log(result);
            return knex.select('*').from('req_sl_delivery')
              .where({ id: result[0].delivery_id })
              .then(req_sql => {
                return knex.select('*').from('companies')
                  .where({ id: req.session.companyID })
                  .then(comp_req => {
                    // sails.log('cpm '+comp_req +' comp id'+req.session.companyID)
                    let status_1 = 'Delivery Order Rejected Notification';
                    let dear = 'Dear ' + result[0].ffname;
                    let status_2 = 'Delivery Order has been rejected from ' + comp_req[0].name;
                    let bl_number = req_sql[0].blnum;
                    let status_3 = 'DELIVERY ORDER REJECTED';
                    let date = moment(result[0].createdate, 'DD-MM-YYYY').format('DD MMM YYYY');
                    let menu = 'Modul Bill Payment';
                    var email_content = mail_content.notif_order_reject(status_1, dear, status_2, bl_number, status_3, date, menu, ket);
                    return knex.select('name', 'email').from('companies')
                      .where({ id: result[0].ffid })
                      .then(comp => {
                        var email_forwading = process.env['EMAIL_FORWARD'];
                        var mail = mail_config.config();
                        console.log(comp[0].email);
                        // setup e-mail data
                        var mailOptions = {
                          from: '"Digico" <' + email_forwading + '>', // sender address (who sends)
                          to: '' + comp[0].email + '', // list of receivers (who receives)
                          cc: result[0].email_co,
                          subject: '[NOTIFICATIONS] Digico.id DELIVERY ORDER REJECTED (BL NUMBER ' + bl_number + ')',
                          text: '',
                          html: email_content
                        };
                        // sails.log('mail ' + mail)
                        // send mail with defined transport object
                        mail.sendMail(mailOptions, (error, info) => {
                          if (error) {
                            console.log(error);
                            console.log(error.response);
                            req.session.flash['warning'].push(error.response);
                            res.redirect('/dashboard');
                          } else {
                            console.log("Message sent: " + info.response);
                            res.send({ status: 'S', message: 'Success Email' });
                            // res.redirect('/dashboard');
                          }
                        });
                      }).catch((err) => {
                        req.session.flash['warning'].push(sails.__('error_database'));
                        res.redirect('/dashboard');
                        console.log('error ' + err);
                      });
                  }).catch((err) => {
                    req.session.flash['warning'].push(sails.__('error_database'));
                    res.redirect('/dashboard');
                    console.log('error ' + err);
                  });
              }).catch((err) => {
                req.session.flash['warning'].push(sails.__('error_database'));
                res.redirect('/dashboard');
                console.log('error ' + err);
              });
          });
      });
  }),
  billpayment_view: ((req, res) => {
    var knex = sails.config.knex;
    let id = req.body.id;
    sails.log(id);
    res.locals.layout = 'layouts/default';
    knex.select(
      'requests.id',
      'requests.reqnum',
      'requests.tag',
      'requests.payment_code',
      'requests.note_request',
      'req_sl_delivery.blnum',
      'req_sl_delivery.donum',
      'req_sl_delivery.ffname',
      'req_sl_delivery.slname',
      // 'req_sl_delivery.date_issue_bl',
      // 'req_sl_delivery.duedate',
      'requests.status',
      'mstatus.status_desc',
      'companies.name as comp_name',
      'banks.name',
      'req_sl_delivery.id as req_sl_delivery_id',
      'req_sl_delivery.amount',
      'req_sl_delivery.amount2',
      'req_sl_delivery.transfee',
      'req_sl_delivery.filedo',
      'req_sl_delivery.fileinv',
      'req_sl_delivery.fileproformainv',
      'req_sl_delivery.filecoreor',
      'req_sl_delivery.total as total_req',
      'req_sl_delivery.vessel',
      'req_sl_delivery.voyage',
      'req_sl_delivery.pkk',
      'req_sl_delivery.refid',
      'req_sl_delivery.no_proforma',
      'req_sl_delivery.service_type'
    )
      .select(knex.raw(`to_char(requests.createdate, 'dd-mm-yyyy') as create_date`))
      .select(knex.raw(`to_char(req_sl_delivery.duedate, 'dd-mm-yyyy') as due_date`))
      .select(knex.raw(`to_char(req_sl_delivery.date_issue_bl, 'dd-mm-yyyy') as date_issue_bl`))
      .from('requests')
      .leftJoin('req_sl_delivery', 'requests.id', '=', 'req_sl_delivery.request_id')
      .leftJoin('mstatus', 'requests.status', '=', 'mstatus.status')
      .leftJoin('companies', 'requests.company_id', '=', 'companies.id')
      .leftJoin('banks', 'requests.bank_id', '=', 'banks.id')
      .where({ 'requests.id': id })
      .orderBy('requests.createdate', 'desc')
      .then(data => {
        sails.log(' data ' + JSON.stringify(data));
        return knex.select(
          'detail_container.id',
          'detail_container.delivery_id',
          'detail_container.no_container'
        )
          .from('detail_container')
          .where({ 'detail_container.delivery_id': data[0].req_sl_delivery_id })
          .leftJoin('req_sl_delivery', 'detail_container.delivery_id', '=', 'req_sl_delivery.request_id')
          .then(data_container => {
            return knex.select(
              'detail_files.id',
              'detail_files.file_name'
            )
              .from('detail_files')
              .where({ 'detail_files.delivery_id': data[0].req_sl_delivery_id })
              .leftJoin('req_sl_delivery', 'detail_files.delivery_id', '=', 'req_sl_delivery.request_id')
              .then(data_files => {
                console.log(data_files);
                res.locals.layout = 'layouts/default';
                if (data[0].status == 'RQST') {
                  return res.view('pages/BillPayment/detail_bill', { data: data, data_container: data_container, data_files: data_files, sdata: req.session, menu: process.env['MENU_BILL'] });
                } else {
                  return res.view('pages/BillPayment/detail_bill_read', { data: data, data_container: data_container, data_files: data_files, sdata: req.session, menu: process.env['MENU_BILL'] });
                }

              }).catch(err => {
                console.log('error ' + err);
              });
          }).catch(err => {
            console.log('error ' + err);
          });
      }).catch(err => {
        console.log('error ' + err);
      });
  }),
  add_container: ((req, res) => {
    var knex = sails.config.knex;
    let id_req = req.body.id_req_bill;
    let cont_no = req.body.cont_no_bill;
    let count = req.body.count_req_bill;
    let user = req.session.userName;
    // insert container
    knex.insert({
      no_container: cont_no,
      delivery_id: id_req,
      createby: user,
      createdate: moment().format('YYYY-MM-DD HH:mm:ss')
    })
      .into('detail_container')
      .then(detail => {
        if (count == 1) {
          return res.send({ status: 'S', message: 'Success' })
        } else {
          if (count > 1) {
            var index = 1
            for (index; index < count; index++) {
              var i_cont = 'cont_no_bill' + index;
              var cont_array = req.body[i_cont];
              // insert container
              knex.insert({
                no_container: cont_array,
                delivery_id: id_req,
                createby: user,
                createdate: moment().format('YYYY-MM-DD HH:mm:ss')
              })
                .into('detail_container')
                .then(detail => {
                  // sails.log(detail);
                }).catch((err) => {
                  req.session.flash['warning'].push(sails.__('error_database'));
                  res.redirect('/dashboard');
                  console.log('error ' + err);
                });
            }
          }
        }
        return res.send({ status: 'S', message: 'Success' })
      }).catch((err) => {
        req.session.flash['warning'].push(sails.__('error_database'));
        res.redirect('/dopayment/request/billpayment');
        console.log('error ' + err);
      });

  }),
  edit_container: ((req, res) => {
    var knex = sails.config.knex;
    let id = req.body.id;
    let delivery_id = req.body.delivery_id;
    let container_before = req.body.container_before;
    let container_after = req.body.container_after;
    let user = req.session.userName;
    // check
    if (container_before !== container_after) {
      return knex.select('id')
        .from('detail_container')
        .where({ 'delivery_id': delivery_id, 'no_container': container_after })
        .then(result => {
          if (result.length > 0) {
            res.send({ status: 'F', message: 'Container Number Already !' })
          } else {
            return knex.update({
              no_container: container_after,
              updateby: user,
              updatedate: moment().format('YYYY-MM-DD HH:mm:ss')
            })
              .from('detail_container')
              .where({ id: id })
              .then(resultt => {
                res.send({ status: 'S', message: 'Container Number has been update !' })
              }).catch((err) => {
                req.session.flash['warning'].push(sails.__('error_database'));
                res.redirect('/dopayment/request/billpayment');
                console.log('error ' + err);
              });
          }
        }).catch((err) => {
          req.session.flash['warning'].push(sails.__('error_database'));
          res.redirect('/dopayment/request/billpayment');
          console.log('error ' + err);
        });
    } else {
      res.send({ status: 'S', message: 'Container Number nothing update !' })
    }
  }),
  delete_container: ((req, res) => {
    var knex = sails.config.knex;
    let id = req.body.id;
    return knex.del().from('detail_container')
      .where({ id: id })
      .then(result => {
        res.send({ status: 'S', message: 'Success' });
      }).catch((err) => {
        req.session.flash['warning'].push(sails.__('error_database'));
        res.redirect('/dopayment/request/billpayment');
        console.log('error ' + err);
      });
  }),
  billpayment_approveDO: ((req, res) => {
    var knex = sails.config.knex;
    let id = req.body.id;
    let user = req.session.userName;
    var id_req = req.body.id_req;
    let amount = parseFloat(req.body.amount.replace(/,/g, ''));
    let amount2 = parseFloat(req.body.warramount.replace(/,/g, ''));
    let transfee = req.body.transfee;
    let vessel = req.body.vessel;
    let voyage = req.body.voyage;
    let pkk = req.body.pkk;
    let no_do = req.body.nodo;
    let ref_id = req.body.refid;
    let duedate = req.body.expDO;
    let reqnum = req.body.reqnum;
    let date_issue_bl = req.body.date_issue_bl;
    let total = parseInt(amount) + parseInt(amount2) + parseInt(transfee);
    var time_code = moment().format('ssmmHHMMDDYYYY');
    let method_payment = req.body.method_payment;
    var stat = '';
    var service_type = req.body.service_type;
    var no_proforma = req.body.no_proforma;
    console.log(service_type);
    console.log(no_proforma);
    console.log(reqnum);
    if(method_payment == 2){
      stat = 'BILL';
    }else{
      stat = 'CLSE';
    }
    console.log('duedate');
    console.log(duedate);
    if (duedate === '' || duedate === undefined) {
      duedate = null;
    } else {
      duedate = moment(duedate, 'DD-MM-YYYY').format('YYYY-MM-DD');
      duedate = duedate + ' 00:00:00';
    }
    if (date_issue_bl === '' || date_issue_bl === undefined) {
      date_issue_bl = null;
    } else {
      date_issue_bl = moment(date_issue_bl, 'DD-MM-YYYY').format('YYYY-MM-DD');
    }
    sails.log('log body ' + JSON.stringify(req.body));
    knex.update({
      status: stat,
      total: total,
      updateby: user,
      updatedate: moment().format('YYYY-MM-DD HH:mm:ss')
    })
      .from('requests')
      .where({ id: id })
      .then(resultt => {

        return knex.update({
          amount: amount,
          amount2: amount2,
          transfee: transfee,
          vessel: vessel,
          voyage: voyage,
          pkk: pkk,
          donum: no_do,
          duedate: duedate,
          refid: ref_id,
          total: total,
          no_proforma: no_proforma,
          service_type: service_type,
          date_issue_bl: date_issue_bl,
          updateby: user,
          updatedate: moment().format('YYYY-MM-DD HH:mm:ss')
        })
          .from('req_sl_delivery')
          .where({ id: id_req })
          .then(result2 => {

            if(method_payment == 2){
              //upload profoma invoice
              req.file('file_pInvoice').upload({
                dirname: require('path').resolve(sails.config.appPath, 'assets/file/'),
                saveAs: ((file, cb) => {

                  sails.log('req file 0' + file.filename);
                  var extension = file.filename.split('.').pop();
                  cb(null, 'PINV_' + user + time_code + '.' + extension);
                })
              }, (err, file_pInvoice) => {
                if(err) {
                  return res.serverError(err);
                }
                if(file_pInvoice.length > 0){
                  knex.update({
                    fileproformainv: file_pInvoice[0].fd.split('/').pop()
                  })
                    .from('req_sl_delivery')
                    .where({ 'id': id_req })
                  .catch((err)=> {
                    req.session.flash['warning'].push(sails.__('error_database'));
                    res.redirect('/dopayment/request/billpayment');
                    console.log('error '+err);
                  });
                }
              });

            //   var url = process.env['IP_PAYMENT'] + process.env['INSW'];
            //   console.log(url);
      
            //   var input_data = 
            //   {
            //     reqnum: reqnum,
            //   };
      
            //   console.log(input_data);
      
            //   axios({
            //     method: 'post',
            //     url: url,
            //     // timeout: 10000,
            //     headers: {
            //       'Content-Type': 'application/json'
            //     },
            //     data: input_data
            //   })
            //   .then((result)=>{
            //       console.log("data_result");
            //       console.log(result.data);
            //   }).catch((error) => {
            //       console.log(error);
            //       sails.log(error);
            //   });
            }
            
            if(method_payment == 4){

              //upload invoice
              req.file('upload_invoice').upload({
                dirname: require('path').resolve(sails.config.appPath, 'assets/file/'),
                saveAs: ((file, cb) => {

                  sails.log('req file 1' + file.filename);
                  var extension = file.filename.split('.').pop();
                  cb(null, 'INV_' + user + time_code + '.' + extension);
                })
              }, (err, upload_invoice) => {
                if(err) {
                  return res.serverError(err);
                }
                if(upload_invoice.length > 0){
                  knex.update({
                    fileinv: upload_invoice[0].fd.split('/').pop()
                  })
                    .from('req_sl_delivery')
                    .where({ 'id': id_req })
                  .catch((err)=> {
                    req.session.flash['warning'].push(sails.__('error_database'));
                    res.redirect('/dopayment/request/billpayment');
                    console.log('error '+err);
                  });
                }
              });

              //upload file do
              req.file('upload_fDO').upload({
                dirname: require('path').resolve(sails.config.appPath, 'assets/file/'),
                saveAs: ((file, cb) => {

                  sails.log('req file 2' + file.filename);
                  var extension = file.filename.split('.').pop();
                  cb(null, 'DO_' + user + time_code + '.' + extension);
                })
              }, (err, upload_fDO) => {
                if(err) {
                  return res.serverError(err);
                }
                if(upload_fDO.length > 0){
                  knex.update({
                    filedo: upload_fDO[0].fd.split('/').pop()
                  }).from('req_sl_delivery')
                  .where({ 'id': id_req })
                  .then(upload => {
                  })
                  .catch((err)=> {
                    req.session.flash['warning'].push(sails.__('error_database'));
                    res.redirect('/dashboard');
                    console.log('error '+err);
                  });
                }
              });

              //upload file coreor
              req.file('upload_coreor').upload({
                dirname: require('path').resolve(sails.config.appPath, 'assets/file/'),
                saveAs: ((file, cb) => {

                  sails.log('req file 2' + file.filename);
                  var extension = file.filename.split('.').pop();
                  cb(null, 'Coreor_' + user + time_code + '.' + extension);
                })
              }, (err, upload_coreor) => {
                if(err) {
                  return res.serverError(err);
                }
                if(upload_coreor.length > 0){
                  knex.update({
                    filecoreor: upload_coreor[0].fd.split('/').pop()
                  }).from('req_sl_delivery')
                  .where({ 'id': id_req })
                  .then(upload => {
                  })
                  .catch((err)=> {
                    req.session.flash['warning'].push(sails.__('error_database'));
                    res.redirect('/dashboard');
                    console.log('error '+err);
                  });
                }
              });
            }

            return knex.select('requests.*', 'companies.name', 'companies.email as email_co', 'req_sl_delivery.id as delivery_id', 'req_sl_delivery.slid', 'req_sl_delivery.slname', 'req_sl_delivery.ffname')
            .where({ 'requests.id': id })
            .from('requests')
            .leftJoin('companies', 'requests.company_id', '=', 'companies.id')
            .leftJoin('req_sl_delivery', 'requests.id', '=', 'req_sl_delivery.request_id')
            .then(result => {
              return knex.select('*').from('req_sl_delivery')
              .where({ id: result[0].delivery_id })
              .then(req_sql => {
                sails.log('select req_sql ' + JSON.stringify(req_sql));
                // sails.log('cpm '+comp_req +' comp id'+req.session.companyID)
                let status_1 = '';
                let subject = '';
                if (method_payment == 4) {
                  status_1 = 'Delivery Order Released Notification';
                  subject = 'DELIVERY ORDER RELEASED';
                } else {
                  status_1 = 'Bill Notification';
                  subject = 'BILL';
                }
                let dear = 'Dear ' + result[0].ffname;
                let status_2 = '';
                if (method_payment == 4) {
                  status_2 = 'New Delivery Order has been Relesead ';
                } else {
                  status_2 = 'New Bill Request has been received';
                }
                let bl_number = req_sql[0].blnum;
                let status_3 = '';
                if (method_payment == 4) {
                  status_3 = 'DELIVERY ORDER RELEASED'
                } else {
                  status_3 = 'BILL'
                }
                let date = moment(result[0].createdate, 'DD-MM-YYYY').format('DD MMM YYYY');
                let menu = 'Modul Bill Payment';
                var email_content = mail_content.notif_order(status_1, dear, status_2, bl_number, status_3, date, menu);
                return knex.select('id', 'name', 'email').from('companies')
                  .where({ 'id': req_sql[0].ffid })
                  .then(comp => {
                    var email_forwading = process.env['EMAIL_FORWARD'];
                    var mail = mail_config.config();

                    sails.log('send email');
                    sails.log(comp[0].email);
                    sails.log(result[0].email_co);

                    // setup e-mail data
                    var mailOptions = {
                      from: '"Digico" <' + email_forwading + '>', // sender address (who sends)
                      to: '' + comp[0].email + '', // list of receivers (who receives)
                      cc: result[0].email_co,
                      subject: '[NOTIFICATIONS] Digico.id ' + subject + ' (BL NUMBER ' + bl_number + ')',
                      text: '',
                      html: email_content
                    };
                    mail.sendMail(mailOptions, (error, info) => {
                      if (error) {
                        console.log(error);
                        console.log(error.response);
                        req.session.flash['warning'].push(error.response);
                        res.redirect('/dashboard');
                      } else {
                        console.log("Message sent: " + info.response);
                        // res.send({ status: 'S', message: 'Success Approve !' });
                        req.session.flash['success'].push(sails.__('success_approve'));
                        res.redirect('/dopayment/request/billpayment');
                      }
                    });
                  }).catch((err) => {
                    req.session.flash['warning'].push(sails.__('error_database'));
                    res.redirect('/dashboard');
                    console.log('error ' + err);
                  });
              }).catch((err) => {
                req.session.flash['warning'].push(sails.__('error_database'));
                res.redirect('/dashboard');
                console.log('error ' + err);
              });
            })
            .catch((err)=> {
              req.session.flash['warning'].push(sails.__('error_database'));
              res.redirect('/dashboard');
              console.log('error '+err);
            });
          })
          .catch((err) => {
            req.session.flash['warning'].push(sails.__('error_database'))
            res.redirect('/dashboard');
            console.log('error ' + err);
          });
      })
      .catch((err) => {
        req.session.flash['warning'].push(sails.__('error_database'))
        res.redirect('/dashboard');
        console.log('error ' + err);
      });

  }),

  submit_paidDO: ((req, res) => {
    var knex = sails.config.knex;
    let id = req.body.id;
    let user = req.session.userName;
    var id_req = req.body.id_req;
    var time_code = moment().format('ssmmHHMMDDYYYY');
    let duedate = req.body.expDO;
    let reqnum = req.body.reqnum;
    var date_issue_bl = req.body.date_issue_bl;
    
    var stat = 'CLSE';

    if (duedate === '') {
      duedate = null;
    } else {
      duedate = moment(duedate, 'DD-MM-YYYY').format('YYYY-MM-DD');
      duedate = duedate + ' 00:00:00';
    }

    if (date_issue_bl === '' || date_issue_bl === undefined) {
      date_issue_bl = null;
    } else {
      date_issue_bl = moment(date_issue_bl, 'DD-MM-YYYY').format('YYYY-MM-DD');
    }
    
    sails.log('log body ' + JSON.stringify(req.body));
    knex.update({
      status: stat,
      updateby: user,
      updatedate: moment().format('YYYY-MM-DD HH:mm:ss')
    })
      .from('requests')
      .where({ id: id })
      .then(resultt => {
        //upload invoice
        var url = process.env['IP_PAYMENT'] + process.env['INSW'];
        console.log(url);

        var input_data = 
        {
          reqnum: reqnum,
        };

        console.log(input_data);

        // axios({
        //   method: 'post',
        //   url: url,
        //   timeout: 10000,
        //   headers: {
        //     'Content-Type': 'application/json'
        //   },
        //   data: input_data
        // })
        // .then((result)=>{
        //     console.log("data_result");
        //     console.log(result.data);
        // }).catch((error) => {
        //     console.log(error);
        //     sails.log(error);
        // });

        knex.update({
          duedate: duedate,
          date_issue_bl: date_issue_bl
        })
        .from('req_sl_delivery')
        .where({ 'id': id_req })
        .catch((err)=> {
          req.session.flash['warning'].push(sails.__('error_database'));
          res.redirect('/dopayment/request/billpayment');
          console.log('error '+err);
        });

        req.file('upload_invoice').upload({
          dirname: require('path').resolve(sails.config.appPath, 'assets/file/'),
          saveAs: ((file, cb) => {

            sails.log('req file 1' + file.filename);
            var extension = file.filename.split('.').pop();
            cb(null, 'INV_' + user + time_code + '.' + extension);
          })
        }, (err, upload_invoice) => {
          if(err) {
            return res.serverError(err);
          }
          if(upload_invoice.length > 0){
            knex.update({
              fileinv: upload_invoice[0].fd.split('/').pop()
            })
              .from('req_sl_delivery')
              .where({ 'id': id_req })
            .catch((err)=> {
              req.session.flash['warning'].push(sails.__('error_database'));
              res.redirect('/dopayment/request/billpayment');
              console.log('error '+err);
            });
          }
        });
        //upload file do
        req.file('upload_fDO').upload({
          dirname: require('path').resolve(sails.config.appPath, 'assets/file/'),
          saveAs: ((file, cb) => {

            sails.log('req file 2' + file.filename);
            var extension = file.filename.split('.').pop();
            cb(null, 'DO_' + user + time_code + '.' + extension);
          })
        }, (err, upload_fDO) => {
          if(err) {
            return res.serverError(err);
          }
          if(upload_fDO.length > 0){
            knex.update({
              filedo: upload_fDO[0].fd.split('/').pop()
            }).from('req_sl_delivery')
            .where({ 'id': id_req })
            .then(upload => {
            })
            .catch((err)=> {
              req.session.flash['warning'].push(sails.__('error_database'));
              res.redirect('/dashboard');
              console.log('error '+err);
            });
          }
        });
        //upload file do
        req.file('upload_coreor').upload({
          dirname: require('path').resolve(sails.config.appPath, 'assets/file/'),
          saveAs: ((file, cb) => {

            sails.log('req file 3' + file.filename);
            var extension = file.filename.split('.').pop();
            cb(null, 'Coreor_' + user + time_code + '.' + extension);
          })
        }, (err, upload_coreor) => {
          if(err) {
            return res.serverError(err);
          }
          if(upload_coreor.length > 0){
            knex.update({
              filecoreor: upload_coreor[0].fd.split('/').pop()
            }).from('req_sl_delivery')
            .where({ 'id': id_req })
            .then(upload => {
            })
            .catch((err)=> {
              req.session.flash['warning'].push(sails.__('error_database'));
              res.redirect('/dashboard');
              console.log('error '+err);
            });
            const fs = require('fs');
            const Path = require('path');
            let sftp = new Client();

            const config = {
              host: process.env['HOST_INSW'],
              port: process.env['PORT_INSW'],
              username: process.env['USERNAME_INSW'],
              password: process.env['PASSWORD_INSW']
            };

            var name_file_coreor = upload_coreor[0].fd.split('/').pop();
            console.log('name_file_coreor ' + name_file_coreor);
            var path_local = Path.resolve('./assets/file/' + name_file_coreor);
            console.log(path_local);
            let data = fs.createReadStream(path_local);
            let remote = '/home/digi-comp/'+ name_file_coreor;
              
            sftp.connect(config)
            .then(() => {
                console.log('put');
                return sftp.put(data, remote);
            })
            .then(() => {
                console.log('end');
                return sftp.end();
            })
            .catch(err => {
                console.log('error');
                console.error(err.message);
                req.session.flash['warning'].push(sails.__('error_insw'));
                res.redirect('/dashboard');
            });
          }
        });

        return knex.select('requests.*', 'companies.name', 'companies.email as email_co', 'req_sl_delivery.id as delivery_id', 'req_sl_delivery.slid', 'req_sl_delivery.slname', 'req_sl_delivery.ffname')
        .where({ 'requests.id': id })
        .from('requests')
        .leftJoin('companies', 'requests.company_id', '=', 'companies.id')
        .leftJoin('req_sl_delivery', 'requests.id', '=', 'req_sl_delivery.request_id')
        .then(result => {
          return knex.select('*').from('req_sl_delivery')
          .where({ id: result[0].delivery_id })
          .then(req_sql => {
            sails.log('select req_sql ' + JSON.stringify(req_sql));

            var status_1 = 'Delivery Order Released Notification';
            let dear = 'Dear ' + result[0].ffname;
            let status_2 = 'New Delivery Order has been Relesead ';
            let bl_number = req_sql[0].blnum;
            let status_3 = 'DELIVERY ORDER RELEASED';
            let date = moment(result[0].createdate, 'DD-MM-YYYY').format('DD MMM YYYY');
            let menu = 'Modul Bill Payment';
            var email_content = mail_content.notif_order(status_1, dear, status_2, bl_number, status_3, date, menu);
            return knex.select('id', 'name', 'email').from('companies')
              .where({ 'id': req_sql[0].ffid })
              .then(comp => {
                var email_forwading = process.env['EMAIL_FORWARD'];
                var mail = mail_config.config();

                sails.log('send email');
                sails.log(comp[0].email);
                sails.log(result[0].email_co);

                // setup e-mail data
                var mailOptions = {
                  from: '"Digico" <' + email_forwading + '>', // sender address (who sends)
                  to: '' + comp[0].email + '', // list of receivers (who receives)
                  cc: result[0].email_co,
                  subject: '[NOTIFICATIONS] Digico.id DELIVERY ORDER RELEASED (BL NUMBER ' + bl_number + ')',
                  text: '',
                  html: email_content
                };
                mail.sendMail(mailOptions, (error, info) => {
                  if (error) {
                    console.log(error);
                    console.log(error.response);
                    req.session.flash['warning'].push(error.response);
                    res.redirect('/dashboard');
                  } else {
                    console.log("Message sent: " + info.response);
                    console.log("success terkirim before");
                    // res.send({ status: 'S', message: 'Success Approve !' });
                    req.session.flash['success'].push(sails.__('success_approve'));
                    res.redirect('/dopayment/request/billpayment');
                    console.log("success terkirim after");
                  }
                });
              }).catch((err) => {
                req.session.flash['warning'].push(sails.__('error_database'));
                res.redirect('/dashboard');
                console.log('error ' + err);
              });
          }).catch((err) => {
            req.session.flash['warning'].push(sails.__('error_database'));
            res.redirect('/dashboard');
            console.log('error ' + err);
          });
        })
      .catch((err) => {
        req.session.flash['warning'].push(sails.__('error_database'))
        res.redirect('/dashboard');
        console.log('error ' + err);
      });
    })
    .catch((err) => {
      req.session.flash['warning'].push(sails.__('error_database'))
      res.redirect('/dashboard');
      console.log('error ' + err);
    });

  }),

  search: ((req, res) => {
    sails.log('masuk ksini');
    var knex = sails.config.knex;
    let searchBy = req.body.searchBybill;
    let searchFor = req.body.searchFor;
    // let searchForExp = req.body.searchForExp;
    if (searchBy === 'exp_do') {
      if (searchFor === '') {
        searchFor = null;
      } else {
        searchFor = moment(searchFor, 'DD-MM-YYYY').format('YYYY-MM-DD');
      }
    }

    var list = knex
      .select(
        // 'requests.id as id_req',
        'requests.id',
        'requests.reqnum',
        'req_sl_delivery.blnum',
        'req_sl_delivery.ffname',
        'req_sl_delivery.slname',
        'req_sl_delivery.donum',
        'req_sl_delivery.total',
        'requests.status',
        'requests.note_request',
        'companies.name as comp_name',
        'banks.name',
        'req_sl_delivery.createdate',
        // 'req_sl_delivery.id',
        'req_sl_delivery.id as req_sl_delivery_id',
        'requests.payment_code',
        'mstatus.status_desc'
      )
    list.select(knex.raw(`to_char(req_sl_delivery.duedate, 'dd-mm-yyyy') as due_date`))
    list.select(knex.raw(`to_char(req_sl_delivery.createdate, 'dd-mm-yyyy HH24:MI:SS') as create_date`))
    list.from('requests')
    list.leftJoin('req_sl_delivery', 'requests.id', '=', 'req_sl_delivery.request_id')
    list.leftJoin('banks', 'requests.bank_id', '=', 'banks.id')
      .leftJoin('companies', 'requests.company_id', '=', 'companies.id')
    list.leftJoin('mstatus', 'requests.status', '=', 'mstatus.status')
    list.where({ 'req_sl_delivery.slid': req.session.companyID })
    if (searchBy === 'req_number') {
      // list.where({'requests.reqnum' : })
      list.whereRaw('requests.reqnum like \'%' + searchFor + '%\'')
    }
    if (searchBy === 'bl_number') {
      list.whereRaw('req_sl_delivery.blnum like \'%' + searchFor + '%\'')
    }
    if (searchBy === 'cargo_owner') {
      list.whereRaw('LOWER(companies.name) like \'%' + searchFor.toLowerCase() + '%\'')
    }
    if (searchBy === 'shipping') {
      list.whereRaw('LOWER(req_sl_delivery.slname) like \'%' + searchFor.toLowerCase() + '%\'')
    }
    if (searchBy === 'no_do') {
      list.whereRaw('req_sl_delivery.donum like \'%' + searchFor + '%\'')
    }
    if (searchBy === 'exp_do') {
      list.where('req_sl_delivery.duedate', '=', searchFor)
    }
    if (searchBy === 'amount') {
      list.whereRaw('req_sl_delivery.total like \'%' + searchFor + '%\'')
    }
    if (searchBy === 'status') {
      list.whereRaw('LOWER(mstatus.status_desc) like \'%' + searchFor.toLowerCase() + '%\'')
    }
    if (searchBy === 'bank') {
      list.whereRaw('LOWER(banks.name) like \'%' + searchFor.toLowerCase() + '%\'')
    }
    if (searchBy === 'kd_bayar') {
      list.whereRaw('requests.payment_code like \'%' + searchFor + '%\'')
    }
    if (searchBy === 'date'){
      list.whereRaw("to_char(req_sl_delivery.createdate, 'dd-mm-yyyy HH24:MI:SS') like  \'%'" + searchFor + "'%\'")
    }
    list.orderBy('req_sl_delivery.createdate', 'desc')
    list.then(data => {
      res.send({ data: data });
    })
      .catch(err => {
        console.log('error ' + err);
      });
  })
}