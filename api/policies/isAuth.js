
module.exports = function (req, res, next) {
  if (!req.session.flash) {
    req.session.flash = {};
    req.session.flash['success'] = [];
    req.session.flash['warning'] = [];
  }
  if(req.session.authenticated){
    return next();
  }else{
    req.session.flash['warning'].push('Cannot Access, please login !');
    res.redirect('/');
    return;
  }
};
