var nodemailer = require("nodemailer");
module.exports = {
    config: function(){
        var host_email = process.env['HOST_EMAIL'];
        var email_forwading = process.env['EMAIL_FORWARD'];
        var pass_email = process.env['PASS_EMAIL_FORWARD'];

        var transporter = nodemailer.createTransport({
            host: host_email,
            port: 465,
            secure: true, // use SSL
            auth: {
            user: email_forwading,
            pass: pass_email
            }
        });
        return transporter;
    },
}