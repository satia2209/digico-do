module.exports = {
	notif_order: function (Status_1, Dear, Status_2, Bl_no, Status_3, Date_order, Menu) {
        var content = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">'+
        '<html xmlns="http://www.w3.org/1999/xhtml">'+
        '<head>'+
        '<meta name="viewport" content="width=device-width" />'+
        '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />'+
        '<title>Title</title>'+
        '<link href="/styles.css" media="all" rel="stylesheet" type="text/css" />'+
        '</head>'+
        '<body>'+
        '<table class="body-wrap">'+
        '<tr>'+
        '<td></td>'+
        '<td class="container">'+
        '<div class="content">'+
        '<table class="main" width="100%" cellpadding="0" cellspacing="0">'+
        '<tr>'+
        '<td class="content-wrap alignleft">'+
        '<table width="100%" cellpadding="0" cellspacing="0">'+
        '<tr>'+
        '<td class="content-block">'+
        '<h2>DIGICO GudDO</h1>'+
        '<h3>'+Status_1+'</h2>'+
        '</td>'+
        '<td> <img style="margin-left:630px" src="'+process.env['BASE_URL']+'/images/logo/DigicoAsset_email.png" alt=""></td>'+
        '</tr>'+
        '<tr>'+
        '<td class="content-block aligncenter" colspan="5">'+
        '<table class="invoice">'+
        '<tr>'+
        '<td>'+
        '<h4>'+Menu+'</h4>'+
        '</td>'+
        '</tr>'+
        '<tr>'+
        '<td>'+
        '<i>*** The contents of this email was automatically generated ***</i>'+
        '</td>'+
        '</tr>'+
        '<tr>'+
        '<td>'+
        '<i>'+Dear+','+'</i>'+
        '<br>'+Status_2+'.'+
        '<br> The Summary of Job Order is as follows :</td>'+
        '</tr>'+
        '<tr>'+
        '<td>'+
        '<table class="invoice-items" cellpadding="0" cellspacing="0">'+
        '<tr>'+
        '<td style="width: 100px;">Bl Number</td>'+
        '<td>:</td>'+
        '<td class="alignleft">'+Bl_no+'</td>'+
        '</tr>'+
        '<tr>'+
        '<td>Status</td>'+
        '<td>:</td>'+
        '<td class="alignleft">'+Status_3+'</td>'+
        '</tr>'+
        '<tr>'+
        '<td>Date</td>'+
        '<td>:</td>'+
        '<td class="alignleft">'+Date_order+'</td>'+
        '</tr>'+
        '</table>'+
        '</td>'+
        '</tr>'+
        '<tr>'+
        '<td>'+
        '<p>You are receiving this notification because your name and email address registered into our DIGICO GudDO user database. </p>'+
        '<p>Please <b>DOT NOT</b> reply back to this email address sender we are using it for sending purpose only and are not monitoring any incoming email into it. </p>'+
        '<p>If you have any questions or informations regarding this notification, or if you do not want to receive any notifications in the future, please contact our Customer Care officer at below address.  </p>'+
        '<p>Thank you.</p>'+
        '</td>'+
        '</tr>'+
        '</table>'+
        '</td>'+
        '</tr>'+
        '</table>'+
        '</td>'+
        '</tr>'+
        '</table>'+
        '<div class="footer">'+
        '<p>DIGICO GudDO Customer Care</p>'+
        '<p>Gedung Setiabudi 2</p>'+
        '<p>Jl. HR Rasuna Said Kav.62</p>'+
        '<p>Jakara Selatan 12910, Indonesia</p>'+
        '<p>e-Mail: admin@digico.id</p>'+
        '<p>Website: www.digico.id</p>'+
        '</div></div>'+
        '</td>'+
        '<td></td>'+
        '</tr>'+
        '</table>'+
        '</body>'+
        '</html>';
        return content;
    },
    notif_order_reject: function (Status_1, Dear, Status_2, Bl_no, Status_3, Date_order, Menu, Keterangan) {
        var content = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">'+
        '<html xmlns="http://www.w3.org/1999/xhtml">'+
        '<head>'+
        '<meta name="viewport" content="width=device-width" />'+
        '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />'+
        '<title>Title</title>'+
        '<link href="/styles.css" media="all" rel="stylesheet" type="text/css" />'+
        '</head>'+
        '<body>'+
        '<table class="body-wrap">'+
        '<tr>'+
        '<td></td>'+
        '<td class="container">'+
        '<div class="content">'+
        '<table class="main" width="100%" cellpadding="0" cellspacing="0">'+
        '<tr>'+
        '<td class="content-wrap alignleft">'+
        '<table width="100%" cellpadding="0" cellspacing="0">'+
        '<tr>'+
        '<td class="content-block">'+
        '<h2>DIGICO GudDO</h1>'+
        '<h3>'+Status_1+'</h2>'+
        '</td>'+
        '<td> <img style="margin-left:630px" src="'+process.env['BASE_URL']+'/images/logo/DigicoAsset_email.png" alt=""></td>'+
        '</tr>'+
        '<tr>'+
        '<td class="content-block aligncenter" colspan="5">'+
        '<table class="invoice">'+
        '<tr>'+
        '<td>'+
        '<h4>'+Menu+'</h4>'+
        '</td>'+
        '</tr>'+
        '<tr>'+
        '<td>'+
        '<i>*** The contents of this email was automatically generated ***</i>'+
        '</td>'+
        '</tr>'+
        '<tr>'+
        '<td>'+
        '<i>'+Dear+','+'</i>'+
        '<br>'+Status_2+'.'+
        '<br> The Summary of Job Order is as follows :</td>'+
        '</tr>'+
        '<tr>'+
        '<td>'+
        '<table class="invoice-items" cellpadding="0" cellspacing="0">'+
        '<tr>'+
        '<td style="width: 100px;">Bl Number</td>'+
        '<td>:</td>'+
        '<td class="alignleft">'+Bl_no+'</td>'+
        '</tr>'+
        '<tr>'+
        '<td>Status</td>'+
        '<td>:</td>'+
        '<td class="alignleft">'+Status_3+'</td>'+
        '</tr>'+
        '<tr>'+
        '<td>Date</td>'+
        '<td>:</td>'+
        '<td class="alignleft">'+Date_order+'</td>'+
        '</tr>'+
        '<tr>'+
        '<td>Keterangan</td>'+
        '<td>:</td>'+
        '<td class="alignleft">'+Keterangan+'</td>'+
        '</tr>'+
        '</table>'+
        '</td>'+
        '</tr>'+
        '<tr>'+
        '<td>'+
        '<p>You are receiving this notification because your name and email address registered into our DIGICO GudDO user database. </p>'+
        '<p>Please <b>DOT NOT</b> reply back to this email address sender we are using it for sending purpose only and are not monitoring any incoming email into it. </p>'+
        '<p>If you have any questions or informations regarding this notification, or if you do not want to receive any notifications in the future, please contact our Customer Care officer at below address.  </p>'+
        '<p>Thank you.</p>'+
        '</td>'+
        '</tr>'+
        '</table>'+
        '</td>'+
        '</tr>'+
        '</table>'+
        '</td>'+
        '</tr>'+
        '</table>'+
        '<div class="footer">'+
        '<p>DIGICO GudDO Customer Care</p>'+
        '<p>Gedung Setiabudi 2</p>'+
        '<p>Jl. HR Rasuna Said Kav.62</p>'+
        '<p>Jakara Selatan 12910, Indonesia</p>'+
        '<p>e-Mail: admin@digico.id</p>'+
        '<p>Website: www.digico.id</p>'+
        '</div></div>'+
        '</td>'+
        '<td></td>'+
        '</tr>'+
        '</table>'+
        '</body>'+
        '</html>';
        return content;
    },
}