/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {

  /***************************************************************************
  *                                                                          *
  * Make the view located at `views/homepage.ejs` your home page.            *
  *                                                                          *
  * (Alternatively, remove this and add an `index.html` file in your         *
  * `assets` directory)                                                      *
  *                                                                          *
  ***************************************************************************/
  //Login
  'get /': 'LoginController.index',
  'post /login/action': 'LoginController.login',
  'get /login/captcha': 'LoginController.captcha_load',

  //Dashboard
  'get /dashboard': 'DashboardController.index',
  //Companies
  'post /companies/forwarder': 'CompaniesController.forwarder',
  'post /companies/shipping': 'CompaniesController.shipping',

  //Delivery Order
  'get  /dopayment/requests/delivery': 'DeliveryOrderController.index',
  'post /dopayment/requests/listDeliveryOrder': 'DeliveryOrderController.listDeliveryOrder',
  'post /dopayment/requests/listDeliveryOrderbySearch': 'DeliveryOrderController.listDeliveryOrderbySearch',
  'post /dopayment/requests/viewDeliveryOrder': 'DeliveryOrderController.viewDeliveryOrder',
  'post /dopayment/requests/detailContainer': 'DeliveryOrderController.detailContainer',
  'post /dopayment/requests/detailFiles': 'DeliveryOrderController.detailFiles',
  'post /dopayment/requests/downloadFiles': 'DeliveryOrderController.downloadFiles',
  'post /dopayment/requests/addContainer': 'DeliveryOrderController.addContainer',
  'post /dopayment/requests/editContainer': 'DeliveryOrderController.editContainer',
  'post /dopayment/requests/deleteContainer': 'DeliveryOrderController.deleteContainer',
  'post /dopayment/requests/rejectDO': 'DeliveryOrderController.rejectDO',
  'post /dopayment/requests/approveDO': 'DeliveryOrderController.approveDO',
  'post /dopayment/requests/searchListDeliveryOrder': 'DeliveryOrderController.searchListDeliveryOrder',
  'post /dopayment/requests/inquiry': 'DeliveryOrderController.inquiry',
  'post /dopayment/requests/updateStatusPayment': 'DeliveryOrderController.updateStatusPayment',
  'get /dopayment/requests/view_list_payment': 'DeliveryOrderController.view_list_payment',
  'post /dopayment/requests/listPayment': 'DeliveryOrderController.listPayment',
  'post /dopayment/requests/calculate_amount_payment': 'DeliveryOrderController.calculate_amount_payment',
  'post /dopayment/requests/multiple_inquiry': 'DeliveryOrderController.multiple_inquiry',

  //Bill Of Lading
  'get /dopayment/request/billoflading': 'BillOfLadingController.index',
  'post /request/submit': 'BillOfLadingController.submit_request',
  'post /dopayment/request/billoflading_list': 'BillOfLadingController.billoflading_list',
  'post /dopayment/request/billoflading_list_search': 'BillOfLadingController.billoflading_list_search',
  'post /dopayment/request/billoflading_view': 'BillOfLadingController.billoflading_view',
  'post /dopayment/request/billoflading_download': 'BillOfLadingController.download_file',
  'post /dopayment/request/billoflading_add_container': 'BillOfLadingController.add_container',
  'post /dopayment/request/billoflading_del_container': 'BillOfLadingController.delete_container',
  'post /dopayment/request/billoflading_edit_container': 'BillOfLadingController.edit_container',
  'post /dopayment/request/billoflading_search_list': 'BillOfLadingController.search_list',

  //Bill payment
  'get /dopayment/request/billpayment': 'BillPaymentController.index',
  'post /dopayment/request/billpayment_list': 'BillPaymentController.billpayment_list',
  'post /dopayment/request/billpayment_view': 'BillPaymentController.billpayment_view',
  'post /dopayment/request/billpayment_approveDO': 'BillPaymentController.billpayment_approveDO',
  'post /dopayment/request/billpayment_submit_paidDO': 'BillPaymentController.submit_paidDO',
  // 'post /dopayment/request/billpayment_view': 'BillPaymentController.billpayment_view',
  'post /dopayment/request/billpayment_add_container': 'BillPaymentController.add_container',
  'post /dopayment/request/billpayment_edit_container': 'BillPaymentController.edit_container',
  'post /dopayment/request/billpayment_del_container': 'BillPaymentController.delete_container',
  'post /dopayment/request/billpayment_download': 'BillPaymentController.download_file',
  'post /dopayment/requests/billpayment_rejectDO': 'BillPaymentController.billpayment_rejectDO',
  'post /dopayment/request/searchListbill': 'BillPaymentController.search',
  //Logout
  'get /logout/action': 'LogoutController.logout',

  //test layout email
  'get /layout_email' : 'EmailController.index',

  //Customer Support
  'get  /dopayment/support/form': 'CustomerSupportController.index',
  'post /dopayment/support/sendMessage': 'CustomerSupportController.send_trouble',
  'get /dopayment/support/help': 'CustomerSupportController.help'
  /***************************************************************************
  *                                                                          *
  * More custom routes here...                                               *
  * (See https://sailsjs.com/config/routes for examples.)                    *
  *                                                                          *
  * If a request to a URL doesn't match any of the routes in this file, it   *
  * is matched against "shadow routes" (e.g. blueprint routes).  If it does  *
  * not match any of those, it is matched against static assets.             *
  *                                                                          *
  ***************************************************************************/


};
