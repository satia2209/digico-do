/**
 * Seed Function
 * (sails.config.bootstrap)
 *
 * A function that runs just before your Sails app gets lifted.
 * > Need more flexibility?  You can also create a hook.
 *
 * For more information on seeding your app with fake data, check out:
 * https://sailsjs.com/config/bootstrap
 */

module.exports.bootstrap = function (cb) {
  var knex = require('knex')({
    client: 'pg',
    connection: {
      host : process.env['HOST_DB'],
      user : process.env['USER_DB'],
      password : process.env['PASSWORD_DB'],
      database : process.env['DATABASE'],
      max: 20,
      idleTimeoutMillis: 300000,
      connectionTimeoutMillis: 20000,
    }
  });
  knex.instanceId = new Date().getTime();

  sails.config.knex = knex;
  cb();
};

