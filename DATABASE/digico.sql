--
-- PostgreSQL database dump
--

-- Dumped from database version 11.5 (Ubuntu 11.5-3.pgdg18.04+1)
-- Dumped by pg_dump version 11.5 (Ubuntu 11.5-3.pgdg18.04+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: accounts_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.accounts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.accounts_id_seq OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: accounts; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.accounts (
    id bigint DEFAULT nextval('public.accounts_id_seq'::regclass) NOT NULL,
    createby character varying(255),
    createdate timestamp(6) without time zone,
    updateby character varying(255),
    updatedate timestamp(6) without time zone,
    acc_num character varying(255),
    currency character varying(255),
    status character varying(255),
    bank_id integer,
    member_id integer,
    test character varying(255)
);


ALTER TABLE public.accounts OWNER TO postgres;

--
-- Name: advertisement_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.advertisement_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.advertisement_id_seq OWNER TO postgres;

--
-- Name: advertisement; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.advertisement (
    id bigint DEFAULT nextval('public.advertisement_id_seq'::regclass) NOT NULL,
    judul character varying(244),
    description character varying(4000),
    contact character varying(244),
    createdby character varying(200),
    updatedby character varying(200),
    userid integer,
    status character varying(50),
    startdate timestamp(6) without time zone,
    enddate timestamp(6) without time zone,
    gambar character varying(100)
);


ALTER TABLE public.advertisement OWNER TO postgres;

--
-- Name: banks_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.banks_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.banks_id_seq OWNER TO postgres;

--
-- Name: banks; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.banks (
    id bigint DEFAULT nextval('public.banks_id_seq'::regclass) NOT NULL,
    createby character varying(255),
    createdate timestamp(6) without time zone,
    updateby character varying(255),
    updatedate timestamp(6) without time zone,
    admin_fee character varying(255),
    name character varying(255),
    status character varying(255),
    code character varying(20)
);


ALTER TABLE public.banks OWNER TO postgres;

--
-- Name: bl_detail_c_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.bl_detail_c_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.bl_detail_c_id_seq OWNER TO postgres;

--
-- Name: bl_detail_c; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.bl_detail_c (
    id bigint DEFAULT nextval('public.bl_detail_c_id_seq'::regclass) NOT NULL,
    vessel character varying(100),
    call_sign character varying(50),
    voyage character varying(50),
    dtl_no character varying(20),
    container_number character varying(20),
    size_cont character varying(20),
    no_bl character varying(100)
);


ALTER TABLE public.bl_detail_c OWNER TO postgres;

--
-- Name: bl_detail_h_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.bl_detail_h_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.bl_detail_h_id_seq OWNER TO postgres;

--
-- Name: bl_detail_h; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.bl_detail_h (
    id bigint DEFAULT nextval('public.bl_detail_h_id_seq'::regclass) NOT NULL,
    vessel character varying(100),
    call_sign character varying(50),
    voyage character varying(50),
    dtl_no character varying(20),
    no_bl character varying(100) NOT NULL,
    bl_date date
);


ALTER TABLE public.bl_detail_h OWNER TO postgres;

--
-- Name: bl_upload_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.bl_upload_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.bl_upload_id_seq OWNER TO postgres;

--
-- Name: bl_upload; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.bl_upload (
    id bigint DEFAULT nextval('public.bl_upload_id_seq'::regclass) NOT NULL,
    vessel character varying(100),
    call_sign character varying(50),
    voyage character varying(50),
    file_name character varying(100),
    file_path character varying(100),
    user_upload character varying(50),
    date_upload timestamp(6) without time zone
);


ALTER TABLE public.bl_upload OWNER TO postgres;

--
-- Name: channels_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.channels_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.channels_id_seq OWNER TO postgres;

--
-- Name: channels; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.channels (
    id bigint DEFAULT nextval('public.channels_id_seq'::regclass) NOT NULL,
    createby character varying(255),
    createdate timestamp(6) without time zone,
    updateby character varying(255),
    updatedate timestamp(6) without time zone,
    name character varying(255),
    status character varying(255),
    description character varying(255),
    code character varying(20)
);


ALTER TABLE public.channels OWNER TO postgres;

--
-- Name: companies_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.companies_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.companies_id_seq OWNER TO postgres;

--
-- Name: companies; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.companies (
    id bigint DEFAULT nextval('public.companies_id_seq'::regclass) NOT NULL,
    createby character varying(255),
    createdate timestamp(6) without time zone,
    updateby character varying(255),
    updatedate timestamp(6) without time zone,
    address character varying(255),
    corporateid character varying(255),
    email character varying(255),
    name character varying(255),
    phone character varying(255),
    prodcode character varying(255),
    seq_trans_code character varying(255),
    status character varying(255),
    type integer,
    member_id integer,
    epay_p character varying(255),
    epay_u character varying(255),
    npwp character varying(35),
    npwpimg character varying(100),
    siupimg character varying(100),
    ktppemohonimg character varying(100),
    ktppimpinan character varying(100),
    skimg character varying(100),
    intraid character varying(20),
    state character varying(100),
    state_id integer,
    city character varying(100),
    city_id integer,
    country character varying(100),
    flag character varying(20),
    asal_sekolah character varying(50)
);


ALTER TABLE public.companies OWNER TO postgres;

--
-- Name: coreor_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.coreor_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.coreor_id_seq OWNER TO postgres;

--
-- Name: coreor; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.coreor (
    id bigint DEFAULT nextval('public.coreor_id_seq'::regclass) NOT NULL,
    createby character varying(255),
    createdate timestamp(6) without time zone,
    updateby character varying(255),
    updatedate timestamp(6) without time zone,
    file_name character varying(255),
    status character varying(20),
    company_id integer
);


ALTER TABLE public.coreor OWNER TO postgres;

--
-- Name: detail_container_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.detail_container_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.detail_container_id_seq OWNER TO postgres;

--
-- Name: detail_container; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.detail_container (
    id bigint DEFAULT nextval('public.detail_container_id_seq'::regclass) NOT NULL,
    no_container character varying(20),
    delivery_id integer,
    createby character varying(20),
    updateby character varying(20),
    createdate timestamp(6) without time zone,
    updatedate timestamp(6) without time zone
);


ALTER TABLE public.detail_container OWNER TO postgres;

--
-- Name: detail_files_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.detail_files_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.detail_files_id_seq OWNER TO postgres;

--
-- Name: detail_files; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.detail_files (
    id bigint DEFAULT nextval('public.detail_files_id_seq'::regclass) NOT NULL,
    file_name character varying(100),
    delivery_id integer,
    createby character varying(20),
    updateby character varying(20),
    createdate date,
    updatedate date
);


ALTER TABLE public.detail_files OWNER TO postgres;

--
-- Name: gparam_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.gparam_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.gparam_id_seq OWNER TO postgres;

--
-- Name: gparam; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.gparam (
    id_gparam bigint DEFAULT nextval('public.gparam_id_seq'::regclass) NOT NULL,
    rfid1 character varying(11),
    xs1 character varying(255),
    xs2 character varying(255),
    xn1 integer,
    xn2 integer
);


ALTER TABLE public.gparam OWNER TO postgres;

--
-- Name: members_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.members_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.members_id_seq OWNER TO postgres;

--
-- Name: members; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.members (
    id bigint DEFAULT nextval('public.members_id_seq'::regclass) NOT NULL,
    createby character varying(255),
    createdate timestamp(6) without time zone,
    updateby character varying(255),
    updatedate timestamp(6) without time zone,
    npwp character varying(255)
);


ALTER TABLE public.members OWNER TO postgres;

--
-- Name: news_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.news_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.news_id_seq OWNER TO postgres;

--
-- Name: news; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.news (
    id bigint DEFAULT nextval('public.news_id_seq'::regclass) NOT NULL,
    judul character varying(200),
    isi text,
    gambar character varying(200)
);


ALTER TABLE public.news OWNER TO postgres;

--
-- Name: price_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.price_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.price_id_seq OWNER TO postgres;

--
-- Name: price; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.price (
    id bigint DEFAULT nextval('public.price_id_seq'::regclass) NOT NULL,
    createby character varying(255),
    createdate timestamp(6) without time zone,
    updateby character varying(255),
    updatedate timestamp(6) without time zone,
    qmax integer NOT NULL,
    qmin integer NOT NULL,
    price integer NOT NULL,
    scheme integer NOT NULL
);


ALTER TABLE public.price OWNER TO postgres;

--
-- Name: req_sl_delivery_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.req_sl_delivery_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.req_sl_delivery_id_seq OWNER TO postgres;

--
-- Name: req_sl_delivery; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.req_sl_delivery (
    id bigint DEFAULT nextval('public.req_sl_delivery_id_seq'::regclass) NOT NULL,
    createby character varying(255),
    createdate timestamp(6) without time zone,
    updateby character varying(255),
    updatedate timestamp(6) without time zone,
    amount character varying(20),
    blnum character varying(255),
    callsign character varying(255),
    donum character varying(255),
    duedate timestamp(6) without time zone,
    ffid integer,
    ffname character varying(255),
    refid character varying(255),
    slid integer,
    slname character varying(255),
    sppbnum character varying(255),
    vessel character varying(255),
    request_id numeric,
    reqext integer,
    filedo character varying(255),
    fileinv character varying(255),
    fileproformainv character varying(255),
    voyage character varying(255),
    transfee character varying(255),
    qtykontainer integer,
    amount2 character varying(20) DEFAULT 0,
    total character varying(20) DEFAULT 0,
    status_ws character varying(1) DEFAULT 'N'::character varying,
    pkk character varying(255)
);


ALTER TABLE public.req_sl_delivery OWNER TO postgres;

--
-- Name: requests_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.requests_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.requests_id_seq OWNER TO postgres;

--
-- Name: requests; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.requests (
    id bigint DEFAULT nextval('public.requests_id_seq'::regclass) NOT NULL,
    createby character varying(255),
    createdate timestamp(6) without time zone,
    updateby character varying(255),
    updatedate timestamp(6) without time zone,
    reqnum character varying(255),
    status character varying(255),
    total character varying(255),
    trxdate timestamp(6) without time zone,
    trxnum character varying(255),
    company_id integer,
    tag integer,
    email_paid integer,
    notes character varying(255),
    bank_id integer,
    channel_id integer,
    approvalcode character varying(255),
    merchantid character varying(255),
    terminalid character varying(255),
    batchnum character varying(255),
    tracenum character varying(255),
    refnum character varying(255),
    payment_code character varying(50),
    note_request character varying(3000),
    paid_status character varying(30)
);


ALTER TABLE public.requests OWNER TO postgres;

--
-- Name: roles_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.roles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.roles_id_seq OWNER TO postgres;

--
-- Name: roles; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.roles (
    id bigint DEFAULT nextval('public.roles_id_seq'::regclass) NOT NULL,
    createby character varying(255),
    createdate timestamp(6) without time zone,
    updateby character varying(255),
    updatedate timestamp(6) without time zone,
    rolename character varying(255)
);


ALTER TABLE public.roles OWNER TO postgres;

--
-- Name: trans_bank_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.trans_bank_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.trans_bank_id_seq OWNER TO postgres;

--
-- Name: trans_bank; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.trans_bank (
    id bigint DEFAULT nextval('public.trans_bank_id_seq'::regclass) NOT NULL,
    created_date timestamp(6) without time zone,
    no_card character varying(255),
    total_amount numeric,
    trans_id character varying(255),
    input1 character varying(255),
    input2 character varying(255),
    input3 character varying(255),
    t_response character varying(255),
    trans_date character varying(255),
    response_code character varying(255),
    receipt_code character varying(255),
    resonse_desc character varying(255),
    type character varying(255),
    bank_id integer,
    response_desc character varying(255)
);


ALTER TABLE public.trans_bank OWNER TO postgres;

--
-- Name: trans_detail_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.trans_detail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.trans_detail_id_seq OWNER TO postgres;

--
-- Name: trans_detail; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.trans_detail (
    id bigint DEFAULT nextval('public.trans_detail_id_seq'::regclass) NOT NULL,
    trans_date character varying(255),
    created_date timestamp(6) without time zone,
    update_date timestamp(6) without time zone,
    proforma_num character varying(255),
    invoice_num character varying(255),
    amount numeric,
    layanan character varying(255),
    status character varying(255),
    comp_id integer,
    comp_name character varying(255),
    sl_id integer,
    sl_name character varying(255),
    transbank_id integer
);


ALTER TABLE public.trans_detail OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO postgres;

--
-- Name: users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.users (
    id bigint DEFAULT nextval('public.users_id_seq'::regclass) NOT NULL,
    createby character varying(255),
    createdate timestamp(6) without time zone,
    updateby character varying(255),
    updatedate timestamp(6) without time zone,
    address character varying(255),
    admin integer DEFAULT 0,
    email character varying(255),
    firstname character varying(255),
    lastname character varying(255),
    mobile character varying(255),
    password character varying(255),
    username character varying(255),
    company_id integer,
    member_id integer,
    is_active integer,
    portal_id integer,
    user_type character varying(20),
    active_date character varying(35),
    is_login character varying(1),
    session_id character varying(255),
    activity character varying(100),
    departement character varying(20),
    reg_date character varying(35),
    role_id character varying(1),
    reason_reject character varying(20),
    status character varying(1)
);


ALTER TABLE public.users OWNER TO postgres;

--
-- Name: users_register_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.users_register_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_register_id_seq OWNER TO postgres;

--
-- Name: users_register; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.users_register (
    id_users_reg bigint DEFAULT nextval('public.users_register_id_seq'::regclass) NOT NULL,
    createby character varying(255),
    createdate timestamp(6) without time zone,
    updateby character varying(255),
    updatedate timestamp(6) without time zone,
    address character varying(255),
    admin integer,
    email character varying(255),
    firstname character varying(255),
    lastname character varying(255),
    mobile character varying(255),
    password character varying(255),
    username character varying(255),
    company_id integer,
    member_id integer,
    is_active character varying(1) DEFAULT 1,
    gender character varying(1),
    status character varying(1) DEFAULT 1,
    role_id character varying(1),
    user_type character varying(20),
    active_date timestamp(6) without time zone,
    id_group character varying(1),
    departement character varying(20),
    is_login character varying(1),
    session_id character varying(255),
    menu character varying(100),
    ak0 integer,
    ak1 integer,
    ak2 integer,
    ak3 integer,
    ak4 integer,
    ak5 integer,
    ak6 integer,
    ak7 integer,
    ak8 integer,
    ak9 integer,
    ak10 integer,
    ak11 integer,
    birthdate date
);


ALTER TABLE public.users_register OWNER TO postgres;

--
-- Name: users_shippingline_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.users_shippingline_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_shippingline_id_seq OWNER TO postgres;

--
-- Name: users_shippingline; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.users_shippingline (
    sl_id bigint DEFAULT nextval('public.users_shippingline_id_seq'::regclass) NOT NULL,
    shippingline numeric NOT NULL,
    userid integer NOT NULL,
    createby character varying(255),
    createdate timestamp(6) without time zone,
    updateby character varying(255),
    updatedate timestamp(6) without time zone,
    username character varying(255)
);


ALTER TABLE public.users_shippingline OWNER TO postgres;

--
-- Name: vessel_lines_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.vessel_lines_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.vessel_lines_id_seq OWNER TO postgres;

--
-- Name: vessel_lines; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.vessel_lines (
    id bigint DEFAULT nextval('public.vessel_lines_id_seq'::regclass) NOT NULL,
    createby character varying(255),
    createdate timestamp(6) without time zone,
    updateby character varying(255),
    updatedate timestamp(6) without time zone,
    callsign character varying(255),
    vesselname character varying(255),
    voyage character varying(255),
    company_id integer
);


ALTER TABLE public.vessel_lines OWNER TO postgres;

--
-- Data for Name: accounts; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.accounts (id, createby, createdate, updateby, updatedate, acc_num, currency, status, bank_id, member_id, test) FROM stdin;
\.


--
-- Data for Name: advertisement; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.advertisement (id, judul, description, contact, createdby, updatedby, userid, status, startdate, enddate, gambar) FROM stdin;
\.


--
-- Data for Name: banks; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.banks (id, createby, createdate, updateby, updatedate, admin_fee, name, status, code) FROM stdin;
1	system	2016-06-07 00:00:00		2016-06-08 00:00:00	0	MANDIRI	1	8
2	system	2016-06-07 00:00:00		2016-08-02 00:00:00	0	NIAGA	1	22
3	system	2016-06-07 00:00:00		2016-08-31 00:00:00	0	BNI	1	9
4	system	2016-06-07 00:00:00		2016-07-19 00:00:00	0	BCA	1	14
5	system	2016-08-02 00:00:00		2016-08-03 00:00:00	0	BRI	1	2
6	system	2017-02-16 00:00:00		2017-02-16 00:00:00	0	FINNET	1	123
\.


--
-- Data for Name: bl_detail_c; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.bl_detail_c (id, vessel, call_sign, voyage, dtl_no, container_number, size_cont, no_bl) FROM stdin;
1	SNORDWOGE	C4FF2	CY0758-029B	DTL0101I    0000	TCNU3746848	40F	EGLV237800059788
2	SURU BHUM	HSGF2	TH0761-029A	DTL0101I    0000	SEGU4021438	40F	EGLV020700516331
3	SURU BHUM	HSGF2	TH0761-029A	DTL0101I    0000	SEGU4021439	40F	EGLV020700516331
4	SURU BHUM	HSGF2	TH0761-029A	DTL0101I    0000	SEGU4021440	40F	EGLV020700516331
5	SURU BHUM	HSGF2	TH0761-029A	DTL0101I    0000	SEGU4021441	40F	EGLV020700516331
6	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00010000-	EGSU9160074	40F	EGLV001600409293
7	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00010000-	EITU1187311	40F	EGLV001600409293
8	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00010000-	EITU1629774	40F	EGLV001600409293
9	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00020000-	EMCU5803698	20F	EGLV001600414947
10	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00030000-	DRYU2375307	20F	EGLV001600421463
11	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00040000-	DRYU2593443	20F	EGLV001600425825
12	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00040000-	DRYU2719254	20F	EGLV001600425825
13	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00040000-	TEMU3692633	20F	EGLV001600425825
14	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00040000-	TGHU3415414	20F	EGLV001600425825
15	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00050000-	EITU0356377	20F	EGLV001600427071
16	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00060000-	DRYU9552750	40F	EGLV001600427500
17	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00070000-	TCLU3373876	20F	EGLV001600428883
18	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00080000-	TCLU3637773	20F	EGLV001600430322
19	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00080000-	XINU1479434	20F	EGLV001600430322
20	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00090000-	EMCU3532051	20F	EGLV001600432023
21	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00100000-	CLHU3746649	20F	EGLV001600432058
22	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00100000-	TEMU3692295	20F	EGLV001600432058
23	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00100000-	TEMU4272830	20F	EGLV001600432058
24	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00110000-	EITU0049658	20F	EGLV001600433127
25	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00110000-	EITU0295883	20F	EGLV001600433127
26	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00110000-	EMCU6044512	20F	EGLV001600433127
27	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00110000-	TEMU3956905	20F	EGLV001600433127
28	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00120000-	EMCU3796579	20F	EGLV001600434832
29	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00130000-	DRYU2336717	20F	EGLV001600434875
30	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00140000-	DRYU2974835	20F	EGLV001600436614
31	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00150000-	GATU1264080	20F	EGLV002600465663
32	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00160000-	EMCU6087411	20F	EGLV002600472554
33	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00170000-	EISU9265211	40F	EGLV003602091599
34	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00170000-	IMTU9086779	40F	EGLV003602091599
35	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00180000-	SEGU5512056	40F	EGLV003602134981
36	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00190000-	DRYU2698233	20F	EGLV003602183833
37	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00190000-	FSCU3334850	20F	EGLV003602183833
38	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00200000-	DRYU2575516	20F	EGLV003602197885
39	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00210000-	TEMU3923305	20F	EGLV003602223720
40	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00220000-	EISU1580238	40F	EGLV003602243526
41	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00230000-	EISU3798297	20F	EGLV003602243542
42	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00240000-	TGHU3645799	20F	EGLV003602259619
43	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00250000-	TCLU3036685	20F	EGLV003602308709
44	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00260000-	EMCU3967943	20F	EGLV003602323694
45	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00270000-	BMOU2096847	20F	EGLV003602350187
46	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00280000-	TEMU4521229	20F	EGLV003602350659
47	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00290000-	EITU0052667	20F	EGLV003602353976
48	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00300000-	TGHU0707027	20F	EGLV003602357238
49	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00310000-	TCLU8612166	40F	EGLV010600830401
50	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00320000-	EMCU3569976	20F	EGLV010600849187
51	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00330000-	MAGU2409008	20F	EGLV010600872073
52	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00340000-	MAGU5403750	40F	EGLV010600874157
53	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00350000-	EMCU9447039	40F	EGLV010600885027
54	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00360000-	EITU0285381	20F	EGLV010600885043
55	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00370000-	EITU0495217	20F	EGLV010600885051
56	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00380000-	GLDU9752566	20F	EGLV010600901090
57	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00390000-	MAGU2320836	20F	EGLV010600902291
58	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00400000-	EITU0513497	20F	EGLV010600912270
59	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00410000-	TCLU7922824	40F	EGLV010600913896
60	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00420000-	EMCU1443567	40F	EGLV010600918791
61	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00430000-	EITU0115024	20F	EGLV010600921520
62	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00440000-	EMCU1461658	40F	EGLV010600929474
63	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00440000-	GLDU9712959	20F	EGLV010600929474
64	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00450000-	MAGU2373353	20F	EGLV010600936560
65	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00460000-	EMCU3973680	20F	EGLV020600256337
66	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00470000-	EISU2146782	20F	EGLV020600263929
67	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00470000-	EMCU3893820	20F	EGLV020600263929
68	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00480000-	GLDU9745021	20F	EGLV020600265581
69	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00490000-	EMCU5382710	40F	EGLV020600268882
70	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00500000-	EITU0392693	20F	EGLV020600279558
71	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00500000-	EITU1398696	40F	EGLV020600279558
72	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00510000-	IMTU3052072	20F	EGLV020600281811
73	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00520000-	EITU1540645	40F	EGLV020600282974
74	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00530000-	EITU0035485	20F	EGLV020600287267
75	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00540000-	EISU3966106	20F	EGLV020600289324
76	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00550000-	EISU2186229	20F	EGLV022600125603
77	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00560000-	DRYU2471759	20F	EGLV024600045690
78	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00560000-	TEMU4269436	20F	EGLV024600045690
79	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00570000-	EITU0000302	20F	EGLV024600046076
80	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00570000-	EMCU3796434	20F	EGLV024600046076
81	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00570000-	XINU1106364	20F	EGLV024600046076
82	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00580000-	EISU1877870	40F	EGLV024600046912
83	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00590000-	DRYU2725745	20F	EGLV024600050821
84	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00590000-	EISU2183744	20F	EGLV024600050821
85	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00600000-	BMOU2091763	20F	EGLV040600223409
86	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00610000-	CARU9999677	40F	EGLV062600024261
87	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00610000-	OCGU8040064	40F	EGLV062600024261
88	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00620000-	EITU1604518	40F	EGLV062600024270
\.


--
-- Data for Name: bl_detail_h; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.bl_detail_h (id, vessel, call_sign, voyage, dtl_no, no_bl, bl_date) FROM stdin;
1	SNORDWOGE	C4FF2	CY0758-029B	DTL0101I    0000	EGLV237800059788	2018-01-25
2	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00020000-	EGLV001600414947	2016-09-17
3	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00030000-	EGLV001600421463	2016-09-17
4	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00040000-	EGLV001600425825	2016-09-17
5	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00050000-	EGLV001600427071	2016-09-17
6	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00060000-	EGLV001600427500	2016-09-17
7	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00070000-	EGLV001600428883	2016-09-17
8	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00080000-	EGLV001600430322	2016-09-17
9	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00090000-	EGLV001600432023	2016-09-17
10	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00100000-	EGLV001600432058	2016-09-17
11	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00110000-	EGLV001600433127	2016-09-17
12	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00120000-	EGLV001600434832	2016-09-17
13	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00130000-	EGLV001600434875	2016-09-17
14	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00140000-	EGLV001600436614	2016-09-17
15	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00150000-	EGLV002600465663	2016-09-17
16	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00160000-	EGLV002600472554	2016-09-17
17	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00170000-	EGLV003602091599	2016-09-17
18	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00180000-	EGLV003602134981	2016-09-17
19	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00190000-	EGLV003602183833	2016-09-17
20	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00200000-	EGLV003602197885	2016-09-17
21	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00210000-	EGLV003602223720	2016-09-17
22	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00220000-	EGLV003602243526	2016-09-17
23	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00230000-	EGLV003602243542	2016-09-17
24	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00240000-	EGLV003602259619	2016-09-17
25	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00250000-	EGLV003602308709	2016-09-17
26	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00260000-	EGLV003602323694	2016-09-19
27	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00270000-	EGLV003602350187	2016-09-17
28	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00280000-	EGLV003602350659	2016-09-17
29	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00290000-	EGLV003602353976	2016-09-17
30	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00300000-	EGLV003602357238	2016-09-17
31	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00310000-	EGLV010600830401	2016-09-18
32	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00320000-	EGLV010600849187	2016-09-19
33	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00330000-	EGLV010600872073	2016-09-19
34	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00340000-	EGLV010600874157	2016-09-18
35	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00350000-	EGLV010600885027	2016-09-19
36	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00360000-	EGLV010600885043	2016-09-19
37	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00370000-	EGLV010600885051	2016-09-19
38	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00380000-	EGLV010600901090	2016-09-19
39	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00390000-	EGLV010600902291	2016-09-19
40	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00400000-	EGLV010600912270	2016-09-19
41	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00410000-	EGLV010600913896	2016-09-19
42	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00420000-	EGLV010600918791	2016-09-19
43	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00430000-	EGLV010600921520	2016-09-19
44	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00440000-	EGLV010600929474	2016-09-19
45	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00450000-	EGLV010600936560	2016-09-19
46	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00460000-	EGLV020600256337	2016-09-09
47	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00470000-	EGLV020600263929	2016-09-09
48	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00480000-	EGLV020600265581	2016-09-08
49	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00490000-	EGLV020600268882	2016-09-08
50	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00500000-	EGLV020600279558	2016-09-10
51	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00510000-	EGLV020600281811	2016-09-08
52	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00520000-	EGLV020600282974	2016-09-09
53	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00530000-	EGLV020600287267	2016-09-08
54	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00540000-	EGLV020600289324	2016-09-10
55	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00550000-	EGLV022600125603	2016-09-08
56	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00560000-	EGLV024600045690	2016-09-09
57	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00570000-	EGLV024600046076	2016-09-09
58	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00580000-	EGLV024600046912	2016-09-09
59	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00590000-	EGLV024600050821	2016-09-09
60	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00600000-	EGLV040600223409	2016-09-07
61	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00610000-	EGLV062600024261	2016-09-09
62	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00620000-	EGLV062600024270	2016-09-09
63	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00630000-	EGLV140600850434	2016-09-11
64	SURU BHUM	HSGF2	TH0761-029A	DTL0101I    0000	EGLV020700516331	2018-01-25
65	SEM ANDROS	D5AQ4	LR0687-002W	DTL0101I00010000-	EGLV001600409293	2016-09-17
\.


--
-- Data for Name: bl_upload; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.bl_upload (id, vessel, call_sign, voyage, file_name, file_path, user_upload, date_upload) FROM stdin;
1	SNORDWOGE	C4FF2	CY0758-029B	EGLV237800059788.TRIAL.txt	/opt/apache-tomcat/apache-tomcat-7.0.47/webapps/dopayment/WEB-INF/application/public/uploads/demo_sl	demo_sl	2018-11-28 00:00:00
2	SACX DIAMOND	9VFL3	SG1001W	sample.txt	/opt/apache-tomcat/apache-tomcat-7.0.47/webapps/dopayment/WEB-INF/application/public/uploads/demo_sl	demo_sl	2018-11-28 00:00:00
3	SURU BHUM	HSGF2	TH0761-029A	EGLV020700516331.TRIAL.txt	C:\\play\\iCargo_DO@/public/uploads/shippingline	shippingline	2018-02-15 00:00:00
4	SEM ANDROS	D5AQ4	LR0687-002W	EMAD 0687-002W FLATFILE_1.txt	C:\\play\\iCargo_DO@/public/uploads/shippingline	shippingline	2018-02-15 00:00:00
\.


--
-- Data for Name: channels; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.channels (id, createby, createdate, updateby, updatedate, name, status, description, code) FROM stdin;
1	system	2016-06-08 00:00:00		2016-06-08 00:00:00	EDC	1	Menggunakan EDC sebagai alat bantu	2
2	system	2016-06-28 00:00:00		2016-06-28 00:00:00	AUTOCOLLECTION	1	AUTOCOLLECTION	10
3	system	2016-06-28 00:00:00		2016-08-03 00:00:00	CLICKPAY	1	Click Pay	4
4	system	2016-08-02 00:00:00		2016-08-31 00:00:00	H2H	1	Anjungan Tunai Mandiri	1
5	system	2016-08-31 00:00:00		2016-08-31 00:00:00	Cash	1	Bayar Langsung	99
\.


--
-- Data for Name: companies; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.companies (id, createby, createdate, updateby, updatedate, address, corporateid, email, name, phone, prodcode, seq_trans_code, status, type, member_id, epay_p, epay_u, npwp, npwpimg, siupimg, ktppemohonimg, ktppimpinan, skimg, intraid, state, state_id, city, city_id, country, flag, asal_sekolah) FROM stdin;
1	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	Jl. Asem Baris Raya Komplek Selmil block B blok G2		andimatrans@centrin.net.id andimimp@centrin.net.id	PT. Ayutrans Utama	81297968501			ACTIVE	9	1101			01.909.141.2-015.000								\N		\N			
884	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC								ACTIVE	9	1102											\N		\N			
885	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	Plaza Timur Lt. 5, Jl. Timor no. 2 Menteng, Gondangdia, Jakarta Pusat 10350		endang@saranapenida.co.id	PT. Sarana Penida	021-3905564/66			ACTIVE	9	1103			01.307.577.5-076.000								\N		\N			
886	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC								ACTIVE	9	1104											\N		\N			
887	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC								ACTIVE	9	1105											\N		\N			
888	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	Jl. Raya Semper Kebantenan no. 26A Semper Timur, Cilincing		esabinaindoyasa@yahoo.com	PT. Esabina Indoyasa	021-4406120			ACTIVE	9	1106			01.333.289.5.045.000								\N		\N			
889	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	Jl. Raya Semper Kebantenan no. 26A Semper Timur, Cilincing		esabinaindoyasa@yahoo.com	PT. Esabina Indoyasa	021-4406120			ACTIVE	9	1107			01.333.289.5.045.000								\N		\N			
890	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	LAvenue Building Fl.26th			PT Kintetsu World Express Ind	021-80667121/ 0811861417			ACTIVE	9	1108			02.194.050.7.058.000								\N		\N			
891	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	Jl. S Parman kav. 35 Jakarta		puspito.winarko@samudera.id	PT. Samudera Indonesia Logistik Kargo	021-5300585/081514247000			ACTIVE	9	1109			03.271.810.8.031.000								\N		\N			
892	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	Jl. S Parman kav. 35 Jakarta		puspito.winarko@samudera.id	PT. Samudera Indonesia Logistik Kargo	021-5300585/081514247000			ACTIVE	9	1110			03.271.810.8.031.000								\N		\N			
893	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	Jl. S Parman kav. 35 Jakarta		puspito.winarko@samudera.id	PT. Samudera Indonesia Logistik Kargo	021-5300585/081514247000			ACTIVE	9	1111			03.271.810.8.031.000								\N		\N			
894	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	Jl. S Parman kav. 35 Jakarta		puspito.winarko@samudera.id	PT. Samudera Indonesia Logistik Kargo	021-5300585/081514247000			ACTIVE	9	1112			03.271.810.8.031.000								\N		\N			
895	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	Jl. S Parman kav. 35 Jakarta		puspito.winarko@samudera.id	PT. Samudera Indonesia Logistik Kargo	021-5300585/081514247000			ACTIVE	9	1113			03.271.810.8.031.000								\N		\N			
896	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	Jl. S Parman kav. 35 Jakarta		puspito.winarko@samudera.id	PT. Samudera Indonesia Logistik Kargo	021-5300585/081514247000			ACTIVE	9	1114			03.271.810.8.031.000								\N		\N			
897	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	Jl. S Parman kav. 35 Jakarta		puspito.winarko@samudera.id	PT. Samudera Indonesia Logistik Kargo	021-5300585/081514247000			ACTIVE	9	1115			03.271.810.8.031.000								\N		\N			
898	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	Jl. S Parman kav. 35 Jakarta		puspito.winarko@samudera.id	PT. Samudera Indonesia Logistik Kargo	021-5300585/081514247000			ACTIVE	9	1116			03.271.810.8.031.000								\N		\N			
899	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	Jl. S Parman kav. 35 Jakarta		puspito.winarko@samudera.id	PT. Samudera Indonesia Logistik Kargo	021-5300585/081514247000			ACTIVE	9	1117			03.271.810.8.031.000								\N		\N			
900	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	Jl. S Parman kav. 35 Jakarta		puspito.winarko@samudera.id	PT. Samudera Indonesia Logistik Kargo	021-5300585/081514247000			ACTIVE	9	1118			03.271.810.8.031.000								\N		\N			
901	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	Jl. S Parman kav. 35 Jakarta		puspito.winarko@samudera.id	PT. Samudera Indonesia Logistik Kargo	021-5300585/081514247000			ACTIVE	9	1119			03.271.810.8.031.000								\N		\N			
902	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	Jl. S Parman kav. 35 Jakarta		puspito.winarko@samudera.id	PT. Samudera Indonesia Logistik Kargo	021-5300585/081514247000			ACTIVE	9	1120			03.271.810.8.031.000								\N		\N			
903	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	Jl. S Parman kav. 35 Jakarta		puspito.winarko@samudera.id	PT. Samudera Indonesia Logistik Kargo	021-5300585/081514247000			ACTIVE	9	1121			03.271.810.8.031.000								\N		\N			
904	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	Jl. S Parman kav. 35 Jakarta		puspito.winarko@samudera.id	PT. Samudera Indonesia Logistik Kargo	021-5300585/081514247000			ACTIVE	9	1122			03.271.810.8.031.000								\N		\N			
905	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	Jl. S Parman kav. 35 Jakarta		puspito.winarko@samudera.id	PT. Samudera Indonesia Logistik Kargo	021-5300585/081514247000			ACTIVE	9	1123			03.271.810.8.031.000								\N		\N			
906	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	Jl. S Parman kav. 35 Jakarta		puspito.winarko@samudera.id	PT. Samudera Indonesia Logistik Kargo	021-5300585/081514247000			ACTIVE	9	1124			03.271.810.8.031.000								\N		\N			
907	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	Jl. S Parman kav. 35 Jakarta		puspito.winarko@samudera.id	PT. Samudera Indonesia Logistik Kargo	021-5300585/081514247000			ACTIVE	9	1125			03.271.810.8.031.000								\N		\N			
908	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	Jl. S Parman kav. 35 Jakarta		puspito.winarko@samudera.id	PT. Samudera Indonesia Logistik Kargo	021-5300585/081514247000			ACTIVE	9	1126			03.271.810.8.031.000								\N		\N			
909	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	Jl. S Parman kav. 35 Jakarta		puspito.winarko@samudera.id	PT. Samudera Indonesia Logistik Kargo	021-5300585/081514247000			ACTIVE	9	1127			03.271.810.8.031.000								\N		\N			
910	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	Jl. S Parman kav. 35 Jakarta		puspito.winarko@samudera.id	PT. Samudera Indonesia Logistik Kargo	021-5300585/081514247000			ACTIVE	9	1128			03.271.810.8.031.000								\N		\N			
911	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	Jl. S Parman kav. 35 Jakarta		puspito.winarko@samudera.id	PT. Samudera Indonesia Logistik Kargo	021-5300585/081514247000			ACTIVE	9	1129			03.271.810.8.031.000								\N		\N			
912	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	Jl. S Parman kav. 35 Jakarta		puspito.winarko@samudera.id	PT. Samudera Indonesia Logistik Kargo	021-5300585/081514247000			ACTIVE	9	1130			03.271.810.8.031.000								\N		\N			
913	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	Jl. S Parman kav. 35 Jakarta		puspito.winarko@samudera.id	PT. Samudera Indonesia Logistik Kargo	021-5300585/081514247000			ACTIVE	9	1131			03.271.810.8.031.000								\N		\N			
914	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	Jl. S Parman kav. 35 Jakarta		puspito.winarko@samudera.id	PT. Samudera Indonesia Logistik Kargo	021-5300585/081514247000			ACTIVE	9	1132			03.271.810.8.031.000								\N		\N			
915	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	Jl. S Parman kav. 35 Jakarta		puspito.winarko@samudera.id	PT. Samudera Indonesia Logistik Kargo	021-5300585/081514247000			ACTIVE	9	1133			03.271.810.8.031.000								\N		\N			
916	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	Jl. S Parman kav. 35 Jakarta		puspito.winarko@samudera.id	PT. Samudera Indonesia Logistik Kargo	021-5300585/081514247000			ACTIVE	9	1134			03.271.810.8.031.000								\N		\N			
917	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	Jl. S Parman kav. 35 Jakarta		puspito.winarko@samudera.id	PT. Samudera Indonesia Logistik Kargo	021-5300585/081514247000			ACTIVE	9	1135			03.271.810.8.031.000								\N		\N			
918	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	Jl. S Parman kav. 35 Jakarta		puspito.winarko@samudera.id	PT. Samudera Indonesia Logistik Kargo	021-5300585/081514247000			ACTIVE	9	1136			03.271.810.8.031.000								\N		\N			
919	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	Jl. S Parman kav. 35 Jakarta		puspito.winarko@samudera.id	PT. Samudera Indonesia Logistik Kargo	021-5300585/081514247000			ACTIVE	9	1137			03.271.810.8.031.000								\N		\N			
920	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	Jl. S Parman kav. 35 Jakarta		puspito.winarko@samudera.id	PT. Samudera Indonesia Logistik Kargo	021-5300585/081514247000			ACTIVE	9	1138			03.271.810.8.031.000								\N		\N			
921	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	Jl. S Parman kav. 35 Jakarta		puspito.winarko@samudera.id	PT. Samudera Indonesia Logistik Kargo	021-5300585/081514247000			ACTIVE	9	1139			03.271.810.8.031.000								\N		\N			
922	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	Jl. S Parman kav. 35 Jakarta		puspito.winarko@samudera.id	PT. Samudera Indonesia Logistik Kargo	021-5300585/081514247000			ACTIVE	9	1140			03.271.810.8.031.000								\N		\N			
923	system	2017-04-01 00:00:00	demo_ilcs	2019-01-23 00:00:00	swasembada timur no 16 jakarta utara		dkurniawan1415@gmail.com	PT. ILCS			2301190003	ACTIVE	2	875			54.561.161.8-418.000	20180202100211_screenshot-sms.indonesiaferry.id-2017-05-18-14-19-40.png	20180202100211_screenshot-localhost-2017-05-16-11-32-07.png	20180202100211_screenshot-sms.indonesiaferry.id-2017-05-18-14-19-40.png	20180202100211_screenshot-sms.indonesiaferry.id-2017-05-18-14-19-40.png	20180202100211_screenshot-sms.indonesiaferry.id-2017-05-18-14-19-40.png			\N		\N			
924	system	2017-04-01 00:00:00		0001-01-01 00:00:00 BC	Jl. Agung Karya 7 No. 1		import.cs@mail.dunextr.com	PT. Dunia Express	216511137			ACTIVE	9	876			02.238.557.9-046.000	npwp.jpg							\N		\N			
925	system	2017-04-03 00:00:00	demo_ilcs	2018-09-17 00:00:00	Permata Kuningan Building 21st and 22nd Floor. Jl Kuningan Mulia Kav 9C, Guntur Setiabudi, Jakarta Selatan 12980		dja.apulungan@cma-cgm.com; dja.import@cma-cgm.com	PT. CONTAINER MARITIME ACTIVITIES	28546800		1709180001	ACTIVE	2	877	ptp	cmacgm	02.193.924.4-058.000								\N		\N			
926	system	2017-04-04 00:00:00		0001-01-01 00:00:00 BC	Jl. Raya Cakung Cilincing Pal II - Blok A1  Kelurahan Sukapura  Kecamatan Cilincing  Jakarta Utara 14140 		agus.suryadi@ckb.co.id	PT. Cipta Krida Bahari	29976777			ACTIVE	9	878			01.797.289.4-062.000	NPWP_SKT_SPPKP.PDF							\N		\N			
927	system	2017-04-04 00:00:00		0001-01-01 00:00:00 BC			putridiajeng987@gmail.com	PT.ILCS				ACTIVE	1	879											\N		\N			
928	system	2017-04-06 00:00:00		0001-01-01 00:00:00 BC	JLN GAYA MOTOR III NO.5 SUNTER II		dede.ipan@daihatsu.astra.co.id	PT. ASTRA DAIHATSU MOTOR	6510300			ACTIVE	9	880			01.000.571.8-092.000	NPWP.jpg							\N		\N			
929	system	2017-04-06 00:00:00		0001-01-01 00:00:00 BC	¿BDP International, Indonesia Graha Kirana Suite 907 Jl. Yos Sudarso No. 88 Jakarta 14350¿¿		¿donny.metiary@bdpint.com	PT. BDP International	¿6531 330			ACTIVE	8	881			02.059.040.2-058.000	NPWP_BDP_Jkt_(1)_(1).pdf							\N		\N			
930	system	2017-04-06 00:00:00		0001-01-01 00:00:00 BC	sadasd		t.nugraha@gmail.com	DSDD	12312313			ACTIVE	1	882			19.231.231.2-312.312	NPWP.jpg							\N		\N			
931	system	2017-04-07 00:00:00		0001-01-01 00:00:00 BC	Jl.Bouraq No.35 Karang Anyar Neglasari		alfi@hi-lex.co.id	PT.HI-LEX INDONESIA	5522331			ACTIVE	1	883			01.000.271.5-055.000	NPWP.pdf							\N		\N			
989	system	2018-01-26 00:00:00		0001-01-01 00:00:00 BC	Test		fata.mailbox@gmail.com	PT BARU COY				NOT ACTIVE	8	969			12.121.212.1-212.121	20180126022600_68-creative-cv-resumes.jpg							\N		\N			
932	system	2017-04-10 00:00:00		0001-01-01 00:00:00 BC	Noble House 17th Floor Jl. Dr Ide Anak Agung Gde Agung kav E 4.2 no 2 Jakarta 12950 		galih.nurcahyo@kuehne-nagel.com	PT. NAKU FREIGHT INDONESIA	29783108			ACTIVE	9	884			01.936.249.0-058.000	NPWP-SKT-SPPKP_of_PT_Naku_Freight_Indonesia_-_Colour.pdf							\N		\N			
933	system	2017-04-11 00:00:00		0001-01-01 00:00:00 BC	Jl.Tanah Merdeka NO.20 A RT.12 RW.12 Kel.Kali Baru- Kec.Cilincing Jakarta Utara 14110 T: +62-21-4405464 (Hunting)   		krista@pancaran-group.com	PT. Pancaran Logistik Indonesia	4405464			ACTIVE	9	885			31.567.982.9-045.000	NPWP_PLI3.pdf							\N		\N			
934	system	2017-04-11 00:00:00		0001-01-01 00:00:00 BC	PT. INDOKEMIKA JAYATAMA JL. CAKUNG CILINCING KP. BARU RT.007/008, KEL. CAKUNG BARAT, KEC. CAKUNG, JAKARTA TIMUR 13910, INDONESIA 		sriwati@indokemika.co.id	PT. Indokemika Jayatama	215222172			ACTIVE	9	886			01.328.570.5-007.000	NPWP_-_IKJ_warna_(CAKUNG_CILINCING).pdf							\N		\N			
935	system	2017-04-18 00:00:00		0001-01-01 00:00:00 BC	Gedung Telkom Landmark Tower Menara 1 Lt. 1-20 Jl. Jend. Gatot Subroto Kav. 52 Kuningan Bart Mampang Prapatan Jakarta 12710		agus_hendarto@telkomsel.co.id	PT. Telekomunikasi Selular	5240811			ACTIVE	1	897			01.718.327.8-093.000	Kartu_NPWP_Telkomsel_-_2016-1.pdf							\N		\N			
936	system	2017-04-11 00:00:00		0001-01-01 00:00:00 BC	a		putridiajeng987@gmail.com	PT ISH	1500950			ACTIVE	0	888			01.200.023.0-003.400	cure.jpg							\N		\N			
937	system	2017-04-11 00:00:00		0001-01-01 00:00:00 BC	GEDUNG WISMA MULIA JL. JEND.GATOT SUBROTO SUITE 4301 NO.42 KUNINGAN BARAT, JAKARTA SELATAN		icargo_ptsgn@yahoo.com	PT. SCHLUMBERGER GEOPHYSICS NUSANTARA	29819000			ACTIVE	8	889			01.061.617.5-081.000	NPWP_SGN.pdf							\N		\N			
938	system	2017-04-11 00:00:00		0001-01-01 00:00:00 BC	GEDUNG WISMA MULIA JL. JEND.GATOT SUBROTO LANTAIN 42 SUITE 4201 KUNINGAN BARAT, JAKARTA SELATAN		icargo_ptdas@yahoo.com	PT. DOWELL ANADRILL SCHLUMBERGER	29819000			ACTIVE	\N	890			01.061.608.4-081.000	NPWP_+SKT+SPKP_(PT_DAS).pdf							\N		\N			
939	system	2017-04-11 00:00:00		0001-01-01 00:00:00 BC	KAWASAN INDUSTRI TERPADU INDONESIA CHINA (KITIC) KAV. 17 KOTA DELTA MAS NO. 17 KEL. NAGASARI KEC.SERANG BARU KAB. BEKASI		icargo_ptgeo@yahoo.com	PT. GEOPROLOG INTIWIJAYA	29819000			ACTIVE	8	891			01.308.550.1-081.000	NPWP_PT__GEOPROLOG_INTIWIJAYA__.pdf							\N		\N			
940	system	2017-04-11 00:00:00		0001-01-01 00:00:00 BC	JL. AMPERA RAYA NO. 9-10 RAGUNAN PASAR MINGGU - JAKARTA SELATAN		icargo_ptsmith@yahoo.com	PT.SMITH TOIL INDONESIA	29819000			ACTIVE	1	892			01.071.736.1-055.000	NPWP_SMITH_JAKARTA.pdf							\N		\N			
941	system	2017-04-17 00:00:00		0001-01-01 00:00:00 BC	JL. SEULAWAH RAYA PURI SENTRA NIAGA BLOK B-37 JATIWARINGIN		widariyanto@cerindo.co.id	PT. CERINDO TRANSPORT LOGISTIK	218629000			ACTIVE	9	893											\N		\N			
942	system	2017-04-17 00:00:00		0001-01-01 00:00:00 BC	Kawasan Industri MM2100 Blok G1 Cikarang Barat		heri.kurnianto@pantos.com	PT. Pantos Logistics Indonesia	89982855			ACTIVE	9	894			02.021.249.4-431.000	NPWP_Pantos_Indonesia(1).pdf							\N		\N			
943	system	2017-04-17 00:00:00		0001-01-01 00:00:00 BC	JL. BUGIS NO. 96 B TANJUNG PRIOK		citrarubby25@gmail.com	PT. ROMANO PUTRA SERASI	4374754			ACTIVE	8	895			01.364.628.6-046.000	NPWP_PT__ROMANO_PUTRA_SERASI.pdf							\N		\N			
944	system	2017-04-18 00:00:00		0001-01-01 00:00:00 BC	PT.Telkomsel.gatot subroto		tutuko_rio@telkomsel.co.id	PT.Telkomsel	811139808			ACTIVE	1	896											\N		\N			
945	system	2017-04-20 00:00:00		0001-01-01 00:00:00 BC	JL.ENDE NO 21		heni@pt-lnj.com	PT.LINTAS NIAGA JAYA 	2129565555			ACTIVE	0	898			01.716.291.8-613.000	NPWP_PT__LNJ_Perak_Timur_No__561.pdf							\N		\N			
946	system	2017-04-21 00:00:00		0001-01-01 00:00:00 BC	JALAN ENDE NO 21 TANJUNG PRIOK JAKARTA UTARA		ridho@pt-lnj.com	PT . LINTAS NIAGA JAYA 	2129565555			ACTIVE	0	899			01.716.291.8-042.001								\N		\N			
947	system	2017-04-21 00:00:00		0001-01-01 00:00:00 BC	Jl Raya Bekasi Km 28.5 Kp Rawa Pasung Kota Baru Bekasi Barat 17134		inkote.online@gmail.com	Inkote Indonesia	4371363			ACTIVE	9	900			01.105.785.8-431.000								\N		\N			
948	system	2017-04-21 00:00:00		0001-01-01 00:00:00 BC			putridiajeng987@gmail.com	PT FREIGHT FORWARDER TEST				ACTIVE	9	901	ptp	cmacgm	19.231.231.2-312.311	20181219113631_npwp-perorangan.jpg	20181219113631_SIUP.jpg	20181219113631_ktp.jpg					\N		\N			
949	system	2017-04-25 00:00:00		0001-01-01 00:00:00 BC	Jl Kebon Bawang VI No. 24 Tg Priok 		shipping-pls.jkt@kamadjaja.com	PT Pusaka Lintas Samudra	43921591			ACTIVE	9	903			01.466.115.1-042.001	NPWP_PLS.pdf							\N		\N			
950	system	2017-04-26 00:00:00		0001-01-01 00:00:00 BC			putridiajeng987@gmail.com					NON ACTIVE	9	904											\N		\N			
951	system	2017-04-27 00:00:00		0001-01-01 00:00:00 BC	Wisma Slipi Building 9th Floor Jl. Letnan Jenderal S. Parman Kav. 12 Slipi - Palmerah Jakarta Barat - 11480 		sally.bs@samsung.com	PT SAMSUNG SDS GLOBAL SCL INDONESIA	5307188			ACTIVE	9	905			03.093.841.9-031.000	NPWP_SDSID.pdf							\N		\N			
952	system	2017-04-28 00:00:00		0001-01-01 00:00:00 BC	Jl. Raya Cakung Cilincing Kav. 14		nlil-fin@nittsi.com.hk	PT. Nittsu Lemo Indonesia Logistik	2146823912			ACTIVE	9	906			01.824.511.8-058.000	NPWP_NITSSU.pdf							\N		\N			
953	system	2017-04-28 00:00:00		0001-01-01 00:00:00 BC	Jl Denpasar Blok II no 1and16 KBN Marunda Cilincing 		log_mgr@transcon-indonesia.com	PT Transcon Indonesia				ACTIVE	9	907			02.506.653.1-045.000								\N		\N			
954	system	2017-05-03 00:00:00		0001-01-01 00:00:00 BC	Jl. Industri II Blok F No 7 , Jatiuwung, Tangerang		colorpak.online@gmail.com	PT Colorpak Indonesia	5901962			ACTIVE	9	908			01.061.759.5-054.000	NPWP_PTColorpakIndonesia.docx							\N		\N			
955	system	2017-05-03 00:00:00		0001-01-01 00:00:00 BC	Jl Anggrek no 23 E RT 002/ RW 012 Rawabadak Utara, Koja, Jakut		sumber.e.anugerah@gmail.com	PT Sumber Logistindo Anugerah				ACTIVE	9	909			81.630.273.1-045.000	NPWP_SUMBERLOGISTINDO.jpg							\N		\N			
956	system	2017-05-05 00:00:00		0001-01-01 00:00:00 BC	EJIP KAV. 5J, SUKARESMI, CIKARANG SELATAN		import@aisin-indonesia.co.id	PT. AISIN INDONESIA	218970909			ACTIVE	1	910			01.065.305.3-055.000	NPWP_PT__AISIN_INDONESIA.pdf							\N		\N			
957	system	2016-06-27 00:00:00		0001-01-01 00:00:00 BC	Wisma Aldiron, Pancoran, Jakarta		putridiajeng987@gmail.com	PT. Unilever	2179181880			ACTIVE	8	1			12-2334909-01	20181129040040_9861066-textura-de-pared-de-piedra-para-los-diseñadores-y-artistas-3d.jpg	20181129040040_RPL-190_1.jpg	20181129040040_9861066-textura-de-pared-de-piedra-para-los-diseñadores-y-artistas-3d.jpg	20181129040040_9861066-textura-de-pared-de-piedra-para-los-diseñadores-y-artistas-3d.jpg	20181129040040_RPL-190_1.jpg			\N		\N			
958	system	2015-12-22 00:00:00		0001-01-01 00:00:00 BC	Tangerang		putridiajeng987@gmail.com	AGILITY	1234			ACTIVE	9	41			02.194.257.8-058.000						957328		\N		\N			
959	system	2015-12-22 00:00:00		0001-01-01 00:00:00 BC	Jakarta Utara		putridiajeng987@gmail.com	PT. UNILEV	888			ACTIVE	1	45			12-2334909-06								\N		\N			
990	system	2017-03-30 00:00:00		0001-01-01 00:00:00 BC			vm.silkargo@gmail.com	PT. Silkargo Indonesia				ACTIVE	8	871			02.293.587.8-038.000	NPWP_VMSILK.png							\N		\N			
1024	system	2018-02-21 00:00:00		0001-01-01 00:00:00 BC	Bekasi		phemaskd@gmail.com	PT. ACL	77765432			NOT ACTIVE	9	1029			88.089.765.4-654.678	20181219112452_npwp-perorangan.jpg	20181219112452_SIUP.jpg	20181219112452_ktp.jpg					\N		\N			
960	system	2015-12-22 00:00:00	agility01	2017-04-25 00:00:00	Tomang		putridiajeng987@gmail.com	PT. EVERGREEN SHIPPING AGENCY INDONESIA	555		2504170001	ACTIVE	1	50	ptp	icargo	12-2334909-07	20181128052403_9861066-textura-de-pared-de-piedra-para-los-diseñadores-y-artistas-3d.jpg	20181128052403_9861066-textura-de-pared-de-piedra-para-los-diseñadores-y-artistas-3d.jpg	20181128052403_9861066-textura-de-pared-de-piedra-para-los-diseñadores-y-artistas-3d.jpg	20181128052403_9861066-textura-de-pared-de-piedra-para-los-diseñadores-y-artistas-3d.jpg	20181128052403_9861066-textura-de-pared-de-piedra-para-los-diseñadores-y-artistas-3d.jpg			\N		\N			
961	ilcs	2016-09-22 00:00:00	ilcs	2016-09-22 00:00:00	Gedung Graha Indramas Lantai 6-12, Jalan Aipda KS. Tubun No. 77, Daerah Khusus Ibukota Jakarta 11410, Indonesia		putridiajeng987@gmail.com	PT. LAUTAN LUAS TBK	213677777			ACTIVE	1	371			01.315.900.9-054.000								\N		\N			
962	ilcs	2016-09-28 00:00:00	ilcs	2016-09-28 00:00:00	Menara Satu Sentra Kelapa Gading 5th floor Unit 0503 Jalan Bulevar Kelapa Gading LA3 No.1 Summarecon Kelapa Gading Jakarta 14240 Indonesia		putridiajeng987@gmail.com	PT. RGA INTERNATIONAL INDONESIA	2129375688			ACTIVE	1	377			12-2334909-09								\N		\N			
963	ilcs	2017-02-20 00:00:00	ilcs	2017-02-20 00:00:00	Jl. Enggano, Tj. Priok, Kota Jkt Utara, DKI Jakarta 14310, Indonesia		putridiajeng987@gmail.com	PT. FREIGHT FORWARDER	81213143707			ACTIVE	9	446			12-2334909-11	20181228113916_ktp.jpg	20181228093716_npwp-perorangan.jpg	20181213080545_NPWP.PDF	20181213080545_NPWP.PDF				\N		\N			
964	ilcs	2017-02-20 00:00:00	User_ff19	2019-10-04 00:00:00	Komplek Pergudangan Soewarna Unit E No. 16-17, Cengkareng, Pajang, Benda, Kota Tangerang, Banten 15126, Indonesia		putridiajeng987@gmail.com	PT. SHIPPING LINE	2155911757		410190002	ACTIVE	2	450	ptp	rcl	81.838.670.8-042.000	20180417081535_Proforma_TRXD_22092017000000901.pdf							\N		\N			
965	system	2017-05-12 00:00:00		0001-01-01 00:00:00 BC	yosudarso		putridiajeng987@gmail.com	PT. CO ILCS				ACTIVE	1	914			12.345.678.8-999.999								\N		\N			
966	system	2017-05-22 00:00:00	coff01	2017-12-13 00:00:00	Hapag-Lloyd Agency Division, General Agents PT. Samudera Indonesia, Tbk   Cyber 2 Tower Level 3 A, E, F,  Jl. H.R. Rasuna Said Block X-5 No. 13  Jakarta Selatan, Indonesia 12950  		idsci@hlag.com	PT. Samudera Indonesia Tbk, as Hapag-Lloyd Agency Division	2129343600		1312170001	ACTIVE	2	915	ptp	samin	01.001.895.0-054.000	NPWP_PT_SI_Tbk.jpg							\N		\N			
967	system	2017-06-05 00:00:00	User_ff19	2019-09-23 00:00:00	JL. KAKAP 10 NO 139 PERUMNAS 1 TANGERANG 		putridiajeng987@gmail.com	PT. BHUM MULIA PRIMA/PT BINTIKA BANGUNUSA(RCL)	021 5214808		2309190001	ACTIVE	2	916	ptp	rcl	01.871.131.7-063.000	NPWP_BHUM1.jpg							\N		\N			
968	system	2017-07-28 00:00:00		0001-01-01 00:00:00 BC	Jl. Raya Manyar No 151 Km 26 RT 002/ RW 002, Manyarejo, Gresik, Jawa Timur		shcp.indonesia@gmail.com	PT SHCP Indonesia				NOT ACTIVE	2	917			01.082.783.0-641.000								\N		\N			
969	system	2017-08-01 00:00:00		0001-01-01 00:00:00 BC	Pergudangan Bandara Mas Jl. Marsekal Suryadharma Blok B2 no 3 		gsmeternal.online@gmail.com	Gunung Sukses Makmur Eternal	22251645			NOT ACTIVE	9	918			03.222.975.9-027.000	NPWP_GSME.JPG							\N		\N			
970	system	2017-08-11 00:00:00	coff01	2017-11-03 00:00:00	ASJASKJASA		jacobsmarc78@gmail.com	PT SHIPPING LINE	86386483		311170001	NOT ACTIVE	9	919			11.111.111.1-111.111	20180323124611_NPWP.jpg	20180323124611_siup.gif	20180323124611_ktp.JPG	20180323124611_ktp.JPG	20180323124611_surat kuasa perusahaan.jpg			\N		\N			
971	system	2017-10-12 00:00:00		0001-01-01 00:00:00 BC	JL. Sri Rejeki Dalam III NO. 24 Kel. Kalibanteng Kidul, Kec.Semarang Barat ,Semarang 		julizar56@gmail.com	CV. TRESNA HARAPAN MANDIRI	81318557892			NOT ACTIVE	1	921			80.517.390.3-503.000								\N		\N			
972	system	2018-01-12 00:00:00		0001-01-01 00:00:00 BC			putridiajeng987@gmail.com					NOT ACTIVE	9	922											\N		\N			
973	system	2018-01-12 00:00:00		0001-01-01 00:00:00 BC	A		putridiajeng987@gmail.com	PT INFOMEDIA				NOT ACTIVE	\N	923			20.010.020.0-100.999								\N		\N			
974	ilcs	2017-03-14 00:00:00	ilcs	2017-03-14 00:00:00	Jalan Ende No.21 Tanjung Priuk, Jakarta Utara 14310		alwiyah@pt-lnj.com	PT. LINTAS NIAGA JAYA	2129565555			ACTIVE	9	745			01.716.291.8-613.000	NPWP_PT__LNJ_Perak_Timur_No__561.pdf							\N		\N			
975	system	2018-01-12 00:00:00		0001-01-01 00:00:00 BC	Jl. Penghulu RT 009/01 kebayoran lama jakarta selatan		Yuswatichasanah@gmail.com	PT ILCS	89625812856			NOT ACTIVE	1	924			01.234.567.8-999.999	20180227071856_MOCK UP UI LOLO v2(1).pptx	20180227071856_MOCK UP UI LOLO v2(2).pptx	20180227071856_MOCK UP UI LOLO v2(1).pptx	20180227071856_MOCK UP UI LOLO v2(1).pptx	20180227071856_MOCK UP UI LOLO v2(1).pptx			\N		\N			
976	system	2018-01-19 00:00:00		0001-01-01 00:00:00 BC	121212		syabab45@gmail.com	KAKIKUDA				NOT ACTIVE	9	935			11.111.111.1-111.111	20180119031727_4b6801ea745bf518c3eeb03d0a389ebf.jpg							\N		\N			
977	ilcs	2017-03-14 00:00:00	ilcs	2017-03-14 00:00:00	Kawasan Industri EJIP Plot 5J, Cikarang Selatan, Sukaresmi, Cikarang Sel., Bekasi, Jawa Barat 17530, Indonesia		ide.reachani@aisin-indonesia.co.id	PT AISIN INDONESIA	218970909			ACTIVE	1	762			12-2334909-17								\N		\N			
978	system	2018-01-16 00:00:00		0001-01-01 00:00:00 BC	123123		fata.mailbox@gmail.com	TEST PT	12312312			NOT ACTIVE	1	934			234234234234	20180116061200_68-creative-cv-resumes.jpg							\N		\N			
979	system	2018-01-19 00:00:00		0001-01-01 00:00:00 BC	sds		ajshajh@jbasba.com	Jasa Marga Trial				NOT ACTIVE	9	940			26.382.636.2-382.328	20180221024252_ktp-amat-faozi_20170720_083509.jpg	20180221024252_ktp-amat-faozi_20170720_083509.jpg	20180221024252_ktp-amat-faozi_20170720_083509.jpg	20180221024252_ktp-amat-faozi_20170720_083509.jpg	20180221024252_ktp-amat-faozi_20170720_083509.jpg			\N		\N			
980	system	2018-01-21 00:00:00		0001-01-01 00:00:00 BC			gelassungu@gmail.com					NOT ACTIVE	\N	941											\N		\N			
981	system	2018-01-22 00:00:00		0001-01-01 00:00:00 BC	Jakarta		putridiajeng987@gmail.com	Malik	81213514546			NOT ACTIVE	1	942			11.122.233.4-455.556								\N		\N			
982	system	2018-01-22 00:00:00		0001-01-01 00:00:00 BC	Jakarta		putridiajeng987@gmail.com	Malik	81213514546			NOT ACTIVE	1	943			11.122.233.4-455.556								\N		\N			
983	system	2018-01-22 00:00:00		0001-01-01 00:00:00 BC	ss		asjas@jbdja.com	PT Bisa				NOT ACTIVE	9	944			27.392.739.7-297.323								\N		\N			
984	system	2018-01-24 00:00:00		0001-01-01 00:00:00 BC	TESTER		testermddc2@gmail.com	PT BARU				NOT ACTIVE	8	952			68.268.242.4-242.441	20180124090239_invoice_demurrage.pdf							\N		\N			
985	system	2018-01-25 00:00:00		0001-01-01 00:00:00 BC	TEST		syabab45@gmail.com	TEST SATU				NOT ACTIVE	1	953			11.111.111.1-111.111	20180125010347_68-creative-cv-resumes.jpg							\N		\N			
986	system	2018-01-25 00:00:00		0001-01-01 00:00:00 BC	123123		syabab45@gmail.com	SATU TEST				NOT ACTIVE	2	954			22.222.222.2-222.222	20180221060505_ktp-amat-faozi_20170720_083509.jpg							\N		\N			
987	system	2018-01-25 00:00:00		0001-01-01 00:00:00 BC	123123		syabab45@gmail.com	TEST TEST				NOT ACTIVE	1	955			12.121.212.1-212.121	20180125011656_68-creative-cv-resumes.jpg							\N		\N			
988	system	2018-01-26 00:00:00		0001-01-01 00:00:00 BC	tester street 		testermddc4@gmail.com	PT NEW COMPANY				NOT ACTIVE	8	964			98.988.808.8-889.888	20180126100200_122465.png							\N		\N			
991	system	2017-05-10 00:00:00		0001-01-01 00:00:00 BC	JL.bsd boulevard barat green office park kavling 3 BSD  sampora cisauk kab Tanggerang banten		suci.adelia@unilever.com	PT. UNILEVER INDONESIA TBK	2180827000			ACTIVE	1	911			01.001.701.0-92.000	NPWP_UNILEVER.pdf							\N		\N			
992	system	2017-05-10 00:00:00		0001-01-01 00:00:00 BC	JL DANAU SUNTER UTARA BLOK O3 KAV. 30 SUNTER JAKARTA UTARA 14350		rahmat.budip@isuzu.astra.co.id	PT. ISUZU ASTRA MOTOR INDONESIA	6501000			ACTIVE	9	912			01.000.514.8-092.000	NPWP1.jpg							\N		\N			
993	system	2018-01-26 00:00:00		0001-01-01 00:00:00 BC	test		fata.mailbox@gmail.com	PT BIKIN BARU COY				NOT ACTIVE	2	970			12.312.312.1-313.133	20180126023104_4b6801ea745bf518c3eeb03d0a389ebf.jpg							\N		\N			
994	system	2018-01-26 00:00:00		0001-01-01 00:00:00 BC	orchard road		testermddc4@gmail.com	PT BISA2				NOT ACTIVE	1	971			26.323.626.8-326.382	20180126023851_122465.png							\N		\N			
995	system	2018-01-26 00:00:00		0001-01-01 00:00:00 BC	Jakarta		putridiajeng987@gmail.com	PT. ILT				NOT ACTIVE	1	972			88.089.765.4-654.678	20180126025318_kittens-555822__340.jpg							\N		\N			
996	system	2018-01-30 00:00:00		0001-01-01 00:00:00 BC	JL RAYA CAKUNG CILINCING KM 3  JAKARTA TIMUR		fajar.wahyudin@bsa-logistics.co.id	PT BINA SINAR AMITY	62214603401			NOT ACTIVE	9	974			01.670.200.3-058.000						847436		\N		\N			
997	system	2018-02-02 00:00:00		0001-01-01 00:00:00 BC	tester street		testermddc@gmail.com	company abcd				NOT ACTIVE	8	980			37.493.747.3-437.947	20180202094308_CR iCargo_20170214.pdf							\N		\N			
998	system	2018-02-02 00:00:00		0001-01-01 00:00:00 BC	tester 123		testermddc2@gmail.com	company ghf	232923			NOT ACTIVE	8	981			27.392.793.7-273.927	20180202095606_CR iCargo_20170523.docx	20180202100511_CR iCargo_20170523.docx	20180202100511_CR iCargo_20170522.pdf	20180202100511_CR iCargo_20170412.pdf	20180202100512_CR iCargo_20170517.docx			\N		\N			
999	system	2018-02-02 00:00:00		0001-01-01 00:00:00 BC	tester street		testermddc3@gmail.com	pt company abcdf				NOT ACTIVE	1	983			93.849.389.4-993.943	20180202013452_FCR-ICARGO-20180102.docx							\N		\N			
1000	system	2018-02-03 00:00:00		0001-01-01 00:00:00 BC	add		testermddc4@gmail.com	cargo logistics				NOT ACTIVE	8	985			02.323.203.0-230.023	20180203061452_1.jpg							\N		\N			
1001	system	2018-02-04 00:00:00		0001-01-01 00:00:00 BC	aa		testermddc5@gmail.com	James dean company				NOT ACTIVE	9	986			92.379.239.2-332.323	20180204095755_1.jpg							\N		\N			
1002	system	2018-02-05 00:00:00		0001-01-01 00:00:00 BC	Bekasi		putridiajeng987@gmail.com	PT ILC	9988678907			NOT ACTIVE	1	987			99.890.765.4-678.908	20180205114611_NPWP.jpg							\N		\N			
1003	system	2018-02-05 00:00:00		0001-01-01 00:00:00 BC	sjhds		adaaksa@jaj.com	new company abfd				NOT ACTIVE	9	988			29.323.263.2-638.268	20180205114859_1.jpg							\N		\N			
1004	system	2018-02-05 00:00:00		0001-01-01 00:00:00 BC	bekasi		putridiajeng987@gmail.com	PT ILCT	8977678765460			NOT ACTIVE	1	989			66.422.976.2-405.000	20180205120758_NPWP.jpg							\N		\N			
1005	system	2018-02-05 00:00:00		0001-01-01 00:00:00 BC	Bekasi		phemaskd@gmail.com	PT. ACL	776543217			NOT ACTIVE	9	991			66.422.976.2-405.000	20181010030620_NPWP.jpg	20181010030620_siup.gif	20181010030620_ktp.JPG	20180205011747_ktp.jpg	20180205011747_surat kuasa.jpg			\N		\N			
1006	system	2018-02-05 00:00:00		0001-01-01 00:00:00 BC	hasasjasas		testermddc@gmail.com	New company hhh				NOT ACTIVE	9	992			49.394.934.9-394.379	20180205013844_2.jpg	20180205013844_CR iCargo_20170213.pdf	20180205013844_CR iCargo_20170309.docx	20180205013844_CR iCargo_20170412.docx	20180205013844_CR iCargo_20170522.pdf			\N		\N			
1007	system	2018-02-05 00:00:00		0001-01-01 00:00:00 BC	tester		testermddc4@gmail.com	Company ggg				NOT ACTIVE	9	995			92.739.723.7-923.727	20180205053307_1.jpg							\N		\N			
1008	system	2018-02-05 00:00:00		0001-01-01 00:00:00 BC	akhsahs		testermddc5@gmail.com	company adelia				NOT ACTIVE	9	996			63.623.263.8-283.623	20180205054904_1.jpg							\N		\N			
1009	system	2018-02-05 00:00:00		0001-01-01 00:00:00 BC	jagjsgas		jajsasg@jgajsj.com	company adeliaa				NOT ACTIVE	9	997			29.327.372.9-379.273	20180205055338_1.jpg							\N		\N			
1010	system	2018-02-05 00:00:00		0001-01-01 00:00:00 BC	askhakhsaishdsi		testermddc3@gmail.com	company adelia abc				NOT ACTIVE	\N	998			23.727.372.3-929.372	20180205060116_2.jpg							\N		\N			
1011	system	2018-02-05 00:00:00		0001-01-01 00:00:00 BC	kasjas		testermddc4@gmail.com	adelia abcd				NOT ACTIVE	8	999			09.273.927.3-273.293	20180205060754_1.jpg							\N		\N			
1012	system	2018-02-05 00:00:00		0001-01-01 00:00:00 BC	kdhaka		testermddc5@gmail.com	james dean abc				NOT ACTIVE	8	1000			93.493.943.9-434.739	20180205061248_1.jpg							\N		\N			
1013	system	2018-02-05 00:00:00		0001-01-01 00:00:00 BC	asa		ajshjahs@jhaksh.com	aaaa				NOT ACTIVE	9	1001			20.830.283.2-032.838	20180205062111_4.jpg	20180205063852_[D2P] DO Payment (Portal).doc						\N		\N			
1014	system	2018-02-05 00:00:00		0001-01-01 00:00:00 BC	Lalalala		devilkazuma@gmail.com	Keren	3123123123123			NOT ACTIVE	1	1003			13.123.123.1-231.232	20180205063353_26994063_10209093761174990_2146280642533730517_n.jpg	20180205063624_27540059_324748378036574_5889128533487961843_n.jpg	20180205063624_pp.jpg					\N		\N			
1015	system	2018-01-25 00:00:00		0001-01-01 00:00:00 BC	tester street		testermdd3@gmail.com	PT BARU LAGI				NOT ACTIVE	8	956			27.392.732.9-737.293	20180205081514_2.jpg	20180205081514_CR iCargo 20170519.pdf	20180205081514_CR iCargo_20170213.docx	20180205081514_CR iCargo 20170519.pdf	20180205081514_CR iCargo_20170309.docx			\N		\N			
1016	system	2018-02-06 00:00:00		0001-01-01 00:00:00 BC			jkt_suhadi@id.yangming.com	Yang Ming				ACTIVE	2	1006			72.937.927.3-729.392								\N		\N			
1017	system	2018-02-06 00:00:00		0001-01-01 00:00:00 BC	Jl. Kenanga II RT 01/02 Kalisari Pasar rebo, Jakarta Timur		semuaemailpelabuhan@gmail.com		82260023474			NOT ACTIVE	1	1011											\N		\N			
1018	system	2018-02-14 00:00:00		0001-01-01 00:00:00 BC	SECURE BUILDING,BUILDING C JL.RAYA PROTOKOL HALIM PERDANAKUSUMA		wprihatin@agility.com	PT.AGILITY INTERNATIONAL	2126645170			NOT ACTIVE	9	1015											\N		\N			
1019	system	2017-05-12 00:00:00		0001-01-01 00:00:00 BC	Kawasan Industri Kota Bukit Indah Blok D1 No.1		sandi@hino-motors.co.id	PT. Hino Motors Manufacturing Indonesia	0			ACTIVE	1	913			01.060.143.3-092.000	20180219080826_ktp-amat-faozi_20170720_083509.jpg	20180219082924_ktp-amat-faozi_20170720_083509.jpg	20180219082924_ktp-amat-faozi_20170720_083509.jpg	20180219082924_ktp-amat-faozi_20170720_083509.jpg	20180219082924_ktp-amat-faozi_20170720_083509.jpg			\N		\N			
1020	system	2018-02-19 00:00:00		0001-01-01 00:00:00 BC	perumahan alam indah		verenclarita10@gmail.com	company baru	81289311618			NOT ACTIVE	9	1017			83.748.378.4-374.387								\N		\N			
1021	system	2018-02-20 00:00:00		0001-01-01 00:00:00 BC	Alamat Test		putridiajeng987@gmail.com	PT. ILCS1	2188			NOT ACTIVE	1	1024				20180220112250_telkom.png							\N		\N			
1022	system	2018-02-20 00:00:00		0001-01-01 00:00:00 BC	Jakarta		putridiajeng987@gmail.com	PT ILT				NOT ACTIVE	1	1022			88.089.765.4-654.678	20180220114931_NPWP.jpg	20180220114931_SIUP.jpg	20180220114931_ktp.jpg	20180220114931_ktp.jpg	20180220114931_surat kuasa.jpg			\N		\N			
1023	system	2018-02-21 00:00:00		0001-01-01 00:00:00 BC			ekorah95@gmail.com					NOT ACTIVE	9	1026											\N		\N			
1025	system	2018-02-21 00:00:00		0001-01-01 00:00:00 BC	Jakarta		phemaskd@gmail.com	PT. ILT	98678643			NOT ACTIVE	1	1030			88.764.567.3-423.156	20180221031607_NPWP.jpg							\N		\N			
1026	system	2018-02-21 00:00:00		0001-01-01 00:00:00 BC	Jakarta		phemaskd@gmail.com	PT ACL	980768897			NOT ACTIVE	9	1031			88.908.769.0-056.007	20181010072913_npwp-perorangan.jpg	20181010072913_SIUP.jpg	20181010072913_ktp.jpg					\N		\N			
1027	system	2018-02-21 00:00:00		0001-01-01 00:00:00 BC	Jakarta		phemaskd@gmail.com	PT. ACLT	62			NOT ACTIVE	1	1032			88.987.654.3-789.007	20180221032824_NPWP.jpg							\N		\N			
1028	system	2018-02-21 00:00:00		0001-01-01 00:00:00 BC	Jakarta		phemaskd@gmail.com	PT. ACLT	90879786			NOT ACTIVE	1	1033			88.987.654.3-789.007	20180221041745_NPWP.jpg	20180221041745_SIUP.jpg	20180221041745_ktp.jpg	20180221041745_ktp.jpg	20180221041745_surat kuasa.png			\N		\N			
1029	system	2018-02-21 00:00:00		0001-01-01 00:00:00 BC	tester mddc		testermddc@gmail.com	lautan biru	973927377			NOT ACTIVE	8	1034			23.279.372.9-372.739	20180221042415_BL.jpeg							\N		\N			
1030	system	2018-02-21 00:00:00		0001-01-01 00:00:00 BC	Jakarta		phemaskd@gmail.com	PT. ACLT	87678987			NOT ACTIVE	1	1035			88.987.654.3-789.007	20180221042801_NPWP.jpg							\N		\N			
1031	system	2018-02-21 00:00:00		0001-01-01 00:00:00 BC	jakarta selatan		testermddc2@gmail.com	lautan unilever	2929737273			NOT ACTIVE	2	1036			39.439.743.9-749.374	20180221043627_BL.jpeg							\N		\N			
1032	system	2018-02-21 00:00:00		0001-01-01 00:00:00 BC	Jakarta		phemaskd@gmail.com	PT. ACLT	98076865			NOT ACTIVE	1	1037			88.987.654.3-789.007	20180221043642_NPWP.jpg							\N		\N			
1033	system	2018-02-21 00:00:00		0001-01-01 00:00:00 BC	Jakarta		phemaskd@gmail.com	PT. ACLT	98768765			NOT ACTIVE	1	1038			88.987.654.3-789.007	20180221045326_NPWP.jpg	20180221045326_SIUP.jpg	20180221045326_ktp.jpg	20180221045326_ktp.jpg	20180221045326_surat kuasa.png			\N		\N			
1034	system	2018-02-22 00:00:00		0001-01-01 00:00:00 BC	tests street		testermddc3@gmail.com	company test1	232323232			NOT ACTIVE	8	1039			23.020.302.3-028.302	20180222074154_BL.jpeg							\N		\N			
1035	system	2018-02-22 00:00:00		0001-01-01 00:00:00 BC	Jakarta		putri.dhkd@gmail.com	PT. CLL	99087651			NOT ACTIVE	1	1040			88.098.678.6-541.006	20180222105521_NPWP.jpg	20180222105521_siup.gif	20180222105521_ktp.JPG	20180222105521_ktp.JPG	20180222105521_surat kuasa perusahaan.jpg			\N		\N			
1036	system	2018-02-22 00:00:00		0001-01-01 00:00:00 BC	Jakarta		putri.dhkd@gmail.com	PT. CLL	89098767			NOT ACTIVE	1	1041			88.098.678.6-541.006	20180222110355_NPWP.jpg							\N		\N			
1037	system	2018-02-22 00:00:00		0001-01-01 00:00:00 BC	Jakarta		putri.dhkd@gmail.com	PT. CLL	98909876			NOT ACTIVE	1	1042			88.098.678.6-541.006	20180222110617_NPWP.jpg							\N		\N			
1038	system	2018-02-22 00:00:00		0001-01-01 00:00:00 BC	Jakarta		putri.dhkd@gmail.com	PT. CLL	9897656			NOT ACTIVE	1	1043			88.098.678.6-541.006	20180222111208_NPWP.jpg							\N		\N			
1039	system	2018-02-22 00:00:00		0001-01-01 00:00:00 BC	Jakarta		putridiajeng987@gmail.com	PT. ATD	87898765			NOT ACTIVE	1	1044			88.987.687.0-076.587	20180222115347_NPWP.jpg	20180222115347_siup.gif	20180222115347_ktp.JPG	20180222115347_ktp.JPG	20180222115347_surat kuasa perusahaan.jpg			\N		\N			
1040	system	2018-02-26 00:00:00		0001-01-01 00:00:00 BC	jalan tester		agsjagsj@adhj.com	test company				NOT ACTIVE	9	1051			28.382.738.2-783.782								\N		\N			
1041	system	2018-02-26 00:00:00		0001-01-01 00:00:00 BC	ajksja		agsjagsj@adhj.com	test company baru 2	293792739			NOT ACTIVE	9	1052			29.379.273.7-297.372	20180331081703_bongkar_container.pdf	20180413070155_banner1.jpg						\N		\N			
1042	system	2018-03-02 00:00:00		0001-01-01 00:00:00 BC	sjhksh		akshakjh@kdjh.com	burden	937834			NOT ACTIVE	9	1055			29.392.392.7-392.372	20180302092835_proforma.png							\N		\N			
1043	system	2018-03-04 00:00:00		0001-01-01 00:00:00 BC	aa		amnsjans@knasxz.com	abz				NOT ACTIVE	9	1056			23.232.323.2-323.293	20180304093448_del container.PNG							\N		\N			
1044	system	2018-03-08 00:00:00		0001-01-01 00:00:00 BC			ahsja@kqk.com					NOT ACTIVE	1	1057											\N		\N			
1045	system	2018-03-09 00:00:00		0001-01-01 00:00:00 BC	akjhskahsh		asd@gmail.com	baru 1				NOT ACTIVE	9	1058			23.792.732.7-392.739	npwp1234.jpg							\N		\N			
1046	system	2018-03-23 00:00:00		0001-01-01 00:00:00 BC	Jakarta		hjghhifehfjkl@gmail.com	PT.ATD	87898765			NOT ACTIVE	1	1060			88.987.687.0-076.587	20180323095541_NPWP.jpg							\N		\N			
1047	system	2018-03-23 00:00:00		0001-01-01 00:00:00 BC	Jakarta		putridiajeng987@gmail.com	PT.ATD	87898765			NOT ACTIVE	1	1062			88.987.687.0-076.587	20180323110032_NPWP.jpg							\N		\N			
1048	system	2018-03-23 00:00:00		0001-01-01 00:00:00 BC	Jakarta		putridiajeng987@gmail.com	PT.ATD	87898765			NOT ACTIVE	1	1066			88.987.687.0-076.587	20180323110524_NPWP.jpg							\N		\N			
1049	system	2018-03-23 00:00:00		0001-01-01 00:00:00 BC	Jakarta		putridiajeng987@gmail.com	PT.ATD	87898765			NOT ACTIVE	1	1067			88.987.687.0-076.587	20180323110533_NPWP.jpg							\N		\N			
1050	system	2018-03-23 00:00:00		0001-01-01 00:00:00 BC	Jakarta		putridiajeng987@gmail.com	PT.ATD	87898765			NOT ACTIVE	1	1068			88.987.687.0-076.587	20180323110537_NPWP.jpg							\N		\N			
1051	system	2018-03-23 00:00:00		0001-01-01 00:00:00 BC	Jakarta		putridiajeng987@gmail.com	PT.ATD	87898765			NOT ACTIVE	1	1069			88.987.687.0-076.587	20180323110814_NPWP.jpg							\N		\N			
1052	system	2018-03-23 00:00:00		0001-01-01 00:00:00 BC	Jakarta		putridiajeng987@gmail.com	PT.ATD	87898765			NOT ACTIVE	1	1070			88.987.687.0-076.587	20180323111200_NPWP.jpg							\N		\N			
1053	system	2018-03-23 00:00:00		0001-01-01 00:00:00 BC	Jakarta		putridiajeng115@gmail.com	PT.ATD	87898765			NOT ACTIVE	1	1071			88.987.687.0-076.587	20180323121750_NPWP.jpg							\N		\N			
1054	system	2018-04-01 00:00:00		0001-01-01 00:00:00 BC	Swakarsa 4		pratamajulianto18@gmail.com	PT WIRATRANS GLOBAL				NOT ACTIVE	\N	1077											\N		\N			
1055	system	2018-04-08 00:00:00		0001-01-01 00:00:00 BC	Jl. Otto Iskandardinata Raya No.66 Jatinegara, Jakarta Timur Kode Pos: 13330		taufik.rizkiandi@gmail.com	Patra Logistik	85912408			NOT ACTIVE	8	1079			01.061.152.3-051.000								\N		\N			
1056	system	2018-04-09 00:00:00		0001-01-01 00:00:00 BC	villa nusa indah blok w5/2		Julizar56@gmail.com	PT. MITRA SEKAWAN ULUNG				NOT ACTIVE	\N	1080											\N		\N			
1057	ilcs	2017-03-22 00:00:00	ilcs	2017-03-22 00:00:00	jakarta utara		putridiajeng987@gmail.com	PT. FORWARDING CARGO				ACTIVE	9	854	ptp	cmacgm	01.321.865.6-054.000	20190218032110_Business Model Canvas Bang Kiri Bang (BKB).png	20190218032110_Business Model Canvas Bang Kiri Bang (BKB).png	20190218032110_Business Model Canvas Bang Kiri Bang (BKB).png	20190218032110_Business Model Canvas Bang Kiri Bang (BKB).png	20190218032110_Business Model Canvas Bang Kiri Bang (BKB).png	804376		\N		\N			
1058	system	2018-04-20 00:00:00		0001-01-01 00:00:00 BC			hilda@gemilangexpresindo.com					NOT ACTIVE	8	1083				20180420111512_NPWP GE.jpg							\N		\N			
1059	system	2018-04-25 00:00:00		0001-01-01 00:00:00 BC	18A jl Pangkungsari, Kerobokan		jctassoni@jft-msconsulting.com	JFT Management Services				NOT ACTIVE	\N	1085											\N		\N			
1060	system	2018-05-09 00:00:00		0001-01-01 00:00:00 BC	asdasd		pandabeary@yandex.com	PLS	92			NOT ACTIVE	8	1086			18.273.172.3-981.729								\N		\N			
1061	system	2018-05-21 00:00:00		0001-01-01 00:00:00 BC	Jakarta		ptr.diajeng06@gmail.com	PT. SHIPPING LINE	87898765			NOT ACTIVE	2	1088			88.987.687.0-076.587	20180521014009_NPWP.jpeg							\N		\N			
1062	system	2018-05-22 00:00:00		0001-01-01 00:00:00 BC	Jalan Yos sudarso no.3C		marketing@sinartama.com	PT. Sinar Pratama Transport	43910167			NOT ACTIVE	8	1090				20180522010825_NPWP SPT Yos.pdf							\N		\N			
1063	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC								ACTIVE	9	1091											\N		\N			
1064	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC								ACTIVE	9	1092											\N		\N			
1065	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	Jl. S Parman kav. 35 Jakarta		puspito.winarko@samudera.id	PT. Samudera Indonesia Logistik Kargo	021-5300585/081514247000			ACTIVE	9	1093			03.271.810.8.031.000								\N		\N			
1066	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	Jl. S Parman kav. 35 Jakarta		puspito.winarko@samudera.id	PT. Samudera Indonesia Logistik Kargo	021-5300585/081514247000			ACTIVE	9	1094			03.271.810.8.031.000								\N		\N			
1067	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	Jl. S Parman kav. 35 Jakarta		puspito.winarko@samudera.id	PT. Samudera Indonesia Logistik Kargo	021-5300585/081514247000			ACTIVE	9	1095			03.271.810.8.031.000								\N		\N			
1068	ALFI	2018-05-18 00:00:00		0001-01-01 00:00:00 BC	18 Office Park Tower unit 10F Jl. TB Simatupang No. 18 Kebagusan Pasar Minggu, Jakarta Selatan		rhia@kog-indonesia.com	PT. KOG Indonesia	021-22978636/081357802328			ACTIVE	9	1087			02.091.660.7-011.000								\N		\N			
1069	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	Jl. S Parman kav. 35 Jakarta		puspito.winarko@samudera.id	PT. Samudera Indonesia Logistik Kargo	021-5300585/081514247000			ACTIVE	9	1096			03.271.810.8.031.000								\N		\N			
1070	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	Jl. S Parman kav. 35 Jakarta		puspito.winarko@samudera.id	PT. Samudera Indonesia Logistik Kargo	021-5300585/081514247000			ACTIVE	9	1097			03.271.810.8.031.000								\N		\N			
1071	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	Jl. S Parman kav. 35 Jakarta		puspito.winarko@samudera.id	PT. Samudera Indonesia Logistik Kargo	021-5300585/081514247000			ACTIVE	9	1098			03.271.810.8.031.000								\N		\N			
1072	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	Pulogadung Trade Center Blok A8 no. 51		moha-pngn@cbn.net.id / moha-jkt@cbn.net.id	PT. Citra Mandiri Trans	021-4603187/0816776760			ACTIVE	9	1099			01.839.563.2-004.000								\N		\N			
1073	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	Jl. MT Haryono kav. 23 Gedung Menara MTH		andimimp@centrin.net.id andimatrans@centrin.net.id	PT. Andima Transportindo	021-85918180/081297968501			ACTIVE	9	1100			01.762.668.0-015.000								\N		\N			
1074	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	Jl. S Parman kav. 35 Jakarta		puspito.winarko@samudera.id	PT. Samudera Indonesia Logistik Kargo	021-5300585/081514247000			ACTIVE	9	1141			03.271.810.8.031.000								\N		\N			
1075	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	Jl. S Parman kav. 35 Jakarta		puspito.winarko@samudera.id	PT. Samudera Indonesia Logistik Kargo	021-5300585/081514247000			ACTIVE	9	1142			03.271.810.8.031.000								\N		\N			
1076	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	Jl. S Parman kav. 35 Jakarta		puspito.winarko@samudera.id	PT. Samudera Indonesia Logistik Kargo	021-5300585/081514247000			ACTIVE	9	1143			03.271.810.8.031.000								\N		\N			
1077	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	Jl. S Parman kav. 35 Jakarta		puspito.winarko@samudera.id	PT. Samudera Indonesia Logistik Kargo	021-5300585/081514247000			ACTIVE	9	1144			03.271.810.8.031.000								\N		\N			
1078	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	Jl. S Parman kav. 35 Jakarta		puspito.winarko@samudera.id	PT. Samudera Indonesia Logistik Kargo	021-5300585/081514247000			ACTIVE	9	1145			03.271.810.8.031.000								\N		\N			
1079	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	Jl. S Parman kav. 35 Jakarta		puspito.winarko@samudera.id	PT. Samudera Indonesia Logistik Kargo	021-5300585/081514247000			ACTIVE	9	1146			03.271.810.8.031.000								\N		\N			
1080	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	Jl. S Parman kav. 35 Jakarta		puspito.winarko@samudera.id	PT. Samudera Indonesia Logistik Kargo	021-5300585/081514247000			ACTIVE	9	1147			03.271.810.8.031.000								\N		\N			
1081	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	Jl. S Parman kav. 35 Jakarta		puspito.winarko@samudera.id	PT. Samudera Indonesia Logistik Kargo	021-5300585/081514247000			ACTIVE	9	1148			03.271.810.8.031.000								\N		\N			
1082	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	Jl. S Parman kav. 35 Jakarta		puspito.winarko@samudera.id	PT. Samudera Indonesia Logistik Kargo	021-5300585/081514247000			ACTIVE	9	1149			03.271.810.8.031.000								\N		\N			
1083	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	Jl. S Parman kav. 35 Jakarta		puspito.winarko@samudera.id	PT. Samudera Indonesia Logistik Kargo	021-5300585/081514247000			ACTIVE	9	1150			03.271.810.8.031.000								\N		\N			
1084	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	Jl. S Parman kav. 35 Jakarta		puspito.winarko@samudera.id	PT. Samudera Indonesia Logistik Kargo	021-5300585/081514247000			ACTIVE	9	1151			03.271.810.8.031.000								\N		\N			
1085	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	Jl. S Parman kav. 35 Jakarta		puspito.winarko@samudera.id	PT. Samudera Indonesia Logistik Kargo	021-5300585/081514247000			ACTIVE	9	1152			03.271.810.8.031.000								\N		\N			
1086	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	Jl. S Parman kav. 35 Jakarta		puspito.winarko@samudera.id	PT. Samudera Indonesia Logistik Kargo	021-5300585/081514247000			ACTIVE	9	1153			03.271.810.8.031.000								\N		\N			
1087	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	Pulogadung Trade Center Blok A8 no. 51		moha-pngn@cbn.net.id / moha-jkt@cbn.net.id	PT. Citra Mandiri Trans	021-4603187/0816776760			ACTIVE	9	1154			01.839.563.2-004.000								\N		\N			
1088	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	Pulogadung Trade Center Blok A8 no. 51		moha-pngn@cbn.net.id / moha-jkt@cbn.net.id	PT. Citra Mandiri Trans	021-4603187/0816776760			ACTIVE	9	1155			01.839.563.2-004.000								\N		\N			
1089	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	Pulogadung Trade Center Blok A8 no. 51		moha-pngn@cbn.net.id / moha-jkt@cbn.net.id	PT. Citra Mandiri Trans	021-4603187/0816776760			ACTIVE	9	1156			01.839.563.2-004.000								\N		\N			
1090	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	Jl. S Parman kav. 35 Jakarta		puspito.winarko@samudera.id	PT. Samudera Indonesia Logistik Kargo	021-5300585/081514247000			ACTIVE	9	1157			03.271.810.8.031.000								\N		\N			
1091	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	Pulogadung Trade Center Blok A8 no. 51		moha-pngn@cbn.net.id / moha-jkt@cbn.net.id	PT. Citra Mandiri Trans	021-4603187/0816776760			ACTIVE	9	1158			01.839.563.2-004.000								\N		\N			
1092	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	Pulogadung Trade Center Blok A8 no. 51		moha-pngn@cbn.net.id / moha-jkt@cbn.net.id	PT. Citra Mandiri Trans	021-4603187/0816776760			ACTIVE	9	1159			01.839.563.2-004.000								\N		\N			
1093	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	Pulogadung Trade Center Blok A8 no. 51		moha-pngn@cbn.net.id / moha-jkt@cbn.net.id	PT. Citra Mandiri Trans	021-4603187/0816776760			ACTIVE	9	1160			01.839.563.2-004.000								\N		\N			
1094	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	Pulogadung Trade Center Blok A8 no. 51		moha-pngn@cbn.net.id / moha-jkt@cbn.net.id	PT. Citra Mandiri Trans	021-4603187/0816776760			ACTIVE	9	1161			01.839.563.2-004.000								\N		\N			
1095	ALFI	2018-05-31 00:00:00		0001-01-01 00:00:00 BC								ACTIVE	9	1162											\N		\N			
1096	ALFI	2018-05-31 00:00:00		0001-01-01 00:00:00 BC	Jl. Sunter Paradise A.41, Jakarta, 14350			PT Gema Cipta Mandiri	021-22706731/ 087703261979			ACTIVE	9	1163			02.475.880.7.048.000								\N		\N			
1097	ALFI	2018-05-31 00:00:00		0001-01-01 00:00:00 BC	Jakarta Bandung Surabaya		putridiajeng987@gmail.com	PT. Shipping Line	98987679			ACTIVE	9	1164			987654321123456								\N		\N			
1098	ALFI	2018-05-31 00:00:00		0001-01-01 00:00:00 BC	Jl. S Parman kav. 35 Jakarta		puspito.winarko@samudera.id	PT. Samudera Indonesia Logistik Kargo	021-5300585/081514247000			ACTIVE	9	1165			03.271.810.8.031.000								\N		\N			
1099	system	2018-06-03 00:00:00		0001-01-01 00:00:00 BC	Jl. Beringin no 579 Kec. Dungingi		anugerahbahari.logistik@gmail.com	JPT. ANUGERAH BAHARI	85256472050			NOT ACTIVE	9	1166			03.170.814.2-822.000	20180603012625_20160809_173325-1.jpg							\N		\N			
1100	ALFI	2018-07-13 00:00:00		0001-01-01 00:00:00 BC	18 Office Park Tower unit 10F Jl. TB Simatupang No. 18 Kebagusan Pasar Minggu, Jakarta Selatan		rhia@kog-indonesia.com	PT. KOG Indonesia	021-22978636/081357802328			ACTIVE	9	1169			02.091.660.7-011.000								\N		\N			
1101	ALFI	2018-07-18 00:00:00		0001-01-01 00:00:00 BC	Jakarta Bandung Surabaya		demo_coff@yahoo.com	PT. Cargo Owner	98987679			ACTIVE	9	1170			987654321123456	20180830112528_npwp-perorangan.jpg	20180830112528_SIUP.jpg	20180830112528_ktp.jpg	20180830112528_ktp.jpg	20180830112528_surat kuasa.jpg			\N		\N			
1102	ALFI	2018-07-18 00:00:00		0001-01-01 00:00:00 BC	jakarta		ptr.diajeng06@gmail.com	PT. LAUTAN LUAS	98987679			ACTIVE	9	1171			987654321123456								\N		\N			
1103	system	2018-07-25 00:00:00		0001-01-01 00:00:00 BC	Jakarta		andrey.wibowo@kamadjaja.com	PT Kamadjaja Logistics	3564576			NOT ACTIVE	9	1179			01.234.567.8-900.000	20180725052534_6753df49efe3059563406f944fc760af.jpg	20180725023339_SIUP.jpg	20180725023339_ktp.jpg			804376	 DKI Jakarta	\N	KOTA ADM. JAKARTA BARAT	\N	Indonesia		
1104	ALFI	2018-08-03 00:00:00		0001-01-01 00:00:00 BC	bekasi		putri.dhkd@gmail.com	PT. LAUTAN LUAS	65768786786			ACTIVE	9	1185			5676867								\N		\N			
1105	ALFI	2018-08-20 00:00:00		0001-01-01 00:00:00 BC	Jl. Raya Protokol Halim Perdanakusuma Jakarta		smudiarso@agility.com	PT. Agility International	8151890331			ACTIVE	9	1186									957328		\N		\N			
1106	ALFI	2018-08-20 00:00:00		0001-01-01 00:00:00 BC	Jl. Raya Protokol Halim Perdanakusuma Jakarta		mindra@agility.com	PT. Agility International	81807975955			ACTIVE	9	1187											\N		\N			
1107	ALFI	2018-08-20 00:00:00		0001-01-01 00:00:00 BC	JL.IR.H.JUANDA III, NO. 26 A-B KEBON KELAPA, GAMBIR JAKARTA PUSAT		cepanang@gmail.com	PT. GPI Logistics	8119568530			ACTIVE	9	1188											\N		\N			
1108	ALFI	2018-08-20 00:00:00		0001-01-01 00:00:00 BC	Kantor Operasi: Perkantoran Pulomas Satu, Jl. A. Yani 2 Cempaka Putih 13210		rizki@ptigl.com	PT. Intertrans Global Logistics	021-22861657			ACTIVE	9	1189			71.245.857.9-421.00	20180924050442_001.jpg	20181001124329_001.jpg	20181001124329_KTP KIKI.jpg	20181001124329_KTP AS.jpeg		979240		\N		\N			
1109	ALFI	2018-08-20 00:00:00		0001-01-01 00:00:00 BC	Jl. Ir. H. Juanda III No. 25 A-B Jakarta Pusat 10120, Indonesia		cepanang@gmail.com	PT. GPI Logistics	8119568530			ACTIVE	9	1190											\N		\N			
1110	ALFI	2018-08-28 00:00:00		0001-01-01 00:00:00 BC	Gedung Linggar Jati Lt.3, Jalan Kaye Butik, Tengah II No. 7			PT ASA Trans	021-29834003/081218696264			ACTIVE	9	1191			03.286.141.1-643.000								\N		\N			
1111	ALFI	2018-08-28 00:00:00		0001-01-01 00:00:00 BC	Gedung Linggar Jati Lt.3, Jalan Kaye Butik, Tengah II No. 7			PT ASA Trans	021-29834003/081218696264			ACTIVE	9	1192			03.286.141.1-643.000								\N		\N			
1112	ALFI	2018-08-28 00:00:00		0001-01-01 00:00:00 BC	Gedung Linggar Jati Lt.3, Jalan Kaye Butik, Tengah II No. 7			PT ASA Trans	021-29834003/081218696264			ACTIVE	9	1193			03.286.141.1-643.000								\N		\N			
1113	ALFI	2018-08-28 00:00:00		0001-01-01 00:00:00 BC	Gedung Linggar Jati Lt.3, Jalan Kaye Butik, Tengah II No. 7			PT ASA Trans	021-29834003/081218696264			ACTIVE	9	1194			03.286.141.1-643.000								\N		\N			
1114	ALFI	2018-08-28 00:00:00		0001-01-01 00:00:00 BC	Gedung Linggar Jati Lt.3, Jalan Kaye Butik, Tengah II No. 7			PT ASA Trans	021-29834003/081218696264			ACTIVE	9	1195			03.286.141.1-643.000								\N		\N			
1115	ALFI	2018-08-28 00:00:00		0001-01-01 00:00:00 BC	Gedung Linggar Jati Lt.3, Jalan Kaye Butik, Tengah II No. 7			PT ASA Trans	021-29834003/081218696264			ACTIVE	9	1196			03.286.141.1-643.000								\N		\N			
1116	ALFI	2018-08-28 00:00:00		0001-01-01 00:00:00 BC	Gedung Linggar Jati Lt.3, Jalan Kaye Butik, Tengah II No. 7			PT ASA Trans	021-29834003/081218696264			ACTIVE	9	1197			03.286.141.1-643.000								\N		\N			
1117	system	2018-08-28 00:00:00		0001-01-01 00:00:00 BC			demo.coff1@gmail.com	PT. COFF1	21			NOT ACTIVE	9	1200			12.345.678.9-012.345	20180828065341_user.png					804376	 DKI Jakarta	\N	KOTA ADM. JAKARTA UTARA	\N	Indonesia		
1118	system	2018-08-28 00:00:00		0001-01-01 00:00:00 BC			demo.coff2@gmail.com	PT. COFF2	22			NOT ACTIVE	9	1201			12.345.678.9-009.876	20180828070028_user.png						 DKI Jakarta	\N	KOTA ADM. JAKARTA UTARA	\N	Indonesia		
1119	system	2018-08-28 00:00:00		0001-01-01 00:00:00 BC	addraksa		kaskan@kaskaks.com	company baru1	808			NOT ACTIVE	9	1202			11.111.111.1-111.110							ksjksj	\N	kjkj	\N	kjkj		
1120	system	2018-08-28 00:00:00		0001-01-01 00:00:00 BC			demo.coff3@gmail.com	PT. COFF3	21			NOT ACTIVE	9	1203			12.345.678.9-067.890	20180828070936_user.png					804376	 DKI Jakarta	\N	KOTA ADM. JAKARTA UTARA	\N	Indonesia		
1121	system	2018-08-28 00:00:00		0001-01-01 00:00:00 BC			demo.coff4@gmail.com	PT. COFF4				NOT ACTIVE	9	1204			12.345.676.5-456.789	20180828071320_user.png					804376	 DKI Jakarta	\N	KOTA ADM. JAKARTA UTARA	\N	Indonesia		
1122	system	2018-08-28 00:00:00		0001-01-01 00:00:00 BC			demo.coff5@gmail.com	PT. COFF5				NOT ACTIVE	9	1205			12.345.678.7-654.323	20180828071847_user.png					804376	 DKI Jakarta	\N	KOTA ADM. JAKARTA UTARA	\N	Indonesia		
1123	ALFI	2018-09-03 00:00:00		0001-01-01 00:00:00 BC	Ruko Mega Grosir Cempaka Mas Blok P No. 6 Jl. Letjen Suprapto, Jakarta 10640, Indonesia		dekki@dextercargo.com	PT. DEXTER EUREKATAMA	+6221 42886677			ACTIVE	8	1210			02.409.293.4-027.000	20180914021849_NPWP Dexter.pdf	20180914021849_SIUJPT Perpanjangan 2015.pdf	20180914021849_eKTP Baru .pdf	20180914021849_eKTP Baru .pdf		899997		\N		\N			
1124	ALFI	2018-09-04 00:00:00		0001-01-01 00:00:00 BC	Menara Satu Sentra Kelapa Gading Lantai 5 Unit 0503 Jl. Bulevar Kelapa Gading LA3 No. 1, Summarecon Kelapa Gading Jakarta 14240, Indonesia		erwin@rgainter.com	PT. RGA INTERNATIONAL INDONESIA	+62 21 2937 5688			ACTIVE	8	1212			02.706.998.8-043.000	20180917111458_NPWP Jakarta.jpg	20180917111458_SIUPJPT 2018.pdf	20180917111459_KTP pemohon.jpeg	20180917111459_KTP an Cho Hyun A.pdf	20180917111459_SURAT KUASA.jpeg	971143		\N		\N			
1125	ALFI	2018-09-04 00:00:00		0001-01-01 00:00:00 BC	TIFA BUILDING, 4TH FLOOR SUITE 402A, Jl. KUNINGAN  BARAT No. 26, Jakarta 12710		info@geo-freight.com	PT. GEOFREIGHT INDONESIA	+62 21 52922272			ACTIVE	9	1213			02.816.620-5.014.000						909027		\N		\N			
1126	ALFI	2018-09-07 00:00:00		0001-01-01 00:00:00 BC	JL.IR.H.JUANDA III, NO.26 A-B KEBON KELAPA, GAMBIR JAKARTA PUSAT		cepanang@gmail.com	PT. GPI Logistics	8119568530			ACTIVE	9	1214									874602		\N		\N			
1127	ALFI	2018-09-10 00:00:00		0001-01-01 00:00:00 BC	Kantor Taman E.3.3, Uniut D3, Lantai 2. Jl. Dr. Ide anak Agung Gde Agung, Kawasan Mega Kuningan, Kuningan Timur, Setia Budi (Satu Lokasi dengan Menara Anugerah)     Jakarta Selatan, DKI Jakarta 12950 Indonesia    		henry.ruswoto@gmail.com	LSP Logistik Insan Prima	8151832102			ACTIVE	9	1215											\N		\N			
1128	ALFI	2018-09-10 00:00:00		0001-01-01 00:00:00 BC	JL. Raya Protokol Halim Perdanakusuma Jakarta		mindra@agility.com	PT. Agility International	81807975955			ACTIVE	9	1216									957328		\N		\N			
1129	ALFI	2018-09-10 00:00:00		0001-01-01 00:00:00 BC	Jl. IPN No. 2 cipinang Besar Selatan - Jakarta timur		didiet.hidayat@gmail.com	ITL Trisakti	81310626290			ACTIVE	9	1217											\N		\N			
1130	ALFI	2018-09-10 00:00:00		0001-01-01 00:00:00 BC	Jl. IPN Kebon Nanas No.2, RT.9/RW.6, Cipinang Besar Sel., Jatinegara, Kota Jakarta Timur, Daerah Khusus Ibukota Jakarta 13410		kurniawan.ljohanes@yahoo.com	ITL Trisakti	816715283			ACTIVE	9	1218											\N		\N			
1131	ALFI	2018-09-10 00:00:00		0001-01-01 00:00:00 BC	Jl. Lembur No. 11A RT.08/06 Kec. Makasar Jakarta Timur		saepudinamirhamzah@gmail.com	PT. Buana Eka Jaya	021-80878488 / 021-8097786			ACTIVE	9	1219			02.378.731.0-005.000	20181004033906_NPWP.PDF	20181004033906_SIUJPT.PDF	20181004033906_KTP DAN NPWP DIREKTUR FAJAR.pdf			979287		\N		\N			
1132	ALFI	2018-09-10 00:00:00		0001-01-01 00:00:00 BC	JL.IR.H.JUANDA III, NO. 26 A-B KEBON KELAPA, GAMBIR JAKARTA PUSAT		firdaus@gpilog.com	PT. GPI LOGISTICS	3813688			ACTIVE	9	1220			20974002074000						874602		\N		\N			
1133	ALFI	2018-09-10 00:00:00		0001-01-01 00:00:00 BC	JL PROF DR SOEPOMO SH NO.45BZ BLOK C TEBET JAKARTA SELATAN 12810 INDONESIA		firman@ugc.co.id	PT UTAMA GLOBALINDO CARGO	62218350788			ACTIVE	9	1221			03.178.278.2-015.000						946342		\N		\N			
1134	ALFI	2018-09-12 00:00:00		0001-01-01 00:00:00 BC	Jl. Lembur No. 11A RT. 08/06 Kec. Makasar Jakarta Timur		iman@bejlogistics.com, admin@bejlogistics.com	PT. Buana Eka Jaya	021-80878488/021-8097786			ACTIVE	9	1222			02.378.731.0-005.000						979287		\N		\N			
1135	ALFI	2018-09-13 00:00:00		0001-01-01 00:00:00 BC	Fontana Office Tower Lt. 36, Jl. Trembesi Blok D, Kemayoran Jakarta Utara, Indonesia 14410		admin@sinarfajarlogistics.com	PT. SINAR FAJAR LOGISTIK	8118131369			ACTIVE	9	1223			84.577.797.8-044.000	20181004115043_revisi npwp.pdf	20181004115043_SIUJPT SFL.pdf	20181004115043_KTP DAN NPWP DIREKTUR FAJAR.pdf			979288		\N		\N			
1136	ALFI	2018-09-13 00:00:00		0001-01-01 00:00:00 BC	Kantor Operasi: Perkantoran Pulomas Satu, Jl. A. Yani 2 Cempaka Putih 13210		rizki@ptigl.com	PT. Intertrans Global Logistics	021-22861657			ACTIVE	9	1224			71.245.857.9-421.000						979240		\N		\N			
1137	ALFI	2018-09-13 00:00:00		0001-01-01 00:00:00 BC	GD. MENARA 165 LT.17 SUITE B,CandD  JL. TB. SIMATUPANG KAV.1 CILANDAK TIMUR  JAKARTA 12560 INDONESIA		INA.A@JAK.COLLYERLOGISTICS.COM	PT. TIRTA WAHANA TRANSPORTAMA 	29534500			ACTIVE	9	1225			01.802.727.6-017.000						833921		\N		\N			
1138	ALFI	2018-09-13 00:00:00		0001-01-01 00:00:00 BC	Plaza Pasifik BI Bulevard Barat Kelapa Gading Jakarta Utara			PT. Varuna Prakarsa	021-44835651/082210200062			ACTIVE	9	1226											\N		\N			
1139	ALFI	2018-09-13 00:00:00		0001-01-01 00:00:00 BC	Uniair Building, Jl. Danau Sunter Barat A-3/40		salmon_josefiano@dimerco.com	PT. Uniair Indotama Cargo	021-65838080			ACTIVE	9	1227									866869		\N		\N			
1140	ALFI	2018-09-13 00:00:00		0001-01-01 00:00:00 BC	 PT. SENATOR INTERNATIONAL INDONESIA Secure Building A, 2nd Floor  Jl. Raya Halim Perdanakusuma Jakarta 13610, Indonesia Tel.¿¿¿¿¿¿¿¿ +62-21-8089 0011		sea.export@jkt.senator-international.com	PT. SENATOR INTERNATIONAL INDONESIA	+62-21-8089 0011			ACTIVE	9	1228			21944814058000						844504		\N		\N			
1141	ALFI	2018-09-13 00:00:00		0001-01-01 00:00:00 BC	Perkantoran Enggano Megah Blok C No. 15 H and I Jl. Raya Enggano Kel. Tanjung Priok, Kec. Tanjung Priok, Jakarta Utara 14310		pt.psam@yahoo.com	PT. PRIMA SEJAHTERA ABADI MANDIRI	021-29484822 			ACTIVE	9	1229			01.938.521.0-042.000								\N		\N			
1142	ALFI	2018-09-14 00:00:00		0001-01-01 00:00:00 BC	jakarta		putridiajeng987@gmail.com	PT. Cargo Owner	8976787654			ACTIVE	9	1230											\N		\N			
1143	ALFI	2018-09-17 00:00:00		0001-01-01 00:00:00 BC	Jl Gudang Peluru Raya B1 no 18-A Kebon baru Tebet Jakarta Selatan DKI Jakarta, RT.7/RW.1, Kb. Baru, Tebet, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12830		supatno@indoprimatrans.com	PT. Indoprima Trans	021-8305574			ACTIVE	9	1234											\N		\N			
1144	ALFI	2018-09-17 00:00:00		0001-01-01 00:00:00 BC	Jl. Komodor Halim Perdana Kusuma No.1,RW.006, RT.2/RW.1, Kb. Pala, Makasar, Kota Jakarta Timur, Daerah Khusus Ibukota Jakarta 13650		wirdan@ironbird.co.id	PT. Iron Bird Transport	2180882324			ACTIVE	9	1235			02.161.014.2-014.000	20181005024620_WhatsApp Image 2018-10-05 at 14.28.09.jpeg	20181005024620_WhatsApp Image 2018-10-05 at 14.29.07.jpeg	20181005024620_WhatsApp Image 2018-10-05 at 14.29.07 (1).jpeg			850368		\N		\N			
1145	ALFI	2018-09-18 00:00:00		0001-01-01 00:00:00 BC	Ruko Puri Mutiara Blok D No. 25		endang.jkt@usiship.com	PT. Universal Shipping Indonesia	62216521395			ACTIVE	8	1236			24169450044000	20180920120924_NPWP - Rukan Puri Mutiara.pdf	20180920120924_NIB - PT. Universal Shipping Ind.pdf	20180920120924_KTP - Endang Intarti.pdf	20180920120924_Caroline Anggraini Pandu - KTP.pdf		837124		\N		\N			
1146	ALFI	2018-09-18 00:00:00		0001-01-01 00:00:00 BC	Jl. Raya Protokol Halim Perdanakusuma Jakarta		dnoraeni@agility.com	PT. Agility International	021-28529193			ACTIVE	9	1237			12-2334909-02						957328		\N		\N			
1147	ALFI	2018-09-18 00:00:00		0001-01-01 00:00:00 BC	Jl Gudang Peluru Raya B1 No 18-A Kebon baru Tebet Jakarta Selatan DKI Jakarta, RT.7/RW.1, Kb. Baru, Tebet, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12830		supadmo@indoprimatrans.com	PT. Indoprima Trans	218305574			ACTIVE	9	1238			16967192015000	20181010105859_NPWP.jpg	20181010105859_IMG.jpg	20181010105859_KTP DIRUT.jpg	20181010105859_KTP DIRUT.jpg		884404		\N		\N			
1148	system	2018-09-18 00:00:00		0001-01-01 00:00:00 BC	JL. GUDANG PELURU RAYA BLOK B1 NO.18A		supadmo@indoprimatrans.com	PT.INDOPRIMA TRANS	21 930 5574			NOT ACTIVE	9	1239			01.696.719.2-015.000	20180918043212_IMG.jpg					884404		\N	TEBET, JAKARTA SELATAN	\N	INDONESIA		
1149	system	2018-09-21 00:00:00		0001-01-01 00:00:00 BC	SECURE BUILDING, BUILDING C JL RAYA PROTOKOL HALIM PERDANAKUSUMA JAKARTA TIMUR 13610		smudiarso@agility.com	PT AGILITY INTERNATIONAL	212852900			NOT ACTIVE	9	1241			02.194.257.8-058.000	20180921100612_npwp agility intl.pdf						 DKI Jakarta	\N	KOTA ADM. JAKARTA TIMUR	\N	INDONESIA		
1150	ALFI	2018-09-24 00:00:00		0001-01-01 00:00:00 BC	PERKANTORAN PULOMAS SATU GD.IV LANTAI II ROOM 7-10 JL.JENDERAL AHMAD YANI KAV : 2 KELURAHAN KAYU PUTIH KECAMATAN PULOGADUNG JAKARTA TIMUR 13210		kos73top@gmail.com	PT.CARGO PLAZA	021-2984-7264/65			ACTIVE	9	1242			02.194.000.2-003.000						892807		\N		\N			
1151	ALFI	2018-09-24 00:00:00		0001-01-01 00:00:00 BC	Halim Trans Cargo, Gedung Graha Intirub, Jl. Cililitan Besar no. 454, Jakarta Timur, DKI Jakarta 13650		salifansyah@halimtranscargo.com	PT. HALIM TRANS CARGO	021-29379012			ACTIVE	9	1243			02.056.230.2-005.000								\N		\N			
1152	ALFI	2018-10-04 00:00:00		0001-01-01 00:00:00 BC	JL. CAKUNG INDUSTRI SELATAN 1 NO, 12 ,ROROTAN, JAKARTA UTARA 14140		mian@tungya.co.id	PT TUNGYA COLLINS FREIGHT FORWARDING	214613070			ACTIVE	9	1245									979307		\N		\N			
1153	ALFI	2018-10-05 00:00:00		0001-01-01 00:00:00 BC	Komplek Kawasan Berikat Nusantara Gedung Mawar SBU Kawasan Cakung Jalan Raya Cakung Cilincing, Jakarta Utara 14110		vtp@vtp.co.id	PT. Varuna Tirta Prakasya	021-44835651			ACTIVE	9	1249			10004935093000	20181005035639_NPWP.jpeg	20181005035639_SIUPJPT.jpeg	20181005035639_KTP.jpeg			806780		\N		\N			
1154	ALFI	2018-10-09 00:00:00		0001-01-01 00:00:00 BC	Jakarta		putribizax@gmail.com	PT. LAUTAN LUAS	98987679			ACTIVE	9	1252			987654321123456								\N		\N			
1155	system	2018-10-10 00:00:00		0001-01-01 00:00:00 BC	Jakarta		alya@gmail.com	PT.ABS	789888			NOT ACTIVE	9	1255			12.345.678.9-000.097	20181010022841_Dashboard My Cargo.png					111	 DKI Jakarta	\N	KOTA ADM. JAKARTA SELATAN	\N	Indonesia		
1156	system	2018-10-10 00:00:00		0001-01-01 00:00:00 BC	Jakarta		alya1@gmail.com	PT. Sejahtera Abadi	5464566			NOT ACTIVE	9	1256			12.365.467.8-900.000	20181010023416_npwp-perorangan.jpg						 DKI Jakarta	\N	KOTA ADM. JAKARTA UTARA	\N	Indonesia		
1157	system	2018-10-10 00:00:00		0001-01-01 00:00:00 BC	Jakarta		alia@gmail.com	PT. SINAR BINTANG	657			NOT ACTIVE	9	1257			12.345.678.9-098.700	20181010024020_NPWP.jpg						 DKI Jakarta	\N	KOTA ADM. JAKARTA SELATAN	\N	Indonesia		
1158	system	2018-10-10 00:00:00		0001-01-01 00:00:00 BC	JAKARTA		alyyamaulina@gmail.com	PT. CS IND	35353666			NOT ACTIVE	9	1258			29.479.312.9-479.419	20181010024415_WhatsApp Image 2018-10-05 at 14.47.51 (1).jpeg						 DKI Jakarta	\N	JAKARTA UTARA	\N	INDONESIA		
1159	ALFI	2018-10-10 00:00:00		0001-01-01 00:00:00 BC	jakarta		alia@gmail.com	PT. ABC	766786778			ACTIVE	9	1259				20181011105257_npwp-perorangan.jpg	20181011105257_SIUP.jpg	20181011105257_ktp.jpg					\N		\N			
1160	system	2018-10-10 00:00:00		0001-01-01 00:00:00 BC	Jakarta		alia@gmail.com	PT. ALT				NOT ACTIVE	9	1260			12.345.678.0-098.788	20181010024845_npwp-perorangan.jpg						 DKI Jakarta	\N	KOTA ADM. JAKARTA SELATAN	\N	Indonesia		
1161	system	2018-10-10 00:00:00		0001-01-01 00:00:00 BC	Jakarta		alia@gmail.com	PT. TEST	45			NOT ACTIVE	9	1261			12.345.671.2-400.000	20181010025352_npwp-perorangan.jpg						 DKI Jakarta	\N	KOTA ADM. JAKARTA UTARA	\N	Indonesia		
1162	system	2018-10-10 00:00:00		0001-01-01 00:00:00 BC	Jakarta		allyya@gmail.com	PT. BGB	35463626			NOT ACTIVE	9	1264			35.653.253.5-345.455	20181010031324_WhatsApp Image 2018-10-05 at 14.47.51 (1).jpeg						 DKI Jakarta	\N	KOTA ADM. JAKARTA UTARA	\N	Indonesia		
1163	system	2018-10-10 00:00:00		0001-01-01 00:00:00 BC	Jakarta		allyyaa@gmail.com	PT. HJK	2144585975			NOT ACTIVE	9	1265			12.443.657.6-544.534	20181010031916_NPWP.PDF						 DKI Jakarta	\N	KOTA ADM. JAKARTA UTARA	\N	Indonesia		
1164	system	2018-10-10 00:00:00		0001-01-01 00:00:00 BC	Jakarta		allyyaa@gmail.com	PT. HOO	98787868799			NOT ACTIVE	9	1267			21.324.657.8-654.333	20181010033018_WhatsApp Image 2018-10-05 at 14.47.51 (1).jpeg						 DKI Jakarta	\N	KOTA ADM. JAKARTA UTARA	\N	Indonesia		
1165	system	2018-10-10 00:00:00		0001-01-01 00:00:00 BC	Jakarta		allyyaa@gmail.com	PT. CKC	2978384892			NOT ACTIVE	9	1268			21.345.678.6-543.456	20181010033557_WhatsApp Image 2018-10-05 at 14.47.51 (1).jpeg					1111	 DKI Jakarta	\N	KOTA ADM. JAKARTA UTARA	\N	Indonesia		
1166	system	2018-10-10 00:00:00		0001-01-01 00:00:00 BC	Jakarta		aliah@gmail.com	PT. KIL	72637642821			NOT ACTIVE	9	1269			77.677.575.7-564.675	20181010034532_WhatsApp Image 2018-10-05 at 14.47.51 (1).jpeg						 DKI Jakarta	\N	KOTA ADM. JAKARTA UTARA	\N	Indonesia		
1167	system	2018-10-10 00:00:00		0001-01-01 00:00:00 BC	Jakarta		aallyyaa@gmail.com	PT. THU	235346			NOT ACTIVE	9	1270			23.456.789.9-234.456	20181010065833_Slide1.PNG						 DKI Jakarta	\N	KOTA ADM. JAKARTA UTARA	\N	Indonesia		
1168	system	2018-10-11 00:00:00		0001-01-01 00:00:00 BC	Secure Building,Building C+, Jl.Raya Protokol Halim Perdana Kusuma, 		ymaisaroh@agility.com	PT.Agility	2128529000			NOT ACTIVE	8	1274			01.571.411.6-005.000						957328	INDONESIA	\N	JAKARTA TIMUR	\N	INDONESIA		
1169	system	2018-10-14 00:00:00		0001-01-01 00:00:00 BC	Jakarta		putri.d0001@gmail.com	PT ABS	90987890			NOT ACTIVE	9	1278			19.890.989.0-989.000	20181014070428_npwp-perorangan.jpg						 DKI Jakarta	\N	KOTA ADM. JAKARTA BARAT	\N	Indonesia		
1170	system	2018-10-14 00:00:00		0001-01-01 00:00:00 BC	Jakarta		putri.d0001@gmail.com	PT. Sejahtera Jaya	890987890			NOT ACTIVE	9	1279			12.000.898.7-900.000	20181014071331_npwp-perorangan.jpg						 DKI Jakarta	\N	KOTA ADM. JAKARTA SELATAN	\N	Indonesia		
1171	system	2018-10-14 00:00:00		0001-01-01 00:00:00 BC	Jakarta		putri.d0001@gmail.com	PT. Sejatera Abadi	78989089			NOT ACTIVE	9	1280			12.768.989.0-878.000	20181014071803_npwp-perorangan.jpg						 DKI Jakarta	\N	KOTA ADM. JAKARTA TIMUR	\N	Indonesia		
1172	ALFI	2018-10-30 00:00:00		0001-01-01 00:00:00 BC	Komplek Kawasan Berikat Nusantara Gedung Mawar SBU Kawasan Cakung Jalan Raya Cakung Cilincing, Jakarta Utara 14110		vtp@vtp.co.id	PT. Varuna Tirta Prakasya	021-44835651			ACTIVE	9	1281			10004935093000	20181030010142_NPWP.jpeg	20181030010142_SIUPJPT.jpeg	20181030010142_KTP.jpeg			806780		\N		\N			
1173	ALFI	2018-10-30 00:00:00		0001-01-01 00:00:00 BC	Komplek Kawasan Berikat Nusantara Gedung Mawar SBU Kawasan Cakung Jalan Raya Cakung Cilincing, Jakarta Utara 14110		saad@vtp.co.id	PT. Varuna Tirta Prakasya	021-44835651			ACTIVE	9	1282											\N		\N			
1174	ALFI	2018-10-30 00:00:00		0001-01-01 00:00:00 BC	Komplek Kawasan Berikat Nusantara Gedung Mawar SBU Kawasan Cakung Jalan Raya Cakung Cilincing, Jakarta Utara 14110		saad@vtp.co.id	PT. Varuna Tirta Prakasya	021-44835651			ACTIVE	9	1283			10004935093000								\N		\N			
1175	system	2018-11-14 00:00:00		0001-01-01 00:00:00 BC	Jl. Raya Cakung Cilincing KM.3 Jakarta 13910 - Indonesia		heni.baeti@bsa-logistics.co.id	BSA Logistics	214603401			ACTIVE	9	1285			01.670.200.3-058.000	20181114065505_20181114042200_NPWP PT. BSA0001.pdf		20181116111559_20181114_162511.jpg			847436	 DKI Jakarta	\N	KOTA ADM. JAKARTA UTARA	\N	Indonesia		
1176	system	2018-11-15 00:00:00	User_ff19	2019-09-23 00:00:00	Graha Paramita Building 12th Floor Jl Denpasar Raya Blok D-2 Kav.8, Kuningan Jakarta 12940		rizza.fahriza@benline.co.id	PT. Bahari Eka Nusantara	021 – 2522110		2309190001	ACTIVE	2	1292			02.433.881.6-011.000								\N		\N			
1177	system	2018-11-16 00:00:00		0001-01-01 00:00:00 BC	RUKAN PURI NIAGA 1 BLOK K.7/3 KEMBANGAN SELATAN,KEMBANGAN JAKARTA		jepririot@yahoo.co.id	PT BRAHMAN FARM	2158303128			NOT ACTIVE	1	1293			03.078.258.5-086.000	20181116065421_NPWP.pdf	20181116065421_SIUP.pdf	20181116065421_KTP Vania.pdf				 DKI Jakarta	\N	KOTA ADM. JAKARTA BARAT 	\N	JAKARTA		
1206	system	2019-08-05 00:00:00		0001-01-01 00:00:00 BC	The Prime Office Suites 7th Floor, Suites C Jalan Yos Sudarso Kav 30, Sunter Jaya - Jakarta Utara 14350		beny@gatotkaca.co.id	PT. GATOTKACA  TRANS SYSTEMINDO	22655073			ACTIVE	8	1355			02.040.422.4-435.000							DKI Jakarta Jakarta	\N	Jakarta Utara	\N	Indonesia		
1178	ilcs	2017-02-20 00:00:00	ilcs	2017-02-20 00:00:00	Jl. Pemuda No.27, Kb. Pedes, Tanah Sereal, Kota Bogor, Jawa Barat 16162, Indonesia		putridiajeng987@gmail.com	PT. CARGO OWNER	62251322071			ACTIVE	9	442			33.242.424.2-424.234	20181123102029_approve todak bisa setelah extend do.PNG	20181123102029_berubah menjadi do request lagi setelah melakukan do extention.PNG	20181123102029_BUTTON SEARCH DI DELIVERY ORDER TIDAK BERFUNGSI.PNG	20181123102029_approve todak bisa setelah extend do.PNG	20181123102029_berubah menjadi do request lagi setelah melakukan do extention.PNG			\N		\N			
1179	system	2018-11-23 00:00:00		0001-01-01 00:00:00 BC	JAKARTA		alyamaulina9b@yahoo.com	PT. FORWARDERKU				NOT ACTIVE	8	1294			35.456.754.3-567.865	20181123104602_pexels-photo.jpg						DKI Jakarta	\N	NORTH JAKARTA	\N	Indonesia		
1180	system	2016-06-27 00:00:00		0001-01-01 00:00:00 BC	Wisma Aldiron, Pancoran, Jakarta		ASunarya@agility.com	PT. Agility International	217949272			NON ACTIVE	9	3			02.194.257.8-058.000	20181128045034_9861066-textura-de-pared-de-piedra-para-los-diseñadores-y-artistas-3d.jpg	20181128045034_9861066-textura-de-pared-de-piedra-para-los-diseñadores-y-artistas-3d.jpg	20181128045034_9861066-textura-de-pared-de-piedra-para-los-diseñadores-y-artistas-3d.jpg	20181128045034_9861066-textura-de-pared-de-piedra-para-los-diseñadores-y-artistas-3d.jpg	20181128045034_9861066-textura-de-pared-de-piedra-para-los-diseñadores-y-artistas-3d.jpg	957328		\N		\N			
1181	system	2016-06-27 00:00:00	Lingga Pradana	2019-03-18 00:00:00	Mega Plaza Kav. C, No. 3 Kuningan, Jl. HR Rasuna Said, Kecamatan Setiabudi, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta, Indonesia		dwitotochandra@evergreen-shipping.co.id	PT. Evergreen Shipping Agency Indonesia	215212310		1803190002	ACTIVE	2	5	ptp	icargo	12-2334909-04	20181128083302_ilcs.mp4	20181128083302_9861066-textura-de-pared-de-piedra-para-los-diseñadores-y-artistas-3d.jpg	20181128083302_9861066-textura-de-pared-de-piedra-para-los-diseñadores-y-artistas-3d.jpg	20181128083302_9861066-textura-de-pared-de-piedra-para-los-diseñadores-y-artistas-3d.jpg	20181128083302_9861066-textura-de-pared-de-piedra-para-los-diseñadores-y-artistas-3d.jpg			\N		\N			
1182	system	2018-12-12 00:00:00		0001-01-01 00:00:00 BC	jakarta		o2969523@nwytg.net	pt.testing cibo				NOT ACTIVE	9	1314			34.568.326.8-739.748	20181212052911_NPWP.PDF						 DKI Jakarta	\N	KOTA ADM. JAKARTA UTARA	\N	indonesia		
1183	system	2018-12-12 00:00:00		0001-01-01 00:00:00 BC	jakarta		andhi.noerdianto@gmail.com	pt.andhi  jaya				NOT ACTIVE	9	1315			62.530.984.3-738.257	20181212053615_NPWP.PDF						 DKI Jakarta	\N	KOTA ADM. JAKARTA UTARA	\N	indonesia		
1184	system	2019-01-15 00:00:00		0001-01-01 00:00:00 BC	PURI INDAH FINANCIAL TOWER LT. 5, JALAN PURI LINGKAR DALAM BLOK T-8, JAKARTA BARAT 11610.		wijaya.gondokusumo@sml.id	PT SURYAMAS LOGISTIK	021 5049 8888			NOT ACTIVE	9	1331			73.835.548.6-086.000	20190115111611_NPWP SML.tif	20190115112608_SIUJPT SML 2017.pdf	20190115112608_KTP WIWIT.pdf	20190115112608_KTP BAPAK.pdf	20190115112608_SURAT  KUASA DELIVERY ORDER SML_.pdf		 DKI Jakarta	\N	KOTA ADM. JAKARTA BARAT	\N	INDONESIA		
1185	system	2019-01-25 00:00:00		0001-01-01 00:00:00 BC	JL.MAJAPAHIT 34 NO.30-32 JAKARTA PUSAT		sujarwo@larsen.co.id	PT.LAYAR SENTOSA SHIPPING	3854781			ACTIVE	2	1333			01.385.991.3-028.000	20190125024709_NPWP.pdf						 DKI Jakarta	\N	KOTA ADM. JAKARTA PUSAT	\N	INDONESIA		
1186	system	2019-02-07 00:00:00		0001-01-01 00:00:00 BC	ded		support@mail-apps.com	1212	54545			NOT ACTIVE	9	1334			23.232.323.3-232.323							sffs	\N	fd	\N	vvrvrvvr		
1187	system	2019-03-12 00:00:00		0001-01-01 00:00:00 BC	Komplek Sentra Bisnis Harapan Indah Blok SS10 No. 03 Kota Bekasi, Jawa Barat		admin3@wiratrans.co.id	PT. WIRATRANS GLOBAL	2188385405			NOT ACTIVE	8	1336			73.317.915.4-407.000	20190322050245_Tax Id company.pdf	20190322050245_SIUP WIRATRANS.pdf	20190322050245_WhatsApp Image 2019-01-29 at 20.52.07.jpeg	20190322050245_KTPRUHYAT.pdf			 Jawa Barat	\N	KOTA BEKASI	\N	Indonesia		
1188	system	2019-03-14 00:00:00		0001-01-01 00:00:00 BC	Komplek Sentra Bisnis Harapan Indah Blok SS10 No. 03 Kota Bekasi, Jawa Barat		prtmjl7@gmail.com		2188380146			NOT ACTIVE	9	1337			73.317.915.4-407.000	20190314035411_Tax Id company.pdf						 Jawa Barat	\N	KOTA BEKASI	\N	INDONESIA		
1189	system	2019-03-20 00:00:00		0001-01-01 00:00:00 BC	Komp. Pratama Green Apple Jl. Al Markaz Block CD 2 No. 3, Makassar South Sulawesi, Indonesia 		ilyascabatrans@gmail.com	PT. JPT CATURMITRA ADHIKARSA BATARA TRANSPORTINDO				NOT ACTIVE	9	1338			02.383.515.0-805.000							Sulawesi Selatan	\N	Makassar	\N	Indonesia		
1190	system	2019-04-13 00:00:00		0001-01-01 00:00:00 BC			emkl4@ricakusuma.co.id	PT. Ricakusuma Lestari Abadi				NOT ACTIVE	9	1340			02.094.539.0-027.000	20190413101406_NPWP PT  RICAKUSUMA.pdf							\N	Jakarta	\N	Indonesia		
1191	system	2019-04-13 00:00:00		0001-01-01 00:00:00 BC	GRAHA KANA LT.2, JL.ANGKASA 1 BLOK.B16 KAV.4, KEMAYORAN, JAKARTA PUSAT		umurillahriri@gmail.com	PT. RICAKUSUMA LESTARI ABADI				NOT ACTIVE	9	1341			02.094.539.0-027.000	20190413103811_NPWP PT. RICAKUSUMA0001.PDF						DKI Jakarta	\N	Jakarta Pusat-Kota Jakarta Pusat	\N	Indonesia		
1192	system	2019-05-10 00:00:00		0001-01-01 00:00:00 BC	Jl. Antara No. 5-7 Pasar Baru, Sawah Besar		bpcglass@gmail.com	PT. Bali Permai Crafindo	213451517			NOT ACTIVE	1	1342			01.320.057.1-075.000	20190510023526_NPWP BPC TERBARU.pdf						 DKI Jakarta	\N	KOTA ADM. JAKARTA PUSAT	\N	Indonesia		
1193	ALFI	2019-05-13 00:00:00		0001-01-01 00:00:00 BC	TEST		putridiajeng000@gmail.com	PT RUMIT	70850380			ACTIVE	9	1343			6749837834								\N		\N			
1194	ALFI	2019-05-14 00:00:00		0001-01-01 00:00:00 BC	bekasi		putrilolo68@gmail.com	PT. Cargo Owner	3424545435			ACTIVE	9	1344			7676686786786								\N		\N			
1195	ALFI	2019-05-14 00:00:00		0001-01-01 00:00:00 BC	jakarta		putridiajeng987@gmail.com	PT. Cargo Owner	812345678			ACTIVE	9	1345											\N		\N			
1196	ALFI	2019-05-14 00:00:00		0001-01-01 00:00:00 BC	tes		putridiajeng000@gmail.com	PT. LAUT LEPAS	88025803458			ACTIVE	9	1346											\N		\N			
1197	ALFI	2019-05-14 00:00:00		0001-01-01 00:00:00 BC	tes		putridiajeng000@gmail.com	PT. LAUT LEPAS	88025803458			ACTIVE	9	1347											\N		\N			
1198	ALFI	2019-05-14 00:00:00		0001-01-01 00:00:00 BC	test		notif.mycargo1@gmail.com	PT. TEEST	547547757			ACTIVE	9	1348											\N		\N			
1199	ALFI	2019-05-14 00:00:00		0001-01-01 00:00:00 BC	test		putridiajeng987@gmail.com	PT. TESt				ACTIVE	9	1349											\N		\N			
1200	ALFI	2019-05-14 00:00:00		0001-01-01 00:00:00 BC	jakarta		putridiajeng987@gmail.com	PT. Cargo Owner	812345678			ACTIVE	9	1350											\N		\N			
1201	ALFI	2019-05-14 00:00:00		0001-01-01 00:00:00 BC			notif.mycargo1@gmail.com	PT. 123				ACTIVE	9	1351											\N		\N			
1202	ALFI	2019-05-14 00:00:00		0001-01-01 00:00:00 BC								ACTIVE	9	1352											\N		\N			
1203	ALFI	2019-05-16 00:00:00		0001-01-01 00:00:00 BC	Jl. Jakarta Utara		panduwidi86@gmail.com	PT. Intralogistik	211234567890			ACTIVE	9	1356			431311								\N		\N			
1204	ALFI	2019-05-16 00:00:00		0001-01-01 00:00:00 BC	Jl. Jaksa		panduwidi86@gmail.com	PT. Intra Cargo	2132131231			ACTIVE	9	1357			312312313								\N		\N			
1205	system	2019-05-21 00:00:00		0001-01-01 00:00:00 BC	enggano		roland@gatotkaca.co.id	PT Gatotkaca Trans Systemindo	211234567			NOT ACTIVE	9	1358			00.000.000.0-000.000							 DKI Jakarta	\N	KOTA ADM. JAKARTA PUSAT	\N	indonesia		
\.


--
-- Data for Name: coreor; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.coreor (id, createby, createdate, updateby, updatedate, file_name, status, company_id) FROM stdin;
1	demo_sl	2018-11-28 00:00:00		\N	281118175723_EGLV237800059788.TRIAL.txt	ON PROCESS	451
2	demo_sl	2018-11-28 00:00:00		\N	281118175753_agility.txt	ON PROCESS	451
\.


--
-- Data for Name: detail_container; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.detail_container (id, no_container, delivery_id, createby, updateby, createdate, updatedate) FROM stdin;
1	BEAU4394595	2286	Yuliawati		2018-02-15 00:00:00	0001-01-01 00:00:00 BC
2	BMOU5764364	2290	Yuliawati		2018-02-15 00:00:00	0001-01-01 00:00:00 BC
3	EISU3999959	2290	Yuliawati		2018-02-15 00:00:00	0001-01-01 00:00:00 BC
4	TRHU1192150	2452	Taufik Ariesandy		2018-02-27 00:00:00	0001-01-01 00:00:00 BC
5	TRHU3391382	2452	Taufik Ariesandy		2018-02-27 00:00:00	0001-01-01 00:00:00 BC
6	MAGU2464392	2452	Taufik Ariesandy		2018-02-27 00:00:00	0001-01-01 00:00:00 BC
7	TEMU0511515	2452	Taufik Ariesandy		2018-02-27 00:00:00	0001-01-01 00:00:00 BC
8	OCGU2081873	2452	Taufik Ariesandy		2018-02-27 00:00:00	0001-01-01 00:00:00 BC
9	TRHU2844463	2452	Taufik Ariesandy		2018-02-27 00:00:00	0001-01-01 00:00:00 BC
10	TCLU9132264	2461	Taufik Ariesandy		2018-02-28 00:00:00	0001-01-01 00:00:00 BC
11	DRYU2357746	2465	Lingga Pradana		2018-03-01 00:00:00	0001-01-01 00:00:00 BC
12	MAGU2464094	2468	Lingga Pradana		2018-03-01 00:00:00	0001-01-01 00:00:00 BC
13	TCLU3860339	2468	Lingga Pradana		2018-03-01 00:00:00	0001-01-01 00:00:00 BC
14	MAGU2322700	2472	Lingga Pradana		2018-03-01 00:00:00	0001-01-01 00:00:00 BC
15	DRYU2669976	2479	Lingga Pradana		2018-03-01 00:00:00	0001-01-01 00:00:00 BC
16	DRYU4167914	2483	Lingga Pradana		2018-03-02 00:00:00	0001-01-01 00:00:00 BC
17	EGHU1034590	2483	Lingga Pradana		2018-03-02 00:00:00	0001-01-01 00:00:00 BC
18	EISU1768478	2483	Lingga Pradana		2018-03-02 00:00:00	0001-01-01 00:00:00 BC
19	EMCU1303863	2483	Lingga Pradana		2018-03-02 00:00:00	0001-01-01 00:00:00 BC
20	EMCU1335541	2483	Lingga Pradana		2018-03-02 00:00:00	0001-01-01 00:00:00 BC
21	FSCU4981450	2483	Lingga Pradana		2018-03-02 00:00:00	0001-01-01 00:00:00 BC
22	HMCU1099644	2483	Lingga Pradana		2018-03-02 00:00:00	0001-01-01 00:00:00 BC
23	IMTU1066117	2483	Lingga Pradana		2018-03-02 00:00:00	0001-01-01 00:00:00 BC
24	TCLU4322220	2483	Lingga Pradana		2018-03-02 00:00:00	0001-01-01 00:00:00 BC
25	TCLU4341810	2483	Lingga Pradana		2018-03-02 00:00:00	0001-01-01 00:00:00 BC
26	TGCU2021085	2496	Lingga Pradana		2018-03-06 00:00:00	0001-01-01 00:00:00 BC
27	EITU0462173	2499	Lingga Pradana		2018-03-07 00:00:00	0001-01-01 00:00:00 BC
28	TRHU1208425	2499	Lingga Pradana		2018-03-07 00:00:00	0001-01-01 00:00:00 BC
29	FCIU9790523 	2503	Lingga Pradana		2018-03-08 00:00:00	0001-01-01 00:00:00 BC
30	EISU9477458 	2503	Lingga Pradana		2018-03-08 00:00:00	0001-01-01 00:00:00 BC
31	MAGU5477074	2503	Lingga Pradana		2018-03-08 00:00:00	0001-01-01 00:00:00 BC
32	EMCU3962807	2510	Lingga Pradana		2018-03-09 00:00:00	0001-01-01 00:00:00 BC
33	GLDU5741080	2510	Lingga Pradana		2018-03-09 00:00:00	0001-01-01 00:00:00 BC
34	TCLU7843860	2515	Yuliawati		2018-03-13 00:00:00	0001-01-01 00:00:00 BC
35	TEMU8890837	2515	Yuliawati		2018-03-13 00:00:00	0001-01-01 00:00:00 BC
36	DRYU9131853	2519	Yuliawati		2018-03-13 00:00:00	0001-01-01 00:00:00 BC
37	TEMU8684100	2519	Yuliawati		2018-03-13 00:00:00	0001-01-01 00:00:00 BC
38	EITU1476818	2523	Yuliawati		2018-03-13 00:00:00	0001-01-01 00:00:00 BC
39	EITU1935012	2523	Yuliawati		2018-03-13 00:00:00	0001-01-01 00:00:00 BC
40	TEMU6220754	2523	Yuliawati		2018-03-13 00:00:00	0001-01-01 00:00:00 BC
41	EISU7104630	2528	Yuliawati		2018-03-13 00:00:00	0001-01-01 00:00:00 BC
42		2528	Yuliawati		2018-03-13 00:00:00	0001-01-01 00:00:00 BC
43	kakaskas	2533	coff01		2018-03-14 00:00:00	0001-01-01 00:00:00 BC
44	TCNU3361300	2537	Taufik Ariesandy		2018-03-14 00:00:00	0001-01-01 00:00:00 BC
45	EITU1595669	2540	Taufik Ariesandy		2018-03-14 00:00:00	0001-01-01 00:00:00 BC
46	EGSU9114280	2545	Yuliawati		2018-03-14 00:00:00	0001-01-01 00:00:00 BC
47	EITU1369770	2545	Yuliawati		2018-03-14 00:00:00	0001-01-01 00:00:00 BC
48		2545	Yuliawati		2018-03-14 00:00:00	0001-01-01 00:00:00 BC
49	SEGU6000265	2551	Lingga Pradana		2018-03-16 00:00:00	0001-01-01 00:00:00 BC
50	SEGU4854521	2556	Yuliawati		2018-03-19 00:00:00	0001-01-01 00:00:00 BC
51		2556	Yuliawati		2018-03-19 00:00:00	0001-01-01 00:00:00 BC
52	TCKU3314794	2560	Lingga Pradana		2018-03-19 00:00:00	0001-01-01 00:00:00 BC
53	EITU1441180	2566	Lingga Pradana		2018-03-19 00:00:00	0001-01-01 00:00:00 BC
54	DRYU9832365	2566	Lingga Pradana		2018-03-19 00:00:00	0001-01-01 00:00:00 BC
55	MRKU1234567	2583	coff01		2018-03-21 00:00:00	0001-01-01 00:00:00 BC
56	MRKU1234567	2587	coff01		2018-03-21 00:00:00	0001-01-01 00:00:00 BC
57	TCLU7917849	2590	Taufik Ariesandy		2018-03-21 00:00:00	0001-01-01 00:00:00 BC
58	TCNU1646900	2596	Yuliawati		2018-03-22 00:00:00	0001-01-01 00:00:00 BC
59	TRHU3343933	2596	Yuliawati		2018-03-22 00:00:00	0001-01-01 00:00:00 BC
60	EITU1040280	2600	Yuliawati		2018-03-22 00:00:00	0001-01-01 00:00:00 BC
61	HMCU9176881	2600	Yuliawati		2018-03-22 00:00:00	0001-01-01 00:00:00 BC
62	EITU1739262	2604	Yuliawati		2018-03-22 00:00:00	0001-01-01 00:00:00 BC
63	GLDU5741692	2604	Yuliawati		2018-03-22 00:00:00	0001-01-01 00:00:00 BC
64	EISU9151815	2608	Yuliawati		2018-03-22 00:00:00	0001-01-01 00:00:00 BC
65	EMCU6019037	2608	Yuliawati		2018-03-22 00:00:00	0001-01-01 00:00:00 BC
66	MAGU2206320	2612	Yuliawati		2018-03-22 00:00:00	0001-01-01 00:00:00 BC
67	TRLU9140720	2612	Yuliawati		2018-03-22 00:00:00	0001-01-01 00:00:00 BC
68	HMCU9091887	2616	Yuliawati		2018-03-22 00:00:00	0001-01-01 00:00:00 BC
69	MAGU5431880	2616	Yuliawati		2018-03-22 00:00:00	0001-01-01 00:00:00 BC
70	PUTRI1145	2620	putri987		2018-03-23 00:00:00	0001-01-01 00:00:00 BC
71	MRKU2345678	2657	coff01		2018-03-29 00:00:00	0001-01-01 00:00:00 BC
72	BMOU5129841	2660	Lingga Pradana		2018-04-03 00:00:00	0001-01-01 00:00:00 BC
73	TCLU8340176	2660	Lingga Pradana		2018-04-03 00:00:00	0001-01-01 00:00:00 BC
74	EITU1027680	2660	Lingga Pradana		2018-04-03 00:00:00	0001-01-01 00:00:00 BC
75	TLLU4008731	2660	Lingga Pradana		2018-04-03 00:00:00	0001-01-01 00:00:00 BC
76	HMCU3008366	2666	Lingga Pradana		2018-04-04 00:00:00	0001-01-01 00:00:00 BC
77	EITU0247951	2669	Lingga Pradana		2018-04-04 00:00:00	0001-01-01 00:00:00 BC
78	TEMU8403918	2669	Lingga Pradana		2018-04-04 00:00:00	0001-01-01 00:00:00 BC
79	EMCU1375210	2673	Lingga Pradana		2018-04-04 00:00:00	0001-01-01 00:00:00 BC
80	HCMU9155596	2680	Yuliawati		2018-04-10 00:00:00	0001-01-01 00:00:00 BC
81	EISU2248779	2680	Yuliawati		2018-04-10 00:00:00	0001-01-01 00:00:00 BC
82	EITU1003585	2685	Yuliawati		2018-04-10 00:00:00	0001-01-01 00:00:00 BC
83	EITU1629563	2688	Yuliawati		2018-04-10 00:00:00	0001-01-01 00:00:00 BC
84	TCLU3495149	2688	Yuliawati		2018-04-10 00:00:00	0001-01-01 00:00:00 BC
85	EGHU3161830	2719	Lingga Pradana		2018-04-11 00:00:00	0001-01-01 00:00:00 BC
86	MRKU0000001	2722	coff01		2018-04-11 00:00:00	0001-01-01 00:00:00 BC
87	MRKU0000002	2722	coff01		2018-04-11 00:00:00	0001-01-01 00:00:00 BC
88	TCNU1647764	2728	UnileverIndonesia		2018-04-12 00:00:00	0001-01-01 00:00:00 BC
89	EITU1416260	2728	UnileverIndonesia		2018-04-12 00:00:00	0001-01-01 00:00:00 BC
90	EGSU9125284	2728	UnileverIndonesia		2018-04-12 00:00:00	0001-01-01 00:00:00 BC
91	EITU0109459	2728	UnileverIndonesia		2018-04-12 00:00:00	0001-01-01 00:00:00 BC
92	IMTU1065208	2762	Lingga Pradana		2018-04-13 00:00:00	0001-01-01 00:00:00 BC
93	TCLU8655018	2766	Taufik Ariesandy		2018-04-13 00:00:00	0001-01-01 00:00:00 BC
94	DRYU9795770	2772	Yuliawati		2018-04-17 00:00:00	0001-01-01 00:00:00 BC
95	EISU9299016	2772	Yuliawati		2018-04-17 00:00:00	0001-01-01 00:00:00 BC
96	HMCU3058393	2841	Lingga Pradana		2018-04-23 00:00:00	0001-01-01 00:00:00 BC
97	EGHU3150141	2844	Lingga Pradana		2018-04-24 00:00:00	0001-01-01 00:00:00 BC
98	EISU9338489	2847	Yuliawati		2018-04-25 00:00:00	0001-01-01 00:00:00 BC
99	BMOU1074611	2847	Yuliawati		2018-04-25 00:00:00	0001-01-01 00:00:00 BC
100	TCNU5279402	2847	Yuliawati		2018-04-25 00:00:00	0001-01-01 00:00:00 BC
101	no1212	2995	demo_ilcs		2018-10-31 00:00:00	0001-01-01 00:00:00 BC
102	1111111	3350	demo_ilcs		2019-01-17 00:00:00	0001-01-01 00:00:00 BC
103	JIKL09090JIKL	3354	demo_ilcs		2019-01-18 00:00:00	0001-01-01 00:00:00 BC
104	CO87070	3386	demo_ilcs		2019-01-23 00:00:00	0001-01-01 00:00:00 BC
105	CONT423880	3404	demo_ilcs		2019-01-25 00:00:00	0001-01-01 00:00:00 BC
106	REGU5112871	3408	Lingga Pradana		2019-01-29 00:00:00	0001-01-01 00:00:00 BC
107	CAXU9925237	3412	Lingga Pradana		2019-01-29 00:00:00	0001-01-01 00:00:00 BC
108	REGU5088559	3416	Lingga Pradana		2019-01-29 00:00:00	0001-01-01 00:00:00 BC
109	REGU3265280	3420	Lingga Pradana		2019-01-29 00:00:00	0001-01-01 00:00:00 BC
110	REGU5084229	3420	Lingga Pradana		2019-01-29 00:00:00	0001-01-01 00:00:00 BC
111	CAIU2846371	3426	Lingga Pradana		2019-01-31 00:00:00	0001-01-01 00:00:00 BC
112	DFSU7771343	3430	Lingga Pradana		2019-01-31 00:00:00	0001-01-01 00:00:00 BC
113	REGU5089154	3434	Lingga Pradana		2019-01-31 00:00:00	0001-01-01 00:00:00 BC
114	BSIU9615467	3438	Lingga Pradana		2019-01-31 00:00:00	0001-01-01 00:00:00 BC
115	REGU5077908	3438	Lingga Pradana		2019-01-31 00:00:00	0001-01-01 00:00:00 BC
116	OCGU8004046	3443	Lingga Pradana		2019-01-31 00:00:00	0001-01-01 00:00:00 BC
117	REGU3260777	3447	Heni		2019-01-31 00:00:00	0001-01-01 00:00:00 BC
118	REGU3234211	3447	Heni		2019-01-31 00:00:00	0001-01-01 00:00:00 BC
119	REGU3234633	3452	Heni		2019-01-31 00:00:00	0001-01-01 00:00:00 BC
120	CONT2492	3534	demo_ilcs		2019-02-18 00:00:00	0001-01-01 00:00:00 BC
121	EGHU3186144	3542	Lingga Pradana		2019-02-21 00:00:00	0001-01-01 00:00:00 BC
122	REGU3235373	3546	Lingga Pradana		2019-02-21 00:00:00	0001-01-01 00:00:00 BC
123	FSCU9536307	3550	Lingga Pradana		2019-02-21 00:00:00	0001-01-01 00:00:00 BC
124	SEGU4329652	3550	Lingga Pradana		2019-02-21 00:00:00	0001-01-01 00:00:00 BC
125	TEMU6077540	3550	Lingga Pradana		2019-02-21 00:00:00	0001-01-01 00:00:00 BC
126	CAIU9660460	3556	Lingga Pradana		2019-02-21 00:00:00	0001-01-01 00:00:00 BC
127	OCGU8004410	3556	Lingga Pradana		2019-02-21 00:00:00	0001-01-01 00:00:00 BC
128	TCNU2395081	3556	Lingga Pradana		2019-02-21 00:00:00	0001-01-01 00:00:00 BC
129	TCNU8584225	3556	Lingga Pradana		2019-02-21 00:00:00	0001-01-01 00:00:00 BC
130	REGU5108362	3563	Lingga Pradana		2019-02-21 00:00:00	0001-01-01 00:00:00 BC
131	TCKU6108307	3567	Lingga Pradana		2019-02-21 00:00:00	0001-01-01 00:00:00 BC
132	TCNU7621896	3618	Lingga Pradana		2019-03-05 00:00:00	0001-01-01 00:00:00 BC
133	CAIU9733593	3622	Lingga Pradana		2019-03-05 00:00:00	0001-01-01 00:00:00 BC
134	GESU5029249	3626	Lingga Pradana		2019-03-05 00:00:00	0001-01-01 00:00:00 BC
135	CAIU8186906	3630	Lingga Pradana		2019-03-05 00:00:00	0001-01-01 00:00:00 BC
136	TGHU6265236	3630	Lingga Pradana		2019-03-05 00:00:00	0001-01-01 00:00:00 BC
137	GESU6545954	3635	Lingga Pradana		2019-03-05 00:00:00	0001-01-01 00:00:00 BC
138	KMBU9182380	3635	Lingga Pradana		2019-03-05 00:00:00	0001-01-01 00:00:00 BC
139	SIPU4600238	3640	Lingga Pradana		2019-03-05 00:00:00	0001-01-01 00:00:00 BC
140	BSLK99453	3614	demo_ilcs		2019-03-05 00:00:00	0001-01-01 00:00:00 BC
141	CTR9823	1594	forwarding1		2018-01-08 00:00:00	0001-01-01 00:00:00 BC
142	MRKU8254346	1599	forwarding1		2018-01-08 00:00:00	0001-01-01 00:00:00 BC
143	MSKU7654123	1599	forwarding1		2018-01-08 00:00:00	0001-01-01 00:00:00 BC
144	EMCU9888137	1734	Lingga Pradana		2018-02-06 00:00:00	0001-01-01 00:00:00 BC
145	test container	1711	coff01		2018-02-06 00:00:00	0001-01-01 00:00:00 BC
146	test container 2	1711	coff01		2018-02-06 00:00:00	0001-01-01 00:00:00 BC
147	909090909	1716	mahesa		2018-02-06 00:00:00	0001-01-01 00:00:00 BC
148	asasa	1739	coff01		2018-02-12 00:00:00	0001-01-01 00:00:00 BC
149	container9239239	1890	Yuliawati		2018-02-13 00:00:00	0001-01-01 00:00:00 BC
150	KDKSDKSKDS	2110	co01		2018-02-14 00:00:00	0001-01-01 00:00:00 BC
151	KJDHFKDFHHFjj	2110	co01	shippingline	2018-02-14 00:00:00	2018-02-15 00:00:00
152	TCNU123456	2116	Ade Sunarya		2018-02-14 00:00:00	0001-01-01 00:00:00 BC
153	abcu1234567	2119	Lingga Pradana		2018-02-14 00:00:00	0001-01-01 00:00:00 BC
154	abcd2222222	2119	Lingga Pradana		2018-02-14 00:00:00	0001-01-01 00:00:00 BC
155	test1	2277	Yuliawati		2018-02-15 00:00:00	0001-01-01 00:00:00 BC
156	test2	2277	Yuliawati		2018-02-15 00:00:00	0001-01-01 00:00:00 BC
157	EITU1023405	2282	Yuliawati		2018-02-15 00:00:00	0001-01-01 00:00:00 BC
158	TEMU8559813	3108	Lingga Pradana		2018-11-19 00:00:00	0001-01-01 00:00:00 BC
159	REGU3256925	3108	Lingga Pradana		2018-11-19 00:00:00	0001-01-01 00:00:00 BC
160	CONT77777	3114	demo_ilcs		2018-11-21 00:00:00	0001-01-01 00:00:00 BC
161	7787	3118	demo_ilcs		2018-11-21 00:00:00	0001-01-01 00:00:00 BC
162	CAIU8041610	3122	Lingga Pradana		2018-11-21 00:00:00	0001-01-01 00:00:00 BC
163	FCIU9422352	3126	Lingga Pradana		2018-11-21 00:00:00	0001-01-01 00:00:00 BC
164	FSCU9502586	3126	Lingga Pradana		2018-11-21 00:00:00	0001-01-01 00:00:00 BC
165	GESU6523873	3126	Lingga Pradana		2018-11-21 00:00:00	0001-01-01 00:00:00 BC
166	CAIU3739070	3132	Lingga Pradana		2018-11-21 00:00:00	0001-01-01 00:00:00 BC
167	CAIU8041610	3142	Lingga Pradana		2018-11-22 00:00:00	0001-01-01 00:00:00 BC
168	TGHU6517364	3146	Lingga Pradana		2018-11-22 00:00:00	0001-01-01 00:00:00 BC
169	CAIU9210334	3150	Lingga Pradana		2018-11-22 00:00:00	0001-01-01 00:00:00 BC
170	REGU5115274	3150	Lingga Pradana		2018-11-22 00:00:00	0001-01-01 00:00:00 BC
171	CAIU9759962	3155	Lingga Pradana		2018-11-22 00:00:00	0001-01-01 00:00:00 BC
172	CAIU7851350	3159	Lingga Pradana		2018-11-22 00:00:00	0001-01-01 00:00:00 BC
173	CAIU8257105	3159	Lingga Pradana		2018-11-22 00:00:00	0001-01-01 00:00:00 BC
174	TCNU7678588	3159	Lingga Pradana		2018-11-22 00:00:00	0001-01-01 00:00:00 BC
175	EGSU3127470	3165	Lingga Pradana		2018-11-22 00:00:00	0001-01-01 00:00:00 BC
176	CAIU6425128	3169	Heni		2018-11-22 00:00:00	0001-01-01 00:00:00 BC
177	REGU3272850	3169	Heni		2018-11-22 00:00:00	0001-01-01 00:00:00 BC
178	REGU3218627	3174	Heni		2018-11-22 00:00:00	0001-01-01 00:00:00 BC
179	BMOU5793737	3179	Lingga Pradana		2018-11-23 00:00:00	0001-01-01 00:00:00 BC
180	SEGU3276352	3183	Lingga Pradana		2018-11-23 00:00:00	0001-01-01 00:00:00 BC
181	TCLU3784619	3187	Lingga Pradana		2018-11-26 00:00:00	0001-01-01 00:00:00 BC
182	CAIU6836983	3191	Heni		2018-11-26 00:00:00	0001-01-01 00:00:00 BC
183	CAIU6375367	3196	Lingga Pradana		2018-11-27 00:00:00	0001-01-01 00:00:00 BC
184	TCNU7650446	3200	Lingga Pradana		2018-11-27 00:00:00	0001-01-01 00:00:00 BC
185	TCNU7437410	3200	Lingga Pradana		2018-11-27 00:00:00	0001-01-01 00:00:00 BC
186	REGU5104304	3200	Lingga Pradana		2018-11-27 00:00:00	0001-01-01 00:00:00 BC
187	FCIU8410697	3200	Lingga Pradana		2018-11-27 00:00:00	0001-01-01 00:00:00 BC
188	CONT9089	3238	demo_ilcs		2018-12-12 00:00:00	0001-01-01 00:00:00 BC
189	CAIU6255588	3242	Heni		2018-12-13 00:00:00	0001-01-01 00:00:00 BC
190	CAIU6567267	3242	Heni		2018-12-13 00:00:00	0001-01-01 00:00:00 BC
191	JUI889	3248	demo_ilcs		2018-12-14 00:00:00	0001-01-01 00:00:00 BC
192	REGU3230480	3252	Heni		2018-12-19 00:00:00	0001-01-01 00:00:00 BC
193	REGU3283643	3252	Heni		2018-12-19 00:00:00	0001-01-01 00:00:00 BC
194	CONT12312	3257	demo_ilcs		2018-12-19 00:00:00	0001-01-01 00:00:00 BC
195	CONT67876	3261	demo_ilcs		2018-12-19 00:00:00	0001-01-01 00:00:00 BC
196	CONT9521	3265	kikoko		2018-12-20 00:00:00	0001-01-01 00:00:00 BC
197	CON568	3269	kikoko		2018-12-20 00:00:00	0001-01-01 00:00:00 BC
198	CONT653	3273	kikoko		2018-12-20 00:00:00	0001-01-01 00:00:00 BC
199	CONT8789876	3277	demo_ilcs		2018-12-20 00:00:00	0001-01-01 00:00:00 BC
200	CONT97789	3281	demo_ilcs		2018-12-26 00:00:00	0001-01-01 00:00:00 BC
201	CONT78989	3285	kikoko		2018-12-26 00:00:00	0001-01-01 00:00:00 BC
202	CONT68767979	3289	demo_ilcs		2018-12-27 00:00:00	0001-01-01 00:00:00 BC
203	CONT787408280	3293	kikoko		2018-12-27 00:00:00	0001-01-01 00:00:00 BC
204	CAIU6053611	3297	Heni		2018-12-27 00:00:00	0001-01-01 00:00:00 BC
205	REGU3211052	3301	Heni		2018-12-27 00:00:00	0001-01-01 00:00:00 BC
206	REGU3269250	3301	Heni		2018-12-27 00:00:00	0001-01-01 00:00:00 BC
207	CONT8088080	3307	demo_ilcs		2019-01-02 00:00:00	0001-01-01 00:00:00 BC
208	REGU3237139	3311	Heni		2019-01-03 00:00:00	0001-01-01 00:00:00 BC
209	REGU3235326	3316	Heni		2019-01-10 00:00:00	0001-01-01 00:00:00 BC
210	REGU3238496	3320	Heni		2019-01-10 00:00:00	0001-01-01 00:00:00 BC
211	BSIU9587370	3325	Lingga Pradana		2019-01-15 00:00:00	0001-01-01 00:00:00 BC
212	REGU5078329	3329	Lingga Pradana		2019-01-15 00:00:00	0001-01-01 00:00:00 BC
213	REGU5085837	3333	Lingga Pradana		2019-01-15 00:00:00	0001-01-01 00:00:00 BC
214	CAIU8053009	3337	Lingga Pradana		2019-01-15 00:00:00	0001-01-01 00:00:00 BC
215	CAIU6481796	3341	Lingga Pradana		2019-01-15 00:00:00	0001-01-01 00:00:00 BC
216	FSCU9073270	3346	Lingga Pradana		2019-01-17 00:00:00	0001-01-01 00:00:00 BC
217	CONT83480	3460	demo_ilcs		2019-02-01 00:00:00	0001-01-01 00:00:00 BC
218	CONT7894894793	3465	demo_ilcs		2019-02-01 00:00:00	0001-01-01 00:00:00 BC
219	CONT234321	3474	demo_ilcs		2019-02-01 00:00:00	0001-01-01 00:00:00 BC
220	CONT87439	3514	demo_ilcs		2019-02-18 00:00:00	0001-01-01 00:00:00 BC
221	cont8749	3518	demo_ilcs		2019-02-18 00:00:00	0001-01-01 00:00:00 BC
222	CONT4820	3522	demo_ilcs		2019-02-18 00:00:00	0001-01-01 00:00:00 BC
223	BSIU9309786	3647	Lingga Pradana		2019-03-11 00:00:00	0001-01-01 00:00:00 BC
224	TCNU4383541	3647	Lingga Pradana		2019-03-11 00:00:00	0001-01-01 00:00:00 BC
225	GESU6276789	3652	Lingga Pradana		2019-03-11 00:00:00	0001-01-01 00:00:00 BC
226	CAIU4414124	3656	Lingga Pradana		2019-03-14 00:00:00	0001-01-01 00:00:00 BC
227	OCGU8102280	3656	Lingga Pradana		2019-03-14 00:00:00	0001-01-01 00:00:00 BC
228	BEAU4464666	3661	Lingga Pradana		2019-03-14 00:00:00	0001-01-01 00:00:00 BC
229	EITU1011338	3661	Lingga Pradana		2019-03-14 00:00:00	0001-01-01 00:00:00 BC
230	BMOU5373210	3661	Lingga Pradana		2019-03-14 00:00:00	0001-01-01 00:00:00 BC
231	TCNU2863515	3661	Lingga Pradana		2019-03-14 00:00:00	0001-01-01 00:00:00 BC
232	EGSU9079770	3661	Lingga Pradana		2019-03-14 00:00:00	0001-01-01 00:00:00 BC
233	TLLU5919352	3661	Lingga Pradana		2019-03-14 00:00:00	0001-01-01 00:00:00 BC
234	HMCU9118859	3661	Lingga Pradana		2019-03-14 00:00:00	0001-01-01 00:00:00 BC
235	EISU9910758	3661	Lingga Pradana		2019-03-14 00:00:00	0001-01-01 00:00:00 BC
236	UESU2309609	3672	Lingga Pradana		2019-03-14 00:00:00	0001-01-01 00:00:00 BC
237	EITU1109026	3685	Lingga Pradana		2019-03-21 00:00:00	0001-01-01 00:00:00 BC
238	SEGU1274860	3685	Lingga Pradana		2019-03-21 00:00:00	0001-01-01 00:00:00 BC
239	TCLU8370099	3690	Lingga Pradana		2019-03-21 00:00:00	0001-01-01 00:00:00 BC
240	CAIU9608420	3694	Lingga Pradana		2019-03-21 00:00:00	0001-01-01 00:00:00 BC
241	CAIU9456903	3698	Lingga Pradana		2019-03-21 00:00:00	0001-01-01 00:00:00 BC
242	TEMU7403600	3702	Lingga Pradana		2019-03-21 00:00:00	0001-01-01 00:00:00 BC
243	EITU1213977	3706	Lingga Pradana		2019-03-28 00:00:00	0001-01-01 00:00:00 BC
244	HMCU9157222	3706	Lingga Pradana		2019-03-28 00:00:00	0001-01-01 00:00:00 BC
245	REGU5107221	3711	Lingga Pradana		2019-03-28 00:00:00	0001-01-01 00:00:00 BC
246	TRHU3340770	3715	Lingga Pradana		2019-03-28 00:00:00	0001-01-01 00:00:00 BC
247	TEMU3960588	3715	Lingga Pradana		2019-03-28 00:00:00	0001-01-01 00:00:00 BC
248	DRYU2995782	3720	Lingga Pradana		2019-03-28 00:00:00	0001-01-01 00:00:00 BC
249	TEMU0045675	3720	Lingga Pradana		2019-03-28 00:00:00	0001-01-01 00:00:00 BC
250	EISU2228026	3725	Lingga Pradana		2019-03-28 00:00:00	0001-01-01 00:00:00 BC
251	CAIU6019197	3729	Lingga Pradana		2019-03-29 00:00:00	0001-01-01 00:00:00 BC
252	CAIU3690703	3744	Lingga Pradana		2019-04-01 00:00:00	0001-01-01 00:00:00 BC
253	CAIU6311043	3748	Lingga Pradana		2019-04-01 00:00:00	0001-01-01 00:00:00 BC
254	CAIU4414058	3748	Lingga Pradana		2019-04-01 00:00:00	0001-01-01 00:00:00 BC
255	REGU5088604	3748	Lingga Pradana		2019-04-01 00:00:00	0001-01-01 00:00:00 BC
256	REGU5111788	3748	Lingga Pradana		2019-04-01 00:00:00	0001-01-01 00:00:00 BC
257	REGU5075490	3748	Lingga Pradana		2019-04-01 00:00:00	0001-01-01 00:00:00 BC
258	EISU1650079	3809	Lingga Pradana		2019-04-05 00:00:00	0001-01-01 00:00:00 BC
259	REGU5116558	3813	Lingga Pradana		2019-04-08 00:00:00	0001-01-01 00:00:00 BC
260	FCIU3548641	3817	Lingga Pradana		2019-04-09 00:00:00	0001-01-01 00:00:00 BC
261	REGU3240153	3821	Lingga Pradana		2019-04-10 00:00:00	0001-01-01 00:00:00 BC
262	EITU0348495	3825	Lingga Pradana		2019-04-11 00:00:00	0001-01-01 00:00:00 BC
263	EITU0114692	3825	Lingga Pradana		2019-04-11 00:00:00	0001-01-01 00:00:00 BC
264	EMCU3554317	3830	Lingga Pradana		2019-04-11 00:00:00	0001-01-01 00:00:00 BC
265	REGU3271703	3834	Lingga Pradana		2019-04-11 00:00:00	0001-01-01 00:00:00 BC
266	REGU3281532	3834	Lingga Pradana		2019-04-11 00:00:00	0001-01-01 00:00:00 BC
267	EGHU3501673	3891	Lingga Pradana		2019-04-26 00:00:00	0001-01-01 00:00:00 BC
268	EITU0473007	3891	Lingga Pradana		2019-04-26 00:00:00	0001-01-01 00:00:00 BC
269	BMOU4801720	3896	Lingga Pradana		2019-04-26 00:00:00	0001-01-01 00:00:00 BC
270	EGHU9239555	3896	Lingga Pradana		2019-04-26 00:00:00	0001-01-01 00:00:00 BC
271	EGSU9164655	3896	Lingga Pradana		2019-04-26 00:00:00	0001-01-01 00:00:00 BC
272	EITU1224483	3896	Lingga Pradana		2019-04-26 00:00:00	0001-01-01 00:00:00 BC
273	HMCU9018232	3896	Lingga Pradana		2019-04-26 00:00:00	0001-01-01 00:00:00 BC
274	TCNU5453601	3896	Lingga Pradana		2019-04-26 00:00:00	0001-01-01 00:00:00 BC
275	EITU1133085	3896	Lingga Pradana		2019-04-26 00:00:00	0001-01-01 00:00:00 BC
276	MAGU5290000	3896	Lingga Pradana		2019-04-26 00:00:00	0001-01-01 00:00:00 BC
277	EITU1615301	3907	Lingga Pradana		2019-04-26 00:00:00	0001-01-01 00:00:00 BC
278	EITU0087540	3911	Lingga Pradana		2019-04-26 00:00:00	0001-01-01 00:00:00 BC
279	EITU0496553	3911	Lingga Pradana		2019-04-26 00:00:00	0001-01-01 00:00:00 BC
280	CAIU3639039	3916	Lingga Pradana		2019-04-29 00:00:00	0001-01-01 00:00:00 BC
281	FCIU2989730	3920	Lingga Pradana		2019-04-30 00:00:00	0001-01-01 00:00:00 BC
282	OCGU8101750	3924	Lingga Pradana		2019-04-30 00:00:00	0001-01-01 00:00:00 BC
283	REGU5024560	3924	Lingga Pradana		2019-04-30 00:00:00	0001-01-01 00:00:00 BC
284	TRHU2843805	3929	Lingga Pradana		2019-05-02 00:00:00	0001-01-01 00:00:00 BC
285	TRHU3641856	3933	Lingga Pradana		2019-05-02 00:00:00	0001-01-01 00:00:00 BC
286	EISU2191919	3933	Lingga Pradana		2019-05-02 00:00:00	0001-01-01 00:00:00 BC
287	TCLU4353160	3938	Lingga Pradana		2019-05-03 00:00:00	0001-01-01 00:00:00 BC
288	BSLK97979	3955	demo_ilcs		2019-05-08 00:00:00	0001-01-01 00:00:00 BC
289	TLLU4762253	3961	Lingga Pradana		2019-05-10 00:00:00	0001-01-01 00:00:00 BC
290	IMTU1047354	3965	Lingga Pradana		2019-05-13 00:00:00	0001-01-01 00:00:00 BC
291	CAIU9659310	3969	Lingga Pradana		2019-05-13 00:00:00	0001-01-01 00:00:00 BC
292	DFSU6278311	3969	Lingga Pradana		2019-05-13 00:00:00	0001-01-01 00:00:00 BC
293	FSCU9512413	3969	Lingga Pradana		2019-05-13 00:00:00	0001-01-01 00:00:00 BC
294	TGHU6556798	3969	Lingga Pradana		2019-05-13 00:00:00	0001-01-01 00:00:00 BC
295	EGHU3279020	3976	Lingga Pradana		2019-05-13 00:00:00	0001-01-01 00:00:00 BC
296	HMCU9012403	2576	Yuliawati		2018-03-20 00:00:00	0001-01-01 00:00:00 BC
297	HMCU3047779	2572	Yuliawati		2018-03-20 00:00:00	0001-01-01 00:00:00 BC
298		2572	Yuliawati		2018-03-20 00:00:00	0001-01-01 00:00:00 BC
299		2576	Yuliawati		2018-03-20 00:00:00	0001-01-01 00:00:00 BC
300	EMCU3923971	2580	Lingga Pradana		2018-03-21 00:00:00	0001-01-01 00:00:00 BC
301	NO123456	2972	dnoraeni		2018-10-14 00:00:00	0001-01-01 00:00:00 BC
302	NO1234567	2975	demo_ilcs		2018-10-14 00:00:00	0001-01-01 00:00:00 BC
303	ABC10000	2978	demo_ilcs		2018-10-14 00:00:00	0001-01-01 00:00:00 BC
304	ajsha97979	2982	demo_ilcs		2018-10-14 00:00:00	0001-01-01 00:00:00 BC
305	NO123451	2985	demo_ilcs		2018-10-14 00:00:00	0001-01-01 00:00:00 BC
306	CONT00006	3010	demo_ilcs		2018-11-08 00:00:00	0001-01-01 00:00:00 BC
307	73846	3061	demo_ilcs		2018-11-15 00:00:00	0001-01-01 00:00:00 BC
308	CAIU6053967	3066	Lingga Pradana		2018-11-15 00:00:00	0001-01-01 00:00:00 BC
309	CAIU4305416	3070	Lingga Pradana		2018-11-15 00:00:00	0001-01-01 00:00:00 BC
310	CAIU8156310	3070	Lingga Pradana		2018-11-15 00:00:00	0001-01-01 00:00:00 BC
311	FCIU8645109	3070	Lingga Pradana		2018-11-15 00:00:00	0001-01-01 00:00:00 BC
312	REGU5013349	3070	Lingga Pradana		2018-11-15 00:00:00	0001-01-01 00:00:00 BC
313	BSIU9619605	3077	Lingga Pradana		2018-11-15 00:00:00	0001-01-01 00:00:00 BC
314	CAIU8846867	3077	Lingga Pradana		2018-11-15 00:00:00	0001-01-01 00:00:00 BC
315	RFCU5129779	3082	Lingga Pradana		2018-11-15 00:00:00	0001-01-01 00:00:00 BC
316	CONT123412	3096	demo_ilcs		2018-11-15 00:00:00	0001-01-01 00:00:00 BC
317	CONT8474	3526	demo_ilcs		2019-02-18 00:00:00	0001-01-01 00:00:00 BC
318	CONT7585	3530	demo_ilcs		2019-02-18 00:00:00	0001-01-01 00:00:00 BC
319	B56757	3538	alyafortesting		2019-02-18 00:00:00	0001-01-01 00:00:00 BC
320	CONT1231233455	3571	demo_ilcs		2019-02-25 00:00:00	0001-01-01 00:00:00 BC
321	GLDU2961190	3575	Lingga Pradana		2019-02-26 00:00:00	0001-01-01 00:00:00 BC
322	REGU5082669	3579	Lingga Pradana		2019-02-28 00:00:00	0001-01-01 00:00:00 BC
323	REGU5110903	3579	Lingga Pradana		2019-02-28 00:00:00	0001-01-01 00:00:00 BC
324	GLDU5336940	3985	Lingga Pradana		2019-05-14 00:00:00	0001-01-01 00:00:00 BC
325	REGU5060891	3980	Lingga Pradana		2019-05-14 00:00:00	0001-01-01 00:00:00 BC
326	TCNU4635601	3980	Lingga Pradana		2019-05-14 00:00:00	0001-01-01 00:00:00 BC
327	CAIU6602809	3985	Lingga Pradana		2019-05-14 00:00:00	0001-01-01 00:00:00 BC
328	BSIU2869855	3990	Lingga Pradana		2019-05-14 00:00:00	0001-01-01 00:00:00 BC
329	FCIU8973453	3994	Lingga Pradana		2019-05-14 00:00:00	0001-01-01 00:00:00 BC
330	TGHU9699719	3998	Lingga Pradana		2019-05-14 00:00:00	0001-01-01 00:00:00 BC
331	CAIU6035860	4002	Lingga Pradana		2019-05-14 00:00:00	0001-01-01 00:00:00 BC
332	EGHU3482953	4006	Lingga Pradana		2019-05-15 00:00:00	0001-01-01 00:00:00 BC
333	EITU0583874	4006	Lingga Pradana		2019-05-15 00:00:00	0001-01-01 00:00:00 BC
334	CONT4820	4013	demo_ilcs		2019-05-17 00:00:00	0001-01-01 00:00:00 BC
335	EITU0047233	4038	Lingga Pradana		2019-05-24 00:00:00	0001-01-01 00:00:00 BC
336	EITU1001725	4038	Lingga Pradana		2019-05-24 00:00:00	0001-01-01 00:00:00 BC
337	BMOU5011627	4043	Lingga Pradana		2019-05-24 00:00:00	0001-01-01 00:00:00 BC
338	TGBU6490852	4043	Lingga Pradana		2019-05-24 00:00:00	0001-01-01 00:00:00 BC
339	CONT123456	4048	demo_ilcs		2019-05-24 00:00:00	0001-01-01 00:00:00 BC
340	CONT123457	4048	demo_ilcs		2019-05-24 00:00:00	0001-01-01 00:00:00 BC
341	CAIU4425664	4055	Lingga Pradana		2019-05-28 00:00:00	0001-01-01 00:00:00 BC
342	KMBU9143465	4059	Lingga Pradana		2019-05-28 00:00:00	0001-01-01 00:00:00 BC
343	KMBU9152770	4059	Lingga Pradana		2019-05-28 00:00:00	0001-01-01 00:00:00 BC
344	kjiio	4114	demo_ilcs		2019-06-20 00:00:00	0001-01-01 00:00:00 BC
345	CAIU6041288	4118	Lingga Pradana		2019-06-27 00:00:00	0001-01-01 00:00:00 BC
346	TCLU1998416	1665	coff01		2018-01-22 00:00:00	0001-01-01 00:00:00 BC
347	TCLU3618120	1669	coff01		2018-01-22 00:00:00	0001-01-01 00:00:00 BC
348	DRYU9320585	1661	coff01		2018-01-22 00:00:00	0001-01-01 00:00:00 BC
349	EITU0459797	1669	coff01		2018-01-22 00:00:00	0001-01-01 00:00:00 BC
350	TCLU3606849	1669	coff01		2018-01-22 00:00:00	0001-01-01 00:00:00 BC
351	ctr555	1682	coff01		2018-01-23 00:00:00	0001-01-01 00:00:00 BC
352	ctr5555	1682	sl01		2018-01-23 00:00:00	0001-01-01 00:00:00 BC
353	ctr3434	1682	coff01		2018-01-23 00:00:00	0001-01-01 00:00:00 BC
354	ctr5555	1688	coff01		2018-01-23 00:00:00	0001-01-01 00:00:00 BC
355	ctr555	1688	coff01		2018-01-23 00:00:00	0001-01-01 00:00:00 BC
356	ctr3434	1688	coff01		2018-01-23 00:00:00	0001-01-01 00:00:00 BC
357	gggggg	1688	coff01		2018-01-23 00:00:00	0001-01-01 00:00:00 BC
358	DCTR9237293	1695	coff01		2018-01-25 00:00:00	0001-01-01 00:00:00 BC
359	TEST0000007	1785	co01		2018-01-17 00:00:00	0001-01-01 00:00:00 BC
360	EXPRESS123	1790	co01		2018-01-17 00:00:00	0001-01-01 00:00:00 BC
361	TEST0000001	1782	co01	co01	2018-01-17 00:00:00	2018-01-17 00:00:00
362	BL01	1748	co_adit		2018-01-15 00:00:00	0001-01-01 00:00:00 BC
363	ctr98989aa	1762	coff01	coff01	2018-01-15 00:00:00	2018-01-15 00:00:00
364	NC001	1748	co_adit		2018-01-15 00:00:00	0001-01-01 00:00:00 BC
365	NC002	1748	co_adit		2018-01-15 00:00:00	0001-01-01 00:00:00 BC
366	NC003	1748	co_adit		2018-01-15 00:00:00	0001-01-01 00:00:00 BC
367	NC004	1748	co_adit		2018-01-15 00:00:00	0001-01-01 00:00:00 BC
368	NC005	1748	co_adit		2018-01-15 00:00:00	0001-01-01 00:00:00 BC
369	ctr010	1762	coff01		2018-01-15 00:00:00	0001-01-01 00:00:00 BC
370	UYAJ1234567890	1766	co01		2018-01-17 00:00:00	0001-01-01 00:00:00 BC
371	inbondcall123	1770	co01		2018-01-17 00:00:00	0001-01-01 00:00:00 BC
372	1	1775	co01		2018-01-17 00:00:00	0001-01-01 00:00:00 BC
373	CONTR800	1702	coff01		2018-01-06 00:00:00	0001-01-01 00:00:00 BC
374	CONTR900	1702	coff01		2018-01-06 00:00:00	0001-01-01 00:00:00 BC
375	CONTR55	1702	coff01		2018-01-06 00:00:00	0001-01-01 00:00:00 BC
376	NC003	1670	co_adit		2017-12-20 00:00:00	0001-01-01 00:00:00 BC
377	NC004	1670	co_adit		2017-12-20 00:00:00	0001-01-01 00:00:00 BC
378	NC005	1670	co_adit		2017-12-20 00:00:00	0001-01-01 00:00:00 BC
379	NC001	1686	co_adit	co_adit	2017-12-27 00:00:00	2018-01-08 00:00:00
380	NC002	1686	co_adit	co_adit	2017-12-27 00:00:00	2018-01-08 00:00:00
381	CONT1	1678	coff01		2017-12-20 00:00:00	0001-01-01 00:00:00 BC
382	BLKU2588214	3086	Heni		2018-11-15 00:00:00	0001-01-01 00:00:00 BC
383	BRKU1250922	3086	Heni		2018-11-15 00:00:00	0001-01-01 00:00:00 BC
384	cont10001	3091	demo_ilcs		2018-11-15 00:00:00	0001-01-01 00:00:00 BC
385	CONT000187	3223	demo_ilcs		2018-12-07 00:00:00	0001-01-01 00:00:00 BC
386	conT678987	3223	demo_ilcs		2018-12-07 00:00:00	0001-01-01 00:00:00 BC
387	CONT567651	3229	demo_ilcs		2018-12-09 00:00:00	0001-01-01 00:00:00 BC
388	CONT11011	3233	demo_ilcs		2018-12-12 00:00:00	0001-01-01 00:00:00 BC
389	CAIU6017790	3359	Lingga Pradana		2019-01-21 00:00:00	0001-01-01 00:00:00 BC
390	CAIU4377740	3363	Lingga Pradana		2019-01-22 00:00:00	0001-01-01 00:00:00 BC
391	OCGU8102459	3363	Lingga Pradana		2019-01-22 00:00:00	0001-01-01 00:00:00 BC
392	REGU5047328	3363	Lingga Pradana		2019-01-22 00:00:00	0001-01-01 00:00:00 BC
393	REGU5107577	3369	Lingga Pradana		2019-01-22 00:00:00	0001-01-01 00:00:00 BC
394	CAIU7939116	3373	Lingga Pradana		2019-01-22 00:00:00	0001-01-01 00:00:00 BC
395	REGU5057820	3377	Lingga Pradana		2019-01-22 00:00:00	0001-01-01 00:00:00 BC
396	REGU3233690	3381	Lingga Pradana		2019-01-22 00:00:00	0001-01-01 00:00:00 BC
397	CON5477	3390	demo_ilcs		2019-01-23 00:00:00	0001-01-01 00:00:00 BC
398	TCNU7720100	3395	Lingga Pradana		2019-01-24 00:00:00	0001-01-01 00:00:00 BC
399	CAIU6467801	3399	Lingga Pradana		2019-01-24 00:00:00	0001-01-01 00:00:00 BC
400	TEMU8528520	3456	Lingga Pradana		2019-02-01 00:00:00	0001-01-01 00:00:00 BC
401	cont8580480	3469	demo_ilcs		2019-02-01 00:00:00	0001-01-01 00:00:00 BC
402	cont089080	3478	demo_ilcs		2019-02-05 00:00:00	0001-01-01 00:00:00 BC
403	REGU3266728	3482	Lingga Pradana		2019-02-07 00:00:00	0001-01-01 00:00:00 BC
404	TEMU0674220	3486	Lingga Pradana		2019-02-07 00:00:00	0001-01-01 00:00:00 BC
405	TCNU6221869	3491	Lingga Pradana		2019-02-13 00:00:00	0001-01-01 00:00:00 BC
406	TGCU2019658	3495	Lingga Pradana		2019-02-13 00:00:00	0001-01-01 00:00:00 BC
407	CAIU4646412	3499	Lingga Pradana		2019-02-18 00:00:00	0001-01-01 00:00:00 BC
408	REGU5100654	3499	Lingga Pradana		2019-02-18 00:00:00	0001-01-01 00:00:00 BC
409	TGHU8802503	3499	Lingga Pradana		2019-02-18 00:00:00	0001-01-01 00:00:00 BC
410	SEGU4680862	3505	Lingga Pradana		2019-02-18 00:00:00	0001-01-01 00:00:00 BC
411	FSCU9615237	3509	Lingga Pradana		2019-02-18 00:00:00	0001-01-01 00:00:00 BC
412	REGU5060171	3509	Lingga Pradana		2019-02-18 00:00:00	0001-01-01 00:00:00 BC
413	CAIU6599840	3676	Lingga Pradana		2019-03-18 00:00:00	0001-01-01 00:00:00 BC
414	GLDU9719640	3680	Lingga Pradana		2019-03-18 00:00:00	0001-01-01 00:00:00 BC
415	CONT7585	3736	demo_ilcs		2019-03-31 00:00:00	0001-01-01 00:00:00 BC
416	YM292929290	3740	demo_ilcs		2019-03-31 00:00:00	0001-01-01 00:00:00 BC
417	COT29394888	3756	demo_ilcs		2019-04-01 00:00:00	0001-01-01 00:00:00 BC
418		3756	demo_ilcs		2019-04-01 00:00:00	0001-01-01 00:00:00 BC
419	OCGU8071701	3762	Lingga Pradana		2019-04-04 00:00:00	0001-01-01 00:00:00 BC
420	TGBU6078402	3762	Lingga Pradana		2019-04-04 00:00:00	0001-01-01 00:00:00 BC
421	TLLU5424414	3762	Lingga Pradana		2019-04-04 00:00:00	0001-01-01 00:00:00 BC
422	TLLU5921806	3762	Lingga Pradana		2019-04-04 00:00:00	0001-01-01 00:00:00 BC
423	TCLU4953776	3762	Lingga Pradana		2019-04-04 00:00:00	0001-01-01 00:00:00 BC
424	MAGU5421455	3770	Lingga Pradana		2019-04-04 00:00:00	0001-01-01 00:00:00 BC
425	BMOU5432539	3770	Lingga Pradana		2019-04-04 00:00:00	0001-01-01 00:00:00 BC
426	CXDU1060200	3775	Lingga Pradana		2019-04-04 00:00:00	0001-01-01 00:00:00 BC
427	FSCU9627840	3775	Lingga Pradana		2019-04-04 00:00:00	0001-01-01 00:00:00 BC
428	REGU5026623	3775	Lingga Pradana		2019-04-04 00:00:00	0001-01-01 00:00:00 BC
429	REGU5090453	3775	Lingga Pradana		2019-04-04 00:00:00	0001-01-01 00:00:00 BC
430	TCNU8594239	3775	Lingga Pradana		2019-04-04 00:00:00	0001-01-01 00:00:00 BC
431	TEMU8527924	3775	Lingga Pradana		2019-04-04 00:00:00	0001-01-01 00:00:00 BC
432	OCGU2032708	3784	Lingga Pradana		2019-04-04 00:00:00	0001-01-01 00:00:00 BC
433	CAIU7904193	3788	Lingga Pradana		2019-04-04 00:00:00	0001-01-01 00:00:00 BC
434	CAIU9609350	3792	Lingga Pradana		2019-04-04 00:00:00	0001-01-01 00:00:00 BC
435	REGU5033978	3796	Lingga Pradana		2019-04-04 00:00:00	0001-01-01 00:00:00 BC
436	REGU5063571	3796	Lingga Pradana		2019-04-04 00:00:00	0001-01-01 00:00:00 BC
437	EISU2232978	3801	Lingga Pradana		2019-04-04 00:00:00	0001-01-01 00:00:00 BC
438	FCIU2380297	3805	Lingga Pradana		2019-04-04 00:00:00	0001-01-01 00:00:00 BC
439	TEMU4588027	3839	Lingga Pradana		2019-04-12 00:00:00	0001-01-01 00:00:00 BC
440	GESU3587370	3843	Lingga Pradana		2019-04-15 00:00:00	0001-01-01 00:00:00 BC
441	CAIU3240265	3847	Lingga Pradana		2019-04-15 00:00:00	0001-01-01 00:00:00 BC
442	TEMU5604760	3851	Lingga Pradana		2019-04-15 00:00:00	0001-01-01 00:00:00 BC
443	BSLK8787	3855	demo_ilcs		2019-04-15 00:00:00	0001-01-01 00:00:00 BC
444	TEMU5418200	3860	Lingga Pradana		2019-04-15 00:00:00	0001-01-01 00:00:00 BC
445	FCIU2491990	3860	Lingga Pradana		2019-04-15 00:00:00	0001-01-01 00:00:00 BC
446	FCIU3226732	3860	Lingga Pradana		2019-04-15 00:00:00	0001-01-01 00:00:00 BC
447	REGU5081087	3866	Lingga Pradana		2019-04-16 00:00:00	0001-01-01 00:00:00 BC
448	REGU5087110	3866	Lingga Pradana		2019-04-16 00:00:00	0001-01-01 00:00:00 BC
449	REGU5088610	3866	Lingga Pradana		2019-04-16 00:00:00	0001-01-01 00:00:00 BC
450	TCNU3848583	3872	Lingga Pradana		2019-04-16 00:00:00	0001-01-01 00:00:00 BC
451	EITU0253147	3876	Lingga Pradana		2019-04-18 00:00:00	0001-01-01 00:00:00 BC
452	CAIU8986637	3880	Lingga Pradana		2019-04-24 00:00:00	0001-01-01 00:00:00 BC
453	CAIU9211937	3880	Lingga Pradana		2019-04-24 00:00:00	0001-01-01 00:00:00 BC
454	CAIU9614249	3880	Lingga Pradana		2019-04-24 00:00:00	0001-01-01 00:00:00 BC
455	JTMU6006485	3880	Lingga Pradana		2019-04-24 00:00:00	0001-01-01 00:00:00 BC
456	GLDU3558888	3887	Lingga Pradana		2019-04-25 00:00:00	0001-01-01 00:00:00 BC
457	TEMU5469911	3942	Lingga Pradana		2019-05-06 00:00:00	0001-01-01 00:00:00 BC
458	CAIU7929611	3942	Lingga Pradana		2019-05-06 00:00:00	0001-01-01 00:00:00 BC
459	FCIU4471694	3947	Lingga Pradana		2019-05-08 00:00:00	0001-01-01 00:00:00 BC
460	FCIU3632840	3951	Lingga Pradana		2019-05-08 00:00:00	0001-01-01 00:00:00 BC
461	POIU9898	4017	demo_ilcs		2019-05-21 00:00:00	0001-01-01 00:00:00 BC
462	CAIU4306412	4023	Lingga Pradana		2019-05-22 00:00:00	0001-01-01 00:00:00 BC
463	REGU5082150	4023	Lingga Pradana		2019-05-22 00:00:00	0001-01-01 00:00:00 BC
464	GESU5815567	4028	Lingga Pradana		2019-05-22 00:00:00	0001-01-01 00:00:00 BC
465	TEST12345	4032	demo_ilcs		2019-05-22 00:00:00	0001-01-01 00:00:00 BC
466	TEST12346	4032	demo_ilcs		2019-05-22 00:00:00	0001-01-01 00:00:00 BC
467	MAGU5278149	4064	Lingga Pradana		2019-05-31 00:00:00	0001-01-01 00:00:00 BC
468	TCLU3455320	4064	Lingga Pradana		2019-05-31 00:00:00	0001-01-01 00:00:00 BC
469	CAIU4245596	4069	Lingga Pradana		2019-06-13 00:00:00	0001-01-01 00:00:00 BC
470	CAIU9658988	4069	Lingga Pradana		2019-06-13 00:00:00	0001-01-01 00:00:00 BC
471	EGHU3404122	4074	Lingga Pradana		2019-06-13 00:00:00	0001-01-01 00:00:00 BC
472	TCLU3872371	4074	Lingga Pradana		2019-06-13 00:00:00	0001-01-01 00:00:00 BC
473	HMCU9036215	4079	Lingga Pradana		2019-06-13 00:00:00	0001-01-01 00:00:00 BC
474	EISU9419446	4079	Lingga Pradana		2019-06-13 00:00:00	0001-01-01 00:00:00 BC
475	EGHU3639493	4084	Lingga Pradana		2019-06-13 00:00:00	0001-01-01 00:00:00 BC
476	EITU3005946	4084	Lingga Pradana		2019-06-13 00:00:00	0001-01-01 00:00:00 BC
477	DFSU1116977	4084	Lingga Pradana		2019-06-13 00:00:00	0001-01-01 00:00:00 BC
478	DFSU1059202	4090	Lingga Pradana		2019-06-14 00:00:00	0001-01-01 00:00:00 BC
479	KMBU9150572	4094	Lingga Pradana		2019-06-19 00:00:00	0001-01-01 00:00:00 BC
480	KMBU9180129	4094	Lingga Pradana		2019-06-19 00:00:00	0001-01-01 00:00:00 BC
481	FSCU9121640	4099	Lingga Pradana		2019-06-19 00:00:00	0001-01-01 00:00:00 BC
482	TEMU6077540	4099	Lingga Pradana		2019-06-19 00:00:00	0001-01-01 00:00:00 BC
483	TEMU7432486	4099	Lingga Pradana		2019-06-19 00:00:00	0001-01-01 00:00:00 BC
484	GLDU3528498	4105	Lingga Pradana		2019-06-19 00:00:00	0001-01-01 00:00:00 BC
485	REGU3214323	4109	Lingga Pradana		2019-06-19 00:00:00	0001-01-01 00:00:00 BC
486	CONT4564	4188	demo_ff19		2019-08-26 00:00:00	0001-01-01 00:00:00 BC
487	CONT4353434388	4175	demo_ff19	demo_ff19	2019-08-22 00:00:00	2019-08-22 00:00:00
488	CONY22345678	4205	demo_ff19		2019-08-27 00:00:00	0001-01-01 00:00:00 BC
489	CONT4564	4197	demo_ff19		2019-08-27 00:00:00	0001-01-01 00:00:00 BC
490	CONTA92333	4201	demo_ff19		2019-08-27 00:00:00	0001-01-01 00:00:00 BC
491	con77987979	4209	demo_ff19		2019-08-27 00:00:00	0001-01-01 00:00:00 BC
492	CONTA92333	4213	demo_ff19		2019-08-27 00:00:00	0001-01-01 00:00:00 BC
493	CONTA92333	4217	demo_ff19		2019-08-27 00:00:00	0001-01-01 00:00:00 BC
494	CONTB72882	4217	demo_ff19		2019-08-27 00:00:00	0001-01-01 00:00:00 BC
495	CONTB72882	4222	demo_ff19		2019-08-27 00:00:00	0001-01-01 00:00:00 BC
496	CONTA92333	4222	demo_ff19		2019-08-27 00:00:00	0001-01-01 00:00:00 BC
497	CONTA67890	4229	User_ff19		2019-08-28 00:00:00	0001-01-01 00:00:00 BC
498	CONTA30036	4233	User_ff19		2019-08-28 00:00:00	0001-01-01 00:00:00 BC
499	CONT89347594379	4237	User_ff19		2019-08-28 00:00:00	0001-01-01 00:00:00 BC
500	CONT89749279	4241	User_ff19		2019-08-28 00:00:00	0001-01-01 00:00:00 BC
501	CONT74389	4246	User_ff19		2019-09-01 00:00:00	0001-01-01 00:00:00 BC
502	CONT758	4251	User_ff19		2019-09-03 00:00:00	0001-01-01 00:00:00 BC
503	CONT34758934	4255	User_ff19		2019-09-04 00:00:00	0001-01-01 00:00:00 BC
504	CONT12345	4259	User_ff19		2019-09-04 00:00:00	0001-01-01 00:00:00 BC
505	CONT56789	4263	User_ff19		2019-09-04 00:00:00	0001-01-01 00:00:00 BC
506	TRXU1234567	4267	User_ff19		2019-09-05 00:00:00	0001-01-01 00:00:00 BC
507	ABCD2223333	4271	User_ff19		2019-09-05 00:00:00	0001-01-01 00:00:00 BC
508	TGCU1234567	4276	User_ff19		2019-09-05 00:00:00	0001-01-01 00:00:00 BC
509	TGCU1222334	4280	User_ff19		2019-09-05 00:00:00	0001-01-01 00:00:00 BC
510	ABCD1231231	4284	User_ff19		2019-09-05 00:00:00	0001-01-01 00:00:00 BC
511	CON4859384	4288	User_ff19		2019-09-06 00:00:00	0001-01-01 00:00:00 BC
512	CONT7586798	4292	User_ff19		2019-09-09 00:00:00	0001-01-01 00:00:00 BC
513	CONTTEST5837	4297	User_ff19		2019-09-10 00:00:00	0001-01-01 00:00:00 BC
514	CONTEST74598	4301	User_ff19		2019-09-10 00:00:00	0001-01-01 00:00:00 BC
515	cont485093480	4305	User_ff19		2019-09-11 00:00:00	0001-01-01 00:00:00 BC
516	CONTTEST12345	4309	User_ff19		2019-09-12 00:00:00	0001-01-01 00:00:00 BC
517	CONTTEST67890	4313	User_ff19		2019-09-16 00:00:00	0001-01-01 00:00:00 BC
518	CONTTEST112233	4317	User_ff19		2019-09-16 00:00:00	0001-01-01 00:00:00 BC
519	CONT54321	4321	User_ff19		2019-09-16 00:00:00	0001-01-01 00:00:00 BC
520	CONT11111	4327	User_ff19		2019-09-19 00:00:00	0001-01-01 00:00:00 BC
521	CONT78675	4331	User_ff19		2019-09-20 00:00:00	0001-01-01 00:00:00 BC
522	CONT657863	4335	User_ff19		2019-09-20 00:00:00	0001-01-01 00:00:00 BC
523	1	4340	demo_gatotkaca19		2019-09-21 00:00:00	0001-01-01 00:00:00 BC
524	2	4340	demo_gatotkaca19		2019-09-21 00:00:00	0001-01-01 00:00:00 BC
525	3	4340	demo_gatotkaca19		2019-09-21 00:00:00	0001-01-01 00:00:00 BC
526	ABCD1234567	4346	User_ff19		2019-09-23 00:00:00	0001-01-01 00:00:00 BC
527	CONT4539879	4353	User_ff19		2019-09-23 00:00:00	0001-01-01 00:00:00 BC
528	REGU3297842	4357	demo_agility19		2019-09-24 00:00:00	0001-01-01 00:00:00 BC
529	ABCD1234567	4361	User_ff19		2019-10-02 00:00:00	0001-01-01 00:00:00 BC
530	con789	4365	User_ff19		2019-10-04 00:00:00	0001-01-01 00:00:00 BC
531	TLLU4446790	2623	Yuliawati		2018-03-26 00:00:00	0001-01-01 00:00:00 BC
532	TCLU9329130	2623	Yuliawati		2018-03-26 00:00:00	0001-01-01 00:00:00 BC
533	TRHU3634965	2623	Yuliawati		2018-03-26 00:00:00	0001-01-01 00:00:00 BC
534	GLDU9355039	2628	Yuliawati		2018-03-26 00:00:00	0001-01-01 00:00:00 BC
535	TCLU6291220	2628	Yuliawati		2018-03-26 00:00:00	0001-01-01 00:00:00 BC
536	TEMU3688274	2632	Yuliawati		2018-03-26 00:00:00	0001-01-01 00:00:00 BC
537	TEMU6221663	2632	Yuliawati		2018-03-26 00:00:00	0001-01-01 00:00:00 BC
538	CTR72323	1589	forwarding1		2018-01-07 00:00:00	0001-01-01 00:00:00 BC
539	CTR45555	1589	forwarding1		2018-01-07 00:00:00	0001-01-01 00:00:00 BC
540	CTR5444	1594	forwarding1		2018-01-08 00:00:00	0001-01-01 00:00:00 BC
541	EISU2255021	2636	Yuliawati		2018-03-26 00:00:00	0001-01-01 00:00:00 BC
542	HCMU9093386	2636	Yuliawati		2018-03-26 00:00:00	0001-01-01 00:00:00 BC
543	ECMU3891447	2640	Yuliawati		2018-03-26 00:00:00	0001-01-01 00:00:00 BC
544	IMTU9010053	2640	Yuliawati		2018-03-26 00:00:00	0001-01-01 00:00:00 BC
545	DRYU2266224	2644	Lingga Pradana		2018-03-27 00:00:00	0001-01-01 00:00:00 BC
546	LTIU3043577	2644	Lingga Pradana		2018-03-27 00:00:00	0001-01-01 00:00:00 BC
547	FSCU4961900	2648	Lingga Pradana		2018-03-27 00:00:00	0001-01-01 00:00:00 BC
548	EITU0266416	2651	Lingga Pradana		2018-03-27 00:00:00	0001-01-01 00:00:00 BC
549	EGSU3093111	2654	Lingga Pradana		2018-03-27 00:00:00	0001-01-01 00:00:00 BC
550	EMCU9831403	2692	Yuliawati		2018-04-10 00:00:00	0001-01-01 00:00:00 BC
551	MSCU0000111	2699	coff01		2018-04-11 00:00:00	0001-01-01 00:00:00 BC
552	MSCU0000222	2699	coff01		2018-04-11 00:00:00	0001-01-01 00:00:00 BC
553	SETU0001	2703	coff01		2018-04-11 00:00:00	0001-01-01 00:00:00 BC
554	SETU00002	2703	coff01		2018-04-11 00:00:00	0001-01-01 00:00:00 BC
555	DEFG00009	2707	coff01		2018-04-11 00:00:00	0001-01-01 00:00:00 BC
556	QWER00007	2707	coff01		2018-04-11 00:00:00	0001-01-01 00:00:00 BC
557	TEST0123	2711	coff01		2018-04-11 00:00:00	0001-01-01 00:00:00 BC
558	TEST0987	2711	coff01		2018-04-11 00:00:00	0001-01-01 00:00:00 BC
559	FCIU3399759	2715	Taufik Ariesandy		2018-04-11 00:00:00	0001-01-01 00:00:00 BC
560	TCNU1551960	2728	Taufik Ariesandy		2018-04-12 00:00:00	0001-01-01 00:00:00 BC
561	TCLU8866900	2736	Lingga Pradana		2018-04-12 00:00:00	0001-01-01 00:00:00 BC
562	EGHU1049029	2739	Lingga Pradana		2018-04-13 00:00:00	0001-01-01 00:00:00 BC
563	EISU1701155	2739	Lingga Pradana		2018-04-13 00:00:00	0001-01-01 00:00:00 BC
564	EISU1830785	2739	Lingga Pradana		2018-04-13 00:00:00	0001-01-01 00:00:00 BC
565	EISU1776447	2739	Lingga Pradana		2018-04-13 00:00:00	0001-01-01 00:00:00 BC
566	HMCU1005178	2739	Lingga Pradana		2018-04-13 00:00:00	0001-01-01 00:00:00 BC
567	LTIU6063833	2739	Lingga Pradana		2018-04-13 00:00:00	0001-01-01 00:00:00 BC
568	EGHU3227984	2748	Taufik Ariesandy		2018-04-13 00:00:00	0001-01-01 00:00:00 BC
569	EGHU3228260	2748	Taufik Ariesandy		2018-04-13 00:00:00	0001-01-01 00:00:00 BC
570	EGHU3228168	2748	Taufik Ariesandy		2018-04-13 00:00:00	0001-01-01 00:00:00 BC
571	EGHU3227290	2748	Taufik Ariesandy		2018-04-13 00:00:00	0001-01-01 00:00:00 BC
572	EGHU3228044	2748	Taufik Ariesandy		2018-04-13 00:00:00	0001-01-01 00:00:00 BC
573	EGHU3228039	2748	Taufik Ariesandy		2018-04-13 00:00:00	0001-01-01 00:00:00 BC
574	TCLU7980034	2756	Taufik Ariesandy		2018-04-13 00:00:00	0001-01-01 00:00:00 BC
575		2756	Taufik Ariesandy		2018-04-13 00:00:00	0001-01-01 00:00:00 BC
576	DRYU9287276	2777	Yuliawati		2018-04-17 00:00:00	0001-01-01 00:00:00 BC
577	HMCU9063479	2777	Yuliawati		2018-04-17 00:00:00	0001-01-01 00:00:00 BC
578	DFSU6894493	2781	Yuliawati		2018-04-17 00:00:00	0001-01-01 00:00:00 BC
579	EGSU3036770	2781	Yuliawati		2018-04-17 00:00:00	0001-01-01 00:00:00 BC
580	EITU1435296	2785	Yuliawati		2018-04-17 00:00:00	0001-01-01 00:00:00 BC
581	TCNU4783065	2785	Yuliawati		2018-04-17 00:00:00	0001-01-01 00:00:00 BC
582	BSIU9592016	2789	Lingga Pradana		2018-04-17 00:00:00	0001-01-01 00:00:00 BC
583	ahs927392	2809	demo_sl		2018-04-17 00:00:00	0001-01-01 00:00:00 BC
584	abjsasa	2812	demo_coff		2018-04-17 00:00:00	0001-01-01 00:00:00 BC
585	EITU1461187	2806	Yuliawati		2018-04-17 00:00:00	0001-01-01 00:00:00 BC
586	ahsjhasa	2816	demo_coff		2018-04-17 00:00:00	0001-01-01 00:00:00 BC
587	EMCU3638280	2819	Lingga Pradana		2018-04-19 00:00:00	0001-01-01 00:00:00 BC
588	XINU1362941	2822	Lingga Pradana		2018-04-19 00:00:00	0001-01-01 00:00:00 BC
589	TLLU5202628	2825	Taufik Ariesandy		2018-04-19 00:00:00	0001-01-01 00:00:00 BC
590	TCNU1648708	2825	Taufik Ariesandy		2018-04-19 00:00:00	0001-01-01 00:00:00 BC
591	TLLU5206114	2825	Taufik Ariesandy		2018-04-19 00:00:00	0001-01-01 00:00:00 BC
592	TGCU2015190	2825	Taufik Ariesandy		2018-04-19 00:00:00	0001-01-01 00:00:00 BC
593	EISU2002018	2831	Lingga Pradana		2018-04-19 00:00:00	0001-01-01 00:00:00 BC
594	BEAU2101110	2834	Lingga Pradana		2018-04-20 00:00:00	0001-01-01 00:00:00 BC
595	EISU9120692	2837	Lingga Pradana		2018-04-20 00:00:00	0001-01-01 00:00:00 BC
596	EGHU9072370	2837	Lingga Pradana		2018-04-20 00:00:00	0001-01-01 00:00:00 BC
597	EISU9252507	2853	Yuliawati		2018-04-25 00:00:00	0001-01-01 00:00:00 BC
598	TEMU2297059	2853	Yuliawati		2018-04-25 00:00:00	0001-01-01 00:00:00 BC
599	EISU3832302	2857	Yuliawati		2018-04-25 00:00:00	0001-01-01 00:00:00 BC
600	TGHU9106330	2857	Yuliawati		2018-04-25 00:00:00	0001-01-01 00:00:00 BC
601	BMOU5209103	2861	Yuliawati		2018-04-25 00:00:00	0001-01-01 00:00:00 BC
602	XINU8175536	2861	Yuliawati		2018-04-25 00:00:00	0001-01-01 00:00:00 BC
603	CARU9481498	2865	Yuliawati		2018-04-25 00:00:00	0001-01-01 00:00:00 BC
604	TEMU3871583	2865	Yuliawati		2018-04-25 00:00:00	0001-01-01 00:00:00 BC
605	TCNU1404337	2869	Taufik Ariesandy		2018-04-26 00:00:00	0001-01-01 00:00:00 BC
606	EISU9207344	2872	Lingga Pradana		2018-04-27 00:00:00	0001-01-01 00:00:00 BC
607	TCNU5456998	2872	Lingga Pradana		2018-04-27 00:00:00	0001-01-01 00:00:00 BC
608	EISU9313583	2872	Lingga Pradana		2018-04-27 00:00:00	0001-01-01 00:00:00 BC
609	HMCU9185395	2872	Lingga Pradana		2018-04-27 00:00:00	0001-01-01 00:00:00 BC
610	EITU1738733	2872	Lingga Pradana		2018-04-27 00:00:00	0001-01-01 00:00:00 BC
611	TCNU2419250	2872	Lingga Pradana		2018-04-27 00:00:00	0001-01-01 00:00:00 BC
612	HMCU9089679	2872	Lingga Pradana		2018-04-27 00:00:00	0001-01-01 00:00:00 BC
613	BMOU4467145	2872	Lingga Pradana		2018-04-27 00:00:00	0001-01-01 00:00:00 BC
614	TGBU5004426	2872	Lingga Pradana		2018-04-27 00:00:00	0001-01-01 00:00:00 BC
615	TGHU8995151	2872	Lingga Pradana		2018-04-27 00:00:00	0001-01-01 00:00:00 BC
616	BMOU4480738	2872	Lingga Pradana		2018-04-27 00:00:00	0001-01-01 00:00:00 BC
617	DRYU9273755	2872	Lingga Pradana		2018-04-27 00:00:00	0001-01-01 00:00:00 BC
618	TEMU6276202	2872	Lingga Pradana		2018-04-27 00:00:00	0001-01-01 00:00:00 BC
619	XINU8077530	2872	Lingga Pradana		2018-04-27 00:00:00	0001-01-01 00:00:00 BC
620	EITU1353644	2872	Lingga Pradana		2018-04-27 00:00:00	0001-01-01 00:00:00 BC
621	TCLU9412408	2872	Lingga Pradana		2018-04-27 00:00:00	0001-01-01 00:00:00 BC
622	EGHU3234730	2890	Taufik Ariesandy		2018-04-30 00:00:00	0001-01-01 00:00:00 BC
623	EITU0096109	2893	Lingga Pradana		2018-05-02 00:00:00	0001-01-01 00:00:00 BC
624	TEMU0533725	2896	Lingga Pradana		2018-05-02 00:00:00	0001-01-01 00:00:00 BC
625	DFSU1440040	2899	Lingga Pradana		2018-05-02 00:00:00	0001-01-01 00:00:00 BC
626	GLDU9524519	2899	Lingga Pradana		2018-05-02 00:00:00	0001-01-01 00:00:00 BC
627	EISU2241706	2903	Lingga Pradana		2018-05-03 00:00:00	0001-01-01 00:00:00 BC
628	EGHU3494492	2906	Lingga Pradana		2018-05-07 00:00:00	0001-01-01 00:00:00 BC
629	EGHU3238309	2909	Taufik Ariesandy		2018-05-08 00:00:00	0001-01-01 00:00:00 BC
630	EGHU3238870	2909	Taufik Ariesandy		2018-05-08 00:00:00	0001-01-01 00:00:00 BC
631	EGHU3236245	2909	Taufik Ariesandy		2018-05-08 00:00:00	0001-01-01 00:00:00 BC
632	EGHU3235594	2909	Taufik Ariesandy		2018-05-08 00:00:00	0001-01-01 00:00:00 BC
633	EGHU3236585	2909	Taufik Ariesandy		2018-05-08 00:00:00	0001-01-01 00:00:00 BC
634	EGHU3236008	2909	Taufik Ariesandy		2018-05-08 00:00:00	0001-01-01 00:00:00 BC
635	GLDU9407981	2917	Taufik Ariesandy		2018-05-08 00:00:00	0001-01-01 00:00:00 BC
636	HMCU3073459	2917	Taufik Ariesandy		2018-05-08 00:00:00	0001-01-01 00:00:00 BC
637	EITU0544693	2917	Taufik Ariesandy		2018-05-08 00:00:00	0001-01-01 00:00:00 BC
638	EITU0288523	2917	Taufik Ariesandy		2018-05-08 00:00:00	0001-01-01 00:00:00 BC
639	BEAU2545389	2917	Taufik Ariesandy		2018-05-08 00:00:00	0001-01-01 00:00:00 BC
640	MAGU2195544	2917	Taufik Ariesandy		2018-05-08 00:00:00	0001-01-01 00:00:00 BC
641	EMCU3915030	2925	Lingga Pradana		2018-05-09 00:00:00	0001-01-01 00:00:00 BC
642	DRYU2679207	2928	Lingga Pradana		2018-05-11 00:00:00	0001-01-01 00:00:00 BC
643	EITU1686590	2932	Lingga Pradana		2018-05-14 00:00:00	0001-01-01 00:00:00 BC
644	TCLU7841580	2935	Lingga Pradana		2018-05-16 00:00:00	0001-01-01 00:00:00 BC
645	TLLU4917864	2935	Lingga Pradana		2018-05-16 00:00:00	0001-01-01 00:00:00 BC
646	IMTU3085800	2939	Lingga Pradana		2018-05-16 00:00:00	0001-01-01 00:00:00 BC
647	TEMU0790976	2942	Lingga Pradana		2018-05-16 00:00:00	0001-01-01 00:00:00 BC
648	EITU0191282	2945	Lingga Pradana		2018-05-21 00:00:00	0001-01-01 00:00:00 BC
649	EISU1615355	2948	Lingga Pradana		2018-05-22 00:00:00	0001-01-01 00:00:00 BC
650	EMCU1337159	2948	Lingga Pradana		2018-05-22 00:00:00	0001-01-01 00:00:00 BC
651	EMCU1460190	2948	Lingga Pradana		2018-05-22 00:00:00	0001-01-01 00:00:00 BC
652	EITU0554686	2953	Lingga Pradana		2018-05-22 00:00:00	0001-01-01 00:00:00 BC
653	TGBU5995881	2956	Lingga Pradana		2018-05-22 00:00:00	0001-01-01 00:00:00 BC
654	MAGU2185206	2959	Lingga Pradana		2018-05-30 00:00:00	0001-01-01 00:00:00 BC
655	TCNU3264614	2962	Lingga Pradana		2018-05-31 00:00:00	0001-01-01 00:00:00 BC
656	TLLU4463226	2962	Lingga Pradana		2018-05-31 00:00:00	0001-01-01 00:00:00 BC
657	EITU1239441	2966	Lingga Pradana		2018-06-04 00:00:00	0001-01-01 00:00:00 BC
658	EMCU3971028	2969	Lingga Pradana		2018-06-04 00:00:00	0001-01-01 00:00:00 BC
659	NO1234	2988	demo_ilcs		2018-10-15 00:00:00	0001-01-01 00:00:00 BC
660	NO1112	2991	demo_ilcs		2018-10-15 00:00:00	0001-01-01 00:00:00 BC
661	CONT11112	3005	demo_ilcs	demo_ilcs	2018-11-08 00:00:00	2018-11-08 00:00:00
662	CONT00001	3016	demo_ilcs	demo_sl	2018-11-08 00:00:00	2018-11-09 00:00:00
663	CONT11115	3022	demo_ilcs		2018-11-09 00:00:00	0001-01-01 00:00:00 BC
664	con123	3038	demo_ilcs		2018-11-11 00:00:00	0001-01-01 00:00:00 BC
665	gsg	3104	demo_coff2		2018-11-16 00:00:00	0001-01-01 00:00:00 BC
666	TCNU5462054	3137	Lingga Pradana		2018-11-21 00:00:00	0001-01-01 00:00:00 BC
667	TCNU2422084	3137	Lingga Pradana		2018-11-21 00:00:00	0001-01-01 00:00:00 BC
668	CTR27632	1617	adelgmail		2018-01-17 00:00:00	0001-01-01 00:00:00 BC
669	asasa	1719	coff01		2018-02-06 00:00:00	0001-01-01 00:00:00 BC
670	OCGU123456	1722	Lingga Pradana		2018-02-06 00:00:00	0001-01-01 00:00:00 BC
671	123test	1708	Yuswati		2018-01-30 00:00:00	0001-01-01 00:00:00 BC
672	IMTU123456	1722	Lingga Pradana		2018-02-06 00:00:00	0001-01-01 00:00:00 BC
673	EMCU9888137	1726	Yuliawati		2018-02-06 00:00:00	0001-01-01 00:00:00 BC
674	IMTU9058376	1729	Lingga Pradana		2018-02-06 00:00:00	0001-01-01 00:00:00 BC
675	OCGU8058947	1729	Lingga Pradana		2018-02-06 00:00:00	0001-01-01 00:00:00 BC
676	CON11110	2998	demo_ilcs		2018-11-05 00:00:00	0001-01-01 00:00:00 BC
677	CON11111	2998	demo_ilcs		2018-11-05 00:00:00	0001-01-01 00:00:00 BC
678	CONT11123	3002	demo_ilcs		2018-11-05 00:00:00	0001-01-01 00:00:00 BC
679	CONT11115	3028	demo_ilcs		2018-11-09 00:00:00	0001-01-01 00:00:00 BC
680	CONT11112	3033	demo_ilcs		2018-11-09 00:00:00	0001-01-01 00:00:00 BC
681	cont10005	3038	demo_ilcs	demo_ilcs	2018-11-11 00:00:00	2018-11-11 00:00:00
682	CON11111	3048	demo_ilcs		2018-11-11 00:00:00	0001-01-01 00:00:00 BC
683	CON11110	3048	demo_ilcs		2018-11-11 00:00:00	0001-01-01 00:00:00 BC
684	cont8888	3033	demo_sl		2018-11-11 00:00:00	0001-01-01 00:00:00 BC
685	con5555	3054	demo_ilcs		2018-11-11 00:00:00	0001-01-01 00:00:00 BC
\.


--
-- Data for Name: detail_files; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.detail_files (id, file_name, delivery_id, createby, updateby, createdate, updatedate) FROM stdin;
1	20000.pdf	3350	demo_ilcs		2019-01-17	0001-01-01 BC
2	2sunda.jpg	3354	demo_ilcs		2019-01-18	0001-01-01 BC
3	attachment.png	3386	demo_ilcs		2019-01-23	0001-01-01 BC
4	ilcs.png	3404	demo_ilcs		2019-01-25	0001-01-01 BC
5	117734.pdf	3408	Lingga Pradana		2019-01-29	0001-01-01 BC
6	117736.pdf	3412	Lingga Pradana		2019-01-29	0001-01-01 BC
7	117735.pdf	3416	Lingga Pradana		2019-01-29	0001-01-01 BC
8	182837.pdf	3420	Lingga Pradana		2019-01-29	0001-01-01 BC
9	131843.pdf	3426	Lingga Pradana		2019-01-31	0001-01-01 BC
10	117742.pdf	3430	Lingga Pradana		2019-01-31	0001-01-01 BC
11	117750.pdf	3434	Lingga Pradana		2019-01-31	0001-01-01 BC
12	117741.pdf	3438	Lingga Pradana		2019-01-31	0001-01-01 BC
13	117739.pdf	3443	Lingga Pradana		2019-01-31	0001-01-01 BC
14	SWB MUMCB18010729.pdf	3447	Heni		2019-01-31	0001-01-01 BC
15	SWB MUMCB19000180.pdf	3452	Heni		2019-01-31	0001-01-01 BC
16	pexels-photo.jpg	3534	demo_ilcs		2019-02-18	0001-01-01 BC
17	117798.pdf	3542	Lingga Pradana		2019-02-21	0001-01-01 BC
18	200532.pdf	3546	Lingga Pradana		2019-02-21	0001-01-01 BC
19	117794.pdf	3550	Lingga Pradana		2019-02-21	0001-01-01 BC
20	117766.pdf	3556	Lingga Pradana		2019-02-21	0001-01-01 BC
21	117786.pdf	3563	Lingga Pradana		2019-02-21	0001-01-01 BC
22	131846.pdf	3567	Lingga Pradana		2019-02-21	0001-01-01 BC
23	MUMCW19001080.pdf	3584	Lingga Pradana		2019-03-04	0001-01-01 BC
24	MUMCW19001207.pdf	3588	Lingga Pradana		2019-03-04	0001-01-01 BC
25	MDACW19000267 .pdf	3592	Lingga Pradana		2019-03-04	0001-01-01 BC
26	MDACW19000266 .pdf	3596	Lingga Pradana		2019-03-04	0001-01-01 BC
27	MDACW19000204 .pdf	3601	Lingga Pradana		2019-03-04	0001-01-01 BC
28	MAACW19001095.pdf	3606	Lingga Pradana		2019-03-04	0001-01-01 BC
29	2.pptx	3614	demo_ilcs		2019-03-05	0001-01-01 BC
30	117833.pdf	3618	Lingga Pradana		2019-03-05	0001-01-01 BC
31	117835.pdf	3622	Lingga Pradana		2019-03-05	0001-01-01 BC
32	117831.pdf	3626	Lingga Pradana		2019-03-05	0001-01-01 BC
33	MDACW19000266 .pdf	3630	Lingga Pradana		2019-03-05	0001-01-01 BC
34	117789.pdf	3635	Lingga Pradana		2019-03-05	0001-01-01 BC
35	117792.pdf	3640	Lingga Pradana		2019-03-05	0001-01-01 BC
36	bill-lading.gif	3005	demo_ilcs		2018-11-08	0001-01-01 BC
37	ktp.jpg	3005	demo_ilcs		2018-11-08	0001-01-01 BC
38	surat peminjaman container.jpg	3016	demo_ilcs		2018-11-08	0001-01-01 BC
39	surat pengantar.jpg	3016	demo_ilcs		2018-11-08	0001-01-01 BC
40	bill-lading.gif	3016	demo_ilcs		2018-11-08	0001-01-01 BC
41	bill-lading.gif	3022	demo_ilcs		2018-11-09	0001-01-01 BC
42	surat peminjaman container.jpg	3022	demo_ilcs		2018-11-09	0001-01-01 BC
43	bill-lading.gif	3104	demo_coff2		2018-11-16	0001-01-01 BC
44	182635.pdf	3137	Lingga Pradana		2018-11-21	0001-01-01 BC
45	117611.PDF	3142	Lingga Pradana		2018-11-22	0001-01-01 BC
46	117610.pdf	3146	Lingga Pradana		2018-11-22	0001-01-01 BC
47	117619.pdf	3150	Lingga Pradana		2018-11-22	0001-01-01 BC
48	117617.pdf	3155	Lingga Pradana		2018-11-22	0001-01-01 BC
49	117618.pdf	3159	Lingga Pradana		2018-11-22	0001-01-01 BC
50	117621.PDF	3165	Lingga Pradana		2018-11-22	0001-01-01 BC
51	SWB MUMCB18009181.pdf	3169	Heni		2018-11-22	0001-01-01 BC
52	SWB MUMCB18009256.pdf	3174	Heni		2018-11-22	0001-01-01 BC
53	182642.pdf	3179	Lingga Pradana		2018-11-23	0001-01-01 BC
54	131834.PDF	3183	Lingga Pradana		2018-11-23	0001-01-01 BC
55	182632.TIF	3187	Lingga Pradana		2018-11-26	0001-01-01 BC
56	000131021834-3-3.pdf	3191	Heni		2018-11-26	0001-01-01 BC
57	182646.PDF	3196	Lingga Pradana		2018-11-27	0001-01-01 BC
58	117622.pdf	3200	Lingga Pradana		2018-11-27	0001-01-01 BC
59	INFOMEDIA.jpg	3238	demo_ilcs		2018-12-12	0001-01-01 BC
60	cover 1.png	3281	demo_ilcs		2018-12-26	0001-01-01 BC
61	BOOKING.png	3285	kikoko		2018-12-26	0001-01-01 BC
62	117709.pdf	3325	Lingga Pradana		2019-01-15	0001-01-01 BC
63	117725.pdf	3329	Lingga Pradana		2019-01-15	0001-01-01 BC
64	117724.pdf	3333	Lingga Pradana		2019-01-15	0001-01-01 BC
65	117723.pdf	3337	Lingga Pradana		2019-01-15	0001-01-01 BC
66	182793.pdf	3341	Lingga Pradana		2019-01-15	0001-01-01 BC
67	182963.pdf	4118	Lingga Pradana		2019-06-27	0001-01-01 BC
68	surat peminjaman container.jpg	3028	demo_ilcs		2018-11-09	0001-01-01 BC
69	bill-lading.gif	3028	demo_ilcs		2018-11-09	0001-01-01 BC
70	ktp.jpg	3033	demo_ilcs		2018-11-09	0001-01-01 BC
71	bill-lading.gif	3033	demo_ilcs		2018-11-09	0001-01-01 BC
72	bill-lading.gif	3038	demo_ilcs		2018-11-11	0001-01-01 BC
73	surat peminjaman container.jpg	3038	demo_ilcs		2018-11-11	0001-01-01 BC
74	fotocopy id.jpg	3054	demo_ilcs		2018-11-11	0001-01-01 BC
75	BL 2018-322 REVV.PDF	3108	Lingga Pradana		2018-11-19	0001-01-01 BC
76	bill-lading.gif	3114	demo_ilcs		2018-11-21	0001-01-01 BC
77	WhatsApp Image 2018-11-21 at 09.12.38.jpeg	3118	demo_ilcs		2018-11-21	0001-01-01 BC
78	182626.pdf	3122	Lingga Pradana		2018-11-21	0001-01-01 BC
79	182625.pdf	3126	Lingga Pradana		2018-11-21	0001-01-01 BC
80	200518.pdf	3132	Lingga Pradana		2018-11-21	0001-01-01 BC
81	SWB MUMCB1800918001.pdf	3242	Heni		2018-12-13	0001-01-01 BC
82	image001.jpg	3248	demo_ilcs		2018-12-14	0001-01-01 BC
83	9111801989 and 9111801992 SWBL.pdf	3252	Heni		2018-12-19	0001-01-01 BC
84	Alam.jpg	3257	demo_ilcs		2018-12-19	0001-01-01 BC
85	Alam.jpg	3261	demo_ilcs		2018-12-19	0001-01-01 BC
86	Screenshot_2018-12-20-01-14-29-83.png	3265	kikoko		2018-12-20	0001-01-01 BC
87	Screenshot_2018-12-20-01-14-29-83.png	3269	kikoko		2018-12-20	0001-01-01 BC
88	Screenshot_2018-12-20-01-14-29-83.png	3273	kikoko		2018-12-20	0001-01-01 BC
89	Alam.jpg	3277	demo_ilcs		2018-12-20	0001-01-01 BC
90	BOOKING.png	3289	demo_ilcs		2018-12-27	0001-01-01 BC
91	npwp-perorangan.jpg	3293	kikoko		2018-12-27	0001-01-01 BC
92	SWB MUMCB18010001.pdf	3297	Heni		2018-12-27	0001-01-01 BC
93	SWB MUMCB18010054.pdf	3301	Heni		2018-12-27	0001-01-01 BC
94	konfirmasi berhasil.png	3307	demo_ilcs		2019-01-02	0001-01-01 BC
95	SWB MUMCB18010269.pdf	3311	Heni		2019-01-03	0001-01-01 BC
96	SWB MUMCB18010414.pdf	3316	Heni		2019-01-10	0001-01-01 BC
97	SWB MUMCB18010454.pdf	3320	Heni		2019-01-10	0001-01-01 BC
98	131842.pdf	3346	Lingga Pradana		2019-01-17	0001-01-01 BC
99	sign personal.jpg	3460	demo_ilcs		2019-02-01	0001-01-01 BC
100	ilcs.png	3465	demo_ilcs		2019-02-01	0001-01-01 BC
101	saad faray ftv.png	3474	demo_ilcs		2019-02-01	0001-01-01 BC
102	registration succes.png	3514	demo_ilcs		2019-02-18	0001-01-01 BC
103	ilcs.png	3518	demo_ilcs		2019-02-18	0001-01-01 BC
104	ILCS..jpg	3522	demo_ilcs		2019-02-18	0001-01-01 BC
105	117858.pdf	3647	Lingga Pradana		2019-03-11	0001-01-01 BC
106	117857.pdf	3652	Lingga Pradana		2019-03-11	0001-01-01 BC
107	117891.pdf	3656	Lingga Pradana		2019-03-14	0001-01-01 BC
108	117892.pdf	3661	Lingga Pradana		2019-03-14	0001-01-01 BC
109	117887.pdf	3672	Lingga Pradana		2019-03-14	0001-01-01 BC
110	117910.pdf	3685	Lingga Pradana		2019-03-21	0001-01-01 BC
111	117896.pdf	3690	Lingga Pradana		2019-03-21	0001-01-01 BC
112	117895.pdf	3694	Lingga Pradana		2019-03-21	0001-01-01 BC
113	117897.pdf	3698	Lingga Pradana		2019-03-21	0001-01-01 BC
114	117929.PDF	3702	Lingga Pradana		2019-03-21	0001-01-01 BC
115	117934.pdf	3706	Lingga Pradana		2019-03-28	0001-01-01 BC
116	200535.pdf	3711	Lingga Pradana		2019-03-28	0001-01-01 BC
117	117950.PDF	3715	Lingga Pradana		2019-03-28	0001-01-01 BC
118	117937.pdf	3720	Lingga Pradana		2019-03-28	0001-01-01 BC
119	117952.pdf	3725	Lingga Pradana		2019-03-28	0001-01-01 BC
120	131850.pdf	3729	Lingga Pradana		2019-03-29	0001-01-01 BC
121	117969.PDF	3744	Lingga Pradana		2019-04-01	0001-01-01 BC
122	117949.pdf	3748	Lingga Pradana		2019-04-01	0001-01-01 BC
123	117980.pdf	3809	Lingga Pradana		2019-04-05	0001-01-01 BC
124	200539.pdf	3813	Lingga Pradana		2019-04-08	0001-01-01 BC
125	118016.PDF	3817	Lingga Pradana		2019-04-09	0001-01-01 BC
126	200540.pdf	3821	Lingga Pradana		2019-04-10	0001-01-01 BC
127	118021.pdf	3825	Lingga Pradana		2019-04-11	0001-01-01 BC
128	118008.pdf	3830	Lingga Pradana		2019-04-11	0001-01-01 BC
129	118033.pdf	3834	Lingga Pradana		2019-04-11	0001-01-01 BC
130	118058.PDF	3891	Lingga Pradana		2019-04-26	0001-01-01 BC
131	118064.pdf	3896	Lingga Pradana		2019-04-26	0001-01-01 BC
132	118063.pdf	3907	Lingga Pradana		2019-04-26	0001-01-01 BC
133	118073.PDF	3911	Lingga Pradana		2019-04-26	0001-01-01 BC
134	131857.pdf	3916	Lingga Pradana		2019-04-29	0001-01-01 BC
135	200541.pdf	3920	Lingga Pradana		2019-04-30	0001-01-01 BC
136	118118.pdf	3924	Lingga Pradana		2019-04-30	0001-01-01 BC
137	118099.pdf	3929	Lingga Pradana		2019-05-02	0001-01-01 BC
138	118104.PDF	3933	Lingga Pradana		2019-05-02	0001-01-01 BC
139	118116.PDF	3938	Lingga Pradana		2019-05-03	0001-01-01 BC
140	1.jpeg	3955	demo_ilcs		2019-05-08	0001-01-01 BC
141	118144.PDF	3961	Lingga Pradana		2019-05-10	0001-01-01 BC
142	118133.PDF	3965	Lingga Pradana		2019-05-13	0001-01-01 BC
143	118119.pdf	3969	Lingga Pradana		2019-05-13	0001-01-01 BC
144	118132.pdf	3976	Lingga Pradana		2019-05-13	0001-01-01 BC
145	118160.pdf	3980	Lingga Pradana		2019-05-14	0001-01-01 BC
146	118112.pdf	3985	Lingga Pradana		2019-05-14	0001-01-01 BC
147	118177.pdf	3990	Lingga Pradana		2019-05-14	0001-01-01 BC
148	118114.pdf	3994	Lingga Pradana		2019-05-14	0001-01-01 BC
149	118113.pdf	3998	Lingga Pradana		2019-05-14	0001-01-01 BC
150	Pedoman PKL FKOM UMHT 2018 (1).pdf	4188	demo_ff19		2019-08-26	0001-01-01 BC
151	Pedoman PKL FKOM UMHT 2018 (1).pdf	4197	demo_ff19		2019-08-27	0001-01-01 BC
152	user guide GD.doc	4201	demo_ff19		2019-08-27	0001-01-01 BC
153	22.-SOP-Penerimaan-Pemberitahuan-Pabean-Dalam-Rangka-Impor-BC-2.0-Melalui-PDE.pdf	4205	demo_ff19		2019-08-27	0001-01-01 BC
154	22.-SOP-Penerimaan-Pemberitahuan-Pabean-Dalam-Rangka-Impor-BC-2.0-Melalui-PDE.pdf	4209	demo_ff19		2019-08-27	0001-01-01 BC
155	UAT Gud Do.docx	4213	demo_ff19		2019-08-27	0001-01-01 BC
156	UAT Gud Do.pdf	4217	demo_ff19		2019-08-27	0001-01-01 BC
157	UAT Gud Do.pdf	4222	demo_ff19		2019-08-27	0001-01-01 BC
158	npwp.docx	4229	User_ff19		2019-08-28	0001-01-01 BC
159	npwp.docx	4233	User_ff19		2019-08-28	0001-01-01 BC
160	22.-SOP-Penerimaan-Pemberitahuan-Pabean-Dalam-Rangka-Impor-BC-2.0-Melalui-PDE.pdf	4237	User_ff19		2019-08-28	0001-01-01 BC
161	Alam.jpg	4241	User_ff19		2019-08-28	0001-01-01 BC
162	tes.jpg	4246	User_ff19		2019-09-01	0001-01-01 BC
163	11jadilah-hamba-allah-yang-bersyukur-1024x705.png	4251	User_ff19		2019-09-03	0001-01-01 BC
164	uts.docx	4255	User_ff19		2019-09-04	0001-01-01 BC
165	My Cargo - Product Catalog v1.2.docx	4259	User_ff19		2019-09-04	0001-01-01 BC
166	My Cargo - Product Catalog v1.2.docx	4263	User_ff19		2019-09-04	0001-01-01 BC
167	none.jpg	4271	User_ff19		2019-09-05	0001-01-01 BC
168	none.jpg	4276	User_ff19		2019-09-05	0001-01-01 BC
169	none.jpg	4280	User_ff19		2019-09-05	0001-01-01 BC
170	87150.jpg	4297	User_ff19		2019-09-10	0001-01-01 BC
171	bill-lading.gif	4301	User_ff19		2019-09-10	0001-01-01 BC
172	ktp.jpg	4305	User_ff19		2019-09-11	0001-01-01 BC
173	deliveryorder.jpg	4309	User_ff19		2019-09-12	0001-01-01 BC
174	we bare bears.jpg	4327	User_ff19		2019-09-19	0001-01-01 BC
175	My Curriculum Vitae Halberto Sidiq.pdf	4331	User_ff19		2019-09-20	0001-01-01 BC
176	User Guide GudFleet.docx	4335	User_ff19		2019-09-20	0001-01-01 BC
177	llll.png	4340	demo_gatotkaca19		2019-09-21	0001-01-01 BC
178	none.jpg	4346	User_ff19		2019-09-23	0001-01-01 BC
179	none.jpg	4267	User_ff19		2019-09-05	0001-01-01 BC
180	My Cargo - Product Catalog v1.2.docx	4288	User_ff19		2019-09-06	0001-01-01 BC
181	bill-lading.gif	4292	User_ff19		2019-09-09	0001-01-01 BC
182	surat kuasa.png	4292	User_ff19		2019-09-09	0001-01-01 BC
183	bill-lading.gif	4313	User_ff19		2019-09-16	0001-01-01 BC
184	bill-lading.gif	4317	User_ff19		2019-09-16	0001-01-01 BC
185	bill-lading.gif	4321	User_ff19		2019-09-16	0001-01-01 BC
186	Benline New Logo may 2019 (1).jpg	4353	User_ff19		2019-09-23	0001-01-01 BC
187	BL.pdf	4357	demo_agility19		2019-09-24	0001-01-01 BC
188	An Tong Logsitics.png	4361	User_ff19		2019-10-02	0001-01-01 BC
189	bill-receipt-transaction-256.png	4365	User_ff19		2019-10-04	0001-01-01 BC
190	Narasi presentasi OW New.docx	4175	demo_ff19		2019-08-22	0001-01-01 BC
191	118194.pdf	4002	Lingga Pradana		2019-05-14	0001-01-01 BC
192	118207.pdf	4006	Lingga Pradana		2019-05-15	0001-01-01 BC
193	ILCS..jpg	4013	demo_ilcs		2019-05-17	0001-01-01 BC
194	118219.pdf	4038	Lingga Pradana		2019-05-24	0001-01-01 BC
195	118220.pdf	4043	Lingga Pradana		2019-05-24	0001-01-01 BC
196	AirAsia Travel Itinerary - Booking No. (NR8D9P).pdf	4048	demo_ilcs		2019-05-24	0001-01-01 BC
197	GOT.jpg	4048	demo_ilcs		2019-05-24	0001-01-01 BC
198	118224.pdf	4055	Lingga Pradana		2019-05-28	0001-01-01 BC
199	118223.pdf	4059	Lingga Pradana		2019-05-28	0001-01-01 BC
200	1.jpeg	4114	demo_ilcs		2019-06-20	0001-01-01 BC
201	bill-lading.gif	3010	demo_ilcs		2018-11-08	0001-01-01 BC
202	surat pengantar.jpg	3010	demo_ilcs		2018-11-08	0001-01-01 BC
203	surat peminjaman container.jpg	3010	demo_ilcs		2018-11-08	0001-01-01 BC
204	7...jpg	3061	demo_ilcs		2018-11-15	0001-01-01 BC
205	MUMCW18008895 .pdf	3066	Lingga Pradana		2018-11-15	0001-01-01 BC
206	SWB MDACW18001137 (invoice nos 556, 557 560 to 562).pdf	3070	Lingga Pradana		2018-11-15	0001-01-01 BC
207	06112018132017.pdf	3077	Lingga Pradana		2018-11-15	0001-01-01 BC
208	06112018132029.pdf	3082	Lingga Pradana		2018-11-15	0001-01-01 BC
209	surat kuasa.png	3096	demo_ilcs		2018-11-15	0001-01-01 BC
210	ILCS..jpg	3526	demo_ilcs		2019-02-18	0001-01-01 BC
211	ILCS..jpg	3530	demo_ilcs		2019-02-18	0001-01-01 BC
212	Business Model Canvas Bang Kiri Bang (BKB).png	3538	alyafortesting		2019-02-18	0001-01-01 BC
213	pexels-photo.jpg	3571	demo_ilcs		2019-02-25	0001-01-01 BC
214	117795.pdf	3575	Lingga Pradana		2019-02-26	0001-01-01 BC
215	117834.pdf	3579	Lingga Pradana		2019-02-28	0001-01-01 BC
216	LCHCB19004849.pdf	3610	Lingga Pradana		2019-03-04	0001-01-01 BC
217	BL.pdf	3086	Heni		2018-11-15	0001-01-01 BC
218	fotocopy id.jpg	3091	demo_ilcs		2018-11-15	0001-01-01 BC
219	Alam.jpg	3223	demo_ilcs		2018-12-07	0001-01-01 BC
220	Standard code.png	3223	demo_ilcs		2018-12-07	0001-01-01 BC
221	iCargo picturee.png	3229	demo_ilcs		2018-12-09	0001-01-01 BC
222	INTTRA.png	3233	demo_ilcs		2018-12-12	0001-01-01 BC
223	iCargo picturee.png	3233	demo_ilcs		2018-12-12	0001-01-01 BC
224	117740.pdf	3359	Lingga Pradana		2019-01-21	0001-01-01 BC
225	117726.pdf	3363	Lingga Pradana		2019-01-22	0001-01-01 BC
226	117729.pdf	3369	Lingga Pradana		2019-01-22	0001-01-01 BC
227	117730.pdf	3373	Lingga Pradana		2019-01-22	0001-01-01 BC
228	117731.pdf	3377	Lingga Pradana		2019-01-22	0001-01-01 BC
229	117738.pdf	3381	Lingga Pradana		2019-01-22	0001-01-01 BC
230	bookinf.png	3390	demo_ilcs		2019-01-23	0001-01-01 BC
231	117746.pdf	3395	Lingga Pradana		2019-01-24	0001-01-01 BC
232	182819.pdf	3399	Lingga Pradana		2019-01-24	0001-01-01 BC
233	117751.pdf	3456	Lingga Pradana		2019-02-01	0001-01-01 BC
234	saad faray ftv.png	3469	demo_ilcs		2019-02-01	0001-01-01 BC
235	pexels-photo.jpg	3478	demo_ilcs		2019-02-05	0001-01-01 BC
236	182838.pdf	3482	Lingga Pradana		2019-02-07	0001-01-01 BC
237	182857.pdf	3486	Lingga Pradana		2019-02-07	0001-01-01 BC
238	117770.pdf	3491	Lingga Pradana		2019-02-13	0001-01-01 BC
239	117774.pdf	3495	Lingga Pradana		2019-02-13	0001-01-01 BC
240	117767.pdf	3499	Lingga Pradana		2019-02-18	0001-01-01 BC
241	117787.pdf	3505	Lingga Pradana		2019-02-18	0001-01-01 BC
242	117765.pdf	3509	Lingga Pradana		2019-02-18	0001-01-01 BC
243	117877.pdf	3676	Lingga Pradana		2019-03-18	0001-01-01 BC
244	117889.pdf	3680	Lingga Pradana		2019-03-18	0001-01-01 BC
245	ILCS..jpg	3736	demo_ilcs		2019-03-31	0001-01-01 BC
246	Screenshot_20181206-101531_Gallery.jpg	3740	demo_ilcs		2019-03-31	0001-01-01 BC
247	Cash Bonus_Cash Payment Form_042018.pdf	3756	demo_ilcs		2019-04-01	0001-01-01 BC
248	117968.pdf	3762	Lingga Pradana		2019-04-04	0001-01-01 BC
249	117967.PDF	3770	Lingga Pradana		2019-04-04	0001-01-01 BC
250	117974.pdf	3775	Lingga Pradana		2019-04-04	0001-01-01 BC
251	117985.PDF	3784	Lingga Pradana		2019-04-04	0001-01-01 BC
252	200538.pdf	3788	Lingga Pradana		2019-04-04	0001-01-01 BC
253	200537.pdf	3792	Lingga Pradana		2019-04-04	0001-01-01 BC
254	117942.pdf	3796	Lingga Pradana		2019-04-04	0001-01-01 BC
255	117988.pdf	3801	Lingga Pradana		2019-04-04	0001-01-01 BC
256	131852.pdf	3805	Lingga Pradana		2019-04-04	0001-01-01 BC
257	118017.pdf	3839	Lingga Pradana		2019-04-12	0001-01-01 BC
258	131855.pdf	3843	Lingga Pradana		2019-04-15	0001-01-01 BC
259	118039.pdf	3847	Lingga Pradana		2019-04-15	0001-01-01 BC
260	118028.PDF	3851	Lingga Pradana		2019-04-15	0001-01-01 BC
261	KTP Budiarto.png	3855	demo_ilcs		2019-04-15	0001-01-01 BC
262	118048.pdf	3860	Lingga Pradana		2019-04-15	0001-01-01 BC
263	118004.pdf	3866	Lingga Pradana		2019-04-16	0001-01-01 BC
264	118042.pdf	3872	Lingga Pradana		2019-04-16	0001-01-01 BC
265	118054.pdf	3876	Lingga Pradana		2019-04-18	0001-01-01 BC
266	118046.pdf	3880	Lingga Pradana		2019-04-24	0001-01-01 BC
267	118067.PDF	3887	Lingga Pradana		2019-04-25	0001-01-01 BC
268	118134.pdf	3942	Lingga Pradana		2019-05-06	0001-01-01 BC
269	131860.pdf	3947	Lingga Pradana		2019-05-08	0001-01-01 BC
270	118149.pdf	3951	Lingga Pradana		2019-05-08	0001-01-01 BC
271	Surat Kuasa.jpg	4017	demo_ilcs		2019-05-21	0001-01-01 BC
272	Surat Peminjaman Container.png	4017	demo_ilcs		2019-05-21	0001-01-01 BC
273	118187.pdf	4023	Lingga Pradana		2019-05-22	0001-01-01 BC
274	182875.pdf	4028	Lingga Pradana		2019-05-22	0001-01-01 BC
275	MYCARGO_BKG_1_112635_.html	4032	demo_ilcs		2019-05-22	0001-01-01 BC
276	118217.pdf	4064	Lingga Pradana		2019-05-31	0001-01-01 BC
277	131864.pdf	4069	Lingga Pradana		2019-06-13	0001-01-01 BC
278	118250.pdf	4074	Lingga Pradana		2019-06-13	0001-01-01 BC
279	118239.pdf	4079	Lingga Pradana		2019-06-13	0001-01-01 BC
280	118249.PDF	4084	Lingga Pradana		2019-06-13	0001-01-01 BC
281	182914.pdf	4090	Lingga Pradana		2019-06-14	0001-01-01 BC
282	118237.pdf	4094	Lingga Pradana		2019-06-19	0001-01-01 BC
283	118248.pdf	4099	Lingga Pradana		2019-06-19	0001-01-01 BC
284	200547.pdf	4105	Lingga Pradana		2019-06-19	0001-01-01 BC
285	182919.pdf	4109	Lingga Pradana		2019-06-19	0001-01-01 BC
286	none.jpg	4284	User_ff19		2019-09-05	0001-01-01 BC
\.


--
-- Data for Name: gparam; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.gparam (id_gparam, rfid1, xs1, xs2, xn1, xn2) FROM stdin;
1		GEN_USERS_REG_ID	1	337	\N
\.


--
-- Data for Name: members; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.members (id, createby, createdate, updateby, updatedate, npwp) FROM stdin;
2	system	2018-01-22 00:00:00		2018-01-22 00:00:00	11.122.233.4-455.556
3	system	2018-01-22 00:00:00		2018-01-22 00:00:00	11.122.233.4-455.556
4	system	2018-01-21 00:00:00		2018-01-21 00:00:00	
5	system	2018-01-22 00:00:00		2018-01-22 00:00:00	27.392.739.7-297.323
6	system	2018-01-23 00:00:00		2018-01-23 00:00:00	234234234234
7	system	2018-01-23 00:00:00		2018-01-23 00:00:00	234234234234
8	system	2018-01-24 00:00:00		2018-01-24 00:00:00	234234234234
9	system	2018-01-24 00:00:00		2018-01-24 00:00:00	234234234234
10	system	2018-01-24 00:00:00		2018-01-24 00:00:00	234234234234
11	system	2018-01-24 00:00:00		2018-01-24 00:00:00	234234234234
12	system	2018-01-24 00:00:00		2018-01-24 00:00:00	234234234234
13	system	2018-01-24 00:00:00		2018-01-24 00:00:00	68.268.242.4-242.441
14	system	2018-01-25 00:00:00		2018-01-25 00:00:00	11.111.111.1-111.111
15	system	2018-01-25 00:00:00		2018-01-25 00:00:00	22.222.222.2-222.222
16	system	2018-01-25 00:00:00		2018-01-25 00:00:00	12.121.212.1-212.121
17	system	2018-01-25 00:00:00		2018-01-25 00:00:00	27.392.732.9-737.293
18	system	2018-01-25 00:00:00		2018-01-25 00:00:00	02.238.557.9-046.000
19	system	2018-01-25 00:00:00		2018-01-25 00:00:00	01.718.327.8-093.000
20	system	2018-01-25 00:00:00		2018-01-25 00:00:00	02.238.557.9-046.000
21	system	2018-01-25 00:00:00		2018-01-25 00:00:00	02.238.557.9-046.000
22	system	2018-01-25 00:00:00		2018-01-25 00:00:00	02.238.557.9-046.000
23	system	2018-01-26 00:00:00		2018-01-26 00:00:00	12-2334909-01
24	system	2018-01-26 00:00:00		2018-01-26 00:00:00	12-2334909-04
25	system	2018-01-26 00:00:00		2018-01-26 00:00:00	98.988.808.8-889.888
26	system	2018-01-26 00:00:00		2018-01-26 00:00:00	01.060.143.3-092.000
27	system	2018-01-26 00:00:00		2018-01-26 00:00:00	01.082.783.0-641.000
28	system	2018-01-26 00:00:00		2018-01-26 00:00:00	01.315.900.9-054.000
29	system	2018-01-26 00:00:00		2018-01-26 00:00:00	68.268.242.4-242.441
30	system	2018-01-26 00:00:00		2018-01-26 00:00:00	12.121.212.1-212.121
31	system	2018-01-26 00:00:00		2018-01-26 00:00:00	12.312.312.1-313.133
32	system	2018-01-26 00:00:00		2018-01-26 00:00:00	26.323.626.8-326.382
33	system	2018-01-26 00:00:00		2018-01-26 00:00:00	88.089.765.4-654.678
34	system	2018-01-30 00:00:00		2018-01-30 00:00:00	12-2334909-02
35	system	2018-01-30 00:00:00		2018-01-30 00:00:00	01.670.200.3-058.000
36	system	2018-01-30 00:00:00		2018-01-30 00:00:00	01.234.567.8-999.999
37	system	2018-02-01 00:00:00		2018-02-01 00:00:00	01.234.567.8-999.999
38	system	2018-02-01 00:00:00		2018-02-01 00:00:00	01.234.567.8-999.999
39	system	2018-02-01 00:00:00		2018-02-01 00:00:00	01.234.567.8-999.999
40	system	2018-02-01 00:00:00		2018-02-01 00:00:00	01.234.567.8-999.999
41	system	2018-02-02 00:00:00		2018-02-02 00:00:00	37.493.747.3-437.947
42	system	2018-02-02 00:00:00		2018-02-02 00:00:00	27.392.793.7-273.927
43	system	2018-02-02 00:00:00		2018-02-02 00:00:00	54.561.161.8-418.000
44	system	2018-02-02 00:00:00		2018-02-02 00:00:00	93.849.389.4-993.943
45	system	2018-02-02 00:00:00		2018-02-02 00:00:00	01.234.567.8-999.999
46	system	2018-02-03 00:00:00		2018-02-03 00:00:00	02.323.203.0-230.023
47	system	2018-02-04 00:00:00		2018-02-04 00:00:00	92.379.239.2-332.323
48	system	2018-02-05 00:00:00		2018-02-05 00:00:00	99.890.765.4-678.908
49	system	2018-02-05 00:00:00		2018-02-05 00:00:00	29.323.263.2-638.268
50	system	2018-02-05 00:00:00		2018-02-05 00:00:00	66.422.976.2-405.000
51	system	2018-02-05 00:00:00		2018-02-05 00:00:00	01.234.567.8-999.999
52	system	2018-02-05 00:00:00		2018-02-05 00:00:00	66.422.976.2-405.000
53	system	2018-02-05 00:00:00		2018-02-05 00:00:00	49.394.934.9-394.379
54	system	2018-02-05 00:00:00		2018-02-05 00:00:00	49.394.934.9-394.379
55	system	2018-02-05 00:00:00		2018-02-05 00:00:00	49.394.934.9-394.379
56	system	2018-02-05 00:00:00		2018-02-05 00:00:00	92.739.723.7-923.727
57	system	2018-02-05 00:00:00		2018-02-05 00:00:00	63.623.263.8-283.623
58	system	2018-02-05 00:00:00		2018-02-05 00:00:00	29.327.372.9-379.273
59	system	2018-02-05 00:00:00		2018-02-05 00:00:00	23.727.372.3-929.372
60	system	2018-02-05 00:00:00		2018-02-05 00:00:00	09.273.927.3-273.293
61	system	2018-02-05 00:00:00		2018-02-05 00:00:00	93.493.943.9-434.739
62	system	2018-02-05 00:00:00		2018-02-05 00:00:00	20.830.283.2-032.838
63	system	2018-02-05 00:00:00		2018-02-05 00:00:00	01.234.567.8-999.999
64	system	2018-02-05 00:00:00		2018-02-05 00:00:00	13.123.123.1-231.232
65	system	2018-02-05 00:00:00		2018-02-05 00:00:00	11.111.111.1-111.111
66	system	2018-02-05 00:00:00		2018-02-05 00:00:00	92.738.273.7-237.273
67	system	2018-02-06 00:00:00		2018-02-06 00:00:00	72.937.927.3-729.392
68	system	2018-02-06 00:00:00		2018-02-06 00:00:00	90.909.090.9-302.909
69	system	2018-02-06 00:00:00		2018-02-06 00:00:00	09.090.958.7-857.485
70	system	2018-02-06 00:00:00		2018-02-06 00:00:00	54.561.161.8-418.000
71	system	2018-02-06 00:00:00		2018-02-06 00:00:00	54.561.161.8-418.000
72	system	2018-02-06 00:00:00		2018-02-06 00:00:00	
73	system	2018-02-07 00:00:00		2018-02-07 00:00:00	01.824.511.8-058.000
74	system	2018-02-12 00:00:00		2018-02-12 00:00:00	01.234.567.8-999.999
75	system	2018-02-14 00:00:00		2018-02-14 00:00:00	12.345.678.8-999.999
76	system	2018-02-14 00:00:00		2018-02-14 00:00:00	
77	system	2018-02-19 00:00:00		2018-02-19 00:00:00	01.060.143.3-092.000
78	system	2018-02-19 00:00:00		2018-02-19 00:00:00	83.748.378.4-374.387
79	system	2018-02-19 00:00:00		2018-02-19 00:00:00	01.234.567.8-999.999
80	system	2018-02-20 00:00:00		2018-02-20 00:00:00	
81	system	2018-02-20 00:00:00		2018-02-20 00:00:00	
82	system	2018-02-20 00:00:00		2018-02-20 00:00:00	
83	system	2018-02-20 00:00:00		2018-02-20 00:00:00	
84	system	2018-02-20 00:00:00		2018-02-20 00:00:00	88.089.765.4-654.678
85	system	2018-02-20 00:00:00		2018-02-20 00:00:00	
86	system	2018-02-21 00:00:00		2018-02-21 00:00:00	12.121.821.9-218.921
87	system	2018-02-21 00:00:00		2018-02-21 00:00:00	
88	system	2018-02-21 00:00:00		2018-02-21 00:00:00	26.382.636.2-382.328
89	system	2018-02-21 00:00:00		2018-02-21 00:00:00	22.222.222.2-222.222
90	system	2018-02-21 00:00:00		2018-02-21 00:00:00	88.089.765.4-654.678
91	system	2018-02-21 00:00:00		2018-02-21 00:00:00	88.764.567.3-423.156
92	system	2018-02-21 00:00:00		2018-02-21 00:00:00	88.908.769.0-056.007
93	system	2018-02-21 00:00:00		2018-02-21 00:00:00	88.987.654.3-789.007
94	system	2018-02-21 00:00:00		2018-02-21 00:00:00	88.987.654.3-789.007
95	system	2018-02-21 00:00:00		2018-02-21 00:00:00	23.279.372.9-372.739
96	system	2018-02-21 00:00:00		2018-02-21 00:00:00	88.987.654.3-789.007
97	system	2018-02-21 00:00:00		2018-02-21 00:00:00	39.439.743.9-749.374
98	system	2018-02-21 00:00:00		2018-02-21 00:00:00	88.987.654.3-789.007
99	system	2018-02-21 00:00:00		2018-02-21 00:00:00	88.987.654.3-789.007
100	system	2018-02-22 00:00:00		2018-02-22 00:00:00	23.020.302.3-028.302
101	system	2018-02-22 00:00:00		2018-02-22 00:00:00	88.098.678.6-541.006
102	system	2018-02-22 00:00:00		2018-02-22 00:00:00	88.098.678.6-541.006
103	system	2018-02-22 00:00:00		2018-02-22 00:00:00	88.098.678.6-541.006
104	system	2018-02-22 00:00:00		2018-02-22 00:00:00	88.098.678.6-541.006
105	system	2018-02-22 00:00:00		2018-02-22 00:00:00	88.987.687.0-076.587
106	system	2018-02-23 00:00:00		2018-02-23 00:00:00	01.824.511.8-058.000
107	system	2018-02-23 00:00:00		2018-02-23 00:00:00	01.824.511.8-058.000
108	system	2018-02-23 00:00:00		2018-02-23 00:00:00	01.824.511.8-058.000
109	system	2018-02-23 00:00:00		2018-02-23 00:00:00	01.824.511.8-058.000
110	system	2018-02-23 00:00:00		2018-02-23 00:00:00	12-2334909-07
111	system	2018-02-23 00:00:00		2018-02-23 00:00:00	01.234.567.8-999.999
112	system	2018-02-26 00:00:00		2018-02-26 00:00:00	28.382.738.2-783.782
113	system	2018-02-26 00:00:00		2018-02-26 00:00:00	29.379.273.7-297.372
114	system	2018-02-26 00:00:00		2018-02-26 00:00:00	29.379.273.7-297.372
115	system	2018-02-27 00:00:00		2018-02-27 00:00:00	01.234.567.8-999.999
116	system	2018-03-02 00:00:00		2018-03-02 00:00:00	29.392.392.7-392.372
117	system	2018-03-04 00:00:00		2018-03-04 00:00:00	23.232.323.2-323.293
118	system	2018-03-08 00:00:00		2018-03-08 00:00:00	
119	system	2018-03-09 00:00:00		2018-03-09 00:00:00	23.792.732.7-392.739
120	system	2018-03-16 00:00:00		2018-03-16 00:00:00	11.111.111.1-111.111
121	system	2018-03-23 00:00:00		2018-03-23 00:00:00	88.987.687.0-076.587
122	system	2018-03-23 00:00:00		2018-03-23 00:00:00	88.987.687.0-076.587
123	system	2016-06-07 00:00:00		0001-01-01 00:00:00 BC	0
124	system	2016-06-27 00:00:00		0001-01-01 00:00:00 BC	agility
125	system	2018-03-23 00:00:00		2018-03-23 00:00:00	88.987.687.0-076.587
126	system	2018-03-23 00:00:00		2018-03-23 00:00:00	88.987.687.0-076.587
127	system	2018-03-23 00:00:00		2018-03-23 00:00:00	88.987.687.0-076.587
128	system	2018-03-23 00:00:00		2018-03-23 00:00:00	88.987.687.0-076.587
129	system	2018-03-23 00:00:00		2018-03-23 00:00:00	88.987.687.0-076.587
130	system	2018-03-23 00:00:00		2018-03-23 00:00:00	88.987.687.0-076.587
131	system	2018-03-23 00:00:00		2018-03-23 00:00:00	11.111.111.1-111.111
132	system	2018-03-23 00:00:00		2018-03-23 00:00:00	11.111.111.1-111.111
133	system	2018-03-23 00:00:00		2018-03-23 00:00:00	11.111.111.1-111.111
134	system	2018-03-23 00:00:00		2018-03-23 00:00:00	11.111.111.1-111.111
135	system	2018-03-23 00:00:00		2018-03-23 00:00:00	11.111.111.1-111.111
136	system	2018-04-01 00:00:00		2018-04-01 00:00:00	
137	system	2018-04-02 00:00:00		2018-04-02 00:00:00	12-2334909-02
138	system	2018-04-08 00:00:00		2018-04-08 00:00:00	01.061.152.3-051.000
139	system	2018-04-09 00:00:00		2018-04-09 00:00:00	
140	system	2018-04-17 00:00:00		2018-04-17 00:00:00	
141	system	2018-04-17 00:00:00		2018-04-17 00:00:00	12-2334909-12
142	system	2018-04-20 00:00:00		2018-04-20 00:00:00	
143	system	2018-04-20 00:00:00		2018-04-20 00:00:00	54.561.161.8-418.000
144	system	2018-04-25 00:00:00		2018-04-25 00:00:00	
145	system	2018-05-09 00:00:00		2018-05-09 00:00:00	18.273.172.3-981.729
146	system	2018-05-21 00:00:00		2018-05-21 00:00:00	88.987.687.0-076.587
147	system	2018-05-21 00:00:00		2018-05-21 00:00:00	88.987.687.0-076.587
148	system	2018-05-22 00:00:00		2018-05-22 00:00:00	
149	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	
150	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	
151	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	03.271.810.8.031.000
152	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	03.271.810.8.031.000
153	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	03.271.810.8.031.000
154	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	03.271.810.8.031.000
155	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	03.271.810.8.031.000
156	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	03.271.810.8.031.000
157	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	01.839.563.2-004.000
158	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	01.762.668.0-015.000
159	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	01.909.141.2-015.000
160	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	
161	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	01.307.577.5-076.000
162	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	
163	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	
164	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	01.333.289.5.045.000
165	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	01.333.289.5.045.000
166	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	02.194.050.7.058.000
167	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	03.271.810.8.031.000
168	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	03.271.810.8.031.000
169	ALFI	2018-05-18 00:00:00		0001-01-01 00:00:00 BC	02.091.660.7-011.000
170	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	03.271.810.8.031.000
171	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	03.271.810.8.031.000
172	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	03.271.810.8.031.000
173	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	03.271.810.8.031.000
174	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	03.271.810.8.031.000
175	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	03.271.810.8.031.000
176	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	03.271.810.8.031.000
177	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	03.271.810.8.031.000
178	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	03.271.810.8.031.000
179	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	03.271.810.8.031.000
180	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	03.271.810.8.031.000
181	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	03.271.810.8.031.000
182	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	03.271.810.8.031.000
183	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	03.271.810.8.031.000
184	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	03.271.810.8.031.000
185	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	03.271.810.8.031.000
186	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	03.271.810.8.031.000
187	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	03.271.810.8.031.000
188	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	03.271.810.8.031.000
189	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	03.271.810.8.031.000
190	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	03.271.810.8.031.000
191	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	03.271.810.8.031.000
192	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	03.271.810.8.031.000
193	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	03.271.810.8.031.000
194	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	03.271.810.8.031.000
195	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	03.271.810.8.031.000
196	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	03.271.810.8.031.000
197	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	03.271.810.8.031.000
198	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	03.271.810.8.031.000
199	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	03.271.810.8.031.000
200	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	03.271.810.8.031.000
201	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	03.271.810.8.031.000
202	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	03.271.810.8.031.000
203	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	03.271.810.8.031.000
204	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	03.271.810.8.031.000
205	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	03.271.810.8.031.000
206	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	03.271.810.8.031.000
207	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	03.271.810.8.031.000
208	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	03.271.810.8.031.000
209	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	03.271.810.8.031.000
210	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	03.271.810.8.031.000
211	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	03.271.810.8.031.000
212	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	03.271.810.8.031.000
213	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	01.839.563.2-004.000
214	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	01.839.563.2-004.000
215	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	01.839.563.2-004.000
216	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	03.271.810.8.031.000
217	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	01.839.563.2-004.000
218	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	01.839.563.2-004.000
219	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	01.839.563.2-004.000
220	ALFI	2018-05-24 00:00:00		0001-01-01 00:00:00 BC	01.839.563.2-004.000
221	ALFI	2018-05-31 00:00:00		0001-01-01 00:00:00 BC	
222	ALFI	2018-05-31 00:00:00		0001-01-01 00:00:00 BC	02.475.880.7.048.000
223	ALFI	2018-05-31 00:00:00		0001-01-01 00:00:00 BC	987654321123456
224	ALFI	2018-05-31 00:00:00		0001-01-01 00:00:00 BC	03.271.810.8.031.000
225	system	2018-06-03 00:00:00		2018-06-03 00:00:00	03.170.814.2-822.000
226	system	2018-07-12 00:00:00		2018-07-12 00:00:00	12-2334909-02
227	system	2018-07-13 00:00:00		2018-07-13 00:00:00	12-2334909-02
228	ALFI	2018-07-13 00:00:00		0001-01-01 00:00:00 BC	02.091.660.7-011.000
229	ALFI	2018-07-18 00:00:00		0001-01-01 00:00:00 BC	987654321123456
230	ALFI	2018-07-18 00:00:00		0001-01-01 00:00:00 BC	987654321123456
231	system	2018-07-23 00:00:00		2018-07-23 00:00:00	01.234.567.8-900.000
232	system	2018-07-23 00:00:00		2018-07-23 00:00:00	01.234.567.8-900.000
233	system	2018-07-23 00:00:00		2018-07-23 00:00:00	01.234.567.8-900.000
234	system	2018-07-23 00:00:00		2018-07-23 00:00:00	01.234.567.8-900.000
235	system	2018-07-23 00:00:00		2018-07-23 00:00:00	01.234.567.8-900.000
236	system	2018-07-25 00:00:00		2018-07-25 00:00:00	01.234.567.8-900.000
237	system	2018-07-25 00:00:00		2018-07-25 00:00:00	01.234.567.8-900.000
238	system	2018-07-25 00:00:00		2018-07-25 00:00:00	01.234.567.8-900.000
239	system	2018-07-25 00:00:00		2018-07-25 00:00:00	01.234.567.8-900.000
240	system	2018-07-25 00:00:00		2018-07-25 00:00:00	01.234.567.8-900.001
241	system	2018-07-25 00:00:00		2018-07-25 00:00:00	01.234.567.8-900.001
242	system	2018-07-25 00:00:00		2018-07-25 00:00:00	01.234.567.8-900.000
243	system	2018-07-25 00:00:00		2018-07-25 00:00:00	01.234.567.8-900.000
244	ALFI	2018-08-03 00:00:00		0001-01-01 00:00:00 BC	5676867
245	ALFI	2018-08-20 00:00:00		0001-01-01 00:00:00 BC	
246	ALFI	2018-08-20 00:00:00		0001-01-01 00:00:00 BC	
247	ALFI	2018-08-20 00:00:00		0001-01-01 00:00:00 BC	
248	ALFI	2018-08-20 00:00:00		0001-01-01 00:00:00 BC	71.245.857.9-421.000
249	ALFI	2018-08-20 00:00:00		0001-01-01 00:00:00 BC	
250	ALFI	2018-08-28 00:00:00		0001-01-01 00:00:00 BC	03.286.141.1-643.000
251	ALFI	2018-08-28 00:00:00		0001-01-01 00:00:00 BC	03.286.141.1-643.000
252	ALFI	2018-08-28 00:00:00		0001-01-01 00:00:00 BC	03.286.141.1-643.000
253	ALFI	2018-08-28 00:00:00		0001-01-01 00:00:00 BC	03.286.141.1-643.000
254	ALFI	2018-08-28 00:00:00		0001-01-01 00:00:00 BC	03.286.141.1-643.000
255	ALFI	2018-08-28 00:00:00		0001-01-01 00:00:00 BC	03.286.141.1-643.000
256	ALFI	2018-08-28 00:00:00		0001-01-01 00:00:00 BC	03.286.141.1-643.000
257	system	2018-08-28 00:00:00		2018-08-28 00:00:00	12-2334909-04
258	system	2018-08-28 00:00:00		2018-08-28 00:00:00	12-2334909-04
259	system	2018-08-28 00:00:00		2018-08-28 00:00:00	12.345.678.9-012.345
260	system	2018-08-28 00:00:00		2018-08-28 00:00:00	12.345.678.9-009.876
261	system	2018-08-28 00:00:00		2018-08-28 00:00:00	11.111.111.1-111.110
262	system	2018-08-28 00:00:00		2018-08-28 00:00:00	12.345.678.9-067.890
263	system	2018-08-28 00:00:00		2018-08-28 00:00:00	12.345.676.5-456.789
264	system	2018-08-28 00:00:00		2018-08-28 00:00:00	12.345.678.7-654.323
265	system	2018-08-29 00:00:00		2018-08-29 00:00:00	12-2334909-04
266	system	2018-08-29 00:00:00		2018-08-29 00:00:00	12-2334909-02
267	system	2018-08-29 00:00:00		2018-08-29 00:00:00	12-2334909-01
268	system	2018-08-30 00:00:00		2018-08-30 00:00:00	987654321123456
269	ALFI	2018-09-03 00:00:00		0001-01-01 00:00:00 BC	02.409.293.4-027.000
270	system	2018-09-03 00:00:00		2018-09-03 00:00:00	33.242.424.2-424.234
271	ALFI	2018-09-04 00:00:00		0001-01-01 00:00:00 BC	02.706.998.8-043.000
272	ALFI	2018-09-04 00:00:00		0001-01-01 00:00:00 BC	02.816.620-5.014.000
273	ALFI	2018-09-07 00:00:00		0001-01-01 00:00:00 BC	
274	ALFI	2018-09-10 00:00:00		0001-01-01 00:00:00 BC	
275	ALFI	2018-09-10 00:00:00		0001-01-01 00:00:00 BC	
276	ALFI	2018-09-10 00:00:00		0001-01-01 00:00:00 BC	
277	ALFI	2018-09-10 00:00:00		0001-01-01 00:00:00 BC	
278	ALFI	2018-09-10 00:00:00		0001-01-01 00:00:00 BC	
279	ALFI	2018-09-10 00:00:00		0001-01-01 00:00:00 BC	20974002074000
280	ALFI	2018-09-10 00:00:00		0001-01-01 00:00:00 BC	03.178.278.2-015.000
281	ALFI	2018-09-12 00:00:00		0001-01-01 00:00:00 BC	02.378.731.0-005.000
282	ALFI	2018-09-13 00:00:00		0001-01-01 00:00:00 BC	84.577.797.8-044.000
283	ALFI	2018-09-13 00:00:00		0001-01-01 00:00:00 BC	71.245.857.9-421.000
284	ALFI	2018-09-13 00:00:00		0001-01-01 00:00:00 BC	01.802.727.6-017.000
285	ALFI	2018-09-13 00:00:00		0001-01-01 00:00:00 BC	10004935093000
286	ALFI	2018-09-13 00:00:00		0001-01-01 00:00:00 BC	
287	ALFI	2018-09-13 00:00:00		0001-01-01 00:00:00 BC	21944814058000
288	ALFI	2018-09-13 00:00:00		0001-01-01 00:00:00 BC	01.938.521.0-042.000
289	ALFI	2018-09-14 00:00:00		0001-01-01 00:00:00 BC	
290	system	2018-09-14 00:00:00		2018-09-14 00:00:00	33.242.424.2-424.234
291	system	2018-09-14 00:00:00		2018-09-14 00:00:00	02.409.293.4-027.000
292	system	2018-09-14 00:00:00		2018-09-14 00:00:00	02.706.998.8-043.000
293	ALFI	2018-09-17 00:00:00		0001-01-01 00:00:00 BC	16967192015000
294	ALFI	2018-09-17 00:00:00		0001-01-01 00:00:00 BC	02.161.014.2-014.000
295	ALFI	2018-09-18 00:00:00		0001-01-01 00:00:00 BC	24169450044000
296	ALFI	2018-09-18 00:00:00		0001-01-01 00:00:00 BC	
297	ALFI	2018-09-18 00:00:00		0001-01-01 00:00:00 BC	16967192015000
298	system	2018-09-18 00:00:00		2018-09-18 00:00:00	01.696.719.2-015.000
299	system	2018-09-20 00:00:00		2018-09-20 00:00:00	24169450044000
300	system	2018-09-21 00:00:00		2018-09-21 00:00:00	02.194.257.8-058.000
301	ALFI	2018-09-24 00:00:00		0001-01-01 00:00:00 BC	02.194.000.2-003.000
302	ALFI	2018-09-24 00:00:00		0001-01-01 00:00:00 BC	02.056.230.2-005.000
303	system	2018-09-24 00:00:00		2018-09-24 00:00:00	71.245.857.9-421.000
304	ALFI	2018-10-04 00:00:00		0001-01-01 00:00:00 BC	
305	system	2018-10-04 00:00:00		2018-10-04 00:00:00	84.577.797.8-044.000
306	system	2018-10-04 00:00:00		2018-10-04 00:00:00	02.378.731.0-005.000
307	system	2018-10-05 00:00:00		2018-10-05 00:00:00	02.161.014.2-014.000
308	ALFI	2018-10-05 00:00:00		0001-01-01 00:00:00 BC	10004935093000
309	system	2018-10-05 00:00:00		2018-10-05 00:00:00	10004935093000
310	system	2018-10-08 00:00:00		2018-10-08 00:00:00	12.194.257.8-058.000
311	ALFI	2018-10-09 00:00:00		0001-01-01 00:00:00 BC	987654321123456
312	system	2018-10-10 00:00:00		2018-10-10 00:00:00	16967192015000
313	system	2018-10-10 00:00:00		2018-10-10 00:00:00	16967192015000
314	system	2018-10-10 00:00:00		2018-10-10 00:00:00	12.345.678.9-000.097
315	system	2018-10-10 00:00:00		2018-10-10 00:00:00	12.365.467.8-900.000
316	system	2018-10-10 00:00:00		2018-10-10 00:00:00	12.345.678.9-098.700
317	system	2018-10-10 00:00:00		2018-10-10 00:00:00	29.479.312.9-479.419
318	system	2018-10-10 00:00:00		2018-10-10 00:00:00	12.345.678.0-098.788
319	system	2018-10-10 00:00:00		2018-10-10 00:00:00	12.345.671.2-400.000
320	system	2018-10-10 00:00:00		2018-10-10 00:00:00	12.345.678.9-123.000
321	system	2018-10-10 00:00:00		2018-10-10 00:00:00	66.422.976.2-405.000
322	system	2018-10-10 00:00:00		2018-10-10 00:00:00	35.653.253.5-345.455
323	system	2018-10-10 00:00:00		2018-10-10 00:00:00	12.443.657.6-544.534
324	system	2018-10-10 00:00:00		2018-10-10 00:00:00	33.242.424.2-424.234
325	system	2018-10-10 00:00:00		2018-10-10 00:00:00	21.324.657.8-654.333
326	system	2018-10-10 00:00:00		2018-10-10 00:00:00	21.345.678.6-543.456
327	system	2018-10-10 00:00:00		2018-10-10 00:00:00	77.677.575.7-564.675
328	system	2018-10-10 00:00:00		2018-10-10 00:00:00	23.456.789.9-234.456
329	system	2018-10-10 00:00:00		2018-10-10 00:00:00	88.908.769.0-056.007
330	system	2018-10-11 00:00:00		2018-10-11 00:00:00	09.878.909.8-789.000
331	system	2018-10-11 00:00:00		2018-10-11 00:00:00	33.242.424.2-424.234
332	system	2018-10-11 00:00:00		2018-10-11 00:00:00	01.571.411.6-005.000
333	system	2018-10-11 00:00:00		2018-10-11 00:00:00	17.898.789.0-900.000
334	system	2018-10-11 00:00:00		2018-10-11 00:00:00	02.194.257.8-058.000
335	system	2018-10-11 00:00:00		2018-10-11 00:00:00	02.194.257.8-058.000
336	system	2018-10-14 00:00:00		2018-10-14 00:00:00	19.890.989.0-989.000
337	system	2018-10-14 00:00:00		2018-10-14 00:00:00	12.000.898.7-900.000
338	system	2018-10-14 00:00:00		2018-10-14 00:00:00	12.768.989.0-878.000
339	ALFI	2018-10-30 00:00:00		0001-01-01 00:00:00 BC	10004935093000
340	ALFI	2018-10-30 00:00:00		0001-01-01 00:00:00 BC	10004935093000
341	ALFI	2018-10-30 00:00:00		0001-01-01 00:00:00 BC	10004935093000
342	system	2018-10-30 00:00:00		2018-10-30 00:00:00	01.000.493.5-093.000
343	system	2018-11-14 00:00:00		2018-11-14 00:00:00	01.670.200.3-058.000
344	system	2018-11-14 00:00:00		2018-11-14 00:00:00	01.670.200.3-058.000
345	system	2018-11-14 00:00:00		2018-11-14 00:00:00	01.670.200.3-058.000
346	system	2018-11-14 00:00:00		2018-11-14 00:00:00	01.670.200.3-058.000
347	system	2018-11-14 00:00:00		2018-11-14 00:00:00	01.670.200.3-058.000
348	system	2018-11-14 00:00:00		2018-11-14 00:00:00	01.670.200.3-058.000
349	system	2018-11-14 00:00:00		2018-11-14 00:00:00	01.670.200.3-058.000
350	system	2018-11-15 00:00:00		2018-11-15 00:00:00	02.433.881.6-011.000
351	system	2018-11-16 00:00:00		2018-11-16 00:00:00	03.078.258.5-086.000
352	system	2018-11-27 00:00:00		2018-11-27 00:00:00	02.194.257.8-058.000
353	system	2018-11-23 00:00:00		2018-11-23 00:00:00	35.456.754.3-567.865
354	system	2018-11-27 00:00:00		2018-11-27 00:00:00	12-2334909-04
355	system	2018-11-27 00:00:00		2018-11-27 00:00:00	12-2334909-01
356	system	2018-11-27 00:00:00		2018-11-27 00:00:00	02.194.257.8-058.000
357	system	2018-11-27 00:00:00		2018-11-27 00:00:00	12-2334909-06
358	system	2018-11-27 00:00:00		2018-11-27 00:00:00	12-2334909-06
359	system	2018-11-27 00:00:00		2018-11-27 00:00:00	12-2334909-01
360	system	2018-11-27 00:00:00		2018-11-27 00:00:00	12-2334909-01
361	system	2018-11-28 00:00:00		2018-11-28 00:00:00	02.194.257.8-058.000
362	system	2018-11-28 00:00:00		2018-11-28 00:00:00	12-2334909-07
363	system	2018-11-28 00:00:00		2018-11-28 00:00:00	12-2334909-07
364	system	2018-11-28 00:00:00		2018-11-28 00:00:00	12-2334909-07
365	system	2018-11-28 00:00:00		2018-11-28 00:00:00	12-2334909-07
366	system	2018-11-28 00:00:00		2018-11-28 00:00:00	12-2334909-07
367	system	2018-11-28 00:00:00		2018-11-28 00:00:00	12-2334909-04
368	system	2018-11-28 00:00:00		2018-11-28 00:00:00	12-2334909-04
369	system	2018-11-28 00:00:00		2018-11-28 00:00:00	12-2334909-07
370	system	2018-11-28 00:00:00		2018-11-28 00:00:00	12-2334909-07
371	system	2018-11-29 00:00:00		2018-11-29 00:00:00	12-2334909-01
372	system	2018-12-12 00:00:00		2018-12-12 00:00:00	34.568.326.8-739.748
373	system	2018-12-12 00:00:00		2018-12-12 00:00:00	62.530.984.3-738.257
374	system	2018-12-13 00:00:00		2018-12-13 00:00:00	12-2334909-11
375	system	2018-12-19 00:00:00		2018-12-19 00:00:00	88.089.765.4-654.678
376	system	2018-12-19 00:00:00		2018-12-19 00:00:00	12-2334909-11
377	system	2018-12-19 00:00:00		2018-12-19 00:00:00	19.231.231.2-312.311
378	system	2018-12-19 00:00:00		2018-12-19 00:00:00	19.231.231.2-312.311
379	system	2018-12-20 00:00:00		2018-12-20 00:00:00	12-2334909-11
380	system	2018-12-20 00:00:00		2018-12-20 00:00:00	12-2334909-11
381	system	2018-12-20 00:00:00		2018-12-20 00:00:00	12-2334909-11
382	system	2018-12-20 00:00:00		2018-12-20 00:00:00	12-2334909-11
383	system	2018-12-20 00:00:00		2018-12-20 00:00:00	12-2334909-11
384	system	2018-12-20 00:00:00		2018-12-20 00:00:00	12-2334909-11
385	system	2018-12-21 00:00:00		2018-12-21 00:00:00	12-2334909-11
386	system	2018-12-28 00:00:00		2018-12-28 00:00:00	12-2334909-11
387	system	2018-12-28 00:00:00		2018-12-28 00:00:00	12-2334909-11
388	system	2018-12-28 00:00:00		2018-12-28 00:00:00	12-2334909-11
389	system	2019-01-15 00:00:00		2019-01-15 00:00:00	73.835.548.6-086.000
390	system	2019-01-23 00:00:00		2019-01-23 00:00:00	54.561.161.8-418.000
391	system	2019-01-25 00:00:00		2019-01-25 00:00:00	01.385.991.3-028.000
392	system	2019-02-07 00:00:00		2019-02-07 00:00:00	23.232.323.3-232.323
393	system	2019-02-18 00:00:00		2019-02-18 00:00:00	01.321.865.6-054.000
394	system	2019-03-12 00:00:00		2019-03-12 00:00:00	73.317.915.4-407.000
395	system	2019-03-14 00:00:00		2019-03-14 00:00:00	73.317.915.4-407.000
396	system	2019-03-20 00:00:00		2019-03-20 00:00:00	02.383.515.0-805.000
397	system	2019-03-21 00:00:00		2019-03-21 00:00:00	73.317.915.4-407.000
398	system	2019-04-13 00:00:00		2019-04-13 00:00:00	02.094.539.0-027.000
399	system	2019-04-13 00:00:00		2019-04-13 00:00:00	02.094.539.0-027.000
400	system	2019-05-10 00:00:00		2019-05-10 00:00:00	01.320.057.1-075.000
401	system	2019-05-21 00:00:00		2019-05-21 00:00:00	00.000.000.0-000.000
402	ilcs	2016-09-22 00:00:00	ilcs	2016-09-22 00:00:00	13159009054000
403	ilcs	2016-09-28 00:00:00	ilcs	2016-09-28 00:00:00	999111
404	ilcs	2017-02-20 00:00:00	ilcs	2017-02-20 00:00:00	02.238.557.9-046.000
405	ilcs	2017-02-20 00:00:00	ilcs	2017-02-20 00:00:00	983839833
406	ilcs	2017-02-20 00:00:00	ilcs	2017-02-20 00:00:00	45656312454213
407	ilcs	2017-02-22 00:00:00	ilcs	2017-02-22 00:00:00	97273273293
408	system	2017-03-21 00:00:00		2017-03-21 00:00:00	00.000.000.0-000.000
409	system	2017-03-17 00:00:00		2017-03-17 00:00:00	31.223.123.1-231.231
410	system	2017-03-17 00:00:00		2017-03-17 00:00:00	24.424.223.2-323.232
411	system	2017-03-16 00:00:00		2017-03-16 00:00:00	123123
412	system	2017-03-16 00:00:00		2017-03-16 00:00:00	213123
413	system	2017-03-16 00:00:00		2017-03-16 00:00:00	973497397434
414	system	2017-03-17 00:00:00		2017-03-17 00:00:00	87.846.264.2-482.648
415	system	2017-03-21 00:00:00		2017-03-21 00:00:00	82.392.392.3-829.388
416	system	2017-03-20 00:00:00		2017-03-20 00:00:00	12.345.678.9-076.564
417	system	2017-03-20 00:00:00		2017-03-20 00:00:00	72.932.739.2-732.932
418	ilcs	2017-03-14 00:00:00	ilcs	2017-03-14 00:00:00	293923923232
419	ilcs	2017-03-20 00:00:00	ilcs	2017-03-20 00:00:00	02.238.557.9-046.000
420	system	2017-03-21 00:00:00		2017-03-21 00:00:00	27.327.372.7-392.739
421	ilcs	2017-03-14 00:00:00	ilcs	2017-03-14 00:00:00	929329232323
422	system	2017-03-21 00:00:00		2017-03-21 00:00:00	
423	system	2017-03-21 00:00:00		2017-03-21 00:00:00	88.888.888.8-888.888
424	system	2017-03-21 00:00:00		2017-03-21 00:00:00	92.793.723.2-732.937
425	ilcs	2017-03-21 00:00:00	ilcs	2017-03-21 00:00:00	9237492747274
426	system	2017-03-22 00:00:00		2017-03-22 00:00:00	97.232.739.2-737.293
427	system	2017-03-22 00:00:00		2017-03-22 00:00:00	12.312.312.3-231.231
428	ilcs	2017-03-22 00:00:00	ilcs	2017-03-22 00:00:00	93497374739434
429	system	2017-03-22 00:00:00		2017-03-22 00:00:00	12.312.312.3-123.123
430	system	2017-03-22 00:00:00		2017-03-22 00:00:00	29.836.236.2-963.263
431	system	2017-03-22 00:00:00		2017-03-22 00:00:00	19.379.739.2-932.973
432	system	2017-03-22 00:00:00		2017-03-22 00:00:00	98.239.273.2-372.379
433	system	2017-03-22 00:00:00		2017-03-22 00:00:00	12.112.121.2-121.212
434	system	2017-03-23 00:00:00		2017-03-23 00:00:00	13.792.392.7-973.297
435	system	2017-03-23 00:00:00		2017-03-23 00:00:00	92.749.374.3-739.749
436	system	2017-03-23 00:00:00		2017-03-23 00:00:00	12.345.678.9-012.345
437	system	2017-03-23 00:00:00		2017-03-23 00:00:00	00.099.212.2-939.992
438	system	2017-03-23 00:00:00		2017-03-23 00:00:00	12.422.354.3-543.436
439	system	2017-03-23 00:00:00		2017-03-23 00:00:00	62.622.627.2-272.727
440	system	2017-03-24 00:00:00		2017-03-24 00:00:00	39.479.373.7-493.479
441	system	2017-03-24 00:00:00		2017-03-24 00:00:00	17.297.172.9-127.172
442	system	2017-03-24 00:00:00		2017-03-24 00:00:00	29.732.732.9-372.329
443	system	2017-03-24 00:00:00		2017-03-24 00:00:00	23.123.123.1-231.231
444	system	2017-03-30 00:00:00		2017-03-30 00:00:00	01.111.111.1-111.111
445	system	2017-03-30 00:00:00		2017-03-30 00:00:00	
446	system	2017-03-31 00:00:00		2017-03-31 00:00:00	12.345.768.9-797.643
447	system	2017-03-31 00:00:00		2017-03-31 00:00:00	12.312.312.3-123.123
448	system	2017-03-31 00:00:00		2017-03-31 00:00:00	97.297.397.2-372.793
449	system	2017-04-01 00:00:00		2017-04-01 00:00:00	54.561.161.8-418.000
450	system	2017-04-01 00:00:00		2017-04-01 00:00:00	02.238.557.9-046.000
451	system	2017-04-03 00:00:00		2017-04-03 00:00:00	02.193.924.4-058.000
452	system	2017-04-04 00:00:00		2017-04-04 00:00:00	01.797.289.4-062.000
453	system	2017-04-04 00:00:00		2017-04-04 00:00:00	
454	system	2017-04-06 00:00:00		2017-04-06 00:00:00	01.000.571.8-092.000
455	system	2017-04-06 00:00:00		2017-04-06 00:00:00	02.059.040.2-058.000
456	system	2017-04-06 00:00:00		2017-04-06 00:00:00	19.231.231.2-312.312
457	system	2017-04-07 00:00:00		2017-04-07 00:00:00	01.000.271.5-055.000
458	system	2017-04-10 00:00:00		2017-04-10 00:00:00	01.936.249.0-058.000
459	system	2017-04-11 00:00:00		2017-04-11 00:00:00	31.567.982.9-045.000
460	system	2017-04-11 00:00:00		2017-04-11 00:00:00	01.328.570.5-007.000
461	system	2017-04-11 00:00:00		2017-04-11 00:00:00	01.233.162.5-631.000
462	system	2017-04-11 00:00:00		2017-04-11 00:00:00	01.200.023.0-003.400
463	system	2017-04-11 00:00:00		2017-04-11 00:00:00	01.061.617.5-081.000
464	system	2017-04-11 00:00:00		2017-04-11 00:00:00	01.061.608.4-081.000
465	system	2017-04-11 00:00:00		2017-04-11 00:00:00	01.308.550.1-081.000
466	system	2017-04-11 00:00:00		2017-04-11 00:00:00	01.071.736.1-055.000
467	system	2017-04-17 00:00:00		2017-04-17 00:00:00	
468	system	2017-04-17 00:00:00		2017-04-17 00:00:00	02.021.249.4-431.000
469	system	2017-04-17 00:00:00		2017-04-17 00:00:00	01.364.628.6-046.000
470	system	2017-04-18 00:00:00		2017-04-18 00:00:00	
471	system	2017-04-18 00:00:00		2017-04-18 00:00:00	01.718.327.8-093.000
472	system	2017-04-20 00:00:00		2017-04-20 00:00:00	01.716.291.8-613.000
473	system	2017-04-21 00:00:00		2017-04-21 00:00:00	01.716.291.8-042.001
474	system	2017-04-21 00:00:00		2017-04-21 00:00:00	01.105.785.8-431.000
475	system	2017-04-21 00:00:00		2017-04-21 00:00:00	
476	system	2017-04-21 00:00:00		2017-04-21 00:00:00	
477	system	2017-04-25 00:00:00		2017-04-25 00:00:00	01.466.115.1-042.001
478	system	2017-04-26 00:00:00		2017-04-26 00:00:00	
479	system	2017-04-27 00:00:00		2017-04-27 00:00:00	03.093.841.9-031.000
480	system	2017-04-28 00:00:00		2017-04-28 00:00:00	
481	system	2017-04-28 00:00:00		2017-04-28 00:00:00	02.506.653.1-045.000
482	system	2017-05-03 00:00:00		2017-05-03 00:00:00	01.061.759.5-054.000
483	system	2017-05-03 00:00:00		2017-05-03 00:00:00	81.630.273.1-045.000
484	system	2017-05-05 00:00:00		2017-05-05 00:00:00	01.065.305.3-055.000
485	system	2017-05-10 00:00:00		2017-05-10 00:00:00	01.001.701.0-92.000
486	system	2017-05-10 00:00:00		2017-05-10 00:00:00	01.000.514.8-092.000
487	system	2017-05-12 00:00:00		2017-05-12 00:00:00	01.060.143.3-092.000
488	system	2017-05-12 00:00:00		2017-05-12 00:00:00	12.345.678.8-999.999
489	system	2017-05-22 00:00:00		2017-05-22 00:00:00	01.001.895.0-054.000
490	system	2017-06-05 00:00:00		2017-06-05 00:00:00	79.630.793.2-402.000
491	system	2017-07-28 00:00:00		2017-07-28 00:00:00	01.082.783.0-641.000
492	system	2017-08-01 00:00:00		2017-08-01 00:00:00	03.222.975.9-027.000
493	system	2017-08-11 00:00:00		2017-08-11 00:00:00	11.111.111.1-111.111
494	system	2017-08-28 00:00:00		2017-08-28 00:00:00	
495	system	2017-10-12 00:00:00		2017-10-12 00:00:00	80.517.390.3-503.000
496	system	2018-01-12 00:00:00		2018-01-12 00:00:00	
497	system	2018-01-12 00:00:00		2018-01-12 00:00:00	20.010.020.0-100.999
498	system	2018-01-12 00:00:00		2018-01-12 00:00:00	01.234.567.8-999.999
499	system	2018-01-16 00:00:00		2018-01-16 00:00:00	12.312.312.3-123.123
500	system	2018-01-16 00:00:00		2018-01-16 00:00:00	12.312.312.3-123.123
501	system	2018-01-16 00:00:00		2018-01-16 00:00:00	12.312.312.3-123.123
502	system	2018-01-16 00:00:00		2018-01-16 00:00:00	12.312.312.3-123.123
503	system	2018-01-16 00:00:00		2018-01-16 00:00:00	12.312.312.3-123.123
504	system	2018-01-16 00:00:00		2018-01-16 00:00:00	12.312.312.3-123.123
505	system	2018-01-16 00:00:00		2018-01-16 00:00:00	12.312.312.3-123.123
506	system	2018-01-16 00:00:00		2018-01-16 00:00:00	12.312.312.3-123.123
507	system	2018-01-16 00:00:00		2018-01-16 00:00:00	12.312.312.3-123.123
508	system	2018-01-16 00:00:00		2018-01-16 00:00:00	12.312.312.3-123.123
509	system	2018-01-19 00:00:00		2018-01-19 00:00:00	11.111.111.1-111.111
510	system	2018-01-19 00:00:00		2018-01-19 00:00:00	12-2334909-04
511	system	2018-01-19 00:00:00		2018-01-19 00:00:00	12-2334909-04
512	system	2018-01-19 00:00:00		2018-01-19 00:00:00	12-2334909-01
513	system	2018-01-19 00:00:00		2018-01-19 00:00:00	12-2334909-02
514	system	2018-01-19 00:00:00		2018-01-19 00:00:00	26.382.636.2-382.328
515	system	2018-03-23 00:00:00		2018-03-23 00:00:00	88.987.687.0-076.587
516	system	2018-03-23 00:00:00		2018-03-23 00:00:00	88.987.687.0-076.587
517	system	2018-03-23 00:00:00		2018-03-23 00:00:00	88.987.687.0-076.587
518	system	2018-03-23 00:00:00		2018-03-23 00:00:00	88.987.687.0-076.587
\.


--
-- Data for Name: news; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.news (id, judul, isi, gambar) FROM stdin;
\.


--
-- Data for Name: price; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.price (id, createby, createdate, updateby, updatedate, qmax, qmin, price, scheme) FROM stdin;
\.


--
-- Data for Name: req_sl_delivery; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.req_sl_delivery (id, createby, createdate, updateby, updatedate, amount, blnum, callsign, donum, duedate, ffid, ffname, refid, slid, slname, sppbnum, vessel, request_id, reqext, filedo, fileinv, fileproformainv, voyage, transfee, qtykontainer, amount2, total, status_ws, pkk) FROM stdin;
5	User_ff19	2019-09-12 00:00:00	demo_rcl19	2019-09-16 00:00:00	1300	BLTEST12345		BLTEST12345	2019-09-17 00:00:00	855	PT. FORWARDING CARGO		917	PT. BHUM MULIA PRIMA/PT BINTIKA BANGUNUSA(RCL)		CTP DELTA	4308	0	CDR CAIU2556629.pdf	CDR CAIU2556629.pdf			0	\N	130000	131300	N	
49	User_ff19	2019-09-19 00:00:00		2019-09-19 00:00:00		BLTEST11111			0001-01-01 00:00:00 BC	855	PT. FORWARDING CARGO		1166	PT.LAYAR SENTOSA SHIPPING			4326	0					0	\N	0	0	N	
50	User_ff19	2019-09-20 00:00:00	User_sl19	2019-09-20 00:00:00	22000	BL12345		BL12345	2019-10-04 00:00:00	855	PT. FORWARDING CARGO		451	PT. SHIPPING LINE			4330	0	Itinerary(286).pdf	My Curriculum Vitae Halberto Sidiq.pdf			0	\N	1000000	1022000	N	
51	User_ff19	2019-09-20 00:00:00	User_sl19	2019-09-20 00:00:00	22000	BL45267586297		BL45267586297	2019-09-25 00:00:00	855	PT. FORWARDING CARGO		451	PT. SHIPPING LINE			4334	0	Weekly Review1908.pptx	Kalender Event.pptx			0	\N	1000000	1022000	N	
52	demo_gatotkaca19	2019-09-21 00:00:00		2019-09-21 00:00:00		1231			0001-01-01 00:00:00 BC	1018	PT. Ayutrans Utama		451	PT. SHIPPING LINE			4339	0					0	\N	0	0	N	
53	User_ff19	2019-10-04 00:00:00	User_sl19	2019-10-04 00:00:00	1000	BL789			0001-01-01 00:00:00 BC	855	PT. FORWARDING CARGO		451	PT. SHIPPING LINE			4364	0			Cost-Checklist-Document-256.png		0	\N	1000	2000	N	
54	User_ff19	2019-09-16 00:00:00	demo_benline19	2019-09-16 00:00:00	1000	BLTEST67890		BLTEST67890	2019-10-04 00:00:00	855	PT. FORWARDING CARGO		1160	PT. Bahari Eka Nusantara			4312	0	Delivery Order.pdf	Invoice and Inboud Deposit.pdf			0	\N	0	1000	N	
55	User_ff19	2019-09-16 00:00:00	demo_benline19	2019-09-16 00:00:00	1000	BLTEST112233		BLTEST112233	2019-09-22 00:00:00	855	PT. FORWARDING CARGO		1160	PT. Bahari Eka Nusantara			4316	0	do_190916104316_0001.pdf	inv_190916104351_0001.pdf			0	\N	0	1000	N	
56	User_ff19	2019-09-16 00:00:00	demo_rcl19	2019-09-23 00:00:00	1000	BLTEST54321		BLTEST54321	2019-09-26 00:00:00	855	PT. FORWARDING CARGO		917	PT. BHUM MULIA PRIMA/PT BINTIKA BANGUNUSA(RCL)		MP THE BROWN	4320	0	DO.pdf	MUMCB19006258_inv.pdf		121S	0	\N	0	1000	N	
57	User_ff19	2019-09-23 00:00:00		2019-09-23 00:00:00		BL123445			0001-01-01 00:00:00 BC	855	PT. FORWARDING CARGO		917	PT. BHUM MULIA PRIMA/PT BINTIKA BANGUNUSA(RCL)			4345	0					0	\N	0	0	N	
58	User_ff19	2019-09-23 00:00:00		2019-09-23 00:00:00		BLTEST7846			0001-01-01 00:00:00 BC	855	PT. FORWARDING CARGO		451	PT. SHIPPING LINE			4352	0					0	\N	0	0	N	
59	User_ff19	2019-10-02 00:00:00		2019-10-02 00:00:00		BLCOBA1234			0001-01-01 00:00:00 BC	855	PT. FORWARDING CARGO		451	PT. SHIPPING LINE			4360	0					0	\N	0	0	N	
\.


--
-- Data for Name: requests; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.requests (id, createby, createdate, updateby, updatedate, reqnum, status, total, trxdate, trxnum, company_id, tag, email_paid, notes, bank_id, channel_id, approvalcode, merchantid, terminalid, batchnum, tracenum, refnum, payment_code, note_request, paid_status) FROM stdin;
1	User_ff19	2019-09-12 00:00:00	demo_rcl19	2019-09-16 00:00:00	REQ201991200000	CLSE		0001-01-01 00:00:00 BC		855	4	1		1	2									
2	User_ff19	2019-09-19 00:00:00	User_ff19	2019-09-19 00:00:00	REQ201991900004	RQST		0001-01-01 00:00:00 BC		855	4	1		1	2									
3	User_ff19	2019-09-20 00:00:00	User_sl19	2019-09-20 00:00:00	REQ201992000005	CLSE		0001-01-01 00:00:00 BC		855	4	1		1	2									
4	User_ff19	2019-09-20 00:00:00	User_sl19	2019-09-20 00:00:00	REQ201992000006	CLSE		0001-01-01 00:00:00 BC		855	4	1		1	2									
5	demo_gatotkaca19	2019-09-21 00:00:00		2019-09-21 00:00:00	REQ201992100007	PNDG		0001-01-01 00:00:00 BC		1189	2	1		1	2									
6	User_ff19	2019-10-04 00:00:00	User_sl19	2019-10-04 00:00:00	REQ2019100400011	BILL		0001-01-01 00:00:00 BC		855	2	1		1	2									
7	User_ff19	2019-10-07 00:00:00		2019-10-07 00:00:00	REQ2019100700012	PNDG		0001-01-01 00:00:00 BC		855	2	1		1	2									
8	User_ff19	2019-09-16 00:00:00	demo_benline19	2019-09-16 00:00:00	REQ201991600001	CLSE		0001-01-01 00:00:00 BC		855	4	1		1	2									
9	User_ff19	2019-09-16 00:00:00	demo_benline19	2019-09-16 00:00:00	REQ201991600002	CLSE		0001-01-01 00:00:00 BC		855	4	1		1	2									
10	User_ff19	2019-09-16 00:00:00	demo_rcl19	2019-09-23 00:00:00	REQ201991600003	CLSE		0001-01-01 00:00:00 BC		855	4	1		1	2									
11	User_ff19	2019-09-23 00:00:00		2019-09-23 00:00:00	REQ201992300008	PNDG		0001-01-01 00:00:00 BC		855	2	1		1	2									
12	User_ff19	2019-09-23 00:00:00	User_ff19	2019-09-23 00:00:00	REQ201992300009	RQST		0001-01-01 00:00:00 BC		855	4	1		1	2									
13	User_ff19	2019-10-07 00:00:00		2019-10-07 00:00:00	REQ2019100700012	PNDG		0001-01-01 00:00:00 BC		855	2	1		1	2									
14	User_ff19	2019-10-02 00:00:00	User_ff19	2019-10-02 00:00:00	REQ2019100200010	RQST		0001-01-01 00:00:00 BC		855	4	1		1	2								                                                                             	
\.


--
-- Data for Name: roles; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.roles (id, createby, createdate, updateby, updatedate, rolename) FROM stdin;
1		0001-01-01 00:00:00 BC		0001-01-01 00:00:00 BC	Admin
2		0001-01-01 00:00:00 BC		0001-01-01 00:00:00 BC	Consignee
3		0001-01-01 00:00:00 BC		0001-01-01 00:00:00 BC	Shipping Line
4		0001-01-01 00:00:00 BC		0001-01-01 00:00:00 BC	Forwarder
5		0001-01-01 00:00:00 BC		0001-01-01 00:00:00 BC	Consignee dan Forwarder
6		0001-01-01 00:00:00 BC		0001-01-01 00:00:00 BC	Admin Operation
\.


--
-- Data for Name: trans_bank; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.trans_bank (id, created_date, no_card, total_amount, trans_id, input1, input2, input3, t_response, trans_date, response_code, receipt_code, resonse_desc, type, bank_id, response_desc) FROM stdin;
\.


--
-- Data for Name: trans_detail; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.trans_detail (id, trans_date, created_date, update_date, proforma_num, invoice_num, amount, layanan, status, comp_id, comp_name, sl_id, sl_name, transbank_id) FROM stdin;
1		2016-09-16 00:00:00	0001-01-01 00:00:00 BC	REQ201691600002	EGLV560600338008	0			4	PT. Agility International	6	PT. Evergreen Shipping Agency Indonesia	\N
2		2016-09-23 00:00:00	0001-01-01 00:00:00 BC	REQ201692300003	EGLV062600024261	0			372	PT. LAUTAN LUAS TBK	6	PT. Evergreen Shipping Agency Indonesia	\N
3		2016-09-29 00:00:00	0001-01-01 00:00:00 BC	REQ201692800005	EGLV141688819633	0			4	PT. Agility International	6	PT. Evergreen Shipping Agency Indonesia	\N
4		2016-10-03 00:00:00	0001-01-01 00:00:00 BC	REQ2016100300006	EGLV530600063759	0			372	PT. LAUTAN LUAS TBK	6	PT. Evergreen Shipping Agency Indonesia	\N
5		2016-10-06 00:00:00	0001-01-01 00:00:00 BC	REQ201692800004	EGLV060600101375	0			4	PT. Agility International	6	PT. Evergreen Shipping Agency Indonesia	\N
6		2016-10-11 00:00:00	0001-01-01 00:00:00 BC	REQ2016101000008	EGLV060600107373	0			4	PT. Agility International	6	PT. Evergreen Shipping Agency Indonesia	\N
7		2016-10-31 00:00:00	0001-01-01 00:00:00 BC	REQ2016103100009	EGLV560600441437	0			4	PT. Agility International	6	PT. Evergreen Shipping Agency Indonesia	\N
8		2016-11-15 00:00:00	0001-01-01 00:00:00 BC	REQ2016111400009	EGLV560600487798	0			4	PT. Agility International	6	PT. Evergreen Shipping Agency Indonesia	\N
9		2016-11-24 00:00:00	0001-01-01 00:00:00 BC	REQ2016112400011	EGLV560600487852	0			4	PT. Agility International	6	PT. Evergreen Shipping Agency Indonesia	\N
10		2016-12-20 00:00:00	0001-01-01 00:00:00 BC	REQ2016120500012	EGLV060600129938	0			4	PT. Agility International	6	PT. Evergreen Shipping Agency Indonesia	\N
11		2017-01-19 00:00:00	0001-01-01 00:00:00 BC	REQ201711900014	EGLV093500288224	0			4	PT. Agility International	6	PT. Evergreen Shipping Agency Indonesia	\N
12		2017-01-19 00:00:00	0001-01-01 00:00:00 BC	REQ201711900015	EGLV090600080434	0			4	PT. Agility International	6	PT. Evergreen Shipping Agency Indonesia	\N
13		2017-02-14 00:00:00	0001-01-01 00:00:00 BC	REQ201721400020	EGLV149602755319	0			4	PT. Agility International	6	PT. Evergreen Shipping Agency Indonesia	\N
14		2017-02-14 00:00:00	0001-01-01 00:00:00 BC		EGLV560600487852	0			4	PT. Agility International	6	PT. Evergreen Shipping Agency Indonesia	\N
15		2017-02-20 00:00:00	0001-01-01 00:00:00 BC	REQ201722000023	EGLV149602755335	0			4	PT. Agility International	6	PT. Evergreen Shipping Agency Indonesia	\N
16		2017-02-20 00:00:00	0001-01-01 00:00:00 BC	REQ201720800016	EGLV090600080442	0			4	PT. Agility International	6	PT. Evergreen Shipping Agency Indonesia	\N
17		2017-02-20 00:00:00	0001-01-01 00:00:00 BC	REQ201722000026	BL8787897	0			447	PT. INDO SARANA CARGO	451	PT KLINE LOGISTICS INDONESIA	\N
18		2017-02-21 00:00:00	0001-01-01 00:00:00 BC	REQ201722100027	BL123456	0			4	PT. Agility International	6	PT. Evergreen Shipping Agency Indonesia	\N
19		2017-02-22 00:00:00	0001-01-01 00:00:00 BC	REQ201722200030	BL2017	0			4	PT. Agility International	6	PT. Evergreen Shipping Agency Indonesia	\N
20		2017-02-22 00:00:00	0001-01-01 00:00:00 BC	REQ201722200031	BL2017	0			4	PT. Agility International	6	PT. Evergreen Shipping Agency Indonesia	\N
21		2017-02-22 00:00:00	0001-01-01 00:00:00 BC	REQ201722200033	BL00125	0			447	PT. INDO SARANA CARGO	451	PT KLINE LOGISTICS INDONESIA	\N
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.users (id, createby, createdate, updateby, updatedate, address, admin, email, firstname, lastname, mobile, password, username, company_id, member_id, is_active, portal_id, user_type, active_date, is_login, session_id, activity, departement, reg_date, role_id, reason_reject, status) FROM stdin;
10	system	2018-04-17 00:00:00		\N	jakarta	0	jktmanifest@rclgroup.com	demorcl	sl	81111111	d9ac559b919814cb8eb4a291566feb76	demo_rcl19	917	916	1	1							2018-04-17			
11	system	2018-04-17 00:00:00		\N	jakarta	0	rizza.fahriza@benline.co.id	demobenline	sl	81111111	d9ac559b919814cb8eb4a291566feb76	demo_benline19	1160	1292	1	1							2018-04-17			
12	system	2018-04-17 00:00:00		\N	jakarta	0	sujarwo@larsen.co.id	demolarsen	sl	81111111	d9ac559b919814cb8eb4a291566feb76	demo_larsen19	1166	1333	1	1							2018-04-17			
13	system	2018-04-17 00:00:00		\N	ajhsjahsas	0	chalid.harvey@gmail.com	demo	coff	98723792323	d9ac559b919814cb8eb4a291566feb76	User_ff19	855	1081	1	1	Admin						2018-04-17			3
14	system	2018-04-17 00:00:00		\N	jasjahsasa	0	chalid.harvey@gmail.com	demo	sl	297927392323	d9ac559b919814cb8eb4a291566feb76	User_sl19	451	1082	1	1	Admin						2018-04-17			3
15	system	2019-05-21 00:00:00		\N	jakarta	0	beny@gatotkaca.co.id	beny	gatotkaca	81297876708	d9ac559b919814cb8eb4a291566feb76	demo_gatotkaca19	1189	1358	1	1	Admin						2018-04-17	8		1
16	system	2018-11-14 00:00:00		\N	Jl. Raya Cakung Cilincing KM.3 Jakarta 13910 - Indonesia	0	fajar.wahyudin@bsa-logistics.co.id	Fajar	Wahyudin	88210578690	d9ac559b919814cb8eb4a291566feb76	demo_bsa19	1159	1286	1	1	Admin						2018-04-17			3
17	system	2018-04-17 00:00:00		\N	jakarta	0	lipradana@agility.com	demo	coff	297927392323	d9ac559b919814cb8eb4a291566feb76	demo_agility19	40	442	1	1	Admin						2018-04-17	9		3
18	system	2018-04-17 00:00:00		\N	jakarta	0	suci.Adelia@unilever.com	demo	co	81111111	d9ac559b919814cb8eb4a291566feb76	demo_unilever19	44	1	1	1							2018-04-17			
\.


--
-- Data for Name: users_register; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.users_register (id_users_reg, createby, createdate, updateby, updatedate, address, admin, email, firstname, lastname, mobile, password, username, company_id, member_id, is_active, gender, status, role_id, user_type, active_date, id_group, departement, is_login, session_id, menu, ak0, ak1, ak2, ak3, ak4, ak5, ak6, ak7, ak8, ak9, ak10, ak11, birthdate) FROM stdin;
1		2017-04-05 00:00:00		\N	TEST DATA	\N	FFirmansyah@slb.com	Fata	Salim	333444555	df0e7ee2b48d6d5d837df2083e0cf590	FFirmansyah	\N	\N	1		1			\N						\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2018-01-25
11		2017-04-06 00:00:00	adm@admin.com	\N	TEST DATA	\N	jayusulendra999@gmail.com	jayu	sulendra	333444555	df0e7ee2b48d6d5d837df2083e0cf590	jayu	924	923	1	1	3			\N						\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2018-01-25
12		2017-04-07 00:00:00		\N	TEST DATA	\N	alfi@hi-lex.co.id	Alfia	Alfia	333444555	a39bc9250d199a5a79de014645b0bae7	hi-lex indonesia	884	883	1	1	3	1		\N						\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2018-01-25
13		2017-04-10 00:00:00		\N	TEST DATA	\N	alwiyah@pt-lnj.com	ALWIYAH	ALMASYHUR	333444555	a39bc9250d199a5a79de014645b0bae7	alwiyahlnj	746	745	1	2	3	9		\N						\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2018-01-25
14		2017-04-11 00:00:00		\N	TEST DATA	\N	sriwati@indokemika.co.id	Sriwati	Yo	333444555	a39bc9250d199a5a79de014645b0bae7	sriwatiyo	887	886	1	2	3	9		\N						\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2018-01-25
15		2017-04-11 00:00:00		\N	TEST DATA	\N	eka.prastiwi@pt-lnj.com	Eka	Prastiwi	333444555	a39bc9250d199a5a79de014645b0bae7	ekalnj	888	887	1	2	3	8		\N						\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2018-01-25
16		2017-04-11 00:00:00		\N	TEST DATA	\N	icargo_ptgeo@yahoo.com	FIRMANSYAH	FIRMANSYAH	333444555	a39bc9250d199a5a79de014645b0bae7	ptgeo	892	891	1	1	3	8		\N						\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2018-01-25
17		2017-04-17 00:00:00		\N	TEST DATA	\N	citrarubby25@gmail.com	YELLY CITRA	RUBBYANTY	333444555	a39bc9250d199a5a79de014645b0bae7	citrarubby	896	895	1	2	3	8		\N						\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2018-01-25
18		2017-04-18 00:00:00		\N	TEST DATA	\N	rizka@ilcs.co.id	INDOSC03	FF	333444555	a39bc9250d199a5a79de014645b0bae7	rizka	\N	\N	1		3			\N						\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2018-01-25
19		2018-02-05 00:00:00		\N		\N	jkasjkah@jkhka.com				e86e9c3e171a6d0989be38a30a3b0a46	tes123	\N	\N	1		1			\N						\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2019-01-25
20		2017-04-29 00:00:00		\N	TEST DATA	\N	yusarwendy@gmail.com	Fata	Salim	333444555	a39bc9250d199a5a79de014645b0bae7	PPJKA	\N	\N	1		1			\N						\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2020-01-25
21		2017-05-05 00:00:00		\N	TEST DATA	\N	import@aisin-indonesia.co.id	Dadan	Apriyatna	333444555	a39bc9250d199a5a79de014645b0bae7	Dadan Apriyatna	911	910	1	1	3	1		\N						\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2021-01-25
22		2017-05-05 00:00:00		\N	TEST DATA	\N	import@aisin-indonesia.co.id	Dadan	Apriyatna	333444555	a39bc9250d199a5a79de014645b0bae7	Dadan Apriyatna	\N	\N	1		1			\N						\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2022-01-25
23		2018-02-05 00:00:00		\N	Lalalala	\N	devilkazuma@gmail.com	Wicaksono Hari	Prayoga	633713612863	5d5c17d05a9f7a124682cde5b589a0d6	Wicaksono Hari	961	1003	1	1	2	1		\N						\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2023-01-25
24		2018-02-05 00:00:00		\N		\N	y_arifin@ilcs.co.id				e86e9c3e171a6d0989be38a30a3b0a46	y_arifin	\N	\N	1		3	9		\N						\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2024-01-25
25		2018-02-21 00:00:00		\N		\N	ll@gmail.com				6323c51fff16261b95a0c6f298963f2c	Putri Diajeng HKD	\N	\N	1		1	9	Admin	\N						\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2025-01-25
26		2018-02-06 00:00:00		\N	jakarta	\N	berrymauluddi@gmail.com	beri	prima	4320421	df0e7ee2b48d6d5d837df2083e0cf590	bery	876	1010	1	1	3	1	Member	\N						\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2026-01-25
27		2018-02-06 00:00:00		\N		\N	tmalogisticsindonesia@gmail.com				cdbe5733e88283205df805ff513bcc67	tmalogisticsindonesia	\N	\N	1		1			\N						\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2027-01-25
28		2017-08-28 00:00:00		\N	TEST DATA	\N	hugo@ilcs.co.id	hugo	fff	333444555	a39bc9250d199a5a79de014645b0bae7	hugo	921	920	1	1	2			\N						\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2028-01-25
29		2018-02-22 00:00:00		\N	tests street	\N	testermddc3@gmail.com	adelia	tiga	7239739239273	e86e9c3e171a6d0989be38a30a3b0a46	adelia3	979	1039	1	2	2	8		\N						\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2029-01-25
30		2018-03-06 00:00:00		\N		\N	shh@kjhkdhk.com				e86e9c3e171a6d0989be38a30a3b0a46	gary10	\N	\N	1		1			\N						\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2030-01-25
31		2018-02-21 00:00:00		\N	tester mddc	\N	testermddc@gmail.com	adelia	tri	8183873783	e86e9c3e171a6d0989be38a30a3b0a46	adelia1	974	1034	1	2	3	8		\N						\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2031-01-25
32		2018-02-23 00:00:00		\N	street 	\N	lapis@nittsu.com.hk	Lapis	Lazuli	2083028380	df0e7ee2b48d6d5d837df2083e0cf590	Lapis Lazuli	907	1046	1	1	3	9		\N						\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2032-01-25
\.


--
-- Data for Name: users_shippingline; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.users_shippingline (sl_id, shippingline, userid, createby, createdate, updateby, updatedate, username) FROM stdin;
1	451	561	\N	\N		\N	demo_ahmadsugiono
2	6	575	\N	\N		\N	kamadjaja1
3	920	617	\N	\N		\N	demo_coff1
4	920	561	\N	\N		\N	demo_ahmadsugiono
5	916	649	\N	\N		\N	iman_n
6	920	649	\N	\N		\N	iman_n
7	935	649	\N	\N		\N	iman_n
8	917	649	\N	\N		\N	iman_n
9	451	649	\N	\N		\N	iman_n
10	962	649	\N	\N		\N	iman_n
11	878	649	\N	\N		\N	iman_n
12	6	649	\N	\N		\N	iman_n
13	916	665	\N	\N		\N	dnoraeni
14	920	665	\N	\N		\N	dnoraeni
15	935	665	\N	\N		\N	dnoraeni
16	917	665	\N	\N		\N	dnoraeni
17	451	665	\N	\N		\N	dnoraeni
18	962	665	\N	\N		\N	dnoraeni
19	878	665	\N	\N		\N	dnoraeni
20	6	665	\N	\N		\N	dnoraeni
21	916	697	\N	\N		\N	jaya
22	920	697	\N	\N		\N	jaya
23	935	697	\N	\N		\N	jaya
24	917	697	\N	\N		\N	jaya
25	451	697	\N	\N		\N	jaya
26	962	697	\N	\N		\N	jaya
27	878	697	\N	\N		\N	jaya
28	6	697	\N	\N		\N	jaya
29	1160	753	\N	\N		\N	Rani
30	916	775	\N	\N		\N	kikiko
\.


--
-- Data for Name: vessel_lines; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.vessel_lines (id, createby, createdate, updateby, updatedate, callsign, vesselname, voyage, company_id) FROM stdin;
\.


--
-- Name: accounts_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.accounts_id_seq', 1, false);


--
-- Name: advertisement_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.advertisement_id_seq', 1, false);


--
-- Name: banks_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.banks_id_seq', 1, false);


--
-- Name: bl_detail_c_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.bl_detail_c_id_seq', 88, true);


--
-- Name: bl_detail_h_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.bl_detail_h_id_seq', 65, true);


--
-- Name: bl_upload_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.bl_upload_id_seq', 4, true);


--
-- Name: channels_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.channels_id_seq', 5, true);


--
-- Name: companies_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.companies_id_seq', 1206, true);


--
-- Name: coreor_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.coreor_id_seq', 2, true);


--
-- Name: detail_container_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.detail_container_id_seq', 685, true);


--
-- Name: detail_files_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.detail_files_id_seq', 286, true);


--
-- Name: gparam_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.gparam_id_seq', 1, true);


--
-- Name: members_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.members_id_seq', 518, true);


--
-- Name: news_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.news_id_seq', 1, false);


--
-- Name: price_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.price_id_seq', 1, false);


--
-- Name: req_sl_delivery_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.req_sl_delivery_id_seq', 59, true);


--
-- Name: requests_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.requests_id_seq', 14, true);


--
-- Name: roles_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.roles_id_seq', 6, true);


--
-- Name: trans_bank_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.trans_bank_id_seq', 1, false);


--
-- Name: trans_detail_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.trans_detail_id_seq', 21, true);


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.users_id_seq', 18, true);


--
-- Name: users_register_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.users_register_id_seq', 32, true);


--
-- Name: users_shippingline_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.users_shippingline_id_seq', 30, true);


--
-- Name: vessel_lines_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.vessel_lines_id_seq', 1, false);


--
-- Name: accounts accounts_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.accounts
    ADD CONSTRAINT accounts_pkey PRIMARY KEY (id);


--
-- Name: advertisement advertisement_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.advertisement
    ADD CONSTRAINT advertisement_pkey PRIMARY KEY (id);


--
-- Name: banks banks_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.banks
    ADD CONSTRAINT banks_pkey PRIMARY KEY (id);


--
-- Name: bl_detail_c bl_detail_c_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.bl_detail_c
    ADD CONSTRAINT bl_detail_c_pkey PRIMARY KEY (id);


--
-- Name: bl_detail_h bl_detail_h_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.bl_detail_h
    ADD CONSTRAINT bl_detail_h_pkey PRIMARY KEY (id);


--
-- Name: bl_upload bl_upload_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.bl_upload
    ADD CONSTRAINT bl_upload_pkey PRIMARY KEY (id);


--
-- Name: channels channels_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.channels
    ADD CONSTRAINT channels_pkey PRIMARY KEY (id);


--
-- Name: companies companies_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.companies
    ADD CONSTRAINT companies_pkey PRIMARY KEY (id);


--
-- Name: coreor coreor_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.coreor
    ADD CONSTRAINT coreor_pkey PRIMARY KEY (id);


--
-- Name: detail_container detail_container_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.detail_container
    ADD CONSTRAINT detail_container_pkey PRIMARY KEY (id);


--
-- Name: detail_files detail_files_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.detail_files
    ADD CONSTRAINT detail_files_pkey PRIMARY KEY (id);


--
-- Name: gparam gparam_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.gparam
    ADD CONSTRAINT gparam_pkey PRIMARY KEY (id_gparam);


--
-- Name: members members_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.members
    ADD CONSTRAINT members_pkey PRIMARY KEY (id);


--
-- Name: news news_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.news
    ADD CONSTRAINT news_pkey PRIMARY KEY (id);


--
-- Name: price price_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.price
    ADD CONSTRAINT price_pkey PRIMARY KEY (id);


--
-- Name: req_sl_delivery req_sl_delivery_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.req_sl_delivery
    ADD CONSTRAINT req_sl_delivery_pkey PRIMARY KEY (id);


--
-- Name: requests requests_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.requests
    ADD CONSTRAINT requests_pkey PRIMARY KEY (id);


--
-- Name: roles roles_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.roles
    ADD CONSTRAINT roles_pkey PRIMARY KEY (id);


--
-- Name: trans_bank trans_bank_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.trans_bank
    ADD CONSTRAINT trans_bank_pkey PRIMARY KEY (id);


--
-- Name: trans_detail trans_detail_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.trans_detail
    ADD CONSTRAINT trans_detail_pkey PRIMARY KEY (id);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: users_register users_register_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users_register
    ADD CONSTRAINT users_register_pkey PRIMARY KEY (id_users_reg);


--
-- Name: users_shippingline users_shippingline_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users_shippingline
    ADD CONSTRAINT users_shippingline_pkey PRIMARY KEY (sl_id);


--
-- Name: vessel_lines vessel_lines_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.vessel_lines
    ADD CONSTRAINT vessel_lines_pkey PRIMARY KEY (id);


--
-- PostgreSQL database dump complete
--

